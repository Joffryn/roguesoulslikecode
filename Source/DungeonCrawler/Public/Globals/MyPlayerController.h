
#pragma once

#include "GameFramework/PlayerController.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "ImGuiCommon.h"
#include "MyPlayerController.generated.h"

class UPlayerInteractionComponent;
class UPlayerFeedbackComponent;
class UForceFeedbackEffect;
class APostProcessManager;
class ABaseCharacter;

struct FGameplayTag;

UCLASS(Abstract)
class DUNGEONCRAWLER_API AMyPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	AMyPlayerController();
	
	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static AMyPlayerController* GetMyPlayerController(const UObject* WorldContextObject);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UPlayerInteractionComponent* PlayerInteractionComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UPlayerFeedbackComponent* PlayerFeedbackComponent;

	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TSubclassOf<APostProcessManager> PostProcessManagerClass;

	UPROPERTY(BlueprintReadOnly, Category = PostProcess)
	APostProcessManager* PostProcessManager;
	
	
	UFUNCTION(BlueprintPure, BlueprintImplementableEvent, Category = Getters)
	bool ShouldShowHud();

	UFUNCTION(BlueprintCallable, Category = Possess)
	void ReturnToPlayerCharacter();

	UFUNCTION(BlueprintCallable, Category = Possess)
	void TryToPossesTargetetCharacter();

	UFUNCTION(BlueprintCallable, Category = "Force Feedback")
	void PlayForceFeedback(UForceFeedbackEffect* ForceFeedbackEffect, FName Tag, bool bLooping, bool bIgnoreTimeDilation, bool bPlayWhilePaused);

	void PlayForceFeedback_Internal(UForceFeedbackEffect* ForceFeedbackEffect, FForceFeedbackParameters Params = FForceFeedbackParameters());
	
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag PlayerTag;

	virtual void Tick(float DeltaTime) override;

	virtual bool InputKey(const FInputKeyParams& Params) override;

	virtual void SetInputMode(const FInputModeDataBase& InData) override;
	
#if WITH_IMGUI
	class ImGuiDebugMenu* DebugMenu; 
#endif

protected:
	
	virtual void PostInitializeComponents() override;
	
	void SpawnPostProcessManager();
	
	UPROPERTY(Transient)
	ABaseCharacter* CachedPlayerCharacter;
	
};
