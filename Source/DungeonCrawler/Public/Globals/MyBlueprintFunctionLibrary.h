
#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "MyAbilitySystemComponent.h"
#include "BaseCharacter.h"
#include "CoreMinimal.h"
#include "MyBlueprintFunctionLibrary.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UMyBlueprintFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	
	UFUNCTION(BlueprintPure, meta = (WorldContext = "WorldContextObject"), Category = "Ability System")
	static TArray<TSubclassOf<UGameplayAbility>> GetAbilitiesClassesGrantedByEffect(TSubclassOf<UGameplayEffect> EffectClass);
	
	UFUNCTION(BlueprintPure, meta = (WorldContext = "WorldContextObject"), Category = "Ability System")
	static UMyAbilitySystemComponent* GetPlayerAbilitySystemComponent(const UObject* WorldContextObject);
	
	UFUNCTION(BlueprintPure, Category = "Ability System")
	static float GetScaledAvailableMana(const AActor* Owner);

	UFUNCTION(BlueprintPure, Category = "Ability System")
	static bool TryToCastAbilityWithMana(const AActor* Owner, float ManaCost);
	
	UFUNCTION(BlueprintCallable, Category = "Components|AddComponent")
	static UActorComponent* AddActorComponent(AActor* Owner, const TSubclassOf<UActorComponent> ActorComponentClass);

	UFUNCTION(BlueprintPure, meta = (WorldContext = "WorldContextObject"), Category = "Actors")
	static AActor* GetClosestActorOfClass(const UObject* WorldContextObject, const FVector& Location, const TSubclassOf<AActor> ActorClass);

	UFUNCTION(BlueprintCallable, meta = (WorldContext = "WorldContextObject"), Category = "Ability System")
	static void ApplyEffectWithMagnitudeByCaller(UAbilitySystemComponent* AbilitySystemComponent, const TSubclassOf<UGameplayEffect> EffectToApply, const FGameplayTag SetByCallerTag, float Magnitude);
	
};
