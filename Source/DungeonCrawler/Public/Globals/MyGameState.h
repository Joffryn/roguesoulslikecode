
#pragma once

#include "GameFramework/GameStateBase.h"
#include "CoreMinimal.h"
#include "MyGameState.generated.h"

class USaveComponent;

UCLASS(Abstract)
class DUNGEONCRAWLER_API AMyGameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:

	AMyGameState();
	
	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static AMyGameState* GetMyGameState(const UObject* WorldContextObject);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	USaveComponent* SaveComponent;
	
};
