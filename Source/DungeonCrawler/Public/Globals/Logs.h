
#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogGameplay, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogSpawning, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogCombat, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogPlayer, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogAI, Log, All);
