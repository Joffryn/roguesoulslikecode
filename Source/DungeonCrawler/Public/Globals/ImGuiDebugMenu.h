
#pragma once

#include "ImGuiCommon.h"
#include "CoreMinimal.h"

class AMyPlayerController;
class AEncounter;
class ASpawner;

class DUNGEONCRAWLER_API ImGuiDebugMenu
{
#if WITH_IMGUI
public:

	ImGuiDebugMenu();
	
	void Render(const AMyPlayerController* PlayerController);

	struct FAICombatDebugger
	{
		bool bEnabled = false;
		void Render(const AMyPlayerController* PlayerController);
	};

	struct FMusicManagerDebugger
	{
		bool bEnabled = false;
		void Render(const AMyPlayerController* PlayerController);
	};

	struct FEncounterManagerDebugger
	{
		bool bEnabled = false;
		void Render(const AMyPlayerController* PlayerController);

		void RenderEncounter(AEncounter* Encounter) const;
	};
	
	FEncounterManagerDebugger EncounterManagerDebugger;
	FMusicManagerDebugger MusicManagerDebugger;
	FAICombatDebugger AICombatDebugger;
	
#endif
};

