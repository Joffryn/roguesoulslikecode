
#pragma once

#include "GameFramework/CheatManager.h"
#include "MyDamageType.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "MyCheatManager.generated.h"
class UGameplayEffect;

UCLASS()
class DUNGEONCRAWLER_API UMyCheatManager : public UCheatManager
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UMyCheatManager* GetMyCheatManager(const UObject* WorldContextObject);
	
	UFUNCTION(exec, Category = Gameplay)
	void FriendlyFire() const;

	UFUNCTION(exec, Category = Poise)
	void ChangePoiseLevel(const int32 Level) const;

	UFUNCTION(exec, Category = Poise)
	void Unstaggerable() const;
	
	UFUNCTION(exec, Category = Damage)
	void Suicide() const;

	UFUNCTION(exec, Category = Damage)
	static void DebugMenu();

	UFUNCTION(exec, Category = Damage)
	void EncounterDebug() const;
	
	UFUNCTION(exec, Category = Damage)
	void AICombatDebug() const;

	UFUNCTION(exec, Category = Damage)
	void MusicManagerDebug() const;
	
	UFUNCTION(exec, Category = Damage)
	void Kill() const;

	UFUNCTION(exec, Category = Weapon)
	void EquipWeapon(FName WeaponName) const;
	
	UFUNCTION(exec, Category = Damage)
	void Damage(const float Amount) const;

	UFUNCTION(exec, Category = Damage)
	void KillAll() const;

	UFUNCTION(exec, Category = Encounter)
	void FinishEncounter() const;
	
	UFUNCTION(exec, Category = Boss)
	void SetPhase(const int32 Phase) const;
	
	UFUNCTION(exec, Category = Drink)
	void AddDrinkCharge(const float Amount) const;

	UFUNCTION(exec, Category = Drink)
	void SpendDrinkCharge(const float Amount) const;
	
	UFUNCTION(exec, Category = Gold)
	void AddGold(const float Amount) const;

	UFUNCTION(exec, Category = Gold)
	void SpendGold(const float Amount) const;
	
	UFUNCTION(exec, Category = Damage)
	void TakeDamage(const float Amount) const;

	UFUNCTION(exec, Category = Damage)
	void OneHitKill() const;
	
	UFUNCTION(exec, Category = Damage)
	void DamageMultiplierAI(const float Multiplier) const;

	UFUNCTION(exec, Category = Damage)
	void DamageMultiplierPlayer(const float Multiplier) const;
	
	UFUNCTION(exec, Category = Hero)
	void SetHeroClass(FName HeroClassName) const;


	UFUNCTION(BlueprintImplementableEvent, exec, Category = Hero)
	void Spawn(FName EnemyName) const;

	
	UFUNCTION(exec, Category = Upgrades)
	void AddPerk(FName PerkName) const;
	
	UFUNCTION(exec, Category = Upgrades)
	void AddAllPerks() const;

	UFUNCTION(exec, Category = Upgrades)
	void RemovePerk(FName PerkName) const;
	
	UFUNCTION(exec, Category = Upgrades)
	void RemoveAllPerks() const;

	UFUNCTION(exec, Category = Upgrades)
	void RemoveSkill(FName SkillName) const;
	
	UFUNCTION(exec, Category = Upgrades)
	void RemoveAllSkills() const;
	
	UFUNCTION(exec, Category = Upgrades)
	void AddSkill(FName SkillName) const;
	
	UFUNCTION(exec, Category = Upgrades)
	void AddAllSkills() const;
	
	UFUNCTION(exec, Category = Stats)
	void RestoreAll() const;
	
	UFUNCTION(exec, Category = Saving)
	void SaveGame() const;

	UFUNCTION(exec, Category = Saving)
	void DeleteSave() const;
	
	UFUNCTION(exec, Category = Saving)
	void LoadGame() const;
	
	UFUNCTION(exec, Category = Health)
	void TargetRestoreHealth(const float Amount) const;

	UFUNCTION(exec, Category = Health)
	void TargetRestoreAllHealth() const;
	
	UFUNCTION(exec, Category = Health)
	void RestoreHealth(const float Amount) const;

	UFUNCTION(exec, Category = Health)
	void RestoreAllHealth() const;

	UFUNCTION(exec, Category = Health)
	void IncreaseMaxHealth(const float Amount) const;

	UFUNCTION(exec, Category = Health)
	void DecreaseMaxHealth(const float Amount) const;


	UFUNCTION(exec, Category = Mana)
	void InfiniteMana() const;

	UFUNCTION(exec, Category = Mana)
	void RestoreMana(const float Amount) const;

	UFUNCTION(exec, Category = Mana)
	void ConsumeMana(const float Amount) const;

	UFUNCTION(exec, Category = Mana)
	void ConsumeAllMana() const;

	UFUNCTION(exec, Category = Mana)
	void RestoreAllMana() const;

	UFUNCTION(exec, Category = Mana)
	void IncreaseMaxMana(const float Amount) const;

	UFUNCTION(exec, Category = Mana)
	void DecreaseMaxMana(const float Amount) const; 
	

	UFUNCTION(exec, Category = Poise)
	void ConsumePoise(const float Amount) const;

	UFUNCTION(exec, Category = Poise)
	void BreakStance() const;
	

	UFUNCTION(exec, Category = Stamina)
	void InfiniteStamina() const;

	UFUNCTION(exec, Category = Stamina)
	void RestoreStamina(const float Amount) const;

	UFUNCTION(exec, Category = Stamina)
	void ConsumeStamina(const float Amount) const;

	UFUNCTION(exec, Category = Stamina)
	void ConsumeAllStamina() const;

	UFUNCTION(exec, Category = Stamina)
	void RestoreAllStamina() const;

	UFUNCTION(exec, Category = Stamina)
	void IncreaseMaxStamina(const float Amount) const;

	UFUNCTION(exec, Category = Stamina)
	void DecreaseMaxStamina(const float Amount) const;


	UFUNCTION(BlueprintImplementableEvent, exec, Category = Upgrades)
	void SkillChoice() const;
	
	UFUNCTION(BlueprintImplementableEvent, exec, Category = Upgrades)
	void PerkChoice() const;

	
	UFUNCTION(BlueprintImplementableEvent, exec, Category = Control)
	void TravelMenu() const;

	UFUNCTION(exec, Category = Control)
	void ResetSeed() const;
	
	UFUNCTION(exec, Category = Control)
	void Possess() const;

	UFUNCTION(exec, Category = Control)
	void Unpossess() const;

	UFUNCTION(exec, Category = Control)
	void Demigod() const;

	UFUNCTION(exec, Category = Control)
	void OutsideTheControlToggle() const;

	UFUNCTION(exec, Category = UI)
	void ShowVictoryMessage() const;

	
	virtual void God() override;
	
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> AddDrinkChargeEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	FGameplayTag SetByCallerDrinkChargeTag;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> AddGoldEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	FGameplayTag SetByCallerGoldTag;

	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> InfiniteStaminaEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> DefaultStaminaConsumptionEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> InfiniteManaEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> DefaultManaConsumptionEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> InfinitePoiseEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> DefaultPoiseConsumptionEffect;
	

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> RestoreHealthEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UMyDamageType> InevitableDamageType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> ChangeMaxHealthEffect;


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> RestoreManaEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> LoseManaEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> ChangeMaxManaEffect;


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> RestorePoiseEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> LosePoiseEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> ChangeMaxPoiseEffect;

	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> RestoreStaminaEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> LoseStaminaEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> ChangeMaxStaminaEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Hacks")
	TMap<FName, TSubclassOf<AWeapon>> Weapons;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Hacks")
	TMap<FName, TSubclassOf<ABaseCharacter>> Characters;
	
private:

	void AddSkillByChoice(const FUpgradeChoice* Choice) const;
	void AddSkillByClass(TSubclassOf<UGameplayEffect> SkillToAdd) const;
	void RemoveSkillByClass(TSubclassOf<UGameplayEffect> SkillToRemove) const;
	
};
