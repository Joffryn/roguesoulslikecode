
#pragma once

#include "GameFramework/SaveGame.h"
#include "GameplayEffect.h"
#include "AttributeSet.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "MySaveGame.generated.h"

class USoundsConfig;
class AWeapon;

USTRUCT()
struct FLevelActorSaveData
{
	GENERATED_BODY()

	UPROPERTY()
	FString LevelName;
	
	UPROPERTY()
	FName ActorName;

	UPROPERTY()
	FTransform Transform;

	UPROPERTY()
	TArray<uint8> ByteData;
};

USTRUCT()
struct FSpawnedActorSaveData
{
	GENERATED_BODY()

	UPROPERTY()
	FString LevelName;
	
	UPROPERTY()
	TSubclassOf<AActor> ActorClass;

	UPROPERTY()
	FTransform Transform;

	UPROPERTY()
	TArray<uint8> ByteData;
};


UCLASS()
class DUNGEONCRAWLER_API UMySaveGame : public USaveGame
{
	GENERATED_BODY()

public:

	UPROPERTY()
	FName LevelName;

	UPROPERTY()
	EBiom Biom;
	
	UPROPERTY()
	TMap<FName, FRunRoomRandomizedSetup> LevelsToRoomSetupsMap;

	UPROPERTY()
	TMap<int32, FRunRoomArray> LevelDepthToNextRooms;
	
	UPROPERTY()
	FRunSetup RunSetup;

	
	UPROPERTY()
	bool bSaveTransform;
	
	UPROPERTY()
	FTransform PlayerTransform;
	
	UPROPERTY()
	float Seed;
	
	UPROPERTY()
	USkeletalMesh* Mesh;
	
	UPROPERTY()
	UMaterial* Material;
	
	UPROPERTY()
	TSubclassOf<AWeapon> PrimaryWeaponClass;
	
	UPROPERTY()
	TSubclassOf<AWeapon> SecondaryWeaponClass;

	UPROPERTY()
	TSubclassOf<AWeapon> SecondaryLeftWeaponClass;
	
	UPROPERTY()
	TMap<FGameplayAttribute, float> Attributes;

	UPROPERTY()
	TArray<TSubclassOf<UGameplayAbility>> CastingAbilitiesClasses;

	UPROPERTY()
	TArray<TSubclassOf<UGameplayEffect>> CastingEffectsToApply;
	
	UPROPERTY()
	TArray< FGameplayEffectSaveData > UpgradeEffects;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Config)
	USoundsConfig* SoundsConfig;

	UPROPERTY()
	float VolumeMultiplier;
	
	UPROPERTY()
	float PitchMultiplier;
	
	UPROPERTY()
	TArray<FName> HiddenBones;

	
	UPROPERTY()
	TArray<FLevelActorSaveData> SavedLevelActors;
	
	UPROPERTY()
	TArray<FSpawnedActorSaveData> SavedSpawnedActors;
	
};
