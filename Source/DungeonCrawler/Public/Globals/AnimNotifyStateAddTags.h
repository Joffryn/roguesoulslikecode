
#pragma once

#include "Animation/AnimNotifies/AnimNotifyState.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "AnimNotifyStateAddTags.generated.h"

UCLASS(meta = (DisplayName = "Add Tags"))
class DUNGEONCRAWLER_API UAnimNotifyStateAddTags : public UAnimNotifyState
{
	GENERATED_BODY()

public:

	virtual void NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration) override;

	virtual void NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;

protected:

	UPROPERTY(EditAnywhere)
	FGameplayTagContainer	TagsToApply;

private:

	virtual FString GetNotifyName_Implementation() const override;

};
