
#pragma once

#include "CoreMinimal.h"

UENUM(BlueprintType)
enum class EChamberPositionType : uint8
{
	None,
	Entrance,
	Common,
	Special,
	PreBoss,
	Boss,
	Shop,
	Respite
};

UENUM(BlueprintType)
enum class EBiom : uint8
{
	None,
	Test,
	Caves,
	Ruins,
	Crossroads
};

UENUM(BlueprintType)
enum class EChamberType : uint8
{
	None,
	Encounter,
	Vendor,
	Respite,
	Boss,
	Traps,
	Entrance,
	Unknown
};

UENUM(BlueprintType)
enum class ERewardType : uint8
{
	None,
	Skill,
	Perk,
	Gold,
	Stats
};

UENUM(BlueprintType)
enum class EEnemyTier : uint8
{
	None,
	Weakling,
	Grunt,
	Elite,
	Boss,
	Giant,
	Any
};

inline const TCHAR* LexToString(const EEnemyTier EnemyTier)
{
	switch (EnemyTier)
	{
		case EEnemyTier::Weakling:
			return TEXT("Weakling");
		case EEnemyTier::Grunt:
			return TEXT("Grunt");
		case EEnemyTier::Elite:
			return TEXT("Elite");
		case EEnemyTier::Boss:
			return TEXT("Boss");
		case EEnemyTier::Giant:
			return TEXT("Giant");
		case EEnemyTier::Any:
			return TEXT("Any");
		default:
			return TEXT("Unknown");
	}
}

UENUM(BlueprintType)
enum class EAIMode : uint8
{
	None,
	OutOfCombat,
	InCombat,
	AfterCombat
};

UENUM(BlueprintType)
enum class EAIWakeMethod : uint8
{
	Default,
	WhenDamaged,
	Manually
};

UENUM(BlueprintType)
enum class EHitMapType : uint8
{
	ThreeDirections,
	FourDirections,
	TwoDirections
};

UENUM(BlueprintType)
enum class EDirection : uint8
{
	None,
	Front,
	Back,
	Left,
	Right,
	FrontLeft,
	FrontRight,
	BackLeft,
	BackRight
};

UENUM(BlueprintType)
enum class ELogCategory : uint8
{
	NoLogging, 
	Fatal, 
	Error, 
	Warning, 
	Display, 
	Log, 
	Verbose, 
	VeryVerbose,
	All,
	BreakOnLog
};

UENUM(BlueprintType)
enum class EGender : uint8
{
	Male,
	Female
};

UENUM(BlueprintType)
enum class EEnchantmentTypes : uint8
{
	PowerUp,
	Element,
	Immunity
};

UENUM(BlueprintType, meta=(ScriptName="EColor"))
enum class EColor : uint8
{
	None,
	Basic,
	Poison,
	PoisonUI,
	Explosion,
	Chill,
	Red,
	Green,
	Blue,
	Enraged,
	Poise,
	PoiseUI,
	Yellow,
	Violet,
	Brown,
	White,
	Black,
	Health,
	Stamina,
	Mana,
	Gold,
	CritUI,
	UnblockableUI,
	MissingBarUI,
	Fire
};

UENUM(BlueprintType)
enum class EUpgradeType : uint8
{
	Active,
	Passive
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	None,
	TwoHanded,
	Katana,
	OneHandedAndShield,
	Dual,
	Assassin,
	Shield,
	Spear,
	BodyPart,
	Giant,
	Greatsword,
	OneHanded,
	OrientalSpear,
	Rapier,
	Whip,
	Throwing
};

UENUM(BlueprintType)
enum class EAttackReply : uint8
{
	Hit,
	Crit,
	Block,
	Dodge,
	Parry
};

UENUM(BlueprintType)
enum class EWeaponSocket : uint8
{
	RightHand,
	LeftHand,
	RightLeg,
	LeftLeg,
	Secondary,
	SecondaryLeft
};

UENUM(BlueprintType)
enum class EStat : uint8
{
	Health,
	MaxHealth,
	Mana,
	MaxMana,
	Stamina,
	MaxStamina,
	Poise,
	MaxPoise
};

UENUM(BlueprintType)
enum class EAttackStaggerStrength : uint8
{
	None,
	Graze,
	Light,
	Medium,
	Heavy,
	UltraHeavy
};

UENUM(BlueprintType)
enum class EPoiseLevel : uint8
{
	None,
	Light,
	Medium,
	Heavy,
	UltraHeavy,
	Unstaggerable
};

UENUM(BlueprintType)
enum class EStaggerReaction : uint8
{
	None,
	AdditiveHit,
	Hit,
	HeavyHit,
	Stun,
	Knockdown
};

UENUM(BlueprintType)
enum class EAttackResponse : uint8
{
	Hit,
	Crit,
	Block,
	Parry,
	InvincibleHit
};

UENUM(BlueprintType)
enum class EPickupType : uint8
{
	None,
	Consumable,
	Progression
};

UENUM(BlueprintType)
enum class EAIActionRangeType: uint8
{
	None,
	MeleeAttack,
	CloseDistanceAttack
};

UENUM(BlueprintType)
enum class EAIDesiredEquipment : uint8
{
	Melee,
	Ranged,
	NoWeapon
};

UENUM(BlueprintType)
enum class EAINoCombatBehavior : uint8
{
	None,
	StartingPosition,
	WalkingAround
};


UENUM(BlueprintType)
enum class EAIActionType : uint8
{
	None,
	MeleeAttack,
	CloseDistanceAttack,
	RangedAttack,
	Block,
	Strafe,
	Taunt,
	Flex,
	Drink,
	ChangePhase,
	EquipMelee,
	EquipRanged,
	RotateInPlace,
	StepBack,
	RepositionWalk,
	WeaponChange,
	WeaponUnequip,
	WeaponEquip
};

inline const TCHAR* LexToString(const EAIActionType AIActionType)
{
	switch (AIActionType)
	{
	case EAIActionType::None:
		return TEXT("None");
	case EAIActionType::MeleeAttack:
		return TEXT("MeleeAttack");
	case EAIActionType::CloseDistanceAttack:
		return TEXT("CloseDistanceAttack");
	case EAIActionType::RangedAttack:
		return TEXT("RangedAttack");
	case EAIActionType::Block:
		return TEXT("Block");
	case EAIActionType::Strafe:
		return TEXT("Strafe");
	case EAIActionType::Taunt:
		return TEXT("Taunt");
	case EAIActionType::Flex:
		return TEXT("Flex");
	case EAIActionType::Drink:
		return TEXT("Drink");
	case EAIActionType::ChangePhase:
		return TEXT("ChangePhase");
	case EAIActionType::EquipMelee:
		return TEXT("EquipMelee");
	case EAIActionType::EquipRanged:
		return TEXT("EquipRanged");
	case EAIActionType::RotateInPlace:
		return TEXT("EquipRanged");
	case EAIActionType::StepBack:
		return TEXT("StepBack");
	case EAIActionType::RepositionWalk:
		return TEXT("RepositionWalk");
	case EAIActionType::WeaponChange:
		return TEXT("WeaponChange");
	case EAIActionType::WeaponUnequip:
		return TEXT("WeaponUnequip");
	case EAIActionType::WeaponEquip:
		return TEXT("WeaponEquip");
	default:
		return TEXT("Unknown");
	}
}

UENUM(BlueprintType)
enum class EEncounterType : uint8
{
	Randomized,
	Sure,
	Interaction
};

inline const TCHAR* LexToString(const EEncounterType EncounterType)
{
	switch (EncounterType)
	{
	case EEncounterType::Randomized:
		return TEXT("Randomized");
	case EEncounterType::Sure:
		return TEXT("Sure");
	case EEncounterType::Interaction:
		return TEXT("Interaction");
	default:
		return TEXT("Unknown");
	}
}

UENUM(BlueprintType)
enum class EEncounterState : uint8
{
	NotSpawned,
	Inactive,
	Active,
	Finished
};

inline const TCHAR* LexToString(const EEncounterState EncounterState)
{
	switch (EncounterState)
	{
	case EEncounterState::NotSpawned:
		return TEXT("NotSpawned");
	case EEncounterState::Inactive:
		return TEXT("Inactive");
	case EEncounterState::Active:
		return TEXT("Active");
	case EEncounterState::Finished:
		return TEXT("Finished");
	default:
		return TEXT("Unknown");
	}
}

UENUM(BlueprintType)
enum class EEncounterActivationType : uint8
{
	None,
	Trigger,
	Perception,
	Any
};