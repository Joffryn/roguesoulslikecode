
#pragma once

#include "Engine/GameInstance.h"
#include "CoreMinimal.h"
#include "MyGameInstance.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnIsUsingGamepadSet, bool, bNewIsUsingGamepad);

UCLASS(Abstract)
class DUNGEONCRAWLER_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	
	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UMyGameInstance* GetMyGameInstance(const UObject* WorldContextObject);

	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FOnIsUsingGamepadSet OnIsUsingGamepadSet;

	UPROPERTY(BlueprintReadWrite, Category = State)
	bool bIsTravellingBetweenLevels;

	UPROPERTY(BlueprintReadOnly, Category = State)
	bool bIsUsingGamepad;

	UFUNCTION(BlueprintCallable, Category = State)
	void SetIsUsingGamepad(bool bNewIsUsingGamepad);
	
};
