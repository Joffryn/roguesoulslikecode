
#pragma once

#include "GameFramework/GameModeBase.h"
#include "CoreMinimal.h"
#include "MyGameMode.generated.h"

class UReactionSolverComponent;
class UGameConfigComponent;
class URandomnessManager;
class UDataTablesManager;
class UTravellingManager;
class UEncounterManager;
class ULevelsManager;
class UMusicManager;
class UColorManager;
class ULootManager;
class UCritSolver;
class UAIManager;
class UUIManager;

UCLASS(Abstract)
class DUNGEONCRAWLER_API AMyGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	AMyGameMode();

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static AMyGameMode* GetMyGameMode(const UObject* WorldContextObject);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UGameConfigComponent* GameConfigComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UTravellingManager* TravellingManager;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UEncounterManager* EncounterManager;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	URandomnessManager* RandomnessManager;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UReactionSolverComponent* ReactionSolver;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UDataTablesManager* DataTablesManager;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UMusicManager* MusicManager;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UColorManager* ColorManager;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	ULevelsManager* LevelsManager;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	ULootManager* LootManager;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UCritSolver* CritSolver;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UAIManager* AIManager;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UUIManager* UIManager;
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	bool bLoadRunDataOnStartPlay = true;
	
	virtual void StartPlay() override;

};
