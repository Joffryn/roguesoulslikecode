
#pragma once

#include "Engine/DataTable.h"
#include "GameplayEffect.h"
#include "MyAttributeSet.h"
#include "CoreMinimal.h"
#include "Enums.h"
#include "Structs.generated.h"

class UCharacterAttributesConfig;
class UAIAttackMontageSetup;
class UGameplayAbility;
class ABaseCharacter;
class USkeletalMesh;
class USoundsConfig;
class UAnimMontage;
class UEnchantment;
class UStaticMesh;
class UTexture2D;
class USoundBase;
class UMaterial;
class UUpgrade;
class ASpawner;
class AWeapon;
class APickup;
class ATrap;

USTRUCT(BlueprintType)
struct FSoundSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USoundBase* Sound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float StartTime = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bAffectedByOwnerMultipliers = true;
	
};

USTRUCT(BlueprintType)
struct FEncounterTierSetup
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<TSubclassOf<ABaseCharacter>> PossibleCharacterClasses;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 SpawnChance = 1;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 UnitCost = 1;
	
	FEncounterTierSetup() {}
	
};

USTRUCT(BlueprintType)
struct FEnemyWaveRandomizedWave
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<TSubclassOf<ABaseCharacter>> PossibleCharacterClasses;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 Amount;

	FEnemyWaveRandomizedWave(): Amount(0) {}
	
};

USTRUCT(BlueprintType)
struct FAdditiveHitSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<UAnimMontage*> Montages;

	FAdditiveHitSetup() {}
	
};

USTRUCT(BlueprintType)
struct FSingleHitDirectionSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EDirection Direction = EDirection::None;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector2D AngleRange = FVector2d::ZeroVector;

	FSingleHitDirectionSetup(): Direction() {}

	FSingleHitDirectionSetup(const EDirection InDirection, const FVector2D InAngleRange)
	{
		Direction = InDirection;
		AngleRange = InAngleRange;
	}
	
};

USTRUCT(BlueprintType)
struct FMontageToAngleSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage* Montage = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector2D AngleRange = FVector2d::ZeroVector;

	FMontageToAngleSetup() {}
	
};

USTRUCT(BlueprintType)
struct FSingleAttachmentSetup
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Look)
	bool bUseStaticMesh = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Look, meta = (EditCondition = "bUseStaticMesh"))
	UStaticMesh* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Look, meta = (EditCondition = "!bUseStaticMesh"))
	USkeletalMesh* SkeletalMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Look)
	FTransform AttachCorrection;

	FSingleAttachmentSetup(): StaticMesh(nullptr), SkeletalMesh(nullptr) {}
	
};

USTRUCT(BlueprintType)
struct FSocketAttachmentSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bUseMaterialFromCharacter;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Look, meta = (EditCondition = "!bUseMaterialFromCharacter"))
	TArray<UMaterial*> PossibleMaterials;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bOptional;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName Socket = NAME_None;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FSingleAttachmentSetup> Attachments;
	
	FSocketAttachmentSetup(): bUseMaterialFromCharacter(false), bOptional(false) {}
	
};

USTRUCT(BlueprintType)
struct FRunRoomRandomizedSetup : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FName LevelName;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	ERewardType Reward = ERewardType::None;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EChamberType ChamberType = EChamberType::None;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 RoomDepth = 0;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FName> NextLevelsNames;

	FRunRoomRandomizedSetup() {};

	FRunRoomRandomizedSetup(const TArray<FName>& InNextLevelsNames, const ERewardType InReward = ERewardType::None, const EChamberType InChamberType = EChamberType::None)
	{
		NextLevelsNames = InNextLevelsNames;
		Reward = InReward;
		ChamberType = InChamberType;
	};
};

USTRUCT(BlueprintType)
struct FRunRoomArray
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FRunRoomRandomizedSetup> Rooms;
	
};
USTRUCT(BlueprintType)
struct FRunSetup : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EBiom Biom = EBiom::None;

	// Summarical run length including entrance, pre boss and boss chamber
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 RunLength = 5;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<EChamberType, int32> ChamberTypesLimits;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<EChamberType, int32> ChamberTypesProbabilities;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 MinShopDepth = 1;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 MinRespiteDepth = 2;
	
	FRunSetup()
	{
		ChamberTypesLimits.Add(EChamberType::Respite, 1);
		ChamberTypesLimits.Add(EChamberType::Vendor, 1);
		
		ChamberTypesProbabilities.Add(EChamberType::Encounter, 5);
		ChamberTypesProbabilities.Add(EChamberType::Respite, 1);
		ChamberTypesProbabilities.Add(EChamberType::Vendor, 1);
	}
	
};

USTRUCT(BlueprintType)
struct FRoomsNamesStorage
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FName> EntranceLevels;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FName> CommonLevels;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FName> SpecialLevels;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FName> PreBossLevels;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FName> BossLevels;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FName> ShopLevels;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FName> RespiteLevels;
	
};

USTRUCT(BlueprintType)
struct FLevelSetup : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<EChamberType> PossibleChallenges;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<ERewardType> PossibleRewards;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EChamberPositionType Position = EChamberPositionType::None;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName LevelName;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText LevelDisplayName;
	
};

USTRUCT(BlueprintType)
struct FRandomHeroSetup : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Look)
	bool bEnabled = true; 
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Look)
	UCharacterAttributesConfig* Attributes;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Look)
	TArray<EGender> PossibleGenders;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Look)
	USkeletalMesh* MaleMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Look)
	USkeletalMesh* FemaleMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Look)
	TArray<UMaterial*> PossibleMaterials;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Look)
	TArray<FSocketAttachmentSetup> MaleAttachments;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Look)
	TArray<FSocketAttachmentSetup> FemaleAttachments;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Look)
	TArray<FName> BoneNamesToHide;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Combat)
	TSubclassOf<AWeapon> PrimaryWeaponClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Combat)
	TSubclassOf<AWeapon> SecondaryWeaponClass;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Sound)
	USoundsConfig* MaleSoundsConfig;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Sound)
	USoundsConfig* FemaleSoundsConfig;
	

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Sound)
	float VolumeMultiplier = 1.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Sound)
	float PitchMultiplier = 1.0f;

	
	FRandomHeroSetup(): Attributes(nullptr), MaleMesh(nullptr), FemaleMesh(nullptr), MaleSoundsConfig(nullptr), FemaleSoundsConfig(nullptr) {}
	
};

USTRUCT(BlueprintType)
struct FCharacterStatsSetup : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ABaseCharacter> CharacterClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<FGameplayAttribute, float> AttributesToValuesSetup;

	FCharacterStatsSetup()
	{
		//General stats important for everyone
		AttributesToValuesSetup.Add(UMyAttributeSet::GetMaxHealthAttribute(), 100.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetHealthAttribute(), 100.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetGoldAttribute(), 50.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetMaxPoiseAttribute(), 100.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetPoiseAttribute(), 100.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetPoiseRegenAttribute(), 5.0f);

		//Right now only really important to the player
		AttributesToValuesSetup.Add(UMyAttributeSet::GetMaxStaminaAttribute(), 100.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetStaminaAttribute(), 100.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetMaxManaAttribute(), 100.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetManaAttribute(), 100.0f);
		
		//Mostly important just to the player
		AttributesToValuesSetup.Add(UMyAttributeSet::GetStaminaSprintCostAttribute(), 5.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetStaminaRegenAttribute(), 10.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetDrinkChargesAttribute(), 0.0f);

		//Several miscellaneous multipliers
		AttributesToValuesSetup.Add(UMyAttributeSet::GetCritDamageMultiplierAttribute(), 1.5f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetBlockingStaminaCostMultiplierAttribute(), 1.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetDamageMultiplierAttribute(), 1.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetSpeedAttribute(), 1.0f);

		//Base stats multipliers
		AttributesToValuesSetup.Add(UMyAttributeSet::GetStaminaGainMultiplierAttribute(), 1.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetStaminaLossMultiplierAttribute(), 0.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetHealthGainMultiplierAttribute(), 1.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetHealthLossMultiplierAttribute(), 1.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetManaGainMultiplierAttribute(), 1.0f);
		AttributesToValuesSetup.Add(UMyAttributeSet::GetManaLossMultiplierAttribute(), 0.0f);
	}
	
};

USTRUCT(BlueprintType)
struct FEnchantsParams : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanBeRandomlyApplied = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UEnchantment> Effect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EEnchantmentTypes Type = EEnchantmentTypes::Element;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTagContainer RequiredTags;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTagContainer BlockedTags;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTag EnchantTag;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Text;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EColor Color = EColor::Basic;

	FEnchantsParams(): Type() {}
	
};

USTRUCT(BlueprintType)
struct FStatusParams : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTag StatusTag;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* Icon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EColor Color = EColor::Basic;

	FStatusParams(): Icon(nullptr) {}
	
};

USTRUCT(BlueprintType)
struct FDamageParams
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UDamageType> DamageClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Amount;

	FDamageParams(): Amount(0) {}
	
};

USTRUCT(BlueprintType)
struct FOnHitEffect
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UGameplayEffect> Effect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Level;

	FOnHitEffect(): Level(0) {}
	
};

USTRUCT(BlueprintType)
struct FWeaponElementalParams
{
	GENERATED_BODY()
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Trail")
	UParticleSystem* TrailOverride;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Trail")
	EColor WeaponColor;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FOnHitEffect OnHitEffect;

	FWeaponElementalParams(): TrailOverride(nullptr), WeaponColor() {}
	
};

USTRUCT(BlueprintType)
struct FUpgradeChoice : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UUpgrade> EffectToApply;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* Icon;
		
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTagContainer RequiredTags;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FGameplayTagContainer BlockedTags;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EColor Color = EColor::Basic;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanBeStacked;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Title;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Description;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EUpgradeType Type = EUpgradeType::Passive;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanBeLearnedFromScroll = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bIsUnique = false;
	
	FUpgradeChoice(): Icon(nullptr), bCanBeStacked(false) {}
	
};

USTRUCT(BlueprintType)
struct FAttackStrengthToReactionSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<EAttackStaggerStrength, EStaggerReaction> AttackToReactionMap;

	FAttackStrengthToReactionSetup()
	{
		AttackToReactionMap.Add(EAttackStaggerStrength::None, EStaggerReaction::None);
		AttackToReactionMap.Add(EAttackStaggerStrength::Light, EStaggerReaction::Hit);
		AttackToReactionMap.Add(EAttackStaggerStrength::Medium, EStaggerReaction::HeavyHit);
		AttackToReactionMap.Add(EAttackStaggerStrength::Heavy, EStaggerReaction::Stun);
		AttackToReactionMap.Add(EAttackStaggerStrength::UltraHeavy, EStaggerReaction::Knockdown);
	}
	
};

USTRUCT(BlueprintType)
struct FPoiseToStaggerSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<EPoiseLevel, FAttackStrengthToReactionSetup> PoiseLevelToStaggerSetupMap;

	FPoiseToStaggerSetup()
	{
		PoiseLevelToStaggerSetupMap.Add(EPoiseLevel::None, FAttackStrengthToReactionSetup());
		PoiseLevelToStaggerSetupMap.Add(EPoiseLevel::Light, FAttackStrengthToReactionSetup());
		PoiseLevelToStaggerSetupMap.Add(EPoiseLevel::Medium, FAttackStrengthToReactionSetup());
		PoiseLevelToStaggerSetupMap.Add(EPoiseLevel::Heavy, FAttackStrengthToReactionSetup());
		PoiseLevelToStaggerSetupMap.Add(EPoiseLevel::UltraHeavy, FAttackStrengthToReactionSetup());
		PoiseLevelToStaggerSetupMap.Add(EPoiseLevel::Unstaggerable, FAttackStrengthToReactionSetup());
	}
	
};

USTRUCT(BlueprintType)
struct FAiActionSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanExecuteAction;

	//Does action require target to be set
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction", EditConditionHides), Category = Trigger)
	bool bRequiresTarget = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction", EditConditionHides), Category = Trigger)
	EAIActionRangeType RangeActionType = EAIActionRangeType::None;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction && RangeActionType == EAIActionRangeType::None", EditConditionHides), Category = Trigger)
	bool bIsRangeDependant;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction", EditConditionHides), Category = Trigger)
	float MinRange;

	//Used always to determine how close the character should get to the player
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction", EditConditionHides), Category = Trigger)
	float MaxRange;

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction && RangeActionType == EAIActionRangeType::None", EditConditionHides), Category = Trigger)
	bool bIsAngleDependant;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction && bIsAngleDependant && RangeActionType == EAIActionRangeType::None", EditConditionHides), Category = Trigger)
	float MinAngle = 0.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction && bIsAngleDependant && RangeActionType == EAIActionRangeType::None", EditConditionHides), Category = Trigger)
	float MaxAngle = 90.f;

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction", EditConditionHides), Category = Movement)
	int32 Priority = 0;

	//Gameplay irrelevant actions are things like taunt, flex etc
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction", EditConditionHides), Category = Movement)
	bool bIsGameplayRelevant = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction", EditConditionHides), Category = Movement)
	bool bShouldAffectMovement = true;

	//Prevent some actions like taunt to be played right after spawning
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction", EditConditionHides), Category = Cooldown)
	bool bStartOnCooldown = false;
	
	//Cooldown for this action
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction", EditConditionHides), Category = Cooldown)
	float Cooldown;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction", EditConditionHides), Category = Cooldown)
	float CooldownRandomInterval = 0.0f;

	//Cooldown for all actions after this one is executed
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction", EditConditionHides), Category = Cooldown)
	float GlobalActionCooldown = 0.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction", EditConditionHides), Category = Cooldown)
	float GlobalActionCooldownRandomInterval = 0.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction", EditConditionHides), Category = Activation)
	float ActivationChance = 1.0f;

	//For kinda scripted actions like changing the boss phase
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bCanExecuteAction", EditConditionHides), Category = Activation)
	bool bHasToBeForced = false;
	
	FAiActionSetup(const bool bInIsRangeDependant, const float InMinRange, const float InMaxRange, const bool bInIsAngleDependant,const float InMinAngle, const  float InMaxAngle, const float InCooldown,
		const bool CanExecuteAction = true, const float InActivationChance = 1.0f, const int32 InPriority = 0, 	EAIActionRangeType InRangeActionType = EAIActionRangeType::None)
	{
		bIsRangeDependant = bInIsRangeDependant;
		MinRange = InMinRange;
		MaxRange = InMaxRange;
		bIsAngleDependant = bInIsAngleDependant;
		MinAngle = InMinAngle;
		MaxAngle = InMaxAngle;
		Cooldown = InCooldown;
		bCanExecuteAction = CanExecuteAction;
		ActivationChance = InActivationChance;
		Priority = InPriority;
		RangeActionType = InRangeActionType;
	}

	FAiActionSetup(): bCanExecuteAction(false), bIsRangeDependant(false), MinRange(0), MaxRange(0), bIsAngleDependant(false), Cooldown(0),	GlobalActionCooldown(0) {}
	
};

USTRUCT(BlueprintType)
struct FAIAttackWithIndex
{
	GENERATED_BODY()

	UPROPERTY()
	UAIAttackMontageSetup* Attack = nullptr;
			
	UPROPERTY()
	int32 Index = 0;

	FAIAttackWithIndex(UAIAttackMontageSetup* InAttack, const int32 InIndex)
	{
		Attack = InAttack;
		Index = InIndex;
	}
	
	FAIAttackWithIndex() {}
	
};

USTRUCT()
struct FGameplayEffectSaveData
{
	GENERATED_BODY()

	UPROPERTY()
	FGameplayEffectSpec EffectSpec;

	UPROPERTY()
	float EffectElapsedTime;

	FGameplayEffectSaveData(): EffectElapsedTime(0){}
};

USTRUCT(BlueprintType)
struct FPickupSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EPickupType PickupType = EPickupType::None;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<TSubclassOf<APickup>> PickupClasses;

	FPickupSetup() {}
	
};

USTRUCT(BlueprintType)
struct FAfterEncounterEffectSetup
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<UGameplayEffect> Effect;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Level = 0.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FGameplayTagContainer RequiredTags;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FGameplayTagContainer BlockingTags;
	
};

USTRUCT(BlueprintType)
struct FDamageTakenInfo
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EColor Color = EColor::Basic;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Amount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCrit;
	
	FDamageTakenInfo(const EColor InColor, const float InAmount, const bool bInCrit)
	{
		Color = InColor;
		Amount = InAmount;
		bCrit = bInCrit;
	}
	
	FDamageTakenInfo(): Color(), Amount(0), bCrit(false){}
	
};

USTRUCT(BlueprintType)
struct FCameraFadeSetup
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly)
	float FromAlpha = 0.0f;

	UPROPERTY(EditDefaultsOnly)
	float ToAlpha = 1.0f;

	UPROPERTY(EditDefaultsOnly)
	float Duration = 1.0f;

	UPROPERTY(EditDefaultsOnly)
	bool bShouldFadeAudio = true;

	UPROPERTY(EditDefaultsOnly)
	bool bHoldWhenFinished = true;
	
};

USTRUCT(BlueprintType)
struct FActionFeedbackSetup
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shake)
	TSubclassOf< UCameraShakeBase> Shake;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ForceFeedback)
	UForceFeedbackEffect* ForceFeedback;
	
	UPROPERTY(EditDefaultsOnly, Category = Sound)
	USoundBase* Sound;

	//Could be replaced by a curve
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Slomo)
	float CustomTimeDilation = 1.0f;
	
	//Could be replaced by a curve
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Slomo)
	float SlomoTime = 0.0f;
	
	FActionFeedbackSetup(): ForceFeedback(nullptr), Sound(nullptr) {}
	
};

USTRUCT(BlueprintType)
struct FBossPhaseInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float HealthPercentage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 PhaseID;

	FBossPhaseInfo(): HealthPercentage(0), PhaseID(0) {}
	
};

USTRUCT()
struct FMusic
{
	GENERATED_BODY()

	UPROPERTY()
	USoundBase* Sound = nullptr;

	FMusic() {}

	explicit FMusic(USoundBase* InSound)
	{
		Sound = InSound;
	}
};

USTRUCT(BlueprintType)
struct FTrapsSetup
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<ATrap*> PossibleTraps;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 AmountToLeft = 0;

	FTrapsSetup() {}
	
};

USTRUCT(BlueprintType)
struct FSpawners
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<ASpawner*> Spawners;
	
};

USTRUCT(BlueprintType)
struct FTierAndCharacter
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EEnemyTier Tier = EEnemyTier::Any;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<ABaseCharacter> CharacterClass;
	
};

USTRUCT(BlueprintType)
struct FSpawnRequest
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FTierAndCharacter Character;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Time = 0.f;

	FSpawnRequest(const FTierAndCharacter InCharacter, const float InTime)
	{
		Character = InCharacter;
		Time = InTime;
	}
	
	FSpawnRequest() {}
	
};