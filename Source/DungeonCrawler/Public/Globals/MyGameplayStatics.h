
#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "GameplayTagContainer.h"
#include "GameplayEffectTypes.h"
#include "CoreMinimal.h"
#include "Enums.h"
#include "MyGameplayStatics.generated.h"

class UGameplayEffect;
class UMyAbilitySystemComponent;
class UAIPerceptionComponent;
class UCombatComponent;
class AAIController;

UCLASS()
class DUNGEONCRAWLER_API UMyGameplayStatics : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = GameplayCues)
	static void ExecuteGameplayCue(UAbilitySystemComponent* AbilitySystem, const  FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters);

	UFUNCTION(BlueprintCallable, Category = GameplayCues)
	static void RemoveGameplayCue(UAbilitySystemComponent* AbilitySystem, const  FGameplayTag GameplayCueTag);

	UFUNCTION(BlueprintCallable, Category = GameplayCues)
	static void AddGameplayCue(UAbilitySystemComponent* AbilitySystem, const  FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters);

	
	UFUNCTION(BlueprintPure, meta=(WorldContext="WorldContextObject"), Category = Controls)
	static bool IsUsingGamepad(const UObject* WorldContextObject);

	
	UFUNCTION(BlueprintPure, Category = Getters)
	static bool IsEditor();

	UFUNCTION(BlueprintPure, Category = Getters)
	static bool IsDebug();
	
	UFUNCTION(BlueprintPure, Category = Getters)
	static bool IsDevelopment();

	UFUNCTION(BlueprintPure, Category = Getters)
	static bool IsShipping();

	UFUNCTION(BlueprintPure, Category = Getters)
	static bool GetActorNetStartup(const AActor* Actor);
	
	
	UFUNCTION(BlueprintPure, Category = Helpers)
	static UMyAbilitySystemComponent* GetMyAbilitySystemComponent(const AActor* Actor);

	UFUNCTION(BlueprintPure, Category = Helpers)
	static UCombatComponent* GetCombatComponent(const AActor* Actor);
	
	UFUNCTION(BlueprintPure, meta=(WorldContext="WorldContextObject"))
	static bool IsPlayerAlive(const UObject* WorldContextObject);
	
	UFUNCTION(BlueprintCallable, meta=(WorldContext="WorldContextObject"), Category = Travel)
	static void TravelToLevel(const UObject* WorldContextObject, FName LevelName, bool bAbsolute = true, FString Options = FString(TEXT("")));

	UFUNCTION(BlueprintCallable, meta=(WorldContext="WorldContextObject"))
	static void DeactivateActor(AActor* Actor);
	
	UFUNCTION(BlueprintCallable, Category = Audio)
	static void SetAudioAssetSoundClass(USoundBase* Sound, USoundClass* InSoundClass);

	UFUNCTION(BlueprintCallable, Category = Audio)
	static USoundClass* GetAudioAssetSoundClass(USoundBase* Sound);
	
	UFUNCTION(BlueprintPure, Category = Color)
	static FLinearColor GetAverageColor(TArray<FLinearColor> Colors);

	UFUNCTION(BlueprintPure, Category = Mesh)
	static TArray<UMeshComponent*> GetActorMeshes(const AActor* Actor);

	UFUNCTION(BlueprintPure, Category = Rotation)
	static float GetYawAngleBetweenActorAndTarget(const AActor* Actor, const FVector& Target);
	
	UFUNCTION(BlueprintPure, Category = Rotation)
	static float GetYawAngleBetweenOriginAndTarget(const FVector& Origin, const AActor* Target);

	UFUNCTION(BlueprintPure, Category = Rotation)
	static EDirection GetHitDirectionForTargetFromOrigin(const FVector& Origin, const AActor* Target, EHitMapType HitMapType = EHitMapType::ThreeDirections);

	UFUNCTION(BlueprintCallable, Category = Ability)
	static FGameplayEffectContextHandle MakeGameplayEffectContextWithParams(UAbilitySystemComponent* AbilitySystemComponent, AActor* Instigator, AActor* Causer);

	
	UFUNCTION(BlueprintCallable, Category = Logs)
	static void GameplayLog(FString Message);
	
	UFUNCTION(BlueprintCallable, Category = Logs)
	static void SpawningLog(FString Message);

	UFUNCTION(BlueprintCallable, Category = Logs)
	static void CombatLog(FString Message);

	UFUNCTION(BlueprintCallable, Category = Logs)
	static void AILog(FString Message);

	
	UFUNCTION(BlueprintCallable, Category = AI)
	static void SetAIControllerPerceptionComponent(AAIController* Controller, UAIPerceptionComponent* PerceptionComponent);

	
};
