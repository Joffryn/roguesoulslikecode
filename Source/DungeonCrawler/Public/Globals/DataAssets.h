
#pragma once

#include "Engine/DataAsset.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "Enums.h"
#include "DataAssets.generated.h"

class UAnimMontage;
class UBlendSpace;

UCLASS(BlueprintType)
class UCharacterAttributesConfig : public UDataAsset
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<FGameplayAttribute, float> AttributesToValuesSetup;
	
};

UCLASS(BlueprintType)
class UWeaponLookAtConfig : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly)
	float InterpSpeed = 5.0f;
	
	UPROPERTY(EditDefaultsOnly)
	FVector2D MaxYawAngles = FVector2D(-90.0f, 90.0f);
	
	UPROPERTY(EditDefaultsOnly)
	FVector2D MaxPitchAngles = FVector2D(-45.0f, 45.0f);

	UPROPERTY(EditDefaultsOnly)
	FVector2D YawBreakAngles = FVector2D(-120.0f, 120.0f);
	
};

UCLASS(BlueprintType)
class UWeaponConfig : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EWeaponType WeaponType;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EAttackStaggerStrength BaseAttackStaggerStrength = EAttackStaggerStrength::Light;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FDamageParams> DamageParams;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FOnHitEffect> OnHitEffects;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float BaseStaminaCost = 10.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UWeaponLookAtConfig* WeaponLookAtConfig;
	
};

UCLASS(BlueprintType)
class UReactionSet : public UDataAsset
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage* BlockedMontage;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage* KnockBackMontage;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<UAnimMontage*> KnockDownMontages;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EHitMapType HitMapType;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<EDirection, UAnimMontage*> HitMontagesMap;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EHitMapType AdditiveHitMapType = EHitMapType::FourDirections;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<EDirection, FAdditiveHitSetup> AdditiveHitMontagesMap; 
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<UAnimMontage*> DyingMontages;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage* ParriedParams;
	
};

UCLASS(BlueprintType)
class UWeaponPlayerAnimSet : public UDataAsset
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UBlendSpace* MovementBlendspace;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPlayerAttackMontageSetup* AirAttack;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<UPlayerAttackMontageSetup*> LightAttacks;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPlayerAttackMontageSetup* RollAttack;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPlayerAttackMontageSetup* SpecialAttack;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UPlayerAttackMontageSetup* SprintAttack;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAttackMontageSetup* Parry;
	
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage* StepBackMontage;
	
};

UCLASS(BlueprintType)
class UWeaponBaseAnimSet : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage* EquipMontage;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimMontage* UnequipMontage;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UBlendSpace* MovementBlendspace;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimSequence* GuardAnimation;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimSequence* IdleAnimation;
	
};

UCLASS(BlueprintType)
class UAttackMontageSetup : public UDataAsset
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
	int32 ExtraPoise = 1;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Base)
	UAnimMontage* MontageToPlay;
	
};

UCLASS(BlueprintType)
class UAIAttackMontageSetup : public UAttackMontageSetup
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	float MinDistance = 0.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	float MaxDistance = 200.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	float MinAngle = 0.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI)
	float MaxAngle = 90.0f;
	
};

UCLASS(BlueprintType)
class UPlayerAttackMontageSetup : public UAttackMontageSetup
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Player)
	float StaminaCostMultiplier = 1.0f;
	
};

UCLASS(BlueprintType)
class UAIAction : public UDataAsset
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Activation")
	float ActivationChance = 1.0f;

	//For kinda scripted actions like changing the boss phase
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Activation")
	bool bHasToBeForced = false;

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	int32 Priority = 0;

	//Gameplay irrelevant actions are things like taunt, flex etc
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	bool bIsGameplayRelevant = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
	bool bShouldAffectMovement = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bShouldAffectMovement", EditConditionHides), Category = Movement)
	bool bAffectMovementWhenOnCooldown;
	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cooldown)
	bool bHasCooldown = true;
	
	//Prevent some actions like taunt to be played right after spawning
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bHasCooldown", EditConditionHides), Category = Cooldown)
	bool bStartOnCooldown = false;
	
	//Cooldown for this action
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bHasCooldown", EditConditionHides), Category = Cooldown)
	float Cooldown;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bHasCooldown", EditConditionHides), Category = Cooldown)
	float CooldownRandomInterval = 0.0f;

	//Cooldown for all actions after this one is executed
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bHasCooldown", EditConditionHides), Category = Cooldown)
	float GlobalActionCooldown = 0.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bHasCooldown", EditConditionHides), Category = Cooldown)
	float GlobalActionCooldownRandomInterval = 0.0f;
	
};

UCLASS(BlueprintType)
class UAICombatAction : public UAIAction
{
	GENERATED_BODY()

public:

	//Does action require target to be set
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat")
	bool bRequiresTarget = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bRequiresTarget", EditConditionHides), Category = "Combat")
	EAIActionRangeType RangeActionType = EAIActionRangeType::None;

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bRequiresTarget && RangeActionType == EAIActionRangeType::None", EditConditionHides), Category = "Combat | Range")
	bool bIsRangeDependant;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bRequiresTarget && bIsRangeDependant && RangeActionType == EAIActionRangeType::None", EditConditionHides), Category = "Combat | Range")
	float MinRange;

	//Used always to determine how close the character should get to the target
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bRequiresTarget && bIsRangeDependant && RangeActionType == EAIActionRangeType::None", EditConditionHides), Category = "Combat | Range")
	float MaxRange;

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "RangeActionType == EAIActionRangeType::None && bRequiresTarget", EditConditionHides), Category = "Combat | Angle")
	bool bIsAngleDependant;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bIsAngleDependant && RangeActionType == EAIActionRangeType::None && bRequiresTarget", EditConditionHides), Category = "Combat | Angle")
	float MinAngle = 0.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bIsAngleDependant && RangeActionType == EAIActionRangeType::None && bRequiresTarget", EditConditionHides), Category = "Combat | Angle")
	float MaxAngle = 90.f;

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat | Health")
	bool bIsHealthDependent;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bIsHealthDependent", EditConditionHides), Category = "Combat | Health")
	float MinHealthPercentage = 0.5f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "bIsHealthDependent", EditConditionHides), Category = "Combat | Health")
	float MaxHealthPercentage = 1.f;
	
};

UCLASS(BlueprintType)
class UAICombatConfig : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Actions)
	TMap<EAIActionType, UAIAction*> AIActionsConfig;

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Anims)
	TArray<UAIAttackMontageSetup*> MeleeAttacks;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Anims)
	TArray<UAIAttackMontageSetup*> CloseDistanceAttacks;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Anims)
	TArray<UAIAttackMontageSetup*> RangedAttacks;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Anims)
	TArray<UAIAttackMontageSetup*> Casts;
	
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Anims)
	TArray<UAnimMontage*> TauntMontages;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Anims)
	TArray<UAnimMontage*> FlexMontages;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Anims)
	bool bSupportRotationsInPlace;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category =Anims, meta = (EditCondition = bSupportRotationsInPlace))
	TArray<FMontageToAngleSetup> RotationsInPlaceSetup;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Anims)
	bool bSupportStepBack = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Anims, meta = (EditCondition = bSupportStepBack))
	UAnimMontage* StepBackMontage = nullptr;
	
};

UCLASS(BlueprintType)
class ULootConfig : public UDataAsset
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(UIMin=0, ClampMin=0))
	int32 MinGold = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(UIMin=0, ClampMin=0))
	int32 MaxGold = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<EPickupType> PickupsToSpawn;

};

UCLASS(BlueprintType)
class USoundsConfig : public UDataAsset
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Config)
	TMap<FGameplayTag, FSoundSetup> SoundsToTagsMap;
	
};

