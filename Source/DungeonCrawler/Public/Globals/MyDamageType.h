
#pragma once

#include "GameFramework/DamageType.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "Enums.h"
#include "MyDamageType.generated.h"

enum class EAttackStaggerStrength : uint8;
UCLASS()
class DUNGEONCRAWLER_API UMyDamageType : public UDamageType
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Responses)
	EColor Color = EColor::Basic;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Responses)
	FGameplayTag HitTag;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Responses)
	EAttackStaggerStrength HitStaggerStrength;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Damage)
	FGameplayTagContainer DamageTags;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Damage)
	bool bCanShowNumbers = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Damage)
	bool bCanCrit = true;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Damage)
	bool bDamagesPoise = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Damage, meta = (EditCondition = "bDamagesPoise"))
	float PoiseDamageMultiplier = 1.0f;
	
};
