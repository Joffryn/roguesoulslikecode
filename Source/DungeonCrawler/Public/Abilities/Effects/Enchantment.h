// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffect.h"
#include "Enchantment.generated.h"

//Empty class existing for sorting in the editor
UCLASS()
class DUNGEONCRAWLER_API UEnchantment : public UGameplayEffect
{
	GENERATED_BODY()
	
};
