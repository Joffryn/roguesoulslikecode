
#pragma once

#include "GameplayEffect.h"
#include "CoreMinimal.h"
#include "Upgrade.generated.h"

//Empty class existing for sorting in the editor
UCLASS()
class DUNGEONCRAWLER_API UUpgrade : public UGameplayEffect
{
	GENERATED_BODY()
	
};
