
#pragma once

#include "Abilities/GameplayAbility.h"
#include "CoreMinimal.h"
#include "MyGameplayAbility.generated.h"

class UMyAbilitySystemComponent;
class ABaseCharacter;

UCLASS()
class DUNGEONCRAWLER_API UMyGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category = Getters)
	UMyAbilitySystemComponent* GetMyAbilitySystemComponentFromOwner() const;
	
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const bool bReplicateEndAbility, const bool bWasCancelled) override;

	const FGameplayTagContainer& GetQueueRequiredTags() const { return QueueRequiredTags; }

	const FGameplayTagContainer& GetQueueBlockedTags() const { return QueueBlockedTags; }

	bool CanQueueAbility(const FGameplayAbilitySpecHandle AbilityToQueue, const FGameplayAbilityActorInfo* ActorInfo, const UAbilitySystemComponent* AbilitySystem) const;

	UFUNCTION(BlueprintPure, Category = Getters)
	FGameplayTagContainer GetAbilityTags() const;

	UFUNCTION(BlueprintPure, Category = Getters)
	ABaseCharacter* GetOwningBaseCharacterFromActorInfo() const;

	UFUNCTION(BlueprintPure, Category = Getters)
	float GetCostPrimaryAttributeAmount() const;
	
	bool IsAllowedByCosts(const FGameplayAbilitySpecHandle AbilityToQueue, const FGameplayAbilityActorInfo* ActorInfo) const;

	UFUNCTION(BlueprintNativeEvent, Category = Ability)
	bool CanBeQueued(const UAbilitySystemComponent* AbilitySystem) const;
	
protected:

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Tags)
	FGameplayTagContainer CancelByAbilitiesWithTag;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Tags)
	FGameplayTagContainer QueueRequiredTags;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Tags)
	FGameplayTagContainer QueueBlockedTags;

private:

	void GameplayTagCallback(const FGameplayTag Tag, const int32 NewCount);

	FDelegateHandle CancelTagEventHandle;

};
