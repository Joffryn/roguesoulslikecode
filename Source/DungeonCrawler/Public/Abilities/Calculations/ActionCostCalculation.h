
#pragma once

#include "GameplayModMagnitudeCalculation.h"
#include "CoreMinimal.h"
#include "ActionCostCalculation.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UActionCostCalculation : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()
	
public:

	virtual float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const override;
	
};
