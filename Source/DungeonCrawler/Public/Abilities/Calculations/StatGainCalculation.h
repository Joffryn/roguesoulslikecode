
#pragma once

#include "GameplayModMagnitudeCalculation.h"
#include "CoreMinimal.h"
#include "StatGainCalculation.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UStatGainCalculation : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()

public:

	virtual float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const override;

};
