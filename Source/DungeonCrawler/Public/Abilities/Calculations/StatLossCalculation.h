
#pragma once

#include "GameplayModMagnitudeCalculation.h"
#include "CoreMinimal.h"
#include "StatLossCalculation.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UStatLossCalculation : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()

public:

	virtual float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const override;

};
