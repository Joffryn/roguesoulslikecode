
#pragma once

#include "GameplayModMagnitudeCalculation.h"
#include "CoreMinimal.h"
#include "MaxStatChange.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UMaxStatChange : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()
	
public:

	virtual float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const override;

};
