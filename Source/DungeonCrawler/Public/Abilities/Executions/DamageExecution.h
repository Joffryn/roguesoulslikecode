
#pragma once

#include "GameplayEffectExecutionCalculation.h"
#include "CoreMinimal.h"
#include "DamageExecution.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UDamageExecution : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()

public:

	UDamageExecution();

	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, OUT FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;
	
};
