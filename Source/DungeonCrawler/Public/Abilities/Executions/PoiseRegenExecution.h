
#pragma once

#include "GameplayEffectExecutionCalculation.h"
#include "CoreMinimal.h"
#include "PoiseRegenExecution.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UPoiseRegenExecution : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()

public:

	UPoiseRegenExecution();

	UPROPERTY(EditDefaultsOnly)
	FGameplayTagContainer PoiseRegenBlockingTags;

	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, OUT FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;
	
};
