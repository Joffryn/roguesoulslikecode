
#pragma once

#include "GameplayEffectExecutionCalculation.h"
#include "CoreMinimal.h"
#include "ApplyDamageExecution.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UApplyDamageExecution : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()
	
public:
	
	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, OUT FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;
	
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	TSubclassOf<class UMyDamageType> DamageType;
	
};
