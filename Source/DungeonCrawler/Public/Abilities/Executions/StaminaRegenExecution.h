
#pragma once

#include "GameplayEffectExecutionCalculation.h"
#include "CoreMinimal.h"
#include "StaminaRegenExecution.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UStaminaRegenExecution : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()

public:

	UStaminaRegenExecution();

	UPROPERTY(EditDefaultsOnly)
	FGameplayTagContainer StaminaRegenBlockingTags;

	UPROPERTY(EditDefaultsOnly)
	FGameplayTag GuardTag;

	UPROPERTY(EditDefaultsOnly)
	FGameplayTag GuardRegenTag;
	
	UPROPERTY(EditDefaultsOnly)
	FGameplayTag SprintTag;

	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, OUT FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;

};
