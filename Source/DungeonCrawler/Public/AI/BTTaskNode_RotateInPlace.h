
#pragma once

#include "AI/BTTaskNode_ExecuteAbilityByTag.h"
#include "CoreMinimal.h"
#include "BTTaskNode_RotateInPlace.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UBTTaskNode_RotateInPlace : public UBTTaskNode_ExecuteAbilityByTag
{
	GENERATED_BODY()

protected:
	
	virtual FGameplayEventData GetEventData(APawn* OwnerPawn, AActor* TargetCharacter) override;

};
