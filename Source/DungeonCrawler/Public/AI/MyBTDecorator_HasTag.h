
#pragma once

#include "BehaviorTree/BTDecorator.h"
#include "CoreMinimal.h"
#include "MyBTDecorator_HasTag.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UMyBTDecorator_HasTag : public UBTDecorator
{
	GENERATED_BODY()

public:

	UMyBTDecorator_HasTag();

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;

protected:

	UPROPERTY(Category = Decorator, EditAnywhere)
	FGameplayTagContainer MonitoredTags;

	void TestConditionalFlowAbort(UBehaviorTreeComponent* OwnerCompCached) const;

private:

	virtual FString GetStaticDescription() const override;

};
