
#pragma once

#include "BehaviorTree/BTTaskNode.h"
#include "GameplayAbilityTypes.h"
#include "AICombatComponent.h"
#include "CoreMinimal.h"
#include "BTTaskNode_ExecuteAbilityByTag.generated.h"

class UMyAbilitySystemComponent;
class UBehaviorTreeComponent;
class UAICombatComponent;
class UGameplayAbility;

UCLASS()
class DUNGEONCRAWLER_API UBTTaskNode_ExecuteAbilityByTag : public UBTTaskNode
{
	GENERATED_BODY()

public:

	UBTTaskNode_ExecuteAbilityByTag();

	UPROPERTY()
	FGameplayTag GameplayTag;

	UPROPERTY(EditAnywhere, Category = Config)
	EAIActionType AIActionType;
	
	UPROPERTY(EditAnywhere, Category = Config)
	bool bCancelAbilityOnAbort;

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

	virtual FString GetStaticDescription() const override;
	
	FDelegateHandle OnAbilityEndedHandle;
	FDelegateHandle OnAbilityFailedHandle;

protected:
	
	UPROPERTY(Transient)
	UMyAbilitySystemComponent* MyAbilitySystemComponent;

	UPROPERTY(Transient)
	UAICombatComponent* AICombatComponent;

	UPROPERTY(Transient)
	UBlackboardComponent* BlackboardComponent;

	
	virtual EBTNodeResult::Type AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

public:

	UFUNCTION()
	void OnAbilityEnded(UGameplayAbility* GameplayAbility, UBehaviorTreeComponent* OwnerComp);

	UFUNCTION()
	void OnAbilityFailed(const UGameplayAbility* GameplayAbility, const FGameplayTagContainer& GameplayTags, UBehaviorTreeComponent* OwnerComp);

	virtual FGameplayEventData GetEventData(APawn* OwnerPawn, AActor* TargetCharacter);

};
