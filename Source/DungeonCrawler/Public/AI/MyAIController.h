
#pragma once

#include "AIController.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "MyAIController.generated.h"

class UBehaviorTreeComponent;
class UAIPerceptionComponent;
class ABaseCharacter;

UCLASS(Abstract)
class DUNGEONCRAWLER_API AMyAIController : public AAIController
{
	GENERATED_BODY()

public:

	AMyAIController(const FObjectInitializer& ObjectInitializer);
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UBehaviorTreeComponent* BehaviorTreeComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UAIPerceptionComponent* Perception;

	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	UBehaviorTree* MainTree;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	UBehaviorTree* CombatTree;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	UBehaviorTree* AfterCombatTree;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	UBehaviorTree* ChaseTree;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	UBehaviorTree* NonCombatTree;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	UBehaviorTree* SearchTargetTree;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	UBehaviorTree* RepositionDuringCombatTree;

	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag CombatInjectTag;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag AfterCombatInjectTag;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag ChaseInjectTag;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag NonCombatInjectTag;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag SearchTargetTreeInjectTag;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag RepositionDuringCombatInjectTag;
	
	UFUNCTION(BlueprintCallable)
	void SetNewTarget(ACharacter* Target);
	
	UFUNCTION(BlueprintCallable)
	void SetupBehaviorTrees();

	AMyAIController();

	virtual void OnPossess(APawn* InPawn) override;
	virtual void OnUnPossess() override;

private:

	UFUNCTION()
	void OnPossessedPawnDeath(ABaseCharacter* Actor);
	
	UPROPERTY()
	ACharacter* LastAwareTarget;


	
};
