
#pragma once

#include "Actors/Props/InteractionActor.h"
#include "CoreMinimal.h"
#include "Portal.generated.h"

UCLASS()
class DUNGEONCRAWLER_API APortal : public AInteractionActor
{
	GENERATED_BODY()
	
public:	

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	bool bStartOpened;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, SaveGame)
	bool bIsOpen;
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void Close(bool bImmediately = false);
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void Open(bool bImmediately = false);
	
	void SetLabelFromEncounter(const FString& EncounterName);

protected:

	virtual void BeginPlay() override;
	
};
