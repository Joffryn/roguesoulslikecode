
#pragma once

#include "GameFramework/Actor.h"
#include "SaveableInterface.h"
#include "CoreMinimal.h"
#include "OverlapPickup.generated.h"

class UStaticMeshComponent;
class USphereComponent;
class UBoxComponent;

UCLASS(Abstract)
class DUNGEONCRAWLER_API AOverlapPickup : public AActor, public ISaveableInterface
{
	GENERATED_BODY()

public:

	AOverlapPickup();

	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UBoxComponent* Box;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UStaticMeshComponent* Mesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	USphereComponent* CollectionSphere;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	USphereComponent* MagnetismSphere;

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config, meta = (ExposoeOnSpawn = true))
	float Magnitude = 1.0f;
	
};
	