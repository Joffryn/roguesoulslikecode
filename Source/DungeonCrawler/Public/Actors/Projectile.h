
#pragma once

#include "GameFramework/Actor.h"
#include "CoreMinimal.h"
#include "Projectile.generated.h"

enum class EColor : uint8;
class UDissolveComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnProjectileHit, FHitResult, HitResult, float, Damage);

UCLASS(Abstract)
class DUNGEONCRAWLER_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	

	AProjectile();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UMyProjectileMovementComponent* ProjectileMovementComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UColorChangingComponent* ColorChangingComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UDissolveComponent* DissolveComponent;
	
	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable)
	FOnProjectileHit OnProjectileHit;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	EColor Color;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	float Damage;

	virtual void BeginPlay() override;
	
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SimulatePhysics();

	UFUNCTION(BlueprintCallable)
	void IgnoreOwnerWhileMoving() const;

	UFUNCTION(BlueprintCallable)
	void ClearTrails();

	void ClearDissolveHandle();

	UFUNCTION(BlueprintPure, BlueprintImplementableEvent)
	UStaticMeshComponent* GetProjectileMesh();

protected:

	UPROPERTY(EditDefaultsOnly, Category = Config)
	float LifeSpan = 60.f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	bool bIsChangingColor;
	
	FTimerHandle DissolveHandle;
	
};
