
#pragma once

#include "GameFramework/Actor.h"
#include "GenericTeamAgentInterface.h"
#include "GameplayTagContainer.h"
#include "AICombatComponent.h"
#include "CoreMinimal.h"
#include "Enums.h"
#include "Spawner.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSpawned, ABaseCharacter*, Character);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FSpawnerDelegate);

enum class EAINoCombatBehavior : uint8;

class UBillboardComponent;
class USceneComponent;
class AMyAIController;
class ABaseCharacter;
class AEncounter;

UCLASS(Abstract)
class DUNGEONCRAWLER_API ASpawner : public AActor
{
	GENERATED_BODY()
public:
	ASpawner();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USceneComponent* Root;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UBillboardComponent* SpriteComponent;

	UPROPERTY(BlueprintAssignable)
	FSpawnerDelegate OnTargetAsPlayerSet;
	
	UPROPERTY(BlueprintAssignable)
	FOnSpawned OnCharacterSpawned;

	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TMap<TSubclassOf<ABaseCharacter>, float> AIClassesZCorrectionMap;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	FGameplayTagContainer CustomTagsToApply;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	EEnemyTier SpawnerTier;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	float RecentlySpawnedResetTime = 5.0f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	float TooCloseRadius = 100.0f;

	
	UPROPERTY(Transient, BlueprintReadOnly, Category = State)
	bool bRecentlySpawnedCharacter;

	
	UFUNCTION(BlueprintCallable)
	void SetEncounterOwner(AEncounter* InEncounterOwner);

	UFUNCTION(BlueprintCallable)
	void KillCharacters();
	
	UFUNCTION(BlueprintPure, Category = Getters)
	TArray<ABaseCharacter*> GetSpawnedActors() const;
	
	UFUNCTION(BlueprintPure, Category = Getters)
	AEncounter* GetEncounterOwner() const { return EncounterOwner; }

	UFUNCTION(BlueprintCallable)
	ABaseCharacter* Spawn(TSubclassOf<ABaseCharacter> CharacterClass, bool bWithMaterialization = true, bool bRotatedTowardsPlayer = false);

	UFUNCTION(BlueprintCallable)
	bool IsOccupied();
	
	UFUNCTION()
	void InactivateCharacters() const;

	UFUNCTION()
	void ActivateCharacters() const;
	
	void SetLabelFromEncounter(const FString& EncounterName);

	UFUNCTION(BlueprintPure)
	bool IsOnScreen() const;
	
protected:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	bool bMaterializeAtStart = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	bool bEnabled = true;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	EAINoCombatBehavior AINoCombatBehavior = EAINoCombatBehavior::WalkingAround;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag InactiveTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	FGenericTeamId TeamID;
	
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Config)
	AEncounter* EncounterOwner;

	virtual void Tick(float DeltaSeconds) override;
	
	UFUNCTION()
	void OnSpawnedActorTargetSet(AActor* Actor);
	
	UFUNCTION()
	void AfterSpawn(AActor* Actor);

	UFUNCTION()
	void OnSpawnedCharacterDeath(ABaseCharacter* SpawnedCharacter);

	UPROPERTY(Transient)
	float TimeToResetRecentlySpawned;
	
	UPROPERTY(Transient)
	TArray<AMyAIController*> MyAiControllersCache;

	UPROPERTY(Transient)
	TArray<ABaseCharacter*> SpawnedCharacters;

	friend class ImGuiDebugManager;
	
};
