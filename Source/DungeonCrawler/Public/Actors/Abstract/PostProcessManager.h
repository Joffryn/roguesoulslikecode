
#pragma once

#include "GameFramework/Actor.h"
#include "CoreMinimal.h"
#include "PostProcessManager.generated.h"

class UPostProcessComponent;
class UBoxComponent;

UCLASS(Abstract)
class DUNGEONCRAWLER_API APostProcessManager : public AActor
{
	GENERATED_BODY()
	
public:	
	APostProcessManager();

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static APostProcessManager* GetPostProcessManager(const UObject* WorldContextObject);

	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UBoxComponent* PostProcessesBox;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UPostProcessComponent* Interaction;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UPostProcessComponent* Vignette;


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Vignette")
	UMaterialInstance* VignetteMaterial;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Vignette")
	FName VignetteCenterColor;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Vignette")
	FName VignetteBorderColor;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Vignette")
	FName PostProcessBlend;
	

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ShowVignette();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void HideVignette();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void PushVignetteColorChange(FLinearColor Color);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void PopVignetteColorChange(FLinearColor Color);
	
	
	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;

};
