
#pragma once

#include "Actors/Props/InteractionActor.h"
#include "CoreMinimal.h"
#include "Pickup.generated.h"

class UInteractionComponent;
class UStaticMeshComponent;
class UBoxComponent;

UCLASS(Abstract)
class DUNGEONCRAWLER_API APickup : public AInteractionActor
{
	GENERATED_BODY()

public:

	APickup();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UBoxComponent* Box;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, SaveGame)
	bool bHasBeenPickedUp;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UInteractionComponent* Interaction;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UStaticMeshComponent* Mesh;

	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config, meta = (ExposoeOnSpawn = true))
	float Magnitude;
};
