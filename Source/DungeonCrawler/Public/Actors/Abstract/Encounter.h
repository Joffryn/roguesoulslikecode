
#pragma once

#include "GameFramework/Actor.h"
#include "Interfaces/SaveableInterface.h"
#include "CoreMinimal.h"
#include "Spawner.h"
#include "Structs.h"
#include "Enums.h"
#include "Encounter.generated.h"

class UPlayerInteractionComponent;
class AEncounterRewardSpawnPoint;
class AEncounterBlockingWall;
class UEditorTickComponent;
class UBillboardComponent;
class AEncounterTrigger;
class AInteractionActor;
class ABaseCharacter;
class APortal;
class ATrap;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEncounterDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEncounterCharacterDeath, ABaseCharacter*, Character);

UCLASS(Abstract)
class DUNGEONCRAWLER_API AEncounter : public AActor, public IGenericTeamAgentInterface, public ISaveableInterface
{
	GENERATED_BODY()
	
public:
	AEncounter();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USceneComponent* Root;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UBillboardComponent* SpriteComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UEditorTickComponent* EditorTickComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Editor")
	FString EncounterName;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	EEncounterType EncounterType;
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	USoundBase* Begin;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	USoundBase* End;

	//Actors that needs to be interacted with to finish the encounter 
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (EditCondition = "EncounterType == EEncounterType::Interaction"), Category = Config)
	TArray<AInteractionActor*> InteractionActors;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (EditCondition = "EncounterType == EEncounterType::Sure"), Category = Config)
	TArray<TSubclassOf<ABaseCharacter>> SureEnemiesClassesToSpawn;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Combat")
	bool bPreSpawnAtBeginPlay;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (EditCondition = "bPreSpawnAtBeginPlay"), Category = Config)
	bool bBeginInactive = true;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	bool bAwareAboutPlayerFromTheSpawn = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	bool bOpenWallsWhenFinished = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	EEncounterActivationType EncounterActivationType = EEncounterActivationType::Any;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, SaveGame, Category = Config)
	bool bIsEnabled = true;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, SaveGame, Category = "Config | Music")
	bool bMusicIsEnabled = true;
	
	UPROPERTY(EditAnywhere, Category = "Config | Music")
	TArray<USoundBase*> PossibleMusics;

	UPROPERTY(EditDefaultsOnly, Category = "Config | Music")
	float MusicFadeInTime = 1.5f;

	UPROPERTY(EditDefaultsOnly, Category = "Config | Music")
	float MusicFadeOutTime = 0.5f;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Combat", meta = (EditCondition = "EncounterType == EEncounterType::Randomized"))
	int32 EncounterEnemyValue = 10;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Combat", meta = (EditCondition = "EncounterType == EEncounterType::Randomized"))
	int32 MaxSimultaneousEncounterEnemyValue = 5;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Combat")
	TArray<AEncounter*> NextEncounters;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Combat")
	FVector2D EnemySpawnTimeRange = FVector2D(0.5f, 2.5f);
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Combat")
	TSet<AEncounterRewardSpawnPoint*> Rewards;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Combat")
	TArray<ASpawner*> Spawners;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Combat")
	TMap<EEnemyTier, FEncounterTierSetup> EncounterTierSetups;
	
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Combat")
	TArray<FTrapsSetup> TrapsSetups;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Combat")
	TSet<AEncounterBlockingWall*> Walls;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Combat")
	TSet<APortal*> Portals;

	UPROPERTY(BlueprintAssignable, Category = Delegates)
	FOnEncounterCharacterDeath OnEncounterCharacterDeath;
	
	UPROPERTY(BlueprintAssignable, Category = Delegates)
	FEncounterDelegate OnEncounterFinished;

	UPROPERTY(BlueprintAssignable, Category = Delegates)
	FEncounterDelegate OnEncounterStarted;
	
	UFUNCTION(BlueprintCallable)
	void Start();

	UFUNCTION(BlueprintCallable)
	void CloseWalls();
	
	UFUNCTION(BlueprintCallable)
	void OpenWalls();

	UFUNCTION(BlueprintCallable)
	void PreSpawn();

	UFUNCTION(BlueprintCallable)
	void Spawn(bool bWithMaterialization = true, const bool bInactive = false);
	
	UFUNCTION(BlueprintCallable)
	void Kill();

	UFUNCTION(BlueprintCallable)
	void ForceFinish();
	
	UFUNCTION(BlueprintCallable)
	void ResetEncounter();

	UFUNCTION(BlueprintCallable)
	void SpawnRewards();
	
	UFUNCTION(BlueprintCallable)
	void Activate();
	
	UFUNCTION(BlueprintCallable)
	void Enable() { bIsEnabled = true; }
	
	UFUNCTION(BlueprintCallable)
	void Disable() { bIsEnabled = false; }

	UFUNCTION(BlueprintPure, Category = Getters)
	bool GetIsEnabled() const { return bIsEnabled; }

	UFUNCTION(BlueprintPure, Category = Getters)
	int32 GetCurrentEncounterEnemyValue() const { return CurrentEncounterEnemyValue; }

	UFUNCTION(BlueprintPure, Category = Getters)
	int32 GetSpawnedEncounterEnemyValue() const { return SpawnedEncounterEnemyValue; }
	
	UFUNCTION(BlueprintPure, Category = Getters)
	bool HasBeenFinished() const { return bHasBeenFinished; }

	UFUNCTION(BlueprintPure, Category = Getters)
	bool HasInteractedWithAllActors() const;
	
	UFUNCTION(BlueprintPure)
	TArray<ABaseCharacter*> GetSpawnedActors();

	UFUNCTION(BlueprintCallable)
	void ToDefaultState();
	
	EEncounterState GetState() const { return State; }

#if WITH_EDITORONLY_DATA
	
	virtual void PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent) override;
	virtual void PostDuplicate(bool bDuplicateForPie) override;

#endif
	
protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;
	
	UFUNCTION()
	void OnCharacterSpawned(ABaseCharacter* Character);

	UFUNCTION()
	void OnInteractionWithActor();
	
	UFUNCTION()
	void OnSpawnedEnemyDeath(ABaseCharacter* Character);
	
	void BindDelegates();
	
	void Finish();
	
	void Register();

	void SetupTraps();

	UFUNCTION()
	void BuildRandomizationSetup();
	void BuildProbabilityMap();
	void BuildSpawnersToTiersMaps();
	
	
	UFUNCTION()
	void UpdateSpawnersReferences();
	
	void ClearReferences();
	void ClearDuplicatedReferences();

	UFUNCTION(CallInEditor, Category = "Config | Editor")
	void UpdateEncounter();

	UFUNCTION(CallInEditor, Category = "Config | Editor")
	void ClearDuplicateSpawners();

	UFUNCTION(CallInEditor, Category = "Config | Editor")
	void CollectUnassignedSpawners();
	
	void ClearEmptyReferences();
	void UpdateReferences();
	
	void UpdateReferencedActorsNames();

	UFUNCTION()
	void OnWallTriggerEntered();

	UFUNCTION()
	void OnSpawnedCharacterPlayerTargetSet(AActor* Target);

	UFUNCTION()
	void SpawnCharacter(FTierAndCharacter Character, bool bWitMaterialization = true, const bool bInactive = false, bool bModifyEncounterValues = true, bool bRotatedTowardsPlayer = false, const bool bShouldSpawnerByOnScreen = false);

	UFUNCTION()
	void SpawnSingleCharacter(const FTierAndCharacter Character);
	
	void SpawnSureSetup();

	void IncreaseSpawnedUnitCosts(int32 Cost);
	
	bool ShouldSpawnMoreEnemies() const;

	void ClearPendingSpawnRequests();
	
	void StartMusic();
	void StopMusic();

	UPROPERTY(Transient)
	USoundBase* CurrentMusic;
	
	UPROPERTY(BlueprintReadOnly, Transient)
	TMap<EEnemyTier, FSpawners> TiersToSpawners;

	UPROPERTY(BlueprintReadOnly, Transient)
	TMap<EEnemyTier, int32> TiersToCosts;
	
	UPROPERTY(BlueprintReadOnly, Transient)
	EEncounterState State = EEncounterState::NotSpawned;
	
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Transient)
	TArray<ABaseCharacter*> SpawnedCharacters;
	
	FTierAndCharacter GetRandomEnemyClassFromTiers() const;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Transient)
	TArray<FSpawnRequest> PendingSpawnRequests;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Transient)
	TArray<EEnemyTier> ProbabilitySetup;
	
	UPROPERTY(BlueprintReadOnly, Transient)
	int32 CurrentEncounterEnemyValue;

	UPROPERTY(BlueprintReadOnly, Transient)
	int32 SpawnedEncounterEnemyValue;
	
	UPROPERTY(BlueprintReadOnly, SaveGame)
	bool bHasBeenFinished;

	UPROPERTY(BlueprintReadWrite, Transient)
	bool bHasBeenLoadedFromSave;
	
	friend class ImGuiDebugManager;
	
};
