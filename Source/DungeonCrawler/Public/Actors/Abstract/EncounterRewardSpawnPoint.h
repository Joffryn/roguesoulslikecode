
#pragma once

#include "GameFramework/Actor.h"
#include "CoreMinimal.h"
#include "EncounterRewardSpawnPoint.generated.h"

class UStaticMeshComponent;
class UMaterialInterface;
class AEncounterReward;

UCLASS(Abstract)
class DUNGEONCRAWLER_API AEncounterRewardSpawnPoint : public AActor
{
	GENERATED_BODY()

public:

	AEncounterRewardSpawnPoint();
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USceneComponent* Root;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* StaticMesh;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	bool bEnabled = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (EditCondition = "bEnabled"), Category = Config)
	TArray<TSubclassOf<AEncounterReward>> PossibleRewards;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FString RewardName;
	
	UFUNCTION()
	void Spawn();
	
	void SetLabelFromEncounter(const FString& EncounterName);
	
};
