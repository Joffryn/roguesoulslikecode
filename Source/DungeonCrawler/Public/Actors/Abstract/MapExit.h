
#pragma once

#include "GameFramework/Actor.h"
#include "SaveableInterface.h"
#include "CoreMinimal.h"
#include "MapExit.generated.h"

class UEditorTickComponent;
class ANextRoomSign;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLevelNameUpdated, FName, Name);

UCLASS(Abstract)
class DUNGEONCRAWLER_API AMapExit : public AActor, public ISaveableInterface
{
	GENERATED_BODY()

public:

	AMapExit();
	
	
	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FOnLevelNameUpdated OnLevelNameUpdated;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UEditorTickComponent* EditorTickComponent;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, SaveGame, Category = Config)
	bool bIsEnabled = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Savegame, Category = State)
	FName LevelName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	int32 Index = 0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Combat")
	ANextRoomSign* BoundNextRoomSign;

#if WITH_EDITORONLY_DATA
	
	virtual void PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent) override;

#endif
	
protected:

	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = Saving)
	void LoadSetupFromLevelsManager();
	
};
