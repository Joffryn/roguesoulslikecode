
#pragma once

#include "GameFramework/Actor.h"
#include "Interfaces/SaveableInterface.h"
#include "CoreMinimal.h"
#include "Enums.h"
#include "NextRoomSign.generated.h"

class UTextRenderComponent;
class UEditorTickComponent;
class UStaticMeshComponent;
class USceneComponent;

UCLASS()
class DUNGEONCRAWLER_API ANextRoomSign : public AActor, public ISaveableInterface
{
	GENERATED_BODY()
	
public:	
	ANextRoomSign();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	USceneComponent* Root;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UTextRenderComponent* TextRenderComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UEditorTickComponent* EditorTickComponent;

	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TMap<EChamberType, UStaticMesh*> ChamberTypeToMeshSetup;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TMap<ERewardType, UStaticMesh*> RewardTypeToMeshSetup;
	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Savegame, Category = State)
	EChamberType ChamberType;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Savegame, Category = State)
	ERewardType RewardType;

	
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Setters)
	void SetChamberAndRewardTypes(EChamberType NewChamberType, ERewardType NewRewardType);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Saving)
	void LoadSetupFromLevelsManager();

	UFUNCTION(BlueprintCallable, Category = Saving)
	void OnMapExitLevelNameChanged(FName NewLevelName);
	
protected:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Savegame, Category = State)
	FName LevelName;
	
};
