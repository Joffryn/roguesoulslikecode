
#pragma once

#include "Actors/Projectile.h"
#include "CoreMinimal.h"
#include "ThrowedProjectile.generated.h"

class UDissolveComponent;

UCLASS(Abstract)
class DUNGEONCRAWLER_API AThrowedProjectile : public AProjectile
{
	GENERATED_BODY()
	
public:
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn))
	bool bIsFake;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, meta = (ExposeOnSpawn))
	FVector InitialVelocity;

	virtual void BeginPlay() override;

};
