
#pragma once

#include "GameFramework/Actor.h"
#include "Interfaces/SaveableInterface.h"
#include "CoreMinimal.h"
#include "EncounterBlockingWall.generated.h"

class UStaticMeshComponent;
class UBoxComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEncounterBlockingWallDelegate);

UCLASS(Abstract)
class DUNGEONCRAWLER_API AEncounterBlockingWall : public AActor, public ISaveableInterface
{
	GENERATED_BODY()

public:

	AEncounterBlockingWall();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UStaticMeshComponent* Mesh;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UBoxComponent* Box;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UBoxComponent* Trigger;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FEncounterBlockingWallDelegate OnPlayerTriggerEnter;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	bool bStartsClosed;

	
	UPROPERTY(BlueprintReadWrite, Savegame, Category = State)
	bool bIsClosed;

	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void CloseImmediately(bool bForced = false);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void Close();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void OpenImmediately(bool bForced = false);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void Open();

	void SetLabelFromEncounter(const FString& EncounterName);

};
