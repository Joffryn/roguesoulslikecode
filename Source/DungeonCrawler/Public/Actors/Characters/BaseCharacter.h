
#pragma once

#include "GameFramework/Character.h"
#include "Perception/AIPerceptionListenerInterface.h"
#include "GenericTeamAgentInterface.h"
#include "GameplayTagAssetInterface.h"
#include "AbilitySystemInterface.h"
#include "InteractableInterface.h"
#include "GameplayTagContainer.h"
#include "LootableInterface.h"
#include "CoreMinimal.h"
#include "BaseCharacter.generated.h"

class UMyCharacterMovementComponent;
class UMyPhysicalAnimationComponent;
class UCharacterAttributesConfig;
class UMyAbilitySystemComponent;
class UDamageResponseComponent;
class UColorChangingComponent;
class UTargetSystemComponent;
class UDamageTakingComponent;
class UAnimationComponent;
class URotationComponent;
class USoundsComponent;
class UCombatComponent;
class UGameplayEffect;
class UStatsComponent;
class UPoiseComponent;
class ABaseCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnTargetSet, AActor*, Target);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDeath, ABaseCharacter*, Actor);

UCLASS(Abstract)
class DUNGEONCRAWLER_API ABaseCharacter : public ACharacter, public IAbilitySystemInterface, public IGameplayTagAssetInterface, public IGenericTeamAgentInterface, public IAIPerceptionListenerInterface, public IInteractableInterface, public ILootableInterface
{
	GENERATED_BODY()

public:

	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FOnTargetSet OnTargetSet;
	
	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FOnDeath OnDeath;
	
	explicit ABaseCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static ABaseCharacter* GetMyPlayerCharacter(const UObject* WorldContextObject);

	virtual void PostInitializeComponents() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UMyAbilitySystemComponent* AbilitySystemComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UDamageTakingComponent* DamageTakingComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UTargetSystemComponent* TargetSystemComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UAnimationComponent* AnimationComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	URotationComponent* RotationComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UCombatComponent* CombatComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	USoundsComponent* SoundsComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UPoiseComponent* PoiseComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UStatsComponent* StatsComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UDamageResponseComponent* DamageResponseComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UColorChangingComponent* ColorChangingComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UMyPhysicalAnimationComponent* PhysicalAnimationComponent;
	
	UFUNCTION(BlueprintPure, Category = Getters)
	UMyCharacterMovementComponent* GetMyCharacterMovementComponent() const ;

	
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Config | Abilities")
	TArray<TSubclassOf<UGameplayEffect>> DefaultInfiniteEffects;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Team,  meta = (ExposeOnSpawn="true"))
	FGenericTeamId TeamID; 
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Abilities)
	UCharacterAttributesConfig* Attributes;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Sound")
	float FootstepsVolume = 1.0f;

	
	UFUNCTION(BlueprintPure, Category = Getters)
	bool IsPossessedByPlayerController() const;

	UFUNCTION(BlueprintCallable, Category = Team)
	void SetTeamID(FGenericTeamId ID);

	UFUNCTION(BlueprintImplementableEvent, Category = Transform)
	void SetCharacterTransform(FTransform Transform);
	
	UFUNCTION(BlueprintPure, Category = Getters)
	float GetAnimRootMotionScale() const;

	UFUNCTION(BlueprintCallable, Category = Anims)
	void SetAnimRootMotionScale(float Value = 1.0f);

	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Damage)
	void ForceDeath();

	UFUNCTION(BlueprintPure)
	bool IsAlive() const;

	UFUNCTION(BlueprintCallable)
	void SendTeamEvent(AActor* Target, float EventRange = 1000.0f);


	//IGenericTeamAgentInterface
	virtual void SetGenericTeamId(const FGenericTeamId& NewTeamID) override;
	
	virtual FGenericTeamId GetGenericTeamId() const override; 

	UFUNCTION(BlueprintPure)
	ETeamAttitude::Type GetTeamAttitudeTowardsActor(const AActor* Target) const;
	//End of IGenericTeamAgentInterface
	
	UFUNCTION(BlueprintCallable)
	void SendTouchEvent(AActor* TouchReceiver);

	UFUNCTION(BlueprintCallable)
	void LoadAttributes(UCharacterAttributesConfig* InAttributes = nullptr) const;

	//IGameplayTagAssetInterface
	virtual void GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const override;
	
	//IAIPerceptionListenerInterface
	virtual UAIPerceptionComponent* GetPerceptionComponent() override;

	//IAbilitySystemInterface
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;
	
protected:

	virtual void BeginPlay() override;

	virtual float InternalTakePointDamage(const float Damage, FPointDamageEvent const& PointDamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
	virtual float InternalTakeRadialDamage(float Damage, FRadialDamageEvent const& RadialDamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	
};
