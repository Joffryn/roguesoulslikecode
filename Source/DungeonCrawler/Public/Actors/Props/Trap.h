
#pragma once

#include "GameFramework/Actor.h"
#include "Interfaces/SaveableInterface.h"
#include "CoreMinimal.h"
#include "Trap.generated.h"

UCLASS(Abstract)
class DUNGEONCRAWLER_API ATrap : public AActor, public ISaveableInterface
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	FString TrapName;

	void SetLabelFromEncounter(const FString& EncounterName);

	UPROPERTY(BlueprintReadOnly, SaveGame, Category = State)
	bool bHasBeenDeactivated;
	
};
