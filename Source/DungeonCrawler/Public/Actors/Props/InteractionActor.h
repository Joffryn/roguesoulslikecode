
#pragma once

#include "GameFramework/Actor.h"
#include "Interfaces/InteractableInterface.h"
#include "Interfaces/SaveableInterface.h"
#include "CoreMinimal.h"
#include "InteractionActor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnInteraction);

UCLASS(Abstract)
class DUNGEONCRAWLER_API AInteractionActor : public AActor, public IInteractableInterface, public ISaveableInterface
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintCallable, BlueprintAssignable)
	FOnInteraction OnInteracted;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, SaveGame)
	bool bHasBeenInteractedWith;

	//IInteractableInterface
	virtual void Interact_Implementation() override;
	
};
