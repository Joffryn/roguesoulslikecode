
#pragma once

#include "Actors/Props/InteractionActor.h"
#include "LootableInterface.h"
#include "CoreMinimal.h"
#include "LootInteractionActor.generated.h"

UCLASS()
class DUNGEONCRAWLER_API ALootInteractionActor : public AInteractionActor, public ILootableInterface
{
	GENERATED_BODY()
	
};
