
#pragma once

#include "Actors/Props/InteractionActor.h"
#include "InteractionComponent.h"
#include "CoreMinimal.h"
#include "EncounterReward.generated.h"

class UInteractionComponent;
class UStaticMeshComponent;

UCLASS(Abstract)
class DUNGEONCRAWLER_API AEncounterReward : public AInteractionActor
{
	GENERATED_BODY()

public:

	AEncounterReward();
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USceneComponent* Root;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UInteractionComponent* InteractionComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* StaticMeshComponent;

	
	UPROPERTY(BlueprintReadWrite, SaveGame, Category = State)
	bool bHasBeenPickedUp = false;

	
	UFUNCTION(BlueprintImplementableEvent)
	void Materialize();
	
};
