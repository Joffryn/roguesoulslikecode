
#pragma once

#include "GameFramework/Actor.h"
#include "CoreMinimal.h"
#include "Torch.generated.h"

class UParticleSystemComponent;
class UStaticMeshComponent;
class UPointLightComponent;
class UAudioComponent;

UCLASS(Abstract)
class DUNGEONCRAWLER_API ATorch : public AActor
{
	GENERATED_BODY()
	
public:
	
	ATorch();
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	USceneComponent* Root;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UParticleSystemComponent* ParticleSystemComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UPointLightComponent* PointLightComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UAudioComponent* AudioComponent;
	
};
