
#pragma once

#include "GameFramework/Actor.h"
#include "Interfaces/SaveableInterface.h"
#include "CoreMinimal.h"
#include "ButtonActor.generated.h"

UCLASS()
class DUNGEONCRAWLER_API AButtonActor : public AActor, public ISaveableInterface
{
	GENERATED_BODY()
	
};
