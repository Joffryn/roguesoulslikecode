
#pragma once

#include "Actors/Props/InteractionActor.h"
#include "CoreMinimal.h"
#include "Bonfire.generated.h"

UCLASS()
class DUNGEONCRAWLER_API ABonfire : public AInteractionActor
{
	GENERATED_BODY()
	
public:
	
	ABonfire();
	
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bStartsBurning = true;
	
	UPROPERTY(BlueprintReadWrite, SaveGame)
	bool bIsBurning = true;
	
protected:
	
	virtual void BeginPlay() override;
	
};
