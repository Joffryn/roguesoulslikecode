
#pragma once

#include "GameFramework/Actor.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "Weapon.generated.h"

class UColorChangingComponent;
class UWeaponPlayerAnimSet;
class UAttackMontageSetup;
class UDissolveComponent;
class UWeaponBaseAnimSet;
class USoundsComponent;
class UAICombatConfig;
class UWeaponConfig;
class UReactionSet;
class UBlendSpace;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAttackStarted);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAttackFinished);

UCLASS(Abstract)
class DUNGEONCRAWLER_API AWeapon : public AActor
{
	GENERATED_BODY()
	
public:
	
	AWeapon();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	USoundsComponent* SoundsComponent;	

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UColorChangingComponent* ColorChangingComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UDissolveComponent* DissolveComponent;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attachment")
	FName EquipSocket;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attachment")
	FName SheathSocket;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attachment")
	FName LeftSheathSocket;

	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config")
	UWeaponConfig* WeaponConfig;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config")
	UWeaponBaseAnimSet* BaseAnimsWeaponConfig;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config")
	UAICombatConfig* AICombatConfig;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config")
	UReactionSet* WeaponReactionSet;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config")
	UWeaponPlayerAnimSet* PlayerWeaponConfig;
	
	
	//Optional variable for weapons like dual wielding or one handed and shield, random one will be chosen
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
	TArray<TSubclassOf<AWeapon>> SecondWeaponClasses;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config")
	FGameplayTag WeaponHitTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attachment")
	FVector LocationCorrection;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attachment")
	FRotator RotationCorrection;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attachment")
	FVector SheathLocationCorrection;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attachment")
	FRotator SheathRotationCorrection;
	
	UPROPERTY(BlueprintReadOnly, Category = "State")
	bool bIsDropped;

	
	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FOnAttackStarted OnAttackStarted;
	
	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FOnAttackStarted OnAttackFinished;
	

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> EquippedGameplayEffectClass;
	
	//Only apply when a side weapon
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Effects")
	TSubclassOf<UGameplayEffect> EquippedLeftHandGameplayEffectClass;
	

	UPROPERTY(Transient, BlueprintReadOnly)
	UAttackMontageSetup* CurrentAttack;
	
	
	UFUNCTION(BlueprintCallable)
	TSubclassOf<AWeapon> GetRandomSecondWeaponClass();

	UFUNCTION(BlueprintCallable)
	void SetCurrentAttack(UAttackMontageSetup* NewAttack);
	
	UFUNCTION(BlueprintCallable)
	virtual void AttackStarted();

	UFUNCTION(BlueprintCallable)
	virtual void HideWeapon();

	UFUNCTION(BlueprintCallable)
	virtual void ShowWeapon();
	
	UFUNCTION(BlueprintCallable)
	virtual void AttackFinished();

	UFUNCTION(BlueprintCallable)
	virtual void DetachWeapon();
	
protected:
	
	ECollisionEnabled::Type CollisionEnabledCache;
	
};
