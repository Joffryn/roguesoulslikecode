
#pragma once

#include "TrailInterface.h"
#include "CoreMinimal.h"
#include "Weapon.h"
#include "MeleeWeapon.generated.h"

class UWeaponCollision;
class UTrailComponent;
class UBoxComponent;
class AMeleeWeapon;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponDamageDealingStarted, AMeleeWeapon*, Weapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponDamageDealingFinished, AMeleeWeapon*, Weapon);

UCLASS(Abstract)
class DUNGEONCRAWLER_API AMeleeWeapon : public AWeapon, public ITrailInterface
{
	GENERATED_BODY()
	
public:

	AMeleeWeapon();
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UWeaponCollision* WeaponCollision;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	UTrailComponent* TrailComponent;

	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FOnWeaponDamageDealingStarted OnWeaponDamageDealingStarted;

	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FOnWeaponDamageDealingFinished OnWeaponDamageDealingFinished;

	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Config | Trail")
	TMap<FGameplayTag, FWeaponElementalParams> ElementalEnchantMap;
	
	UPROPERTY(EditDefaultsOnly, Category = "Config | Debug")
	bool bShowDebug;

	UPROPERTY(BlueprintReadWrite, Category = "Damage")
	bool bIsDealingDamage;

	UPROPERTY(BlueprintReadWrite, Category = "Damage")
	bool bNextAttackWillAlwaysCrit;

	UPROPERTY(BlueprintReadWrite, Category = "Damage")
	TArray<FDamageParams> BaseDamageParams;
	
	UPROPERTY(BlueprintReadWrite, Category = "Damage")
	TArray<FDamageParams> CurrentDamageParams;

	UPROPERTY(BlueprintReadWrite, Category = "Damage")
	TArray<FOnHitEffect> BaseOnHitEffects;

	UPROPERTY(BlueprintReadWrite, Category = "Damage")
	TArray<FOnHitEffect> ExtraOnHitEffects;
	
	
	UPROPERTY(BlueprintReadWrite, Category = "Damage")
	bool bIsUnblockable;

	UPROPERTY(BlueprintReadWrite, Category = "Stagger")
	EAttackStaggerStrength CurrentAttackStaggerStrength;

	UFUNCTION(BlueprintCallable)
	virtual void OnActorOverlapped(AActor* Actor, const FHitResult& HitResult);

	UFUNCTION(BlueprintCallable)
	void ClearHitActors();

	UFUNCTION(BlueprintCallable)
	void PlayHitSound() const;

	virtual void Tick(const float DeltaTime) override;

	virtual void BeginPlay() override;

	virtual UStaticMeshComponent* GetStaticMesh_Implementation() override;

	virtual void DetachWeapon() override;
	
	UFUNCTION(BlueprintCallable)	
    void CalculateWeaponDamage(float DamageMultiplier, int32 ExtraAttackStaggerStrength);
	
	UFUNCTION(BlueprintImplementableEvent ,BlueprintCallable)
	void SetUnblockableWeaponMode(bool bNewState);
	
protected:

	virtual void HideWeapon() override;
	
	virtual void ShowWeapon() override;

private:

	void TraceWeaponCollisions();
	
	UPROPERTY(Transient)
	TArray<AActor*> ActorsHitInCurrentAttack;

	UPROPERTY(Transient)
	TArray<UWeaponCollision*> CachedWeaponCollisionComponents;

	void LoadConfigFromDataAsset();

	void ApplyOnHitEffects(const AActor* Target);

	void ApplyOnHitEffect(const AActor* Target, UAbilitySystemComponent* AbilitySystemComponent, const FGameplayEffectContextHandle& EffectContext, FOnHitEffect OnHitEffect) const;

	void DecideStartingLook();

	void DecideLook();
	
	void CollectCollisionComponents();

	void BindDelegates();

	UFUNCTION()
	void OnGameplayTagAddedToTheOwner(FGameplayTag Tag);

	UFUNCTION()
	void OnGameplayTagRemovedFromTheOwner(FGameplayTag Tag);
	
	void CacheWeaponCollisionTransforms();

	TArray<FTransform> PreviousWeaponCollisionsTransformCache;

};
