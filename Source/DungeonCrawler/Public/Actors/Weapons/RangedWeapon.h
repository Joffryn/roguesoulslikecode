
#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "RangedWeapon.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnProjectileStartMoving);

UCLASS(Abstract)
class DUNGEONCRAWLER_API ARangedWeapon : public AWeapon
{
	GENERATED_BODY()

};
