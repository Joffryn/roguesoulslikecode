
#pragma once

#include "UObject/Interface.h"
#include "CoreMinimal.h"
#include "CastableAbilityInterface.generated.h"

UINTERFACE(MinimalAPI)
class UCastableAbilityInterface : public UInterface
{
	GENERATED_BODY()
};

class DUNGEONCRAWLER_API ICastableAbilityInterface
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	float GetManaCost(AActor* Owner = nullptr);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	UTexture2D* GetIcon(AActor* Owner = nullptr);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	FLinearColor GetIconColor(AActor* Owner = nullptr);
	
};
