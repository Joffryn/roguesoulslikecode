
#pragma once

#include "UObject/Interface.h"
#include "CoreMinimal.h"
#include "LootableInterface.generated.h"

UINTERFACE(MinimalAPI)
class ULootableInterface : public UInterface
{
	GENERATED_BODY()
};


class DUNGEONCRAWLER_API ILootableInterface
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Loot)
	FVector GetLootSpawnLocation();
	
};
