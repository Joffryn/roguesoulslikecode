
#pragma once

#include "UObject/Interface.h"
#include "CoreMinimal.h"
#include "TrailInterface.generated.h"

UINTERFACE(MinimalAPI)
class UTrailInterface : public UInterface
{
	GENERATED_BODY()
};

class DUNGEONCRAWLER_API ITrailInterface
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	UStaticMeshComponent* GetStaticMesh();
	
};
