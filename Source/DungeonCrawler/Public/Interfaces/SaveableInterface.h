
#pragma once

#include "UObject/Interface.h"
#include "CoreMinimal.h"
#include "SaveableInterface.generated.h"

UINTERFACE(MinimalAPI)
class USaveableInterface : public UInterface
{
	GENERATED_BODY()
};

class DUNGEONCRAWLER_API ISaveableInterface
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintNativeEvent)
	void OnActorLoaded();

};
