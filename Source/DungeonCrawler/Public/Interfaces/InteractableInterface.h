
#pragma once

#include "UObject/Interface.h"
#include "CoreMinimal.h"
#include "InteractableInterface.generated.h"

UINTERFACE(MinimalAPI, BlueprintType, Blueprintable)
class UInteractableInterface : public UInterface
{
	GENERATED_BODY()
};

class DUNGEONCRAWLER_API IInteractableInterface
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Interaction)
	bool CanInteract();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Interaction)
	FText GetInteractionText();
	
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Interaction)
	void Interact();

};
