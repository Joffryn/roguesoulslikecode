
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "PlayerFeedbackComponent.generated.h"

class AMyPlayerController;
class ABaseCharacter;

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class DUNGEONCRAWLER_API UPlayerFeedbackComponent final : public UActorComponent
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditDefaultsOnly, Category = "Config | ForceFeedback")
	UForceFeedbackEffect* AttackChargingForceFeedback;

	UPROPERTY(EditDefaultsOnly, Category = "Config | ForceFeedback")      
	UForceFeedbackEffect* AttackChargingEndForceFeedback;                                 
	                                                                            
	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnBeingHitFeedback;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnBeingCritFeedback;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnBeingBlockedFeedback;
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnBeingParriedFeedback;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnDeathFeedback;
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnHitFeedback;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnCritFeedback;
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnBlockedFeedback;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnHitInvincibleFeedback;
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnParriedFeedback;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FActionFeedbackSetup OnKillFeedback;

	
	UPROPERTY(EditDefaultsOnly, Category = "Config | Audio")
	USoundBase* StaminaDrainSound;

	UPROPERTY(EditDefaultsOnly, Category = "Config | Audio")
	USoundBase* ManaBurnedSound;
	
	
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
	void BindDelegates();

	UFUNCTION()
	void OnDeath();
	
	UFUNCTION()
	void OnKill(AActor* Target);
	
	UFUNCTION()
	void OnBeingHit();

	UFUNCTION()
	void OnBeingCrit();
	
	UFUNCTION()
	void OnBeingBlocked();

	UFUNCTION()
	void OnBeingParried();
	
	UFUNCTION()
	void OnAttackHit(); 

	UFUNCTION()
	void OnAttackCrit(TSubclassOf<UDamageType> DamageType, AActor* DamageCauser, float Amount);

	UFUNCTION()
	void OnAttackBlocked();

	UFUNCTION()
	void OnAttackParried(AActor* Actor, TSubclassOf<UDamageType> DamageType, float Amount);

	UFUNCTION()
	void OnAttackHitInvincible();

	UFUNCTION()
	void OnAttackChargingStarted(); 

	UFUNCTION()
	void OnAttackChargingFinished();


	UFUNCTION()
	void OnStaminaDrained() const;
	
	UFUNCTION()
	void OnManaBurned() const;


protected:

	UPROPERTY(Transient)
	AMyPlayerController* ControllerOwner;
	UPROPERTY(Transient)
	ABaseCharacter* CharacterOwner;

	
	float CurrentCustomTimeDilation = 1.0f;
	FTimerHandle SlomoTimer;
	
	void PlayActionFeedback(const FActionFeedbackSetup& FeedbackSetup);
	
	void TryToApplySlomo(float Time, float Value);
	void OnSlomoEnded();

};
