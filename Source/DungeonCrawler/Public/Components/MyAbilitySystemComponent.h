
#pragma once

#include "AbilitySystemComponent.h"
#include "CoreMinimal.h"
#include "MyAbilitySystemComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnAttributeValueChange, const FGameplayAttribute, Attribute, float, NewValue, float, OldValue);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAbilityActivatedDelegate, UGameplayAbility*, Ability);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAbilityCommitedDelegate, UGameplayAbility*, Ability);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FMyAbilityFailedDelegate, UGameplayAbility*, Ability, const FGameplayTagContainer&, FailureReason);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FAbilityEndedDelegate, UGameplayAbility*, Ability, bool, WasCancelled);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FGameplayEventDelegate, FGameplayTag, EventTag, FGameplayEventData, EventData);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FGameplayTagChangeDelegate, FGameplayTag, Tag);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnAttributeDepleted, const FGameplayAttribute, Attribute);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FAbilityQueueEvent);

UCLASS()
class DUNGEONCRAWLER_API UMyAbilitySystemComponent final : public UAbilitySystemComponent
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UMyAbilitySystemComponent* GetPlayerAbilitySystemComponent(const UObject* WorldContextObject);
	
	UPROPERTY(Transient, BlueprintAssignable, Category = "Abilities")
	FAbilityActivatedDelegate OnAbilityActivated;

	UPROPERTY(Transient, BlueprintAssignable, Category = "Abilities")
	FAbilityCommitedDelegate OnAbilityCommited;

	UPROPERTY(Transient, BlueprintAssignable, Category = "Abilities")
	FMyAbilityFailedDelegate OnAbilityFailed;

	UPROPERTY(Transient, BlueprintAssignable, Category = "Abilities")
	FAbilityEndedDelegate OnAbilityEnded;

	UPROPERTY(Transient, BlueprintAssignable, Category = "Abilities")
	FGameplayEventDelegate OnGameplayEvent;

	UPROPERTY(Transient, BlueprintAssignable, Category = "Tags")
	FGameplayTagChangeDelegate	OnGameplayTagAdded;
	
	UPROPERTY(Transient, BlueprintAssignable, Category = "Tags")
	FGameplayTagChangeDelegate	OnGameplayTagRemoved;

	FAbilityQueueEvent	OnAbilityQueued;

	UPROPERTY(Transient, BlueprintAssignable, Category = "Queue")
	FAbilityQueueEvent	OnAbilityActivatedFromQueue;

	UPROPERTY(Transient, BlueprintAssignable, Category = "Queue")
	FAbilityQueueEvent	OnAbilityRemovedFromQueue;

	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable)
	FOnAttributeValueChange OnAttributeValueChange;

	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable)
	FOnAttributeDepleted OnAttributeDepleted;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	FGameplayTagContainer TagsToApplyAtBeginPlay;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	float TimeToClearQueue = 0.5f;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	TArray<TSubclassOf<UGameplayEffect>> EffectsToApplyAtBeginPlay;
	

	//Make sure that only one tag is active at the time
	UFUNCTION(BlueprintCallable)
	bool HandleTagChange(FGameplayTag TagToPotentiallyAdd, FGameplayTagContainer TagsToRemove, FGameplayTagContainer BlockingTags);
	
	bool TryActivateAbilitiesByTagWithEventData(const FGameplayTagContainer& GameplayTagContainer, const FGameplayEventData& EventData, bool bAllowRemoteActivation = true);

	UFUNCTION(BlueprintCallable, Category = "Abilities", meta = (DisplayName = "TryActivateAbilitiesByTagWithEventData"))
	bool BP_TryActivateAbilitiesByTagWithEventData(const FGameplayTagContainer& GameplayTagContainer, FGameplayEventData EventData, bool bAllowRemoteActivation = true);

	bool TryActivateAbilityEventData(const FGameplayAbilitySpecHandle& AbilityToActivate, const FGameplayEventData* EventData, bool bPressed, bool bAllowRemoteActivation = true);

	virtual int32 HandleGameplayEvent(FGameplayTag EventTag, const FGameplayEventData* Payload) override;

	virtual void SetUserAbilityActivationInhibited(bool NewInhibit) override;

	virtual void BeginPlay() override;

	
	UFUNCTION(BlueprintCallable, Category = "Tags")
	void AddGameplayTag(const FGameplayTagContainer	TagsToAdd);

	UFUNCTION(BlueprintCallable, Category = "Tags")
	void RemoveGameplayTag(const FGameplayTagContainer	TagsToRemove, const bool bAll = false);

	UFUNCTION(BlueprintCallable, Category = "Queue")
	bool QueueAbility(const FGameplayAbilitySpecHandle AbilityToQueue);

	UFUNCTION(BlueprintCallable, Category = "Queue")
	bool QueueAbilityWithEventData(const FGameplayAbilitySpecHandle AbilityToQueue, const FGameplayEventData EventData);

	UFUNCTION(BlueprintCallable, Category = "Queue")
	bool QueueAbilityByClass(TSubclassOf< UGameplayAbility > AbilityClass);
	
	UFUNCTION(BlueprintCallable, Category = "Queue")
	bool QueueAbilityByTag(const FGameplayTagContainer AbilityTags);

	UFUNCTION(BlueprintCallable, Category = "Queue")
	bool QueueAbilityByEvent(const FGameplayTag EventTag, const FGameplayEventData EventData);

	UFUNCTION(BlueprintCallable, Category = "Queue")
	void ClearQueue();

	UFUNCTION(BlueprintPure, Category = "Queue")
	bool HasQueuedAbility() const;
	
	UFUNCTION(BlueprintPure, Category = "Queue")
	UGameplayAbility* GetQueuedAbility();

	UFUNCTION(BlueprintPure, Category = "Getters")
	UGameplayAbility* GetAbilityInstance(FGameplayAbilitySpecHandle Handle);
		
	void BroadcastAttributeChange(const FOnAttributeChangeData& AttributeChangeData) const;

protected:

	FGameplayAbilitySpecHandle QueuedAbilityHandle;
	FGameplayEventData QueuedAbilitiesEventData;

	bool InternalTryQueueAbility(const FGameplayAbilitySpecHandle AbilityToQueue, const FGameplayEventData* EventData = nullptr);
	void HandleAbilityQueue();
	
	virtual void NotifyAbilityActivated(const FGameplayAbilitySpecHandle Handle, UGameplayAbility* Ability) override;
	virtual void NotifyAbilityFailed(const FGameplayAbilitySpecHandle Handle, UGameplayAbility* Ability, const FGameplayTagContainer& FailureReason) override;
	virtual void NotifyAbilityEnded(const FGameplayAbilitySpecHandle Handle, UGameplayAbility* Ability, bool bWasCancelled) override;
	virtual void NotifyAbilityCommit(UGameplayAbility* Ability) override;
	virtual void OnTagUpdated(const FGameplayTag& Tag, bool TagExists) override;

	void ApplyStartingGameplayEffects();
	void BindDelegates();

	FTimerHandle ClearQueueTimerHandle;

};
