
#pragma once

#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "ColorChangingComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UColorChangingComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:	

	UColorChangingComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FLinearColor DefaultColor = FLinearColor::White;
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	float ColorChangingSpeed = 5.0f;

	UPROPERTY(BlueprintReadOnly)
	FLinearColor CurrentColor = FLinearColor::White;

	UFUNCTION(BlueprintCallable)
	void AddColorChange(FLinearColor Color, FGameplayTag Reason = FGameplayTag());

	UFUNCTION(BlueprintCallable)
	void RemoveColorChange(FGameplayTag Reason);

	UFUNCTION(BlueprintPure)
	bool IsModifyingColor() const;
	
private:
	
	TMap<FGameplayTag, FLinearColor> ReasonsToColorMap;
	
	FLinearColor DesiredColor;

	UPROPERTY()
	TArray<UMaterialInstanceDynamic*> MeshMaterialDynamicInstances;

	void CalculateDesiredColor();

	FLinearColor GetAverageColor();

	void Setup();
	
};
