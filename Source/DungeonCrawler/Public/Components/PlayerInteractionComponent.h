
#pragma once

#include "InteractionComponent.h"
#include "CoreMinimal.h"
#include "PlayerInteractionComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInteractionCandidateSet, class UInteractionComponent*, InteractionCandidate);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UPlayerInteractionComponent final : public UInteractionComponent
{
	GENERATED_BODY()

public:	
	
	UPROPERTY(BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FOnInteractionCandidateSet OnInteractionCandidateSet;
	
	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable)
	void InteractAction();

	UFUNCTION(BlueprintPure)
	AActor* GetOwningActor() const;
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	void SetInteractionCandidate(UInteractionComponent* NewInteractionCandidate);
	
protected:

	virtual void BeginPlay() override;

	UPROPERTY(Transient, BlueprintReadOnly)
	UInteractionComponent* InteractionCandidate;
	
private:

	UPROPERTY(Transient)
	TArray<UInteractionComponent*> PossibleInteractionCandidates;
	
	UInteractionComponent* FindBestCandidate();
	
};
