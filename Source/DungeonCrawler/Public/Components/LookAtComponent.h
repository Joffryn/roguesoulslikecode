
#pragma once

#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "LookAtComponent.generated.h"

class UWeaponLookAtConfig;

UCLASS(ClassGroup=(Custom))
class DUNGEONCRAWLER_API ULookAtComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	ULookAtComponent();
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	FGameplayTag CannotLookAtTag;
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	FName LookAtBoneName = "head";

	UPROPERTY(EditDefaultsOnly, Category = Config)
	float InterpSpeed = 5.0f;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FVector2D MaxYawAngles = FVector2D(-90.0f, 90.0f);
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	FVector2D MaxPitchAngles = FVector2D(-45.0f, 45.0f);

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FVector2D YawBreakAngles = FVector2D(-120.0f, 120.0f);

	
	UPROPERTY(BlueprintReadOnly, Category = State)
	FRotator LookAtRotation;

	UPROPERTY(BlueprintReadOnly, Category = State)
	FRotator DesiredLookAtRotation;

	void SetParamsFromPreset(const UWeaponLookAtConfig* Preset);
	
protected:
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void CalculateLookAtRotation();
	
	void UpdateLookAtRotation(float DeltaTime);

	void SetDesiredLookAtRotation(FRotator& BaseRotation);
	
};
