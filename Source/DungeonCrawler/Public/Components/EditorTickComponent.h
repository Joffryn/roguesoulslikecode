
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "EditorTickComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEditorTickDelegate, float, DeltaTime);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UEditorTickComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UEditorTickComponent();
	
	UPROPERTY(EditAnywhere)
	bool bEditorOnlyTick = false;

	UPROPERTY(BlueprintAssignable)
	FEditorTickDelegate	EditorTick;

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	//UObject
	virtual void PostLoad() override;
	//UActorComponent
	virtual void OnComponentCreated() override;

	
};
