
#pragma once

#include "Components/BoxComponent.h"
#include "CoreMinimal.h"
#include "InteractionComponent.generated.h"

class UMeshComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInteracted, class UPlayerInteractionComponent*, InteractingComponent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UInteractionComponent : public UBoxComponent
{
	GENERATED_BODY()
	
public:	

	UInteractionComponent();
	
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FOnInteracted OnInteracted;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	bool bOutlinesEnabled = true;
	
	UPROPERTY(Transient, BlueprintReadOnly)
	bool bIsFocused;

	UPROPERTY(Transient, BlueprintReadWrite)
	bool bInteractionBlocked;
	
	UFUNCTION(BlueprintCallable)
	void ShowOutlines();

	UFUNCTION(BlueprintCallable)
	void HideOutlines();
	
	UFUNCTION(BlueprintCallable)
	void Interact(UPlayerInteractionComponent* InteractingComponent) const;

	virtual void BeginPlay() override;
	
	virtual void TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	
	TArray<UMeshComponent*> GetOutlineMeshes() const;
	bool bShowOutline;
	bool bIsInRange;
	
	UFUNCTION()
	void OnBeginInteractionOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnEndInteractionOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
};
