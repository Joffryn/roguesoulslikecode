
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "Enums.h"
#include "DamageTakingComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UDamageTakingComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	float TimeToShow = 0.1f;

	// How much the next damage info should be displaced from the previous one, if taken more than single damage in a tick
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	float NextHitLocationDisplacement = 33.0f;
	
	UPROPERTY(BlueprintReadOnly)
	TArray<FDamageTakenInfo> RememberedDamageInfos;
	
	void AddDamageInfo(EColor Color, float Amount, bool bCrit);

	void ShowDamageInfos();

protected:
	
	FTimerHandle ShowDamageInfosHandle;
	
};
