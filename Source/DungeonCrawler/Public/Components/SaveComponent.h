
#pragma once

#include "Components/ActorComponent.h"
#include "GameplayEffect.h"
#include "AttributeSet.h"
#include "CoreMinimal.h"
#include "SaveComponent.generated.h"

class ABaseCharacter;
class UMySaveGame;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnSavingStarted);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameSaved);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameLoaded);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API USaveComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:	

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static USaveComponent* GetSaveComponent(const UObject* WorldContextObject);

	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FOnSavingStarted OnSavingStarted;
	
	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FOnGameSaved OnGameSaved;

	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FOnGameLoaded OnWorldLoaded;

	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FOnGameLoaded OnPlayerLoaded;
	
	UPROPERTY(Transient, BlueprintReadOnly, Category = State)
	bool bIsWorldLoading;

	UPROPERTY(Transient, BlueprintReadOnly, Category = State)
	bool bIsPlayerLoading;
	
	UPROPERTY(EditDefaultsOnly, Category = "Save Game")
	FGameplayEffectQuery SaveGameplayEffectsQuery;
	
	UPROPERTY(EditAnywhere)
	TArray<FGameplayAttribute> AttributesToSave;
	
	UFUNCTION(BlueprintCallable, Category = Save)
	void SaveGame(FName NextLevelName, bool bSaveTransform = true);

	UFUNCTION(BlueprintCallable, Category = Save)
	static void DeleteSave();
	
	UFUNCTION(BlueprintCallable, Category = Load)
	void LoadWorld();

	UFUNCTION(BlueprintCallable, Category = Load)
	void LoadPlayer();
	
	UFUNCTION(BlueprintPure, Category = Load)
	static bool DoesSaveExist();
	
	UFUNCTION(BlueprintPure, Category = Load)
	FName GetSavedLevelName();
	
private:

	UPROPERTY()
	UMySaveGame* Save;

	UPROPERTY()
	ABaseCharacter* PlayerCharacter;

	void SaveSeed() const;
	void SavePlayerTransform() const;
	void SaveLook() const;
	void SaveWeapons() const;
	void SaveAttributes();
	void SaveCasting() const;
	void SaveUpgradeEffects() const;
	void SaveSounds() const;

	void SaveGlobalActors() const;
	void SaveWorldActors() const;

	void OnSaveCompleted(const FString& SlotName, const int32 UserIndex, bool bSuccess) const;
	
	void LoadSeed() const;
	void LoadPlayerTransform() const;
	void LoadLook() const;
	void LoadWeapons() const;
	void LoadAttributes();
	void LoadCasting() const;
	void LoadUpgradeEffects() const;
	void LoadSounds() const;
	
	void LoadWorldActors() const;
	void LoadGlobalActors() const;
	
};
