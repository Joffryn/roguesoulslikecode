
#pragma once

#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "SoundsComponent.generated.h"

class USoundsConfig;

UCLASS()
class DUNGEONCRAWLER_API USoundsComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Config)
	USoundsConfig* SoundsConfig;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	float VolumeMultiplier = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Config)
	float PitchMultiplier = 1.0f;
	

	UFUNCTION(BlueprintCallable, Category = Sounds)
	void SpawnSound(const FGameplayTag SoundTag, float InVolumeMultiplier = 1.0f, float InPitchMultiplier = 1.0f) const;

	UFUNCTION(BlueprintCallable, Category = Sounds)
	void SpawnSound2D(const FGameplayTag SoundTag, float InVolumeMultiplier = 1.0f, float InPitchMultiplier = 1.0f) const;
	
	UFUNCTION(BlueprintCallable, Category = Sounds)
	UAudioComponent* SpawnSoundAttached(const FGameplayTag SoundTag, const FName AttachPointName, float InVolumeMultiplier = 1.0f, float InPitchMultiplier = 1.0) const;

protected:

	USoundAttenuation* GetBaseAttenuation() const;
	
};
