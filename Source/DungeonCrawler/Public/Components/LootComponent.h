
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "LootComponent.generated.h"

class ABaseCharacter;
class ULootConfig;
class ULootManager;

UCLASS( ClassGroup=(Custom), Blueprintable)
class DUNGEONCRAWLER_API ULootComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	ULootConfig* Config;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	bool bSpawnInFront = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config, meta = (EditCondition = bSpawnInFront))
	float SpawnConeAngle = 90.0f;
	
	UPROPERTY(BlueprintReadOnly)
	int32 GoldToDrop;

	UFUNCTION(BlueprintCallable)
	void SpawnLoot();
	
protected:
	
	virtual void BeginPlay() override;

	UFUNCTION()
	void OnOwnerDeath(ABaseCharacter* Owner);
	
	void DropPickups() const;
	void DropGold() const;
	
	void ApplyImpulseOnDroppedItem(const AActor* Actor, const ULootManager* LootManager, float Multiplier = 1.0f) const;
	
};
