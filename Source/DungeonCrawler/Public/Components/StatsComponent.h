
#pragma once

#include "Components/ActorComponent.h"
#include "AttributeSet.h"
#include "CoreMinimal.h"
#include "Enums.h"
#include "StatsComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStatChanged, EStat, Stat, float, Amount);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStatDeplated, EStat, Stat);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnComponentSetup);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class DUNGEONCRAWLER_API UStatsComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TMap<FGameplayAttribute, EStat> AttributesToStatsMap;

	UFUNCTION(BlueprintPure)
	float GetStatValue(const EStat Stat) const;

	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable)
	FOnStatChanged OnStatChanged;

	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable)
	FOnStatDeplated OnStatDepleted;

	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable)
	FOnComponentSetup OnComponentSetup;
	
	void OnAttributeChange(const struct FOnAttributeChangeData& ChangeData);

	virtual void BeginPlay() override;

private:
	
	FTimerHandle BindDelegatesTimerHandle;
	
	void BindDelegates();
	
};
