
#pragma once

#include "Components/LookAtComponent.h"
#include "CoreMinimal.h"
#include "PlayerLookAtComponent.generated.h"

UCLASS(meta=(BlueprintSpawnableComponent))
class DUNGEONCRAWLER_API UPlayerLookAtComponent : public ULookAtComponent
{
	GENERATED_BODY()

protected:
	
	virtual void CalculateLookAtRotation() override;
	
};
