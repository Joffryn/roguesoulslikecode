
#pragma once

#include "Components/SceneComponent.h"
#include "CoreMinimal.h"
#include "CharacterCenterComponent.generated.h"

//Empty class existing for sorting in the editor
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UCharacterCenterComponent : public USceneComponent
{
	GENERATED_BODY()
	
};
