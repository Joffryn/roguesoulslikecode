
#pragma once

#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "GameConfigComponent.generated.h"

class AMeleeWeapon;
class AWeapon;
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UGameConfigComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UGameConfigComponent* GetGameConfigComponent(const UObject* WorldContextObject);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat)
	bool bPlayerUsesPoise;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat)
	bool bUseEnchants;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat)
	bool bFriendlyFireEnabled;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat)
	bool bFallingDamageEnabled;
	

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = UI)
	bool bShowDamageNumbers;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = UI)
	bool bShowBaseActionsWidget;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = UI)
	bool bShowGoldCount;


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat)
	FGameplayTagContainer NegativeStatuses;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat)
	bool bShowInvincibleHitCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat)
	bool bShouldAIInterruptComboses;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category =  Combat)
	float DamageMultiplierAI = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category =  Combat)
	float DamageMultiplierPlayer = 1.0f;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Trail)
	UParticleSystem* BaseTrail;


	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Sound)
	USoundAttenuation* CloseRangeAttenuation;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Sound)
	USoundAttenuation* BaseAttenuation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Sound)
	USoundAttenuation* BaseLongAttenuation;

	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	TSubclassOf<AMeleeWeapon> LegWeaponClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	TSubclassOf<AMeleeWeapon> HandWeaponClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	TSubclassOf<AWeapon> NoWeaponClass;
	
};
