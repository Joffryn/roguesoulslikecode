
#pragma once

#include "PhysicsEngine/PhysicalAnimationComponent.h"
#include "CoreMinimal.h"
#include "MyPhysicalAnimationComponent.generated.h"

class ABaseCharacter;

UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class DUNGEONCRAWLER_API UMyPhysicalAnimationComponent : public UPhysicalAnimationComponent
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditDefaultsOnly)
	FPhysicalAnimationData RagdollPhysicalAnimationData;

	UPROPERTY(EditDefaultsOnly)
	FName PelvisBone = "Pelvis";

	UPROPERTY(EditDefaultsOnly)
	FName SpineBone = "spine_01";

	UFUNCTION(BlueprintPure, Category = Getters)
	USkeletalMeshComponent* GetSkeletalMeshComponent() const { return GetSkeletalMesh(); }

	virtual void BeginPlay() override;

	UFUNCTION()
	void RagdollBlendStart();

	UFUNCTION()
	void RagdollBlendEnd();

	void UpdateRagdollBlend() const;

	float RagdollBlend = 0.f;

protected:

	UPROPERTY(EditDefaultsOnly)
	UCurveFloat* RagdollBlendInCurve;

	UPROPERTY()
	FName HitBoneCache;

	bool bBlendRagdoll;
	FTimerHandle Handle;
	float HitBlend = 0.f;

};
