
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "Enums.h"
#include "DissolveComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDissolveComponentDelegate, AActor*, Actor);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UDissolveComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	
	UDissolveComponent();

	UPROPERTY(BlueprintAssignable, Category = Delegates)
	FDissolveComponentDelegate OnFullyDissolved;

	UPROPERTY(BlueprintAssignable, Category = Delegates)
	FDissolveComponentDelegate OnFullyMaterialized;

	
	UPROPERTY(EditDefaultsOnly, Category = Dissolve)
	float DissolveTime = 0.5f;

	UPROPERTY(EditDefaultsOnly, Category = Dissolve)
	EColor DissolveColor = EColor::Red;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	bool bDestroyActorWhenDone = true;
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	USoundAttenuation* Attenuation;

	UPROPERTY(EditDefaultsOnly, Category = Dissolve)
	USoundBase* DissolveSound;
	
	UPROPERTY(EditDefaultsOnly, Category = Dissolve)
	UCurveFloat* DissolveCurve;

	
	UPROPERTY(EditDefaultsOnly, Category = Materialize)
	USoundBase* MaterializeSound;
	
	UPROPERTY(EditDefaultsOnly, Category = Materialize)
	float MaterializeTime = 0.5f;

	UPROPERTY(EditDefaultsOnly, Category = Materialize)
	EColor MaterializeColor =  EColor::Yellow;
	
	UPROPERTY(EditDefaultsOnly, Category = Materialize)
	UCurveFloat* MaterializeCurve;

	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
		
	UFUNCTION(BlueprintCallable, Category = Dissolve)
	void Dissolve();

	UFUNCTION(BlueprintCallable, Category = Materialize)
	void Materialize();

protected:
	
	UPROPERTY()
	TArray<UMaterialInstanceDynamic*> MaterialsToDissolve;
	
	void CollectInfos();

	void ApplyDissolveOnMaterials(float Progress);
	void ApplyColorOnMaterials(FLinearColor Color);
	
	FName ProgressParameter = "DissolveProgress";
	FName ColorParameter = "DissolveColor";
	float DissolveProgress;
	bool bIsMaterializing;
	bool bIsDissolving;
	
};
