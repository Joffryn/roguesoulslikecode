
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "CustomizationComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UCustomizationComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UFUNCTION(BlueprintCallable)
	void FormHeroFromPresets(FRandomHeroSetup Setup);

protected:

	void AddAttachments(TArray<FSocketAttachmentSetup> AttachmentsSetup);
	
};
