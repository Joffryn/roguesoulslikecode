
#pragma once

#include "GameFramework/CharacterMovementComponent.h"
#include "CoreMinimal.h"
#include "MyCharacterMovementComponent.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UMyCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadWrite, Category = Input)
	float ForwardAxisValue;
	
	UPROPERTY(BlueprintReadWrite, Category = Input)
	float RightAxisValue;

	UFUNCTION(BlueprintPure, Category = "Getters")
	FVector GetFeetLocation() const;
	
};
