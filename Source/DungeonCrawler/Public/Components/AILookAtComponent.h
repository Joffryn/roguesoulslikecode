
#pragma once

#include "Components/LookAtComponent.h"
#include "CoreMinimal.h"
#include "AILookAtComponent.generated.h"

UCLASS(meta=(BlueprintSpawnableComponent))
class DUNGEONCRAWLER_API UAILookAtComponent : public ULookAtComponent
{
	GENERATED_BODY()

public:
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	FName TargetBlackBoardName = "Target";

protected:
	
	virtual void CalculateLookAtRotation() override;
	
};
