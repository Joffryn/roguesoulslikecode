
#pragma once

#include "Components/ActorComponent.h"
#include "GameplayEffectTypes.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "Enums.h"
#include "CombatComponent.generated.h"

class UAttackMontageSetup;
class AWeapon;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FWeaponDelegate, AWeapon*, Weapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FAttackChargingDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCombatDelegate);

UCLASS()
class DUNGEONCRAWLER_API UCombatComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:

	UCombatComponent();

	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FAttackChargingDelegate OnAttackChargingStarted;

	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FAttackChargingDelegate OnAttackChargingFinished;

	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FCombatDelegate OnWeaponChanged;
	
	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FWeaponDelegate OnWeaponEquipped;
	
	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FWeaponDelegate OnWeaponUnequipped;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	TSubclassOf<AWeapon> WeaponClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config, meta = (EditCondition = "!WeaponClass"))
	TArray<TSubclassOf<AWeapon>> PossibleWeaponClasses;
	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	TSubclassOf<AWeapon> SecondaryWeaponClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	TSubclassOf<AWeapon> SecondaryLeftWeaponClass;

	
	UPROPERTY(EditAnywhere, Category = Config)
	FName RightHandAttachmentSocket;

	UPROPERTY(EditAnywhere, Category = Config)
	FName LeftHandAttachmentSocket;
	
	UPROPERTY(EditAnywhere, Category = Config)
	FName RightLegAttachmentSocket;

	UPROPERTY(EditAnywhere, Category = Config)
	FName LeftLegAttachmentSocket;
	
	UPROPERTY(EditAnywhere, Category = Config)
	bool bStartWithNoWeapon;

	UPROPERTY(EditAnywhere, Category = Config)
	FGameplayTag NoWeaponTag;
	
	UPROPERTY(BlueprintReadWrite)
	bool bNoWeaponEquipped;

	
	UPROPERTY(BlueprintReadWrite)
    FGameplayTagContainer CurrentAttackExtraTags;
	
	//UPROPERTY(BlueprintReadOnly)
	//FAttackParams CurrentAttackParams;

	UPROPERTY(BlueprintReadOnly)
	UAttackMontageSetup* CurrentAttack;
	
	UPROPERTY(BlueprintReadOnly)
	FActiveGameplayEffectHandle EquippedRightWeaponEffectHandle;

	UPROPERTY(BlueprintReadOnly)
	FActiveGameplayEffectHandle EquippedLeftWeaponEffectHandle;

	UPROPERTY(Transient, BlueprintReadWrite, Category = Chargining)
	bool bIsAttackChargingBlocked;
	
	UPROPERTY(Transient, BlueprintReadOnly, Category = Chargining)
	bool bIsChargingAttack;

	UPROPERTY(Transient, BlueprintReadOnly)
	float CurrentAttackDamageMultiplier = 0.0f;
	
	UPROPERTY(Transient, BlueprintReadOnly)
	int32 CurrentAttackExtraStaggerStrength = 0;

	
	void Init();

	virtual void BeginPlay() override;
	
	UFUNCTION(BlueprintCallable)
	void EquipWeaponByClass(const TSubclassOf<AWeapon> WeaponClassToEquip);

	UFUNCTION(BlueprintCallable)
	void SwitchWeapon();

	UFUNCTION(BlueprintCallable)
	void EquipNoWeapon();

	UFUNCTION(BlueprintCallable)
	void UnequipNoWeapon();
	
	UFUNCTION(BlueprintCallable)
	void InstantlySwitchWeapon();

	UFUNCTION(BlueprintCallable)
	void InstantlyEquipNoWeapon();
	
	UFUNCTION(BlueprintPure)
	bool HasWeapon() const;

	UFUNCTION(BlueprintPure)
	AWeapon* GetWeaponInSocket(const EWeaponSocket Socket) const;

	UFUNCTION(BlueprintPure)
	AWeapon* GetMainWeapon() const;
	
	UFUNCTION(BlueprintPure)
	AWeapon* GetSecondaryWeaponInSocket(const EWeaponSocket Socket) const;
		
	UFUNCTION(BlueprintPure)
	FORCEINLINE AWeapon* GetRightHandWeapon() const { return RightHandWeapon; }

	UFUNCTION(BlueprintPure)
	FORCEINLINE AWeapon* GetLeftHandWeapon() const { return LeftHandWeapon; }

	UFUNCTION(BlueprintPure)
	FORCEINLINE AWeapon* GetRightLegWeapon() const { return RightLeg; }

	UFUNCTION(BlueprintPure)
	FORCEINLINE AWeapon* GetLeftLegWeapon() const { return LeftLeg; }

	UFUNCTION(BlueprintCallable)
	void CalculateWeaponAttack(const EWeaponSocket WeaponSocket, float DamageMultiplier, int32 ExtraAttackStaggerStrength) const ;

	UFUNCTION(BlueprintCallable, Category = Chargining)
	virtual void StartCharging(float InChargingAnimRateScale, float InNoChargingAnimRateScale, float InTotalDuration, float InDamageMultiplier, int32 InExtraStaggerStrength);

	UFUNCTION(BlueprintCallable, Category = Chargining)
	virtual void StopCharging(bool bFullyCharged);
	
	UFUNCTION(BlueprintCallable)
	void ClearCurrentAttackModifiers();
	
	UFUNCTION(BlueprintPure)
	TArray<AWeapon*> GetWeapons() const;

	UFUNCTION(BlueprintPure)
	TArray<AWeapon*> GetSecondaryWeapons() const;

	UFUNCTION(BlueprintCallable)
	void SheathWeapon(AWeapon* Weapon) const ;

	UFUNCTION(BlueprintCallable)
	void DrawWeapon(AWeapon* Weapon) const ;
	
protected:

	UPROPERTY(Transient)
	AWeapon* RightHandWeapon;

	UPROPERTY(Transient)
	AWeapon* LeftHandWeapon;
	
	UPROPERTY(Transient)
	AWeapon* SecondaryRightHandWeapon;

	UPROPERTY(Transient)
	AWeapon* SecondaryLeftHandWeapon;
	
	UPROPERTY(Transient)
	AWeapon* RightLeg;

	UPROPERTY(Transient)
	AWeapon* LeftLeg;
	
	AWeapon* SpawnAndAttachWeapon(const TSubclassOf<AWeapon> WeaponClassToSpawn, const EWeaponSocket WeaponSocket) const;

	void SpawnAndAttachLegWeapons();
	void SpawnAndAttachSecondaryWeapons();
	void PostEquipActions() ;

	float CachedChargingAnimRateScale = 1.0f;
	float CachedNoChargingAnimRateScale = 1.0f;
	float CachedChargingTotalDuration = 0.0f;
	float CachedChargingStartWorldTime = 0.0f;
	float CachedChargingDamageMultiplier = 0.0f;
	int32 CachedChargingExtraStagger = 0;
	
};
