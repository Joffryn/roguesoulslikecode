
#pragma once

#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "CritSolver.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UCritSolver : public UActorComponent
{
	GENERATED_BODY()

public:	

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UCritSolver* GetCritSolver(const UObject* WorldContextObject);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag AlwaysCrittedTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag CrittedWithNextHit;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag AlwaysCritsTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag CritImmuneTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag SprintAttackTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag SprintAttackCritUpgradeTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag RollAttackTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag RollAttackCritUpgradeTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag FinalComboAttackTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag FinalComboAttackCritUpgradeTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag FullyChargedAttackTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag FullyChargedAttackCritUpgradeTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag AirAttackTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag AirAttackCritUpgradeTag;
	
	UFUNCTION(BlueprintPure)
	bool IsAttackCrit(const AActor* Attacker, AActor* Attacked, const UDamageType* DamageType) const;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	float CritDamageMultiplier = 1.5f;
	
};
