
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "BossComponent.generated.h"

class ABaseCharacter;

struct FGameplayAttribute;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBossPhaseEnter, int32, Phase);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UBossComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UBossComponent();

	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable)
	FOnBossPhaseEnter OnBossPhaseEnter;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TArray<FBossPhaseInfo> PhasesSetup;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	FText Name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Music")
	USoundBase* Music;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Music")
	float MusicFadeInTime = 1.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config | Music")
	float MusicFadeOutTime = 2.5f;

	UPROPERTY(BlueprintReadWrite, Category = Phase)
	int32 Phase;
	
	UFUNCTION(BlueprintCallable, Category = Phase)
	void SetPhase(int32 NewPhase);

protected:

	virtual void BeginPlay() override;

private:	

	bool bIsSetuped;
	
	UFUNCTION()
	void OnTargetSet(AActor* Target);

	UFUNCTION()
	void OnOwnerDeath(ABaseCharacter* Actor);

	UFUNCTION()
	void OnOwnerAttributeChange(const FGameplayAttribute Attribute, float NewValue, float OldValue);
	
};
