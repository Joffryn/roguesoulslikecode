
#pragma once

#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "EnchantmentHandlingComponent.generated.h"

class UAbilitySystemComponent;
class UGameplayEffect;
class ABaseCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UEnchantmentHandlingComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:	

	virtual void BeginPlay() override;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attributes")
	TSubclassOf<UGameplayEffect> RestoreHealthEffect;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag HealOnHitTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag DamageMeleeTag;
	

	UPROPERTY(Transient)
	ABaseCharacter* BaseCharacterOwner;

	UPROPERTY(Transient)
	UAbilitySystemComponent* OwnerAbilitySystemComponent;

	void BindDelegates();
	
	UFUNCTION()
	void OnAttackDamageDealed(TSubclassOf<UDamageType> DamageType, AActor* DamageCauser, float Amount);

};
