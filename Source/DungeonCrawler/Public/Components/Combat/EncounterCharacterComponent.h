
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "Enums.h"
#include "EncounterCharacterComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UEncounterCharacterComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	
	UPROPERTY(BlueprintReadOnly)
	EEnemyTier Tier = EEnemyTier::None;
	
};
