
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "TrailComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UTrailComponent final : public UActorComponent
{
	GENERATED_BODY()

public:	

	UTrailComponent();

	UFUNCTION(BlueprintCallable)
	void StartTrail();

	UFUNCTION(BlueprintCallable)
	void EndTrail() const ;
	
	UPROPERTY(BlueprintReadWrite, Category = Trail)
	UParticleSystem* TrailOverride;

	//if empty generic trail will be used
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Trail)
	UParticleSystem* BaseTrail;
	
	// The particle system to use for this trail.
	UPROPERTY(BlueprintReadWrite, Category = Trail)
	 UParticleSystem* ParticleSystemTemplate;

	// Name of the first socket defining this trail.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Trail)
	FName FirstSocketName;

	//  Name of the second socket defining this trail. 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Trail)
	FName SecondSocketName;


	virtual void BeginPlay() override;
	
	/**
	Controls the way width scale is applied. In each method a width scale of 1.0 will mean the width is unchanged from the position of the sockets. A width scale of 0.0 will cause a trail of zero width.
	From Centre = Trail width is scaled outwards from the centre point between the two sockets.
	From First = Trail width is scaled outwards from the position of the first socket.
	From Second = Trail width is scaled outwards from the position of the Second socket.
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Trail)
	TEnumAsByte<ETrailWidthMode> WidthScaleMode;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Trail)
	uint32 bRecycleSpawnedSystems : 1;

	/** Helper function for outside code to get ParticleSystemComponent that we are using */
	 UParticleSystemComponent* GetParticleSystemComponent(const UMeshComponent* MeshComponent) const;

private:

	void TrailBegin(UMeshComponent* MeshComponent);
	void TrailEnd(const UMeshComponent* MeshComponent) const ;

};
