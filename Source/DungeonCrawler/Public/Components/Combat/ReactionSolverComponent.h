
#pragma once

#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "Enums.h"
#include "ReactionSolverComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UReactionSolverComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UReactionSolverComponent();

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UReactionSolverComponent* GetReactionSolver(const UObject* WorldContextObject);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TMap<EAttackStaggerStrength, FGameplayTag> AttackStrengthToTagsMap;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TMap<EStaggerReaction, FGameplayTag> StaggerReactionToTagsMap;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FPoiseToStaggerSetup ReactionsSetup;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag NoReaction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag AdditiveHitReaction;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag HitReaction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag StrongHitReaction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag StunReaction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag KnockDownReaction;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TArray<FSingleHitDirectionSetup> DirectionsToAnglesSetupsTwoDirections;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TArray<FSingleHitDirectionSetup> DirectionsToAnglesSetupsThreeDirections;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TArray<FSingleHitDirectionSetup> DirectionsToAnglesSetupsFourDirections;

	UFUNCTION(BlueprintPure)
	EDirection GetDirectionFromAngle(float Angle, EHitMapType HitMapType = EHitMapType::ThreeDirections) const;

	UFUNCTION(BlueprintPure)
	FGameplayTag GetGameplayTagForAttackStaggerStrength(EAttackStaggerStrength AttackStaggerStrength);

	UFUNCTION(BlueprintCallable)
    void SendStaggerGameplayEvent(AActor* Target, EAttackStaggerStrength AttackStaggerStrength, const AActor* DamageCauser, EDirection HitDirection = EDirection::None);

	UFUNCTION(BlueprintPure)
	EAttackStaggerStrength GetAttackStaggerStrengthForGameplayTag(FGameplayTag Tag) const;
	
	UFUNCTION(BlueprintPure)
	FGameplayTag GetReactionTag(EAttackStaggerStrength AttackStrength, EPoiseLevel PoiseLevel);
		
private:

	EStaggerReaction DecideStaggerReaction(EAttackStaggerStrength AttackStrength, EPoiseLevel PoiseLevel);

};
