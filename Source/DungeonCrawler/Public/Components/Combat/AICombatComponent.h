
#pragma once
		
#include "Components/ActorComponent.h"
#include "Perception/AIPerceptionTypes.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "AICombatComponent.generated.h"

class UMyAbilitySystemComponent;
class UAIAttackMontageSetup;
class UCombatComponent;
class AMyAIController;
class UAICombatConfig;
class ABaseCharacter;
class AWeapon;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UAICombatComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UAICombatComponent();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FName TargetBlackboardName;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FName DistanceToTargetBlackboardName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FName AngleToTargetBlackboardName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FName TimeSinceLastImpactfulActionBlackboardName;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag RangedTag;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag DuringActionTag;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Config)
    EAIDesiredEquipment DesiredEquipment;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
    EAINoCombatBehavior NoCombatBehavior = EAINoCombatBehavior::StartingPosition;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	TMap<EAIActionType, FGameplayTag> ActionsToTagsMap;

	
	UPROPERTY(Transient, BlueprintReadOnly)
	UAICombatConfig* AICombatConfig;
	
	UPROPERTY(Transient, BlueprintReadOnly, Category = State)
	TMap<EAIActionType, float> AIActionToCooldownsSetup;
	
	UPROPERTY(Transient, BlueprintReadOnly, Category = State)
	TMap<AActor*, float> AIPerceivedEnemiesInfosMap;
	
	UPROPERTY(Transient, BlueprintReadOnly, Category = State)
	TMap<EAIActionType, FGameplayTag> ImportantActionsToTagsMap;

	
	UPROPERTY(Transient, BlueprintReadWrite, Category = Cache)
	int32 LastMeleeAttackIndex = -1;

	UPROPERTY(Transient, BlueprintReadWrite, Category = Cache)
	int32 LastCloseDistanceAttackIndex = -1;
	
	UPROPERTY(Transient, BlueprintReadWrite, Category = Cache)
	int32 LastRangedAttackIndex = -1;

	
	UPROPERTY(Transient, BlueprintReadWrite, Category = State)
	float GlobalActionsCooldown;

	UPROPERTY(Transient, BlueprintReadWrite, Category = State)
	EAIActionType PlannedAction = EAIActionType::None;

	UPROPERTY(Transient, BlueprintReadWrite, Category = State)
	EAIActionType ForcedAction = EAIActionType::None;

	UPROPERTY(Transient, BlueprintReadWrite, Category = State)
	TArray<EAIActionType> WeaponRelatedActions;
	
	UFUNCTION(BlueprintCallable)
	void TryToPlanAction(const int32 Priority);

	//Action will be queued for ai and executed in the soonest possible time
	UFUNCTION(BlueprintCallable)
	void ForceAction(EAIActionType Action);
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual void BeginPlay() override;
	
	UFUNCTION(BlueprintCallable)
	void OnActionExecuted(EAIActionType Action);

	UFUNCTION(BlueprintCallable)
	bool PerformActivationChanceCheck(EAIActionType Action);

	UFUNCTION(BlueprintCallable, Category = Setters)
	void ApplyActionCooldown(EAIActionType Action);
	
	UFUNCTION(BlueprintCallable, Category = Setters)
    void ApplyGlobalActionCooldown(float Amount);


	UFUNCTION(BlueprintPure, Category = Getters)
	bool ShouldChaseCauseTooMuchTimePassedSinceLastImpactfulAction() const;
	
	UFUNCTION(BlueprintCallable, Category = Setters)
	void SetActionCooldown(EAIActionType Action, float Cooldown);

	UFUNCTION(BlueprintPure, Category = Getters)
	bool IsActionEnabled(EAIActionType Action) const;

	UFUNCTION(BlueprintPure, Category = Getters)
	bool IsInRangeOfTheAction(EAIActionType Action) const;
	
	UFUNCTION(BlueprintPure, Category = Getters)
	bool CanExecuteAnyMeaningfulAction(int32 Priority);

	UFUNCTION(BlueprintPure, Category = Getters)
	bool CanExecutePlannedAction() const;
	
	UFUNCTION(BlueprintPure, Category = Getters)
	bool DoesRememberAnyAliveEnemy() const;

	UFUNCTION(BlueprintPure, Category = Getters)
	float GetDesiredRangeValue();
	
	UFUNCTION(BlueprintPure, Category = Getters)
	bool IsActionOnCooldown(EAIActionType Action);

	UFUNCTION(BlueprintPure, Category = Getters)
	bool IsOnGlobalActionCooldown() const;
	
	UFUNCTION(BlueprintPure, Category = Getters)
	float GetActionCooldown(EAIActionType Action);

	UFUNCTION(BlueprintPure, Category = Getters)
	UAIAttackMontageSetup* GetPossibleAttackParams(EAIActionType Action);
	
protected:
	
	UFUNCTION()
	void OnWeaponChanged();

	void UpdatePerceivedEnemiesInfo();
	void UpdateLastImpactfulActionTime(float DeltaTime) const;
	void UpdateCooldowns(float DeltaTime);
	void UpdateDistanceAndAngleToTheTarget();
	
	void InitializeCooldowns();
	void ApplyInitialCooldowns();
	void BuildActionsToTagsMap();
	
	UAIAttackMontageSetup* GetBestAttackForAction(EAIActionType Action, int32& IndexToChange) const;
	TArray<UAIAttackMontageSetup*> GetPossibleAttacksForAction(EAIActionType Action) const;
	
	UFUNCTION()
	void OnOwnerPerceptionUpdated(AActor* Actor, const FAIStimulus Stimulus);

	
	UPROPERTY(Transient)
	ABaseCharacter* BaseCharacterOwner;
	UPROPERTY(Transient)
	UCombatComponent* OwnerCombatComponent;
	UPROPERTY(Transient)
	UMyAbilitySystemComponent* OwnerAbilitySystemComponent;

	AMyAIController* GetOwnerAIController() const;
	
	UPROPERTY(BlueprintReadOnly, Transient)
	float DistanceToTheTarget;
	
	UPROPERTY(BlueprintReadOnly, Transient)
	float AngleToTarget;

	UPROPERTY(BlueprintReadOnly, Transient)
	bool bHasTarget;

	float GetTimeSinceLastImpactfulAction() const;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	float TimeToForceChaseTargetSinceLastImpactfulAction = 5.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	float PerceivedActorForgetTime = 30.0f;

	friend struct FAICombatDebugger;
	friend class ImGuiDebugMenu;
	
};
