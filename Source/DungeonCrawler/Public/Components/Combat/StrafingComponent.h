
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "Enums.h"
#include "StrafingComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStrafingEnded);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UStrafingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UStrafingComponent();
	
	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegetes)
	FOnStrafingEnded OnStrafingEnded;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TArray<EDirection> PossibleStrafeDirections;

	UPROPERTY(Transient, BlueprintReadOnly)
	AActor* Target;
	
	UPROPERTY(Transient, BlueprintReadOnly)
	EDirection StrafeDirection;
	
	//Maybe should be moved to the separate component
	UFUNCTION(BlueprintPure, Category = Rotation)
	bool WantsToStrafe() const { return bWantsToStrafe; }

	UFUNCTION(BlueprintCallable)
	void SetTarget(AActor* NewTarget);
	
	UFUNCTION(BlueprintCallable)
	void StartStrafing();

	UFUNCTION(BlueprintPure)
	FVector GetStrafingDirection() const;
	
	UFUNCTION(BlueprintCallable)
	void StopStrafing();

	
	UFUNCTION(BlueprintCallable)
	void Reset();
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:

	bool bWantsToStrafe;

	void Strafe() const;
	
};
