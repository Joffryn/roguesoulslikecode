
#pragma once

#include "Components/WidgetComponent.h"
#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "TargetSystemComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FComponentOnTargetLocked, AActor*, TargetActor);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UTargetSystemComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UTargetSystemComponent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
	float MinimumDistanceToEnable;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
	TSubclassOf<UUserWidget> TargetLockedOnWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
	float TargetLockedOnWidgetDrawSize;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
	bool ShouldDrawTargetLockedOnWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
	float BreakLineOfSightDelay;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
	float MaxCameraLookAtPitch;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
	bool ShouldControlRotationWhenLockedOn;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Config")
	FVector TargetLockedOnWidgetRelativeLocation;


	UFUNCTION(BlueprintCallable, Category = "Target System")
	void TargetActor();

	UFUNCTION(BlueprintCallable, Category = "Target System")
	void TargetLockOff();

	UFUNCTION(BlueprintCallable, Category = "Target System")
	void TargetActorWithAxisInput(const float AxisValue);

	UPROPERTY(Transient, BlueprintAssignable, Category = "Target System")
	FComponentOnTargetLocked OnTargetLockedOff;

	UPROPERTY(Transient, BlueprintAssignable, Category = "Target System")
	FComponentOnTargetLocked OnTargetLockedOn;

	UFUNCTION(BLueprintPure)
	FORCEINLINE  AActor* GetNearestTarget() const { return NearestTarget; }

	UFUNCTION(BLueprintPure)
	FORCEINLINE bool HasTarget() const { return NearestTarget != nullptr; }

	UFUNCTION(BlueprintCallable, Category = "Target System")
	void TargetLockOn(AActor* TargetToLockOn);

	UPROPERTY(BlueprintReadOnly, Transient)
	AActor* TargetetActor;

	UFUNCTION(BlueprintPure)
	FRotator GetControlRotationOnTarget() const;

private:

	float ClosestTargetDistance;
	bool TargetLocked;
	UPROPERTY(Transient)
	AActor* CharacterOwner;
	UPROPERTY(Transient)
	APlayerController* PlayerController;
	UPROPERTY(Transient)
	AActor* NearestTarget;
	UPROPERTY(Transient)
	UWidgetComponent* TargetLockedOnWidgetComponent;
	FTimerHandle LineOfSightBreakTimerHandle;
	bool bIsBreakingLineOfSight;
	FTimerHandle SwitchingTargetTimerHandle;
	bool bIsSwitchingTarget;

	TArray<AActor*> GetAllActorsOfClass(const TSubclassOf<AActor> ActorClass) const;
	AActor* FindNearestTarget(TArray<AActor*> Actors) const;
	bool LineTrace(FHitResult& HitResult, const AActor* OtherActor, const TArray<AActor*>& ActorsToIgnore = TArray<AActor*>()) const;
	bool LineTraceForActor(const AActor* OtherActor, const TArray<AActor*>& ActorsToIgnore = TArray<AActor*>()) const;
	void SetControlRotationOnTarget() const;
	void CreateAndAttachTargetLockedOnWidgetComponent(AActor* TargetActor);
	bool ShouldBreakLineOfSight() const;
	void BreakLineOfSight();
	bool IsInViewport(const AActor* TargetActor) const;
	
	float GetDistanceFromCharacter(const AActor* OtherActor) const;
	TArray<AActor*> FindTargetsInRange(TArray<AActor*> ActorsToLook, const float RangeMin, const float RangeMax) const;
	float GetAngleUsingCameraRotation(const AActor* ActorToLook) const;
	static FRotator FindLookAtRotation(const FVector Start, const FVector Target);
	void ResetIsSwitchingTarget();

	bool TargetIsTargetable(const AActor* Actor) const;
	void UpdateCameraPitchToLookAtTarget() const;

protected:

	virtual void BeginPlay() override;

	virtual void TickComponent(float const DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	TArray<AActor*> GetPossibleTargetActors() const;

};
