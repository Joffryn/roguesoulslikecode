
#pragma once

#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "StuckingProjectileComponent.generated.h"

class AProjectile;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStuck);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class DUNGEONCRAWLER_API UStuckingProjectileComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	

	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FOnStuck OnStuck;
	
	UPROPERTY(EditAnywhere, Category = Config)
	USoundBase* CharacterStuckSound;

	UPROPERTY(EditAnywhere, Category = Config)
	USoundBase* EnvironmentStuckSound;
	
	UPROPERTY(EditAnywhere, Category = Config)
	USoundBase* DeflectSound;

	UPROPERTY(EditAnywhere, Category = Config)
	FGameplayTag UnstuckableForProjectiles;

protected:

	virtual void BeginPlay() override;
	
private:

	UPROPERTY(Transient)
	AProjectile* ProjectileOwner;

	UFUNCTION()
	void OnOwnerProjectileHit(FHitResult HitResult, float Damage);

	UFUNCTION()
	void OnHitActorDestroyed(AActor* DestroyedActor);

	ECollisionEnabled::Type CollisionEnabledCache;
	
};
