
#pragma once

#include "Components/BoxComponent.h"
#include "CoreMinimal.h"
#include "WeaponCollision.generated.h"

UCLASS(meta = (BlueprintSpawnableComponent))
class DUNGEONCRAWLER_API UWeaponCollision : public UBoxComponent
{
	GENERATED_BODY()
	
public:

	UWeaponCollision();

};
