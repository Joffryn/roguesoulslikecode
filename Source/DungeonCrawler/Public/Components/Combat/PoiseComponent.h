
#pragma once

#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "Enums.h"
#include "PoiseComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UPoiseComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UPoiseComponent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EPoiseLevel PoiseLevel = EPoiseLevel::Light;
	
	UFUNCTION(BlueprintCallable, Category = Config)
	void SetPoise(EPoiseLevel NewPoiseLevel);

	UFUNCTION(BlueprintCallable)
	void ChangePoiseLevel(int32 Amount);
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	FGameplayTagContainer StaggerTags;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	FGameplayTagContainer PoiseTags;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TMap<EPoiseLevel, FGameplayTag> PoiseLevelToTagsMap;

	virtual void BeginPlay() override;
	
	int32 ActualPoiseLevel = 0;

protected:
	
	void HandlePoiseChange();

};
