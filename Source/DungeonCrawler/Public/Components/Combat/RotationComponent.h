
#pragma once

#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "RotationComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnLastMovementInputRotationChangeRequested, FRotator, Rotation);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent, ScriptName="URotationComponent"))
class DUNGEONCRAWLER_API URotationComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	URotationComponent();
	
	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FOnLastMovementInputRotationChangeRequested OnLastMovementInputRotationChangeRequested;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	bool bWithDebug = false;
	
	UPROPERTY(Transient, BlueprintReadOnly, Category = Rotation)
	AActor* Target;

	UPROPERTY(Transient, BlueprintReadWrite, Category = Rotation)
	FRotator ForcedRotation;
	
	UPROPERTY(Transient, BlueprintReadWrite, Category = Rotation)
	bool bHasForcedRotation;
	
	UPROPERTY(Transient, BlueprintReadWrite, Category = Rotation)
	FRotator RequestedRotation;

	UPROPERTY(Transient, BlueprintReadWrite, Category = Rotation)
	FRotator CharacterRotation;

	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = Rotation)
	void RequestRotateInPlace(float YawChange, float Time);

	UFUNCTION(BlueprintCallable, Category = Rotation)
	void BreakRotateInPlace();
	
	UFUNCTION(BlueprintCallable, Category = Rotation)
	void SetCharacterRotation(FRotator NewRequestedRotation, bool bInterp = false, float InterpSpeed = 0.0f);

	UFUNCTION(BlueprintCallable)
	void SetTarget(AActor* NewTarget);
	
	UFUNCTION(BlueprintPure, Category = Rotation)
	bool CanTurn() const;
	
	UFUNCTION(BlueprintCallable, Category = Rotation)
	void Reset();

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	bool bRotationEnabled = true;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config, meta = (EditCondition = "bRotationEnabled"))
	FGameplayTagContainer BlockingGameplayTags;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config, meta = (EditCondition = "bRotationEnabled"))
	float AIRotationSpeed = 180.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config, meta = (EditCondition = "bRotationEnabled"))
	bool bOnlyRotatesDuringActions = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config, meta = (EditCondition = "bOnlyRotatesDuringActions && bRotationEnabled"))
	FGameplayTagContainer ActionsTags;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config, meta = (EditCondition = "bRotationEnabled"))
    UCurveFloat* RotateInPlaceCurve;

	
	UPROPERTY(BlueprintReadWrite, Category = State)
	float RotationSpeedMultiplier = 1.0f;
	
    	
	UPROPERTY(Transient, BlueprintReadOnly, Category = TurnInPlace)
	bool bRotateInPlaceRequested;

	UPROPERTY(Transient, BlueprintReadOnly, Category = TurnInPlace)
	float RotateInPlaceStartYaw;
	
	UPROPERTY(Transient, BlueprintReadOnly, Category = TurnInPlace)
	float RotateInPlaceYaw;
	
	UPROPERTY(Transient, BlueprintReadOnly, Category = TurnInPlace)
	float RotateInPlaceTime;

	UPROPERTY(Transient, BlueprintReadOnly, Category = TurnInPlace)
	float RotateInPlaceTimeElapsed;
	
	UPROPERTY(Transient, BlueprintReadOnly, Category = TurnInPlace)
	float StartingYaw;

	
	bool bWantsToStrafe; 

	void RotateTowardTarget(float DeltaTime);

	void RotateToInPlaceRequestedYaw(float DeltaTime);
	
	FRotator GetLookAtRotation() const;
	
};
