
#pragma once

#include "Components/ActorComponent.h"
#include "Effects/Enchantment.h"
#include "CoreMinimal.h"
#include "EnchantComponent.generated.h"

class UGameplayEffect;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UEnchantComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	int32 NumberOfRandomPowerUps = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	int32 NumberOfRandomElements = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	int32 NumberOfRandomImmunities = 0;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	TArray<TSubclassOf<UEnchantment>> ForcedEnchantments;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Config)
	TArray<TSubclassOf<UEnchantment>> BlockedEnchantments;
	
	UPROPERTY(BlueprintReadOnly)
	TArray<TSubclassOf<UEnchantment>> AppliedEnchants;

	UFUNCTION(BlueprintCallable)
	void ApplyEnchantments();
	
	UFUNCTION(BlueprintPure)
	bool CanApplyEnchantments() const;
	
private:
	
	TArray<TSubclassOf<UEnchantment>> PossiblePowerUps;
	TArray<TSubclassOf<UEnchantment>> PossibleElements;
	TArray<TSubclassOf<UEnchantment>> PossibleImmunities;

	void CollectPossibleEnchantments();
	
	void AddRandomPowerUp();

	void AddRandomElement();
	
	void AddRandomImmunity();
	
	void AddForcedEnchantments();

	void AddEnchantment(TSubclassOf<UEnchantment> EnchantmentClass);
	
};
