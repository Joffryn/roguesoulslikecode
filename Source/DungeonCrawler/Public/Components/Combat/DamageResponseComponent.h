
#pragma once

#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "GameplayEffect.h"
#include "MyDamageType.h"
#include "CoreMinimal.h"
#include "DamageResponseComponent.generated.h"

class ABaseCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnKill, AActor*, Target);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnDamage, TSubclassOf<UDamageType>, DamageType, AActor*, DamageDealer, float, Amount);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnDamageWithTarget, AActor*, Actor, TSubclassOf<UDamageType>, DamageType, float, Amount);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FResponseNoArgument);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UDamageResponseComponent : public UActorComponent
{
	GENERATED_BODY()

public:	

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag UnblockableParryUpgradeTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag BackstabUpgradeTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
    FGameplayTag StaminaFreeBlockTag;
    	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Cues")
	FGameplayTag PointHitGameplayCueTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Cues")
	FVector2D PointHitScaleRange = FVector2D(0.75f, 3.f);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Cues")
	FVector2D PointHitDamageRange = FVector2D(1, 50.f);
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Cues")
	FGameplayTag CritGameplayCueTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Cues")
	FGameplayTag BlockGameplayCueTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Cues")
	FGameplayTag ParryGameplayCueTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Cues")
	FGameplayTag InvincibleHitGameplayCueTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag InvincibilityTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag DamageIgnoreInvincibilityTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag BlockingTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag ParryingTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag UnblockableTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag ParriedReactionTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag BlockedReactionTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag DamageHitTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag DeathReactionTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag MeleeDamageTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag PhysicalDamageTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag MagicalDamageTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag RangedDamageTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag CanBlockProjectilesTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag ManaBurnTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag StaminaDrainTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag HealthyTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag HealthyMeleeAttackExtraDamageTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag HealthyDamageReductionTag;

	
	UPROPERTY(BlueprintReadOnly, Category = "State")
	TSubclassOf<UDamageType> LastDamageTypeTaken;

	
	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FOnDamage OnTakenDamage;
	
	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FOnDamage OnDealedDamage;

	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FOnDamage OnCritDamageDealed;

	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FOnDamageWithTarget OnParry;
	
	
	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FResponseNoArgument OnHit;
	
	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FResponseNoArgument OnBlock;
	
	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FOnKill OnKill;

	
	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FResponseNoArgument OnBeingHit;

	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FResponseNoArgument OnBeingCrit;
	
	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FResponseNoArgument OnBeingBlocked;
	
	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FResponseNoArgument OnBeingParried;
	
	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FResponseNoArgument OnInvincibleHit;
	
	UPROPERTY(Transient, BlueprintAssignable, BlueprintCallable, Category = Delegates)
	FResponseNoArgument OnDeath;
	
	
	//Angle used to calculate if attack will be blocked/parried
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	float PreventingAngle = 90.0f;

	//Min angle used to calculate if attack should be backstab
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	float MinBackstabAngle = 135.0f;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attributes")
	TSubclassOf<UGameplayEffect> LoseHealthEffect;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attributes")
	TSubclassOf<UGameplayEffect> LoseManaEffect;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attributes")
	TSubclassOf<UGameplayEffect> LoseStaminaEffect;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attributes")
	TSubclassOf<UGameplayEffect> LosePoiseEffect;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attributes")
	TSubclassOf<UGameplayEffect> BlockPoiseRegenEffect;

	
	UFUNCTION(BlueprintPure, Category = Getters)
	bool CanTakeDamage(const UDamageType* DamageType) const;

	UFUNCTION(BlueprintPure, Category = Getters)
	float GetYawAngleBetweenOwnerAndLocation(const FVector& Location) const;
	
	UFUNCTION(BlueprintPure, Category = Getters)
	bool ShouldParry(TSubclassOf<UMyDamageType> DamageType, const FVector& AttackLocation, const AActor* DamageCauser) const;
	
	UFUNCTION(BlueprintPure, Category = Getters)
	bool ShouldBlock(TSubclassOf<UMyDamageType> DamageType, const FVector& AttackLocation, const AActor* DamageCauser) const;

	UFUNCTION(BlueprintPure, Category = Getters)
	bool ShouldBackstab(TSubclassOf<UMyDamageType> DamageType, const FVector& AttackLocation, const AActor* DamageCauser) const;
	
	UFUNCTION(BlueprintCallable)
	void OnOwnerAnyDamageTaken(AActor* Actor, const float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);
	
	UFUNCTION(BlueprintCallable)
	float OnOwnerPointDamageTaken(float Damage, const FVector HitLocation, FVector HitFromDirection, TSubclassOf<UDamageType> DamageTypeClass, AActor* DamageCauser);
	
	UFUNCTION(BlueprintCallable)
	float OnOwnerRadialDamageTaken(float Damage, const FVector Origin, TSubclassOf<UDamageType> DamageTypeClass, AActor* DamageCauser);

	void InvincibleHit(TSubclassOf<UMyDamageType> DamageType, AActor* DamageCauser, const FVector& HitLocation) const;
	
	void Parry(TSubclassOf<UMyDamageType> DamageType, AActor* DamageCauser, const FVector& HitLocation, const float Damage) const;

	UFUNCTION(BlueprintCallable)
	void Block(float Damage, TSubclassOf<UMyDamageType> DamageType, AActor* DamageCauser, FVector HitLocation) const;
	
protected:

	virtual void BeginPlay() override;

	UPROPERTY(Transient)
	ABaseCharacter* BaseCharacterOwner;

	UPROPERTY(Transient)
	UAbilitySystemComponent* OwnerAbilitySystemComponent;

	//ToDO: think of cleaner solution
	bool bHasBroadcastedOwnerDeath = false;

	void ShowDamageInfo(float Damage, const UDamageType* DamageType, bool bIsCrit) const;

	FVector LastPointDamageHitLocation;

	FGameplayCueParameters GetGameplayCueParameters(AActor* DamageCauser, const FVector& HitLocation) const;
	
};
