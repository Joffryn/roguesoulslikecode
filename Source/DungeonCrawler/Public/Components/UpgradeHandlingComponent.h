
#pragma once

#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "Enums.h"
#include "UpgradeHandlingComponent.generated.h"

class UAbilitySystemComponent;
class ABaseCharacter;
class UGameplayEffect;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UUpgradeHandlingComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	//Consider using an attribute
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config")
	float ManaPerMeleeDamageGainMultiplier = 0.1f;
	
	//Consider using an attribute
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config")
	float DamageTakenManaGainMultiplier = 0.2f;

	//Consider using an attribute
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config")
	float StaminaCritAmount = 10.0f;

	//Consider using an attribute
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config")
	float HealthGainPerCrit = 1.0f;
	
	//Consider using an attribute
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config")
	float HealthGainPerKill = 2.0f;

	//Consider using an attribute
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config")
	float ManaRestorePerKill = 5.0f;
	
	//Consider using an attribute
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config")
	float StaminaRestorePerKill = 25.0f;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag HealOnKillTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag ManaRestoreOnKillTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag DamageReflectionTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag StaminaRestoreOnKillTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag DamageTakeManaGainTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag CritStaminaGainTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag CritHealTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag CritManaTag;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag DamageMeleeTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag ManaGenerationBlockedTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag ParryStaminaRestorationTag;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Tags")
	FGameplayTag ParryDamageDealTag;
	
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attributes")
	TSubclassOf<UGameplayEffect> RestoreHealthEffect;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attributes")
	TSubclassOf<UGameplayEffect> RestoreStaminaEffect;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attributes")
	TSubclassOf<UGameplayEffect> RestoreManaEffect;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Damage")
	TSubclassOf<UDamageType> DeflectedDamageTypeClass;

	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attributes")
	TSubclassOf<UGameplayEffect> RestoreHealthWithCueEffect;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attributes")
	TSubclassOf<UGameplayEffect> RestoreStaminaWithCueEffect;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Config | Attributes")
	TSubclassOf<UGameplayEffect> RestoreManaWithCueEffect;
	
protected:

	virtual void BeginPlay() override;

	UPROPERTY(Transient)
	ABaseCharacter* BaseCharacterOwner;

	UPROPERTY(Transient)
	UAbilitySystemComponent* OwnerAbilitySystemComponent;

	void BindDelegates();

	UFUNCTION()
	void OnStatChanged( EStat Stat, float Amount);

	UFUNCTION()
	void OnAttackCrit(TSubclassOf<UDamageType> DamageType, AActor* DamageCauser, float Amount);

	UFUNCTION()
	void OnAttackParried(AActor* Actor, TSubclassOf<UDamageType> DamageType, float Amount);
	
	UFUNCTION()
	void OnAttackDamageDealed(TSubclassOf<UDamageType> DamageType, AActor* DamageCauser, float Amount);

	UFUNCTION()
	void OnAttackDamageTaken(TSubclassOf<UDamageType> DamageType, AActor* DamageCauser, float Amount);
	
	UFUNCTION()
	void OnKill(AActor* Target);
	
};
