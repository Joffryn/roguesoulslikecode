
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "AnimationComponent.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UAnimationComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:

	UPROPERTY(Transient, BlueprintReadWrite, Category = MotionSkills)
	float MotionSkillDirection;

	UPROPERTY(EditAnywhere, Category = Config)
	TArray<FName> DefaultHiddenBones;

	UPROPERTY(Transient, BlueprintReadOnly)
	TArray<FName> HiddenBones;

	UFUNCTION(BlueprintCallable)
	void HideBones(TArray<FName> BonesToHide);

protected:

	virtual void BeginPlay() override;

};
