

#pragma once

#include "Components/ActorComponent.h"
#include "Perception/AIPerceptionTypes.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "Enums.h"
#include "AsleepComponent.generated.h"

class UGameplayEffect;
class ABaseCharacter;
class UAnimMontage;

UCLASS( Blueprintable, Abstract, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UAsleepComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
		
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = Config)
	bool bUseAsleepBehavior = false;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Config, meta = (EditCondition = "bUseAsleepBehavior"))
	EAIWakeMethod WakeMethod;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = Config, meta = (EditCondition = "bUseAsleepBehavior"))
	FGameplayTag ExitSleepModeTag;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = Config, meta = (EditCondition = "bUseAsleepBehavior"))
	FGameplayTag EnterSleepModeTag;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = Config, meta = (EditCondition = "bUseAsleepBehavior"))
	UAnimMontage* EnterMontage;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = Config, meta = (EditCondition = "bUseAsleepBehavior"))
	UAnimMontage* ExitMontage;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = Config, meta = (EditCondition = "bUseAsleepBehavior"))
	TSubclassOf<UGameplayEffect> AddAsleepAbility;

	UFUNCTION(BlueprintCallable)
	void Wake() const;

protected:

	virtual void BeginPlay() override;

	UFUNCTION()
	void OnOwnerPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus);

	UFUNCTION()
	void OnStatChanged(EStat Stat, float Amount);

	UPROPERTY()
	ABaseCharacter* BaseCharacterOwner;

};
