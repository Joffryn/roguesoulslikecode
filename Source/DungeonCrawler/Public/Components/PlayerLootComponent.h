
#pragma once

#include "Components/ActorComponent.h"
#include "GameplayTagContainer.h"
#include "CoreMinimal.h"
#include "PlayerLootComponent.generated.h"

class UGameplayEffect;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UPlayerLootComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UPlayerLootComponent* GetPlayerLootComponent(const UObject* WorldContextObject);
	
	UFUNCTION(BlueprintCallable)
	void AddGold(float Amount);

protected:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TSubclassOf< UGameplayEffect> AddGoldEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FGameplayTag SetByCallerGoldTag;
	
};
