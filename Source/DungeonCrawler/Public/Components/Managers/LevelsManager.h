
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "Enums.h"
#include "LevelsManager.generated.h"

class ANextRoomSign;
class UDataTable;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnRunSetupGenerated);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable)
class DUNGEONCRAWLER_API ULevelsManager : public UActorComponent
{
	GENERATED_BODY()

public:
	
	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static ULevelsManager* GetLevelsManager(const UObject* WorldContextObject);

	
	UPROPERTY(Transient, BlueprintCallable, BlueprintAssignable, Category = Delegates)
	FOnRunSetupGenerated OnRunSetupGenerated;

	
	UPROPERTY(BlueprintReadOnly, Category = State)
	TMap<FName, FRunRoomRandomizedSetup> LevelsToRoomSetupsMap;

	UPROPERTY(BlueprintReadOnly, Category = State)
	TMap<int32, FRunRoomArray> LevelDepthToNextRooms;
	
	UPROPERTY(BlueprintReadOnly, Category = State)
	FRunSetup RunSetup;

	
	UFUNCTION(BlueprintCallable, Category = Setup)
	void StartNewRun(EBiom InBiom);
	
	UFUNCTION(BlueprintPure, Category = Getters)
	EBiom GetCurrentBiom() const;

	UFUNCTION(BlueprintCallable, Category = Setters)
	void SetCurrentBiom(EBiom NewBiom);

	UFUNCTION(BlueprintCallable, Category = Setup)
	void SetupRun();
	
protected:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	UDataTable* BiomRunSetups;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	FRoomsNamesStorage RoomsNamesStorage;
	
	UPROPERTY(BlueprintReadOnly, Category = State)
	EBiom CurrentBiom;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TMap<EBiom, UDataTable*> BiomsToChambersConfig;
	
	UPROPERTY(BlueprintReadOnly, Category = State)
	TArray<ANextRoomSign*> NextRoomsSigns;

	UFUNCTION(BlueprintPure, Category = Getters)
	UDataTable* GetCurrentBiomDataTable();

	UFUNCTION(BlueprintCallable, Category = RNG)
	void GenerateRunBiomSetups();

	UFUNCTION(BlueprintCallable, Category = Debug)
	void PrintRunSetup();
	
	void GenerateEntranceRoom();
	void GenerateRoomSetupForSingleDepth(int32 Depth);
	void GeneratePreBossRooms();
	void GenerateBossRoom();
	
	void GenerateRoom(EChamberPositionType Type);
	
	virtual void BeginPlay() override;
	
};
