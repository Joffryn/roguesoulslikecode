
#pragma once

#include "Components/ActorComponent.h"
#include "PlayerLootComponent.h"
#include "CoreMinimal.h"
#include "Encounter.h"
#include "Structs.h"
#include "EncounterManager.generated.h"

class UMyAbilitySystemComponent;
class UPlayerLootComponent;
class AEncounter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEncounterManagerDelegate, AEncounter*, Encounter);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class DUNGEONCRAWLER_API UEncounterManager : public UActorComponent
{
	GENERATED_BODY()

public:	

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UEncounterManager* GetEncounterManager(const UObject* WorldContextObject);
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	TArray<TSubclassOf<UGameplayEffect>> EffectsToApplyDuringEncounter;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	TArray<TSubclassOf<UGameplayEffect>> EffectsToApplyOutsideEncounter;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	TArray<FAfterEncounterEffectSetup> EffectSetupsToApplyAfterEncounter;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	FGameplayTag DuringEncounterTag;
	
	void RegisterEncounter(AEncounter* Encounter);

	virtual void BeginPlay() override;

	bool IsDuringEncounter() const;
	void ApplyOutsideEncounterEffects();
	void ApplyDuringEncounterEffects();

	void RemoveOutsideEncounterEffects();
	void RemoveDuringEncounterEffects();

	void ApplyAfterEncounterEffects();
	
	UFUNCTION(BlueprintCallable)
	void FinishCurrentEncounters();

	UFUNCTION()
	void OnEncounterCharacterDeath(ABaseCharacter* Character);
	
	UFUNCTION()
	void OnEncounterStarted();

	UFUNCTION()
	void OnEncounterFinished();
	
	UPROPERTY(BlueprintReadOnly)
	TArray<AEncounter*> RegisteredEncounters;
	
protected:
	
	UPROPERTY(Transient)
	UMyAbilitySystemComponent* PlayerAbilitySystemComponent;

	UPROPERTY(Transient)
	UPlayerLootComponent* PlayerLootComponent;
	
};
