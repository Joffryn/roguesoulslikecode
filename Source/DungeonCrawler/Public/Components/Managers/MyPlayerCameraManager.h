
#pragma once

#include "Camera/PlayerCameraManager.h"
#include "CoreMinimal.h"
#include "MyPlayerCameraManager.generated.h"

class APawn;

UCLASS(Abstract)
class DUNGEONCRAWLER_API AMyPlayerCameraManager : public APlayerCameraManager
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static AMyPlayerCameraManager* GetMyPlayerCameraManager(const UObject* WorldContextObject);

	UPROPERTY(EditAnywhere, Category = Config)
	FPostProcessSettings PostProcessSettingsOverrideWhenPaused;
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintPure)
	USkeletalMeshComponent* GetCameraComponent();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void OnPossess(APawn* NewPawn);

	virtual void ApplyCameraModifiers(float DeltaTime, FMinimalViewInfo& InOutPOV) override;

protected:

	FPostProcessSettings PostProcessSettingsCache;
	
};
