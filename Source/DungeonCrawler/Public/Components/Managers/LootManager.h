
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "LootManager.generated.h"

class AOverlapPickup;
class APickup;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API ULootManager : public UActorComponent
{
	GENERATED_BODY()

public:	
	
	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static ULootManager* GetLootManager(const UObject* WorldContextObject);

	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TSubclassOf<AOverlapPickup> GoldDropActorClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	float MaxGoldPerPickup = 100.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Config)
	FVector2D DropImpulseStrengthRange = FVector2D(150.f, 300.f);
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TArray<FPickupSetup> PickupDropSetups;
	
};
