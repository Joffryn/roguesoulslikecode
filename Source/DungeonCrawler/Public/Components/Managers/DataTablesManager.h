
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "DataTablesManager.generated.h"

class UDataTable;

//Manager for keeping references to Data Tables
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UDataTablesManager : public UActorComponent
{
	GENERATED_BODY()

public:	

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UDataTablesManager* GetDataTablesManager(const UObject* WorldContextObject);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	UDataTable* PlayerHeroSetups;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	UDataTable* Enchants;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	UDataTable* Perks;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Config)
	UDataTable* Skills;
	
};
