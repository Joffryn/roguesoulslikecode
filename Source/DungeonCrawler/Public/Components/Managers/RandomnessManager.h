
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "RandomnessManager.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API URandomnessManager : public UActorComponent
{
	GENERATED_BODY()

public:
	
	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static URandomnessManager* GetRandomnessManager(const UObject* WorldContextObject);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	bool bAlwaysUseRandomSeed;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	int32 Seed;
	
	UPROPERTY(BlueprintReadWrite, Transient)
	FRandomStream Stream;
	
	UFUNCTION(BlueprintPure, Category = "Getters")
	FRandomStream GetStream() const;
	
	UFUNCTION(BlueprintCallable)
	void GenerateRandomStream();
	
	UFUNCTION(BlueprintCallable)
	void GenerateStream(int32 InSeed = 0);
	
	virtual void BeginPlay() override;
	
};
