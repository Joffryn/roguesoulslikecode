
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "MusicManager.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UMusicManager : public UActorComponent
{
	GENERATED_BODY()

public:
	
	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UMusicManager* GetMusicManager(const UObject* WorldContextObject);
	
	
	UFUNCTION(BlueprintCallable)
	void PlayMusic(USoundBase* Music, float FadeInTime = 1.5f);

	UFUNCTION(BlueprintCallable)
	void StopMusic(USoundBase* Music = nullptr, float FadeOutTime = 1.5f);

	UFUNCTION(BlueprintCallable)
	void PauseMusic() const;

	UFUNCTION(BlueprintCallable)
	void UnpauseMusic() const;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	TArray<USoundBase*> DefaultCombatMusic;
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	float DefaultMusicFadeTime = 0.5f;

	UPROPERTY(BlueprintReadOnly, Category = Components)
	UAudioComponent* AudioComponent;

	UPROPERTY(BlueprintReadOnly, Transient)
	USoundBase* CurrentlyPlayedMusic;

	UPROPERTY(Transient)
	TArray<FMusic> QueuedMusics;

protected:
	
	virtual void BeginPlay() override;
	
	void FadeInMusic(float FadeInTime = 1.5f) const;
	void FadeOutMusic(float FadeOutTime = 1.5f);

	void HandleMusicQueue();
	void RemoveFromQueue(const USoundBase* Music);
	
	void CreateMusicAudioComponent();
	
	bool bUsePrimary = true;


	
};
