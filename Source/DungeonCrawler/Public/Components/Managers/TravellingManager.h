
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "Structs.h"
#include "TravellingManager.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UTravellingManager : public UActorComponent
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UTravellingManager* GetTravellingManager(const UObject* WorldContextObject);
	
	UPROPERTY(EditDefaultsOnly)
	FCameraFadeSetup StartTravelFadeSetup;

	UPROPERTY(EditDefaultsOnly)
	FCameraFadeSetup EndTravelFadeSetup;
	
	UFUNCTION(BlueprintCallable)
	void TravelToLevel(FName LevelName);
	
protected:
	
	virtual void BeginPlay() override;
	
	UFUNCTION()
	void Travel();

	UFUNCTION()
	void BeginPlayFadeFinished();

	UFUNCTION()
	void OnSaveFinished();
	
	void HandleLevelLoaded();

	bool bHasPendingSave;
	bool bHaseFadedOut;
	FName RequestedLevelName;
	FTimerHandle Handle;

};
