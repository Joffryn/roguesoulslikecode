
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "UIManager.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UUIManager : public UActorComponent
{
	GENERATED_BODY()

public:	

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UUIManager* GetAIManager(const UObject* WorldContextObject);
	
	
};
