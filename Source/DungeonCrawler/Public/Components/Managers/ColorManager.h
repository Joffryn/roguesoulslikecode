
#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "Enums.h"
#include "ColorManager.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class DUNGEONCRAWLER_API UColorManager : public UActorComponent
{
	GENERATED_BODY()

public:	

	//Intentionally not using meta = (WorldContext = "WorldContextObject") because ColorManager is used in the places that are not in the world
	//UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	UFUNCTION(BlueprintPure, Category = "Getters")
	static UColorManager* GetColorManager(const UObject* WorldContextObject);

	UFUNCTION(BlueprintPure)
	FLinearColor GetColor(EColor Color);

protected:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	TMap<EColor, FLinearColor> ColorsMap;
	
};
