
#pragma once

#include "CoreMinimal.h"
#include "Enums.h"
#include "HitInfo.generated.h"

UCLASS(BlueprintType)
class DUNGEONCRAWLER_API UHitInfo : public UObject
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadOnly)
	EDirection HitDirection;

};
