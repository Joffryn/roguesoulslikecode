#pragma once

#ifdef IMGUI_API
#define WITH_IMGUI !UE_BUILD_SHIPPING
#else
#define WITH_IMGUI 0
#endif 

#if WITH_IMGUI
#include <imgui.h>
#endif
