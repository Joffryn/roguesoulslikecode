
#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "HudWidget.generated.h"

UCLASS(Abstract)
class DUNGEONCRAWLER_API UHudWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static UHudWidget* GetHudWidget(const UObject* WorldContextObject);

	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void Init();
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void HideHud();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ShowHud();

	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Infos)
	void ShowMessage(const FText& Message);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Infos)
	void ShowErrorMessage(const FText& Message);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = Infos)
	void ShowLocationMessage(const FText& Message);
	
};
