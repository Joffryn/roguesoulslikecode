
#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "SavingWidget.generated.h"

UCLASS(Abstract)
class DUNGEONCRAWLER_API USavingWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static USavingWidget* GetSavingWidget(const UObject* WorldContextObject);
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void Show();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void Hide();
	
};
