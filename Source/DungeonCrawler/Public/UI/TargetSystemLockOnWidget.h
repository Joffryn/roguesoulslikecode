
#pragma once

#include "Blueprint/UserWidget.h"
#include "CoreMinimal.h"
#include "TargetSystemLockOnWidget.generated.h"

UCLASS()
class DUNGEONCRAWLER_API UTargetSystemLockOnWidget : public UUserWidget
{
	GENERATED_BODY()

};
