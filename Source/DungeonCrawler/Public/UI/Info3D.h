
#pragma once

#include "GameFramework/Actor.h"
#include "CoreMinimal.h"
#include "Info3D.generated.h"

UCLASS(Abstract)
class DUNGEONCRAWLER_API AInfo3D : public AActor
{
	GENERATED_BODY()
	
public:
	
	UFUNCTION(BlueprintImplementableEvent, Category = Text)
	void ShowInfo(const FText& Text, FLinearColor Color = FLinearColor::White, bool bIsCrit = false);
	
};
