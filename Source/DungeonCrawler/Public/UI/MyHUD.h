
#pragma once

#include "GameFramework/HUD.h"
#include "CoreMinimal.h"
#include "MyHUD.generated.h"

class USavingWidget;
class UVignettes;
class UHudWidget;
class AInfo3D;

UCLASS()
class DUNGEONCRAWLER_API AMyHUD : public AHUD
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintPure, Category = "Getters", meta = (WorldContext = "WorldContextObject"))
	static AMyHUD* GetMyHud(const UObject* WorldContextObject);

	UPROPERTY(Transient, BlueprintReadWrite, Category = Widgets)
	USavingWidget* SavingWidget;
	
	UPROPERTY(Transient, BlueprintReadWrite, Category = Widgets)
	UHudWidget* HudWidget;
	
	UFUNCTION(BlueprintCallable)
	void ShowDamageInfo(const FVector Location, const FText& Text, const FLinearColor Color = FLinearColor::White, bool bIsCritical = false) const;

	UFUNCTION(BlueprintCallable)
	void ShowGoldCollectedInfo(const FVector Location, float Amount) const;
	
	UFUNCTION(BlueprintCallable)
	void ShowInfo(const FVector Location, const FText& Text, const FLinearColor Color = FLinearColor::White) const;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = Widgets)
	void AddBossHealthBar(class ABaseCharacter* BossCharacter);
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void SetupNewHud(APawn* Pawn);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void HideHudWidget();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ShowHudWidget();
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void ShowDeathMessage();
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    void ShowVictoryMessage();
	
private:
	
	UPROPERTY(EditDefaultsOnly, Category = Config)
	TSubclassOf<AInfo3D> Info3DClass;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	TSubclassOf<AInfo3D> DamageInfoClass;

};
