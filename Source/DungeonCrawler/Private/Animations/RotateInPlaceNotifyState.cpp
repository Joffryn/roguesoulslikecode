
#include "Animations/RotateInPlaceNotifyState.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "RotationComponent.h"
#include "AIController.h"

void URotateInPlaceNotifyState::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const float TotalDuration, const FAnimNotifyEventReference& EventReference)
{
	Super::NotifyBegin(MeshComp, Animation, TotalDuration, EventReference);
	if (MeshComp->GetOwner())
	{
		float YawAngle = 0;
		if(const auto PawnOwner = Cast<APawn>(MeshComp->GetOwner()))
			if(const auto AIController = Cast<AAIController>(PawnOwner->GetController()))
				if(const auto Blackboard = AIController->GetBlackboardComponent())
					YawAngle = Blackboard->GetValueAsFloat(TEXT("AngleToTarget"));
						
		RotationComponent = MeshComp->GetOwner()->FindComponentByClass<URotationComponent>();
		if (RotationComponent)
			RotationComponent->RequestRotateInPlace(YawAngle, TotalDuration);
	}
}

void URotateInPlaceNotifyState::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
	Super::NotifyEnd(MeshComp, Animation, EventReference);
	if (RotationComponent)
		RotationComponent->BreakRotateInPlace();
}
