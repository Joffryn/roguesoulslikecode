
#include "Animations/RagdollBlendNotifyState.h"

#include "Components/MyPhysicalAnimationComponent.h"

void URagdollBlendNotifyState::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const float TotalDuration, const FAnimNotifyEventReference& EventReference)
{
	Super::NotifyBegin(MeshComp, Animation, TotalDuration, EventReference);
	Duration = TotalDuration;
	if (MeshComp->GetOwner())
	{
		PhysicalAnimations = MeshComp->GetOwner()->FindComponentByClass<UMyPhysicalAnimationComponent>();
		if (PhysicalAnimations)
			PhysicalAnimations->RagdollBlendStart();
	}
}


void URagdollBlendNotifyState::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const float FrameDeltaTime, const FAnimNotifyEventReference& EventReference)
{
	Super::NotifyTick(MeshComp, Animation, FrameDeltaTime, EventReference);
	if (PhysicalAnimations)
	{
		PhysicalAnimations->RagdollBlend += FrameDeltaTime / Duration;
		PhysicalAnimations->UpdateRagdollBlend();
	}
}

void URagdollBlendNotifyState::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, const FAnimNotifyEventReference& EventReference)
{
	Super::NotifyEnd(MeshComp, Animation, EventReference);

	if (PhysicalAnimations)
		PhysicalAnimations->RagdollBlendEnd();
}

