
#include "UI/SavingWidget.h"

#include "MyHUD.h"

USavingWidget* USavingWidget::GetSavingWidget(const UObject* WorldContextObject)
{
	if(const auto MyHud = AMyHUD::GetMyHud(WorldContextObject))
		return MyHud->SavingWidget;

	return nullptr;
}