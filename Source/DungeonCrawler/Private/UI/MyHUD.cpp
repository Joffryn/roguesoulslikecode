
#include "MyHUD.h"

#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "ColorManager.h"
#include "Info3D.h"
#include "Enums.h"

AMyHUD* AMyHUD::GetMyHud(const UObject* WorldContextObject)
{
	if(const auto PlayerController = UGameplayStatics::GetPlayerController(WorldContextObject, 0))
		return Cast<AMyHUD>(PlayerController->GetHUD());

	return nullptr;
}

void AMyHUD::ShowDamageInfo(const FVector Location, const FText& Text, const FLinearColor Color, const bool bIsCritical) const
{
	if(this)
		if(const auto Info3D = GetWorld()->SpawnActor<AInfo3D>(DamageInfoClass, Location, FRotator()))
			Info3D->ShowInfo(Text, Color, bIsCritical);
}

void AMyHUD::ShowGoldCollectedInfo(const FVector Location, const float Amount) const
{
	if(this)
		if(const auto Info3D = GetWorld()->SpawnActor<AInfo3D>(Info3DClass, Location, FRotator()))
			if(const auto ColorManager = UColorManager::GetColorManager(this))
				Info3D->ShowInfo(FText::FromString(FString::FromInt(Amount)), ColorManager->GetColor(EColor::Gold));
}

void AMyHUD::ShowInfo(const FVector Location, const FText& Text, const FLinearColor Color /*= FLinearColor::White*/) const
{
	if(this)
		if(const auto Info3D = GetWorld()->SpawnActor<AInfo3D>(Info3DClass, Location, FRotator()))
			Info3D->ShowInfo(Text, Color);
}

