
#include "UI/ChoiceWidget.h"

#include "MyBlueprintFunctionLibrary.h"
#include "MyAbilitySystemComponent.h"
#include "Upgrade.h"

void UChoiceWidget::SetupPossibleUpgrades()
{
	for(const auto DataTable : PossibleUpgradesDataTables)
	{
		const auto PlayerAbilitySystem = UMyBlueprintFunctionLibrary::GetPlayerAbilitySystemComponent(this);
		TArray<FUpgradeChoice*> Rows;
		const TCHAR* ContextString = nullptr;
		DataTable->GetAllRows<FUpgradeChoice>(ContextString, Rows);
		for(const auto Row : Rows)
		{
			if(!Row->bCanBeLearnedFromScroll)
				continue;

			//Check if required tags are applied
			if(!PlayerAbilitySystem->HasAllMatchingGameplayTags(Row->RequiredTags))
				continue;

			//Check if any of the blocking tags is present
			if(PlayerAbilitySystem->HasAnyMatchingGameplayTags(Row->BlockedTags))
				continue;
			
			//Don't allow to add the same upgrade twice
			if(!Row->bCanBeStacked)
				if(PlayerAbilitySystem->GetGameplayEffectCount(Row->EffectToApply, nullptr, false))
					continue;
					
			PossibleUpgrades.Add(*Row);
		}
	}
}
