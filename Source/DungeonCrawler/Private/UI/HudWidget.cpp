
#include "HudWidget.h"

#include "MyHUD.h"

UHudWidget* UHudWidget::GetHudWidget(const UObject* WorldContextObject)
{
	if(const auto MyHud = AMyHUD::GetMyHud(WorldContextObject))
		return MyHud->HudWidget;

	return nullptr;
}
