
#include "AI/MyBTDecorator_HasTag.h"

#include "GameplayTagAssetInterface.h"
#include "AIController.h"

UMyBTDecorator_HasTag::UMyBTDecorator_HasTag()
{
	bNotifyTick = true;
}

void UMyBTDecorator_HasTag::TestConditionalFlowAbort(UBehaviorTreeComponent* OwnerCompCached) const
{
	if (!OwnerCompCached)
		return;

	const auto ActiveNode = OwnerCompCached->GetActiveNode();
	const bool bHasHigherPriority = !ActiveNode || ActiveNode->GetExecutionIndex() > GetExecutionIndex();

	const bool bIsOnActiveBranch = OwnerCompCached->IsExecutingBranch(GetMyNode(), GetChildIndex());

	const bool bShouldPerformTestLowerPriority = bHasHigherPriority && (FlowAbortMode == EBTFlowAbortMode::LowerPriority || FlowAbortMode == EBTFlowAbortMode::Both);
	const bool bShouldPerformTestSelf = bIsOnActiveBranch && (FlowAbortMode == EBTFlowAbortMode::Self || FlowAbortMode == EBTFlowAbortMode::Both);

	if (bShouldPerformTestLowerPriority || bShouldPerformTestSelf)
		ConditionalFlowAbort(*OwnerCompCached, EBTDecoratorAbortRequest::ConditionResultChanged);
}

void UMyBTDecorator_HasTag::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	TestConditionalFlowAbort(&OwnerComp);
}

bool UMyBTDecorator_HasTag::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	const auto Owner = OwnerComp.GetAIOwner()->K2_GetPawn();
	const IGameplayTagAssetInterface* GameplayTagAssetInterface = Cast< IGameplayTagAssetInterface >(Owner);
    if (GameplayTagAssetInterface != nullptr)
		return GameplayTagAssetInterface->HasAnyMatchingGameplayTags(MonitoredTags);
	
	return false;
}

FString UMyBTDecorator_HasTag::GetStaticDescription() const
{
	FString Description(TEXT("Tags"));
	for (auto Tag : MonitoredTags)
	{
		Description.Append(FString(TEXT("\n - ")));
		Description.Append(Tag.ToString());
	}
	return Description;
}
