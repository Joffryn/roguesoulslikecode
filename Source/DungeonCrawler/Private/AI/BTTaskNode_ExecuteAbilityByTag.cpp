
#include "AI/BTTaskNode_ExecuteAbilityByTag.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "MyAbilitySystemComponent.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"
#include "AICombatComponent.h"
#include "AIController.h"
#include "AIManager.h"

UBTTaskNode_ExecuteAbilityByTag::UBTTaskNode_ExecuteAbilityByTag()
{
	bCreateNodeInstance = true;
	bCancelAbilityOnAbort = true;
}

EBTNodeResult::Type UBTTaskNode_ExecuteAbilityByTag::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const AAIController* AIController = Cast<AAIController>(OwnerComp.GetOwner());
	APawn* OwnerPawn = AIController->GetPawn();
	if (!OwnerPawn)
		return EBTNodeResult::Failed;

	BlackboardComponent = OwnerComp.GetBlackboardComponent();
	if (!BlackboardComponent)
		return EBTNodeResult::Failed;

	
	if (const IAbilitySystemInterface* AbilitySystemInterface = Cast<IAbilitySystemInterface>(OwnerPawn))
		MyAbilitySystemComponent = Cast<UMyAbilitySystemComponent>(AbilitySystemInterface->GetAbilitySystemComponent());
	else
		return EBTNodeResult::Failed;
	
	AICombatComponent = OwnerPawn->FindComponentByClass<UAICombatComponent>();
	if(AICombatComponent)
	{
		if(AICombatComponent->ActionsToTagsMap.Find(AIActionType))
			GameplayTag = *AICombatComponent->ActionsToTagsMap.Find(AIActionType);
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Combat Action not found"));
			return EBTNodeResult::Failed;
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("AICombatComponent not found"));
		return EBTNodeResult::Failed;
	}
	
	AActor* TargetCharacter = Cast<AActor>(BlackboardComponent->GetValueAsObject(FName(TEXT("Target"))));
	
	const FGameplayTagContainer TagContainer = FGameplayTagContainer(GameplayTag);
	const FGameplayEventData EventData = GetEventData(OwnerPawn, TargetCharacter);

	MyAbilitySystemComponent->AddGameplayTag(UAIManager::GetAIManager(this)->DuringActionTag.GetSingleTagContainer());
	
	OnAbilityFailedHandle = MyAbilitySystemComponent->AbilityFailedCallbacks.AddUObject(this, &UBTTaskNode_ExecuteAbilityByTag::OnAbilityFailed, &OwnerComp);
	OnAbilityEndedHandle = MyAbilitySystemComponent->AbilityEndedCallbacks.AddUObject(this, &UBTTaskNode_ExecuteAbilityByTag::OnAbilityEnded, &OwnerComp);
	if (MyAbilitySystemComponent->TryActivateAbilitiesByTagWithEventData(TagContainer, EventData))
		return EBTNodeResult::InProgress;
	
	return EBTNodeResult::Failed;
}

FString UBTTaskNode_ExecuteAbilityByTag::GetStaticDescription() const
{
	return FString::Printf(TEXT("Execute ability: %s"), LexToString(AIActionType));
}

EBTNodeResult::Type UBTTaskNode_ExecuteAbilityByTag::AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (bCancelAbilityOnAbort)
	{
		const FGameplayTagContainer TagContainer = FGameplayTagContainer(GameplayTag);
		MyAbilitySystemComponent->CancelAbilities(&TagContainer);
	}

	return Super::AbortTask(OwnerComp, NodeMemory);
}

void UBTTaskNode_ExecuteAbilityByTag::OnAbilityEnded(UGameplayAbility* GameplayAbility, UBehaviorTreeComponent* OwnerComp)
{
	if (GameplayAbility->AbilityTags.HasTag(GameplayTag))
	{
		if(AICombatComponent->ImportantActionsToTagsMap.Find(AIActionType))
			BlackboardComponent->SetValueAsFloat(FName(TEXT("TimeSinceLastImpactfulAction")), 0.0f);

		AICombatComponent->OnActionExecuted(AIActionType);
		MyAbilitySystemComponent->RemoveGameplayTag(UAIManager::GetAIManager(this)->DuringActionTag.GetSingleTagContainer());
		MyAbilitySystemComponent->AbilityFailedCallbacks.Remove(OnAbilityFailedHandle);
		MyAbilitySystemComponent->AbilityEndedCallbacks.Remove(OnAbilityEndedHandle);
		FinishLatentTask(*OwnerComp, EBTNodeResult::Succeeded);
	}
}

void UBTTaskNode_ExecuteAbilityByTag::OnAbilityFailed(const UGameplayAbility* GameplayAbility, const FGameplayTagContainer& GameplayTags, UBehaviorTreeComponent* OwnerComp)
{
	if (GameplayAbility->AbilityTags.HasTag(GameplayTag))
	{
		MyAbilitySystemComponent->AbilityFailedCallbacks.Remove(OnAbilityFailedHandle);
		MyAbilitySystemComponent->AbilityEndedCallbacks.Remove(OnAbilityEndedHandle);
		FinishLatentTask(*OwnerComp, EBTNodeResult::Failed);
	}
}

FGameplayEventData UBTTaskNode_ExecuteAbilityByTag::GetEventData(APawn* OwnerPawn, AActor* TargetCharacter)
{
	FGameplayEventData EventData;
	EventData.Instigator = OwnerPawn;
	EventData.Target = TargetCharacter;
	return EventData;
}

