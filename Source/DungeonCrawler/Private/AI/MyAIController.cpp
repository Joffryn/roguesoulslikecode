
#include "MyAIController.h"

#include "Navigation/CrowdFollowingComponent.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Kismet/GameplayStatics.h"
#include "BaseCharacter.h"
#include "AIManager.h"

AMyAIController::AMyAIController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UCrowdFollowingComponent>(TEXT("PathFollowingComponent")))
{
	BehaviorTreeComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorTreeComponent"));
	Perception = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception"));
	SetPerceptionComponent(*Perception);
}

void AMyAIController::SetNewTarget(ACharacter* Target)
{
	if(Target != UGameplayStatics::GetPlayerCharacter(this, 0))
		if(LastAwareTarget == UGameplayStatics::GetPlayerCharacter(this, 0))
			if(const auto AIManager = UAIManager::GetAIManager(this))
				AIManager->AIStopBeingAwareOfThePlayer(this);

	LastAwareTarget = Target;
	if(Target == UGameplayStatics::GetPlayerCharacter(this, 0))
		if(const auto AIManager = UAIManager::GetAIManager(this))
			AIManager->AIStartBeingAwareOfThePlayer(this);
}

void AMyAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	if(const auto AIManager = UAIManager::GetAIManager(this))
		AIManager->RegisterAI(this);

	if(const auto BaseCharacter = Cast<ABaseCharacter>(GetPawn()))
		BaseCharacter->OnDeath.AddDynamic(this, &AMyAIController::OnPossessedPawnDeath);
}

void AMyAIController::OnUnPossess()
{
	Super::OnUnPossess();
	if(const auto AIManager = UAIManager::GetAIManager(this))
		AIManager->UnregisterAI(this);
}

void AMyAIController::OnPossessedPawnDeath(ABaseCharacter* Actor)
{
	if(LastAwareTarget == UGameplayStatics::GetPlayerCharacter(this, 0))
		if(const auto AIManager = UAIManager::GetAIManager(this))
			AIManager->AIStopBeingAwareOfThePlayer(this);

	Destroy();
}

void AMyAIController::SetupBehaviorTrees()
{
	RunBehaviorTree(MainTree);
	if(CombatTree)
		BehaviorTreeComponent->SetDynamicSubtree(CombatInjectTag, CombatTree);

	if(ChaseTree)
		BehaviorTreeComponent->SetDynamicSubtree(ChaseInjectTag, ChaseTree);
	
	if(NonCombatTree)
		BehaviorTreeComponent->SetDynamicSubtree(NonCombatInjectTag, NonCombatTree);

	if(SearchTargetTree)
		BehaviorTreeComponent->SetDynamicSubtree(SearchTargetTreeInjectTag, SearchTargetTree);

	if(RepositionDuringCombatTree)
		BehaviorTreeComponent->SetDynamicSubtree(RepositionDuringCombatInjectTag, RepositionDuringCombatTree);
	
}
