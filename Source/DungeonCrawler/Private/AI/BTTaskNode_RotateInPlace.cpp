
#include "AI/BTTaskNode_RotateInPlace.h"

#include "MyGameplayStatics.h"
#include "AIController.h"

FGameplayEventData UBTTaskNode_RotateInPlace::GetEventData(APawn* OwnerPawn, AActor* TargetCharacter)
{
	auto EventData = Super::GetEventData(OwnerPawn, TargetCharacter);
	if(const AAIController* AIController = Cast<AAIController>(OwnerPawn->GetController()))
	{
		const auto FocalPoint = AIController->GetFocalPoint();
		if(FocalPoint != FAISystem::InvalidLocation)
			EventData.EventMagnitude = UMyGameplayStatics::GetYawAngleBetweenActorAndTarget(OwnerPawn, FocalPoint);
	}
	return EventData;
}
