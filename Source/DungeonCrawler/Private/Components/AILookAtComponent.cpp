
#include "Components/AILookAtComponent.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/Character.h"
#include "AbilitySystemComponent.h"
#include "MyAIController.h"

void UAILookAtComponent::CalculateLookAtRotation()
{
	DesiredLookAtRotation = FRotator::ZeroRotator;
	
	const auto OwnerCharacter = Cast<ACharacter>(GetOwner());
	if(!OwnerCharacter)
		return;

	if(const auto AbilitySystemComponent = OwnerCharacter->FindComponentByClass<UAbilitySystemComponent>())
		if(AbilitySystemComponent->HasMatchingGameplayTag(CannotLookAtTag))
			return;
	
	const auto OwnerAIController = Cast<AMyAIController>(OwnerCharacter->GetController());
	if(!OwnerAIController)
		return;
	
	const auto Blackboard = OwnerAIController->GetBlackboardComponent();
	if(!Blackboard)
		return;

	const auto Target = Cast<ACharacter>(Blackboard->GetValueAsObject(TargetBlackBoardName));
	if(!Target)
		return;
		
	const auto OwnerMesh = OwnerCharacter->GetMesh();
	if(!OwnerMesh)
		return;

	const auto OwnerRotation = OwnerCharacter->GetActorRotation();

	const auto RawLookAtLocation = UKismetMathLibrary::FindLookAtRotation(OwnerMesh->GetSocketLocation(LookAtBoneName), Target->GetMesh()->GetSocketLocation(LookAtBoneName));
	auto DeltaRotation = UKismetMathLibrary::NormalizedDeltaRotator(RawLookAtLocation, OwnerRotation);
	SetDesiredLookAtRotation(DeltaRotation);
	
}
