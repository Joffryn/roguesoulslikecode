
#include "MyAbilitySystemComponent.h"

#include "BlueprintGameplayTagLibrary.h"
#include "Abilities/GameplayAbility.h"
#include "MyGameplayAbility.h"
#include "BaseCharacter.h"
#include "Logs.h"

UMyAbilitySystemComponent* UMyAbilitySystemComponent::GetPlayerAbilitySystemComponent(const UObject* WorldContextObject)
{
	if(const auto PlayerCharacter = ABaseCharacter::GetMyPlayerCharacter(WorldContextObject))
		return PlayerCharacter->AbilitySystemComponent;
	
	return nullptr;
}

bool UMyAbilitySystemComponent::HandleTagChange(const FGameplayTag TagToPotentiallyAdd, const FGameplayTagContainer TagsToRemove, const FGameplayTagContainer BlockingTags)
{
	if(HasAnyMatchingGameplayTags(BlockingTags))
	{
		RemoveGameplayTag(UBlueprintGameplayTagLibrary::MakeGameplayTagContainerFromTag(TagToPotentiallyAdd));
		RemoveGameplayTag(TagsToRemove);
		return true;
	}
	if(!HasMatchingGameplayTag(TagToPotentiallyAdd))
	{
		AddGameplayTag(UBlueprintGameplayTagLibrary::MakeGameplayTagContainerFromTag(TagToPotentiallyAdd));
		RemoveGameplayTag(TagsToRemove);
		return true;
	}
	return false;
}

bool UMyAbilitySystemComponent::TryActivateAbilitiesByTagWithEventData(const FGameplayTagContainer& GameplayTagContainer, const FGameplayEventData& EventData, const bool bAllowRemoteActivation )
{
	TArray<FGameplayAbilitySpec*> AbilitiesToActivate;
	GetActivatableGameplayAbilitySpecsByAllMatchingTags(GameplayTagContainer, AbilitiesToActivate);

	bool bSuccess = false;

	for (const auto GameplayAbilitySpec : AbilitiesToActivate)
		bSuccess |= TryActivateAbilityEventData(GameplayAbilitySpec->Handle, &EventData, true, bAllowRemoteActivation);

	return bSuccess;
}

bool UMyAbilitySystemComponent::BP_TryActivateAbilitiesByTagWithEventData(const FGameplayTagContainer& GameplayTagContainer, const FGameplayEventData EventData, const bool bAllowRemoteActivation )
{
	return TryActivateAbilitiesByTagWithEventData(GameplayTagContainer, EventData, bAllowRemoteActivation);
}

bool UMyAbilitySystemComponent::TryActivateAbilityEventData(const FGameplayAbilitySpecHandle& AbilityToActivate, const FGameplayEventData* EventData, const bool bPressed, const bool bAllowRemoteActivation )
{
	FGameplayTagContainer FailureTags;
	FGameplayAbilitySpec* Spec = FindAbilitySpecFromHandle(AbilityToActivate);
	if (!Spec)
		return false;

	const UGameplayAbility* Ability = Spec->Ability;

	if (!Ability)
		return false;

	const FGameplayAbilityActorInfo* ActorInfo = AbilityActorInfo.Get();
	
	if (ActorInfo == nullptr || !ActorInfo->OwnerActor.IsValid() || !ActorInfo->AvatarActor.IsValid())
		return false;

	bool bSuccess = false;
	//Do not try activate ability on released event.
	if (bPressed)
		bSuccess = InternalTryActivateAbility(AbilityToActivate, FPredictionKey(), nullptr, nullptr, EventData);

	if (bSuccess)
	{
		if (Spec->Ability->bReplicateInputDirectly && IsOwnerActorAuthoritative() == false)
			ServerSetInputPressed(Spec->Handle);

		AbilitySpecInputPressed(*Spec);

		// Invoke the InputPressed event. This is not replicated here. If someone is listening, they may replicate the InputPressed event to the server.
		InvokeReplicatedEvent(EAbilityGenericReplicatedEvent::InputPressed, Spec->Handle, Spec->ActivationInfo.GetActivationPredictionKey());
	}
	return bSuccess;
}

int32 UMyAbilitySystemComponent::HandleGameplayEvent(const FGameplayTag EventTag, const FGameplayEventData* Payload)
{
	const int32 Result = Super::HandleGameplayEvent(EventTag, Payload);
	OnGameplayEvent.Broadcast(EventTag, Payload != nullptr ? *Payload : FGameplayEventData());
	return Result;
}

void UMyAbilitySystemComponent::SetUserAbilityActivationInhibited(const bool NewInhibit)
{
	Super::SetUserAbilityActivationInhibited(NewInhibit);
	if (!NewInhibit)
		HandleAbilityQueue();
}

void UMyAbilitySystemComponent::BeginPlay()
{
	Super::BeginPlay();
	AddGameplayTag(TagsToApplyAtBeginPlay);
	ApplyStartingGameplayEffects();
	BindDelegates();
}

void UMyAbilitySystemComponent::AddGameplayTag(const FGameplayTagContainer TagsToAdd)
{
	AddLooseGameplayTags(TagsToAdd);
}

void UMyAbilitySystemComponent::RemoveGameplayTag(const FGameplayTagContainer TagsToRemove, const bool bAll /*= false*/)
{
	RemoveLooseGameplayTags(TagsToRemove, bAll ? 9999 : 1);
}

bool UMyAbilitySystemComponent::QueueAbility(const FGameplayAbilitySpecHandle AbilityToQueue)
{
	const bool ActivatedImmediately = TryActivateAbility(AbilityToQueue);
	if (!ActivatedImmediately)
		return InternalTryQueueAbility(AbilityToQueue);

	return ActivatedImmediately;
}

bool UMyAbilitySystemComponent::QueueAbilityWithEventData(const FGameplayAbilitySpecHandle AbilityToQueue, FGameplayEventData EventData)
{
	const bool ActivatedImmediately = TriggerAbilityFromGameplayEvent(AbilityToQueue, AbilityActorInfo.Get(), EventData.EventTag, &EventData, *this);
	if (!ActivatedImmediately)
		return InternalTryQueueAbility(AbilityToQueue, &EventData);

	return ActivatedImmediately;
}

bool UMyAbilitySystemComponent::QueueAbilityByClass(const TSubclassOf<UGameplayAbility> AbilityClass)
{
	bool bSuccess = false;
	const UGameplayAbility* const InAbilityCDO = AbilityClass.GetDefaultObject();
	for (const FGameplayAbilitySpec& Spec : ActivatableAbilities.Items)
	{
		if (Spec.Ability == InAbilityCDO)
		{
			bSuccess |= QueueAbility(Spec.Handle);
			break;
		}
	}
	return bSuccess;
}

bool UMyAbilitySystemComponent::QueueAbilityByTag(const FGameplayTagContainer AbilityTags)
{
	TArray<FGameplayAbilitySpec*> AbilitiesToActivate;
	GetActivatableGameplayAbilitySpecsByAllMatchingTags(AbilityTags, AbilitiesToActivate, false);
	bool bSuccess = false;

	for (const auto GameplayAbilitySpec : AbilitiesToActivate)
		bSuccess |= QueueAbility(GameplayAbilitySpec->Handle);
	
	return bSuccess;
}

bool UMyAbilitySystemComponent::QueueAbilityByEvent(const FGameplayTag EventTag, FGameplayEventData EventData)
{
	EventData.EventTag = EventTag;

	bool bSuccess = false;

	FGameplayTag CurrentTag = EventTag;
	while (CurrentTag.IsValid())
	{
		if (GameplayEventTriggeredAbilities.Contains(CurrentTag))
			for (const FGameplayAbilitySpecHandle& AbilityHandle : GameplayEventTriggeredAbilities[CurrentTag])
				bSuccess |= QueueAbilityWithEventData(AbilityHandle, EventData);

		CurrentTag = CurrentTag.RequestDirectParent();
	}

	return bSuccess;
}

void UMyAbilitySystemComponent::ClearQueue()
{
	QueuedAbilityHandle = FGameplayAbilitySpecHandle();
	QueuedAbilitiesEventData = FGameplayEventData();
	OnAbilityRemovedFromQueue.Broadcast();
	if(ClearQueueTimerHandle.IsValid())
		GetWorld()->GetTimerManager().ClearTimer(ClearQueueTimerHandle);
}

bool UMyAbilitySystemComponent::HasQueuedAbility() const
{
	return QueuedAbilityHandle.IsValid();
}

UGameplayAbility* UMyAbilitySystemComponent::GetAbilityInstance(const FGameplayAbilitySpecHandle Handle)
{
	if (const FGameplayAbilitySpec* AbilitySpec = FindAbilitySpecFromHandle(Handle))
		return AbilitySpec->Ability;
	
	return nullptr;
}

void UMyAbilitySystemComponent::BroadcastAttributeChange(const FOnAttributeChangeData& AttributeChangeData) const
{
	OnAttributeValueChange.Broadcast(AttributeChangeData.Attribute, AttributeChangeData.NewValue, AttributeChangeData.OldValue);
	if(AttributeChangeData.NewValue == 0.0f)
		OnAttributeDepleted.Broadcast(AttributeChangeData.Attribute);
}

void UMyAbilitySystemComponent::BindDelegates()
{
	TArray<FGameplayAttribute> Attributes;
	GetAllAttributes(Attributes);
	for (const auto Attribute : Attributes)
		GetGameplayAttributeValueChangeDelegate(Attribute).AddUObject(this, &UMyAbilitySystemComponent::BroadcastAttributeChange);
}

UGameplayAbility* UMyAbilitySystemComponent::GetQueuedAbility()
{
	if (const FGameplayAbilitySpec* AbilitySpec = FindAbilitySpecFromHandle(QueuedAbilityHandle))
		return AbilitySpec->Ability;
	
	return nullptr;
}

bool UMyAbilitySystemComponent::InternalTryQueueAbility(const FGameplayAbilitySpecHandle AbilityToQueue, const FGameplayEventData* EventData /*= nullptr*/)
{
	bool bCanBeQueued = false;
	if (const FGameplayAbilitySpec* AbilityToQueueSpec = FindAbilitySpecFromHandle(AbilityToQueue))
	{
		UGameplayAbility* AbilityInstance = AbilityToQueueSpec->GetPrimaryInstance();
		if (AbilityInstance)
			AbilityInstance = AbilityToQueueSpec->Ability;

		if (const UMyGameplayAbility* QueueableAbilityInstance = Cast< UMyGameplayAbility >(AbilityInstance))
			bCanBeQueued = QueueableAbilityInstance->CanQueueAbility(AbilityToQueue, AbilityActorInfo.Get(), this);
		else
			bCanBeQueued = true;
		
		if(AbilityInstance && bCanBeQueued)
			UE_LOG(LogPlayer, Log, TEXT("Ability queued %s"), *AbilityInstance->GetName());

	}

	if (bCanBeQueued)
	{
		QueuedAbilityHandle = AbilityToQueue;
		if (EventData)
			QueuedAbilitiesEventData = *EventData;

		OnAbilityQueued.Broadcast();
		GetWorld()->GetTimerManager().SetTimer(ClearQueueTimerHandle, this, &UMyAbilitySystemComponent::ClearQueue, TimeToClearQueue, false);
		return true;
	}
	return false;
}

void UMyAbilitySystemComponent::HandleAbilityQueue()
{
	static bool bIsHandlingQueue = false;
	//Cannot be called recursively
	if (!bIsHandlingQueue && HasQueuedAbility())
	{			
		bIsHandlingQueue = true;
		bool bHasActivated;
		if (QueuedAbilitiesEventData.EventTag.IsValid())
			bHasActivated = TriggerAbilityFromGameplayEvent(QueuedAbilityHandle, AbilityActorInfo.Get(), QueuedAbilitiesEventData.EventTag, &QueuedAbilitiesEventData, *this);
		else
			bHasActivated = TryActivateAbility(QueuedAbilityHandle);
		
		if (bHasActivated)
		{
			OnAbilityActivatedFromQueue.Broadcast();
			ClearQueue();
		}
		bIsHandlingQueue = false;
	}
}

void UMyAbilitySystemComponent::NotifyAbilityActivated(const FGameplayAbilitySpecHandle Handle, UGameplayAbility* Ability)
{
	Super::NotifyAbilityActivated(Handle, Ability);
	OnAbilityActivated.Broadcast(Ability);
}

void UMyAbilitySystemComponent::NotifyAbilityFailed(const FGameplayAbilitySpecHandle Handle, UGameplayAbility* Ability, const FGameplayTagContainer& FailureReason)
{
	Super::NotifyAbilityFailed(Handle, Ability, FailureReason);
	OnAbilityFailed.Broadcast(Ability, FailureReason);
}

void UMyAbilitySystemComponent::NotifyAbilityEnded(const FGameplayAbilitySpecHandle Handle, UGameplayAbility* Ability, const bool bWasCancelled)
{
	Super::NotifyAbilityEnded(Handle, Ability, bWasCancelled);
	OnAbilityEnded.Broadcast(Ability, bWasCancelled);
	HandleAbilityQueue();
}

void UMyAbilitySystemComponent::NotifyAbilityCommit(UGameplayAbility* Ability)
{
	Super::NotifyAbilityCommit(Ability);
	OnAbilityCommited.Broadcast(Ability);
}

void UMyAbilitySystemComponent::OnTagUpdated(const FGameplayTag& Tag, const bool TagExists)
{
	Super::OnTagUpdated(Tag, TagExists);
	if (!TagExists)
	{
		OnGameplayTagRemoved.Broadcast(Tag);
		HandleAbilityQueue();
	}
	else if (GetTagCount(Tag) == 1)
	{
		OnGameplayTagAdded.Broadcast(Tag);
		HandleAbilityQueue();
	}
}

void UMyAbilitySystemComponent::ApplyStartingGameplayEffects()
{
	const FGameplayEffectContextHandle EffectContext;
	for(const auto GameplayEffectClass : EffectsToApplyAtBeginPlay)
		BP_ApplyGameplayEffectToSelf(GameplayEffectClass, 0, EffectContext);
}
