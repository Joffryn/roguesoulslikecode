
#include "Components/LootComponent.h"

#include "Components/Managers/LootManager.h"
#include "Components/StaticMeshComponent.h"
#include "Interfaces/LootableInterface.h"
#include "Components/SphereComponent.h"
#include "Components/BoxComponent.h"
#include "Actors/OverlapPickup.h"
#include "RandomnessManager.h"
#include "BaseCharacter.h"
#include "Globals/Logs.h"
#include "DataAssets.h"
#include "Pickup.h"

void ULootComponent::SpawnLoot()
{
	DropPickups();
	DropGold();
}

void ULootComponent::BeginPlay()
{
	Super::BeginPlay();

	if(!Config)
	{
		UE_LOG(LogSpawning, Log, TEXT("No loot config found for %s"), *GetOwner()->GetName());
		return;
	}
	
	GoldToDrop = URandomnessManager::GetRandomnessManager(this)->GetStream().RandRange(Config->MinGold, Config->MaxGold);

	// Only listen to owner death if it's a character
	if(const auto BaserCharacterOwner = Cast<ABaseCharacter>(GetOwner()))
		BaserCharacterOwner->OnDeath.AddDynamic(this, &ULootComponent::OnOwnerDeath);
}

void ULootComponent::OnOwnerDeath(ABaseCharacter* Owner)
{
	SpawnLoot();
}

void ULootComponent::DropPickups() const
{
	const FTransform Transform = FTransform(FRotator::ZeroRotator.Quaternion(), GetOwner()->GetActorLocation(), FVector::OneVector);
	const auto LootManager = ULootManager::GetLootManager(this);
	
	for(const auto Pickup : Config->PickupsToSpawn)
	{
		TSubclassOf<APickup> PickupClass;
		for(auto PickupDropSetup : LootManager->PickupDropSetups)
		{
			if(PickupDropSetup.PickupType == Pickup)
			{
				PickupClass = PickupDropSetup.PickupClasses[FMath::RandRange(0, PickupDropSetup.PickupClasses.Num() - 1)];
				break;
			}
		}
		if(const auto DroppedPickup = GetWorld()->SpawnActorDeferred<APickup>(PickupClass, Transform, GetOwner(), GetOwner()->GetInstigator(), ESpawnActorCollisionHandlingMethod::AlwaysSpawn))
		{
			DroppedPickup->Box->SetNotifyRigidBodyCollision(true);
			DroppedPickup->Box->SetSimulatePhysics(true);
			DroppedPickup->FinishSpawning(Transform);

			ApplyImpulseOnDroppedItem(DroppedPickup, LootManager);
		}
	}
}

void ULootComponent::DropGold() const
{
	const auto LootManager = ULootManager::GetLootManager(this);
	
	FVector SpawnLocation;
	if(const auto LootableInterface = Cast<ILootableInterface>(GetOwner()))
		SpawnLocation = LootableInterface->Execute_GetLootSpawnLocation(GetOwner());
	else
		SpawnLocation = GetOwner()->GetActorLocation();
	
	for(int32 i =- 0; i < GoldToDrop; i++)
	{
		const FTransform Transform = FTransform(FRotator::ZeroRotator.Quaternion(), SpawnLocation, FVector::OneVector);
		
		if(const auto DroppedGold = GetWorld()->SpawnActorDeferred<AOverlapPickup>(LootManager->GoldDropActorClass, Transform, GetOwner(), GetOwner()->GetInstigator(), ESpawnActorCollisionHandlingMethod::AlwaysSpawn))
		{
			DroppedGold->CollectionSphere->SetGenerateOverlapEvents(false);

			DroppedGold->Box->SetNotifyRigidBodyCollision(true);
			DroppedGold->Box->SetSimulatePhysics(true);
			
			DroppedGold->FinishSpawning(Transform);
			
			ApplyImpulseOnDroppedItem(DroppedGold, LootManager);
		}
	}
}

void ULootComponent::ApplyImpulseOnDroppedItem(const AActor* Actor, const ULootManager* LootManager, const float Multiplier /*= 1.0f*/) const
{
	if(const auto RootComponent = Cast<UPrimitiveComponent>(Actor->GetRootComponent()))
	{
		FVector RandomDirection;
		if(bSpawnInFront)
		{
			const float ActorForwardX = GetOwner()->GetActorForwardVector().X;
			const float ActorRightX = GetOwner()->GetActorRightVector().X;
			RandomDirection = FVector(FMath::FRandRange(ActorRightX / 2.f + SpawnConeAngle / -180.0f, ActorRightX + SpawnConeAngle / 180.0f),
				FMath::FRandRange(ActorForwardX / 2.f + SpawnConeAngle / -180.0f, ActorForwardX + SpawnConeAngle / 180.0f), FMath::FRandRange(0.f, 2.f));
		}
		else
			RandomDirection = FVector(FMath::FRandRange(-1.f, 1.f), FMath::FRandRange(-1.f, 1.f), FMath::FRandRange(0.f, 2.f));
		
		const float DropImpulseStrength = FMath::FRandRange(LootManager->DropImpulseStrengthRange.X, LootManager->DropImpulseStrengthRange.Y) * Multiplier;
		RootComponent->AddImpulse(RandomDirection * FVector(DropImpulseStrength), "", true);
	}
}
