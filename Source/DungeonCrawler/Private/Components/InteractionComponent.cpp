
#include "InteractionComponent.h"

#include "PlayerInteractionComponent.h"
#include "InteractableInterface.h"
#include "MyGameplayStatics.h"
#include "BaseCharacter.h"
#include "Logs.h"

UInteractionComponent::UInteractionComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	UActorComponent::Activate(true);
	UPrimitiveComponent::SetCollisionProfileName(FName("Interactable"));
}

void UInteractionComponent::ShowOutlines()
{
	if(bShowOutline)
		return;
	
	for (const auto MeshComponent : GetOutlineMeshes())
	{
		MeshComponent->SetRenderCustomDepth(true);
		MeshComponent->SetCustomDepthStencilValue(1);
	}
	bShowOutline = true;
}

void UInteractionComponent::HideOutlines()
{
	if(!bShowOutline)
		return;
	
	for (const auto MeshComponent : GetOutlineMeshes())
	{
		MeshComponent->SetRenderCustomDepth(true);
		MeshComponent->SetCustomDepthStencilValue(0);
	}
	bShowOutline = false;
}

void UInteractionComponent::Interact(UPlayerInteractionComponent* InteractingComponent) const
{
	UE_LOG(LogGameplay, Log, TEXT("Interacted with %s."), *GetOwner()->GetName());
	if(Cast<IInteractableInterface>(GetOwner()))
	{
		IInteractableInterface::Execute_Interact(GetOwner());
	}
	OnInteracted.Broadcast(InteractingComponent);
}

void UInteractionComponent::BeginPlay()
{
	Super::BeginPlay();
	OnComponentBeginOverlap.AddDynamic(this, &UInteractionComponent::OnBeginInteractionOverlap);
	OnComponentEndOverlap.AddDynamic(this, &UInteractionComponent::OnEndInteractionOverlap);
}

void UInteractionComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	bool bCanInteract = false;
	const auto OwnerActor = GetOwner();

	if(bInteractionBlocked)
		bCanInteract = false;
	else if(const auto InteractableInterface = Cast<IInteractableInterface>(OwnerActor))
		bCanInteract = InteractableInterface->Execute_CanInteract(OwnerActor);

	if(bOutlinesEnabled)
	{
		if (bIsInRange && bIsFocused && bCanInteract)
			ShowOutlines();
		else
			HideOutlines();
	}
}

void UInteractionComponent::OnBeginInteractionOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor == ABaseCharacter::GetMyPlayerCharacter(this))
		bIsInRange = true;
}

void UInteractionComponent::OnEndInteractionOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if(OtherActor == ABaseCharacter::GetMyPlayerCharacter(this))
		bIsInRange = false;
}

TArray<UMeshComponent*> UInteractionComponent::GetOutlineMeshes() const
{
	TArray<UMeshComponent*> OwnerMeshComponents;
	//Collect owner meshes
	OwnerMeshComponents.Append(UMyGameplayStatics::GetActorMeshes(GetOwner()));
	//And all actors attached to it(weapons)
	TArray<AActor*> AttachedActors;
	GetOwner()->GetAttachedActors(AttachedActors);
	for(const auto Actor : AttachedActors)
		OwnerMeshComponents.Append(UMyGameplayStatics::GetActorMeshes(Actor));
		
	return OwnerMeshComponents;
}
