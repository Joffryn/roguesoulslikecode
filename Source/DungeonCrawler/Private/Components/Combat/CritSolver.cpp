
#include "Components/Combat/CritSolver.h"

#include "MyAbilitySystemComponent.h"
#include "CombatComponent.h"
#include "BaseCharacter.h"
#include "MyDamageType.h"
#include "MyGameMode.h"

UCritSolver* UCritSolver::GetCritSolver(const UObject* WorldContextObject)
{
	if(const auto MyGameMode = AMyGameMode::GetMyGameMode(WorldContextObject))
		return MyGameMode->CritSolver;

	return nullptr;
}

bool UCritSolver::IsAttackCrit(const AActor* Attacker, AActor* Attacked, const UDamageType* DamageType) const
{
	if(Attacked)
	{
		const auto AttackedAbilitySystemComponent = Attacked->FindComponentByClass<UMyAbilitySystemComponent>();

		//Just a null check
		if(!AttackedAbilitySystemComponent)
			return false;

		//Check if damage type allows crit
		if(const auto MyDamageType = Cast<UMyDamageType>(DamageType))
			if(!MyDamageType->bCanCrit)
				return false;
		
		const bool bHasShouldBeCrittedTag = AttackedAbilitySystemComponent->HasMatchingGameplayTag(CrittedWithNextHit);
		if(bHasShouldBeCrittedTag)
			AttackedAbilitySystemComponent->RemoveGameplayTag(CrittedWithNextHit.GetSingleTagContainer());
		
		//Only activate on living opponents
		if(const auto AttackedBaseCharacter = Cast<ABaseCharacter>(Attacked))
			if(!AttackedBaseCharacter->IsAlive())
				return false;
		
		//Crit immunity
		if(AttackedAbilitySystemComponent->HasMatchingGameplayTag(CritImmuneTag))
			return false;

		//Should be critted with next attack
		if(bHasShouldBeCrittedTag)
			return true;
		
		//Should be always critted
		if(AttackedAbilitySystemComponent->HasMatchingGameplayTag(AlwaysCrittedTag))
			return true;
}
	
	if(Attacker)
	{
		if(const auto AttackerAbilitySystemComponent = Attacker->FindComponentByClass<UAbilitySystemComponent>())
			if(AttackerAbilitySystemComponent->HasMatchingGameplayTag(AlwaysCritsTag))
				return true;
		
		const auto AttackerAbilitySystemComponent = Attacker->FindComponentByClass<UAbilitySystemComponent>();
		const auto AttackerCombatComponent = Attacker->FindComponentByClass<UCombatComponent>();

		//Sprint attack
		if(AttackerCombatComponent && AttackerAbilitySystemComponent)
			if(AttackerAbilitySystemComponent->HasMatchingGameplayTag(SprintAttackCritUpgradeTag) && AttackerAbilitySystemComponent->HasMatchingGameplayTag(SprintAttackTag))
				return true;

		//Roll attack
		if(AttackerCombatComponent && AttackerAbilitySystemComponent)
			if(AttackerAbilitySystemComponent->HasMatchingGameplayTag(RollAttackCritUpgradeTag) && AttackerAbilitySystemComponent->HasMatchingGameplayTag(RollAttackTag))
				return true;

		//Fully charged attack
		if(AttackerCombatComponent && AttackerAbilitySystemComponent)
			if(AttackerAbilitySystemComponent->HasMatchingGameplayTag(FullyChargedAttackCritUpgradeTag) && AttackerAbilitySystemComponent->HasMatchingGameplayTag(FullyChargedAttackTag))
				return true;
		
		//Final combo attack
		if(AttackerCombatComponent && AttackerAbilitySystemComponent)
			if(AttackerAbilitySystemComponent->HasMatchingGameplayTag(FinalComboAttackCritUpgradeTag) && AttackerAbilitySystemComponent->HasMatchingGameplayTag(FinalComboAttackTag))
				return true;
		
		//Air attack
		if(AttackerCombatComponent && AttackerAbilitySystemComponent)
			if(AttackerAbilitySystemComponent->HasMatchingGameplayTag(AirAttackCritUpgradeTag) && AttackerAbilitySystemComponent->HasMatchingGameplayTag(AirAttackTag))
				return true;
		
	}		
	return false;
}
