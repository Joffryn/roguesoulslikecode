
#include "TrailComponent.h"

#include "Particles/ParticleSystemComponent.h"
#include "Components/MeshComponent.h"
#include "ParticleEmitterInstances.h"
#include "UObject/UObjectHash.h"
#include "GameFramework/Actor.h"
#include "GameConfigComponent.h"
#include "TrailInterface.h"

typedef TInlineComponentArray<UParticleSystemComponent*, 8> FParticleSystemComponentArray;

static void GetCandidateSystems(const UMeshComponent& MeshComponent, FParticleSystemComponentArray& Components)
{
	if (const AActor* Owner = MeshComponent.GetOwner())
	{
		Owner->GetComponents(Components);
	}
	else
	{
		// No actor owner in some editor windows. Get ParticleSystemComponents spawned by the MeshComponent.
		ForEachObjectWithOuter(&MeshComponent, [&Components](UObject* Child)
			{
				if (UParticleSystemComponent* ChildParticleSystemComponents = Cast<UParticleSystemComponent>(Child))
					Components.Add(ChildParticleSystemComponents);
			
			}, false, RF_NoFlags, EInternalObjectFlags::Garbage);
	}
}

UTrailComponent::UTrailComponent()
{
	FirstSocketName = FName("TrailStart");
	SecondSocketName = FName("TrailEnd");
	WidthScaleMode = ETrailWidthMode_FromCentre;
	bRecycleSpawnedSystems = true;
}

void UTrailComponent::StartTrail()
{
	if(const auto StaticMeshComponent = ITrailInterface::Execute_GetStaticMesh(GetOwner()))
		TrailBegin(StaticMeshComponent);
}

void UTrailComponent::EndTrail() const 
{
	if(const auto StaticMeshComponent = ITrailInterface::Execute_GetStaticMesh(GetOwner()))
		TrailEnd(StaticMeshComponent);
}

UParticleSystemComponent* UTrailComponent::GetParticleSystemComponent(const UMeshComponent* MeshComponent) const
{
	if (!MeshComponent)
		return nullptr;

	FParticleSystemComponentArray Children;
	GetCandidateSystems(*MeshComponent, Children);
	for (UParticleSystemComponent* ParticleComponent : Children)
	{
		if (ParticleComponent->IsActive())
		{
			UParticleSystemComponent::TrailEmitterArray TrailEmitters;
			ParticleComponent->GetOwnedTrailEmitters(TrailEmitters, this, false);
			if (TrailEmitters.Num() > 0)
				// We have a trail emitter, so return this one
				return ParticleComponent;
		}
	}
	return nullptr;
}

void UTrailComponent::BeginPlay()
{
	Super::BeginPlay();
	
	if (!BaseTrail)
		BaseTrail = UGameConfigComponent::GetGameConfigComponent(this)->BaseTrail;
}

void UTrailComponent::TrailBegin(UMeshComponent* MeshComponent)
{
	if (TrailOverride)
		ParticleSystemTemplate = TrailOverride;
	else
		ParticleSystemTemplate = BaseTrail;

	if (MeshComponent->GetWorld()->GetNetMode() == NM_DedicatedServer)
		return;

	if (!ParticleSystemTemplate)
		return;

	FParticleSystemComponentArray Children;
	GetCandidateSystems(*MeshComponent, Children);

	constexpr float Width = 1.0f;

	UParticleSystemComponent* RecycleCandidates[3] = { nullptr, nullptr, nullptr }; // in order of priority
	bool bFoundExistingTrail = false;
	for (UParticleSystemComponent* ParticleComponent : Children)
	{
		if (ParticleComponent->IsActive())
		{
			UParticleSystemComponent::TrailEmitterArray TrailEmitters;
			ParticleComponent->GetOwnedTrailEmitters(TrailEmitters, this, false);

			if (TrailEmitters.Num() > 0)
			{
				// This has active emitters, we'll just restart this one.
				bFoundExistingTrail = true;

				//If there are any trails, ensure the template hasn't been changed. Also destroy the component if there are errors.
				if (ParticleSystemTemplate != ParticleComponent->Template && ParticleComponent->GetOuter() == MeshComponent)
				{
					//The PSTemplate was changed so we need to destroy this system and create it again with the new template. May be able to just change the template?
					ParticleComponent->DestroyComponent();
				}
				else
				{
					for (FParticleAnimTrailEmitterInstance* Trail : TrailEmitters)
					{
						Trail->BeginTrail();
						Trail->SetTrailSourceData(FirstSocketName, SecondSocketName, WidthScaleMode, Width);
					}
				}

				break;
			}
		}
		else if (ParticleComponent->bAllowRecycling && !ParticleComponent->IsActive())
		{
			// We prefer to recycle one with a matching template, and prefer one created by us.
			// 0: matching template, owned by mesh
			// 1: matching template, owned by actor
			// 2: non-matching template, owned by actor or mesh
			int32 RecycleIndex = 2;
			if (ParticleComponent->Template == ParticleSystemTemplate)
				RecycleIndex = ParticleComponent->GetOuter() == MeshComponent ? 0 : 1;
			
			RecycleCandidates[RecycleIndex] = ParticleComponent;
		}
	}

	if (!bFoundExistingTrail)
	{
		// Spawn a new component from ParticleSystemTemplate, or recycle an old one.
		UParticleSystemComponent* RecycleComponent = RecycleCandidates[0] ? RecycleCandidates[0] : RecycleCandidates[1] ? RecycleCandidates[1] : RecycleCandidates[2];
		UParticleSystemComponent* NewParticleComponent = RecycleComponent ? RecycleComponent : NewObject<UParticleSystemComponent>(MeshComponent);
		NewParticleComponent->bAutoDestroy = RecycleComponent ? false : !bRecycleSpawnedSystems;
		NewParticleComponent->bAllowRecycling = true;
		NewParticleComponent->SecondsBeforeInactive = 0.0f;
		NewParticleComponent->bAutoActivate = false;
		NewParticleComponent->bOverrideLODMethod = false;
		NewParticleComponent->GetRelativeScale3D() = FVector(1.f);
		NewParticleComponent->bAutoManageAttachment = true; // Let it detach when finished (only happens if not auto-destroying)
		NewParticleComponent->SetAutoAttachParams(MeshComponent, NAME_None);

		// When recycling we can avoid setting the template if set already.
		if (NewParticleComponent->Template != ParticleSystemTemplate)
			NewParticleComponent->SetTemplate(ParticleSystemTemplate);

		// Recycled components are usually already registered
		if (!NewParticleComponent->IsRegistered())
			NewParticleComponent->RegisterComponentWithWorld(MeshComponent->GetWorld());

		NewParticleComponent->AttachToComponent(MeshComponent, FAttachmentTransformRules::KeepRelativeTransform);
		NewParticleComponent->ActivateSystem(true);

		UParticleSystemComponent::TrailEmitterArray TrailEmitters;
		NewParticleComponent->GetOwnedTrailEmitters(TrailEmitters, this, true);

		for (FParticleAnimTrailEmitterInstance* Trail : TrailEmitters)
		{
			Trail->BeginTrail();
			Trail->SetTrailSourceData(FirstSocketName, SecondSocketName, WidthScaleMode, Width);
		}
	}
}

void UTrailComponent::TrailEnd(const UMeshComponent* MeshComponent) const 
{
	if (MeshComponent->GetWorld()->GetNetMode() == NM_DedicatedServer)
		return;

	FParticleSystemComponentArray Children;
	GetCandidateSystems(*MeshComponent, Children);

	for (UParticleSystemComponent* ParticleComp : Children)
	{
		if (ParticleComp->IsActive())
		{
			UParticleSystemComponent::TrailEmitterArray TrailEmitters;
			ParticleComp->GetOwnedTrailEmitters(TrailEmitters, this, false);
			for (FParticleAnimTrailEmitterInstance* Trail : TrailEmitters)
				Trail->EndTrail();
		}
	}
}

