
#include "Components/Combat/BossComponent.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "MyAbilitySystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "AICombatComponent.h"
#include "MyAttributeSet.h"
#include "BaseCharacter.h"
#include "MusicManager.h"
#include "MyHUD.h"
#include "Logs.h"

UBossComponent::UBossComponent()
{
}

void UBossComponent::SetPhase(const int32 NewPhase)
{
	if(NewPhase > Phase)
		if(const auto AICombatComponent =GetOwner()->FindComponentByClass<UAICombatComponent>())
			AICombatComponent->ForceAction(EAIActionType::ChangePhase);
	
	Phase = NewPhase;
	UE_LOG(LogAI, Log, TEXT("%s boss enetered %d phase"), *GetOwner()->GetName(), NewPhase);

	OnBossPhaseEnter.Broadcast(Phase);
 }

void UBossComponent::BeginPlay()
{
	Super::BeginPlay();

	if(const auto BaseCharacterOwner = Cast<ABaseCharacter>(GetOwner()))
	{
		BaseCharacterOwner->OnTargetSet.AddDynamic(this, &UBossComponent::OnTargetSet);
		BaseCharacterOwner->OnDeath.AddDynamic(this, &UBossComponent::OnOwnerDeath);

		//Ignore if boss has no phases
		if(PhasesSetup.Num() > 0)
			if (const auto Abs = BaseCharacterOwner->AbilitySystemComponent)
				Abs->OnAttributeValueChange.AddDynamic(this, &UBossComponent::OnOwnerAttributeChange);
	}
}

void UBossComponent::OnTargetSet(AActor* Target)
{
	if(bIsSetuped)
		return;
	
	if(Target == UGameplayStatics::GetPlayerCharacter(this, 0))
	{
		AMyHUD::GetMyHud(this)->AddBossHealthBar(Cast<ABaseCharacter>(GetOwner()));
		if (const auto MusicManager = UMusicManager::GetMusicManager(this))
			MusicManager->PlayMusic(Music);

		bIsSetuped = true;
	}
}

void UBossComponent::OnOwnerDeath(ABaseCharacter* Actor)
{
	if (const auto MusicManager = UMusicManager::GetMusicManager(this))
		MusicManager->StopMusic(Music);
}

void UBossComponent::OnOwnerAttributeChange(const FGameplayAttribute Attribute, float NewValue, float OldValue)
{
	if (Attribute == UMyAttributeSet::GetHealthAttribute())
	{
		bool bFound;
		const auto HealthPercentage = UAbilitySystemBlueprintLibrary::GetFloatAttribute(GetOwner(), UMyAttributeSet::GetHealthAttribute(), bFound) / UAbilitySystemBlueprintLibrary::GetFloatAttribute(GetOwner(), UMyAttributeSet::GetMaxHealthAttribute(), bFound);
		int32 BestMatch = -1;
		float BestMatchHealthPercentage = FLT_MAX;
		for (const auto PhaseSetup : PhasesSetup)
		{
			if(PhaseSetup.PhaseID > Phase)
			{
				if (HealthPercentage < PhaseSetup.HealthPercentage && PhaseSetup.HealthPercentage < BestMatchHealthPercentage)
				{
					BestMatchHealthPercentage = HealthPercentage;
					BestMatch = PhaseSetup.PhaseID;
				}
			}
		}
		if(BestMatch > 0)
			if(BestMatch != Phase)
				SetPhase(BestMatch);
	}
}
