
#include "Components/Combat/StuckingProjectileComponent.h"

#include "MyProjectileMovementComponent.h"
#include "Components/SceneComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameConfigComponent.h"
#include "Actors/Projectile.h"

void UStuckingProjectileComponent::BeginPlay()
{
	Super::BeginPlay();

	ProjectileOwner = Cast<AProjectile>(GetOwner());
	ensure(ProjectileOwner);
	if (ProjectileOwner)
		ProjectileOwner->OnProjectileHit.AddDynamic(this, &UStuckingProjectileComponent::OnOwnerProjectileHit);
}

void UStuckingProjectileComponent::OnOwnerProjectileHit(const FHitResult HitResult, const float Damage)
{
	//Very naive solution, it will probably be refactored in the future
	//For now just use base attenuation
	USoundAttenuation* BaseAttenuation = nullptr;
	if(const auto GameConfigComponent = UGameConfigComponent::GetGameConfigComponent(this))
		BaseAttenuation= GameConfigComponent->BaseAttenuation;
	
	//If no damage done doesn't stuck, start physic simulation instead
	if (Damage > 0 && !HitResult.GetActor()->ActorHasTag("NonStuckable"))
		UGameplayStatics::PlaySoundAtLocation(this, CharacterStuckSound, GetOwner()->GetActorLocation(), FRotator(), 1.0f, 1.0f, 0.0f, BaseAttenuation);
	else
	{
		UGameplayStatics::PlaySoundAtLocation(this, DeflectSound, GetOwner()->GetActorLocation(), FRotator(), 1.0f, 1.0f, 0.0f, BaseAttenuation);
		ProjectileOwner->SimulatePhysics();
		return;
	}
	if(const auto HitComponentMesh = Cast<USkinnedMeshComponent>(HitResult.Component))
	{
		const auto BoneLocation = HitComponentMesh->GetBoneLocation(HitResult.BoneName);
		ProjectileOwner->SetActorLocation((ProjectileOwner->GetActorLocation() + BoneLocation) / 2.f);
	}
	
	const FAttachmentTransformRules AttachmentTransformRules{ EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, EAttachmentRule::KeepWorld, true };
	ProjectileOwner->AttachToComponent(HitResult.GetComponent(), AttachmentTransformRules, HitResult.BoneName);
	ProjectileOwner->ProjectileMovementComponent->DestroyComponent();
	ProjectileOwner->ClearTrails();
	
	HitResult.GetActor()->OnDestroyed.AddDynamic(this, &UStuckingProjectileComponent::OnHitActorDestroyed);

	if (const auto RootAsPrimitive = Cast<UPrimitiveComponent>(ProjectileOwner->GetRootComponent()))
	{
		CollisionEnabledCache = RootAsPrimitive->GetCollisionEnabled();
		RootAsPrimitive->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		RootAsPrimitive->SetGenerateOverlapEvents(false);
	}

	OnStuck.Broadcast();
}

void UStuckingProjectileComponent::OnHitActorDestroyed(AActor* DestroyedActor)
{
	if (const auto RootAsPrimitive = Cast<UPrimitiveComponent>(ProjectileOwner->GetRootComponent()))
		RootAsPrimitive->SetCollisionEnabled(CollisionEnabledCache);
	
	ProjectileOwner->SimulatePhysics();
}

