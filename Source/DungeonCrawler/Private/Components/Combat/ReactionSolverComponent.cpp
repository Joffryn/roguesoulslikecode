
#include "Components/Combat/ReactionSolverComponent.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "GameplayTagContainer.h"
#include "Objects/HitInfo.h"
#include "PoiseComponent.h"
#include "MyGameMode.h"
#include "Structs.h"
#include "Enums.h"

UReactionSolverComponent::UReactionSolverComponent()
{
	//Two direction setup only for poor anim sets
	DirectionsToAnglesSetupsTwoDirections.Add(FSingleHitDirectionSetup(EDirection::Front, FVector2D(-90.0f, 90.0f)));
	DirectionsToAnglesSetupsTwoDirections.Add(FSingleHitDirectionSetup(EDirection::Back, FVector2D(-180.0f, -90.0f)));
	DirectionsToAnglesSetupsTwoDirections.Add(FSingleHitDirectionSetup(EDirection::Back, FVector2D(90.0f, 180.0f)));

	//Normally 4 directions would be used, but most used asset packs have only 3 directional hits so the 360 is evenly split between left right and back.
	DirectionsToAnglesSetupsThreeDirections.Add(FSingleHitDirectionSetup(EDirection::Left, FVector2D(0.0f, 120.0f)));
	DirectionsToAnglesSetupsThreeDirections.Add(FSingleHitDirectionSetup(EDirection::Right, FVector2D(-120.0f, 0.0f)));
	DirectionsToAnglesSetupsThreeDirections.Add(FSingleHitDirectionSetup(EDirection::Back, FVector2D(-180.0f, -120.0f)));
	DirectionsToAnglesSetupsThreeDirections.Add(FSingleHitDirectionSetup(EDirection::Back, FVector2D(120.0f, 180.0f)));

	//Classic 4 directional setup
	DirectionsToAnglesSetupsFourDirections.Add(FSingleHitDirectionSetup(EDirection::Front, FVector2D(-45.0f, 45.0f)));
	DirectionsToAnglesSetupsFourDirections.Add(FSingleHitDirectionSetup(EDirection::Left, FVector2D(45.0f, 135.0f)));
	DirectionsToAnglesSetupsFourDirections.Add(FSingleHitDirectionSetup(EDirection::Right, FVector2D(-135.0f, -45.0f)));
	DirectionsToAnglesSetupsFourDirections.Add(FSingleHitDirectionSetup(EDirection::Back, FVector2D(-180.0f, -135.0f)));
	DirectionsToAnglesSetupsFourDirections.Add(FSingleHitDirectionSetup(EDirection::Back, FVector2D(135.0f, 180.0f)));
}

UReactionSolverComponent* UReactionSolverComponent::GetReactionSolver(const UObject* WorldContextObject)
{
	if(const auto MyGameMode = AMyGameMode::GetMyGameMode(WorldContextObject))
		return MyGameMode->ReactionSolver;

	return nullptr;
}

EDirection UReactionSolverComponent::GetDirectionFromAngle(const float Angle, const EHitMapType HitMapType /*= EHitMapType::ThreeDirections*/) const
{
	TArray<FSingleHitDirectionSetup> Setups;
	switch (HitMapType)
	{
	case EHitMapType::ThreeDirections:
		Setups = DirectionsToAnglesSetupsThreeDirections;
		break;

	case EHitMapType::FourDirections:
		Setups = DirectionsToAnglesSetupsFourDirections;
		break;

	case EHitMapType::TwoDirections:
		Setups = DirectionsToAnglesSetupsTwoDirections;
		break;

	default:
		Setups = DirectionsToAnglesSetupsThreeDirections;
	}

	for (const auto Setup : Setups)
		if (Angle >= Setup.AngleRange.X && Angle <= Setup.AngleRange.Y)
			return Setup.Direction;

	return EDirection::None;
}

FGameplayTag UReactionSolverComponent::GetGameplayTagForAttackStaggerStrength(const EAttackStaggerStrength AttackStaggerStrength)
{
	if (const auto Tag = AttackStrengthToTagsMap.Find(AttackStaggerStrength))
			return *Tag;
	
	return FGameplayTag();
}

void UReactionSolverComponent::SendStaggerGameplayEvent(AActor* Target, const EAttackStaggerStrength AttackStaggerStrength, const AActor* DamageCauser, const EDirection HitDirection/* = EHitDirection::None*/)
{
	FGameplayEventData EventData;
	auto HitInfo = NewObject<UHitInfo>(UHitInfo::StaticClass());
	HitInfo->HitDirection = HitDirection;
	EventData.OptionalObject = HitInfo;
	EventData.Instigator = DamageCauser;
	if(const auto PoiseComponent = Target->FindComponentByClass<UPoiseComponent>())
	{
		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(Target, GetReactionTag(AttackStaggerStrength, PoiseComponent->PoiseLevel), EventData);
		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(Target, *AttackStrengthToTagsMap.Find(AttackStaggerStrength), EventData);
	}
}

EAttackStaggerStrength UReactionSolverComponent::GetAttackStaggerStrengthForGameplayTag(const FGameplayTag Tag) const
{
	if (const auto AttackStaggerStrength = AttackStrengthToTagsMap.FindKey(Tag))
		return *AttackStaggerStrength;
	
	return EAttackStaggerStrength::None;
}

FGameplayTag UReactionSolverComponent::GetReactionTag(const EAttackStaggerStrength AttackStrength, const EPoiseLevel PoiseLevel)
{
	switch (DecideStaggerReaction(AttackStrength, PoiseLevel))
	{
		case EStaggerReaction::None:
			return NoReaction;

		case EStaggerReaction::AdditiveHit:
			return AdditiveHitReaction;
		
		case EStaggerReaction::Hit:
			return HitReaction;

		case EStaggerReaction::HeavyHit:
			return StrongHitReaction;

		case EStaggerReaction::Stun:
			return StunReaction;

		case EStaggerReaction::Knockdown:
			return KnockDownReaction;

		default:
			return NoReaction;
	}
}

EStaggerReaction UReactionSolverComponent::DecideStaggerReaction(const EAttackStaggerStrength AttackStrength, const EPoiseLevel PoiseLevel)
{
	if (const auto Setup = ReactionsSetup.PoiseLevelToStaggerSetupMap.Find(PoiseLevel))
		if (const auto Reaction = Setup->AttackToReactionMap.Find(AttackStrength))
			return *Reaction;
	
	return EStaggerReaction::None;
}
