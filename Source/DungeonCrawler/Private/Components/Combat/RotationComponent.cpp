
#include "Components/Combat/RotationComponent.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "GameplayTagAssetInterface.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MyAttributeSet.h"
#include "Logs.h"

URotationComponent::URotationComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void URotationComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(bRotateInPlaceRequested)
		RotateToInPlaceRequestedYaw(DeltaTime);

	if(!bRotationEnabled)
		return;
	
	RotateTowardTarget(DeltaTime);
}

void URotationComponent::RequestRotateInPlace(const float YawChange, const float Time)
{
	StartingYaw = GetOwner()->GetActorRotation().Yaw;
	RotateInPlaceTimeElapsed = 0.0f;
	bRotateInPlaceRequested = true;
	RotateInPlaceYaw = YawChange;
	RotateInPlaceTime = Time;
}

void URotationComponent::BreakRotateInPlace()
{
	bRotateInPlaceRequested = false;
}

void URotationComponent::SetCharacterRotation(const FRotator NewRequestedRotation, const bool bInterp /*= false*/, const float InterpSpeed /*= 0.0f*/)
{
	RequestedRotation = NewRequestedRotation;
	if (!bInterp)
		CharacterRotation = RequestedRotation;
	else
		CharacterRotation = UKismetMathLibrary::RInterpTo(CharacterRotation, RequestedRotation, UGameplayStatics::GetWorldDeltaSeconds(this), InterpSpeed);

	CharacterRotation.Pitch = 0.0f;
	CharacterRotation.Roll = 0.0f;
	GetOwner()->SetActorRotation(CharacterRotation);
	
	if(bHasForcedRotation && CharacterRotation == ForcedRotation)
		bHasForcedRotation = false;
}

void URotationComponent::SetTarget(AActor* NewTarget)
{
	if (NewTarget == Target)
		return;

	if (NewTarget)
	{
		UE_LOG(LogAI, Log, TEXT("%s set rotation target to %s"), *GetOwner()->GetName(), *NewTarget->GetName());
	}
	else
		UE_LOG(LogAI, Log, TEXT("%s cleared rotation target"), *GetOwner()->GetName());

	Target = NewTarget;
}

void URotationComponent::RotateTowardTarget(const float DeltaTime)
{
	if (!Target && !bHasForcedRotation)
		return;
	
	if(bWithDebug)
	{
		GEngine->AddOnScreenDebugMessage(-1, 0.0f, FColor::Emerald, TEXT("Current Rotation = ") + GetOwner()->GetActorRotation().ToString());
		GEngine->AddOnScreenDebugMessage(-1, 0.0f, FColor::Magenta, TEXT("Desired Rotation = ") + GetLookAtRotation().ToString());
		GEngine->AddOnScreenDebugMessage(-1, 0.0f, FColor::Silver, FString::Printf(TEXT("Yaw Diff = %f"), (GetOwner()->GetActorRotation() - GetLookAtRotation()).Yaw));
	}
		
	if (!CanTurn())
	{
		bHasForcedRotation = false;
		return;
	}

	bool bFoundAttribute;
	const auto FoundAttributeValue = UAbilitySystemBlueprintLibrary::GetFloatAttribute(GetOwner(), UMyAttributeSet::GetSpeedAttribute(), bFoundAttribute);
	auto CharacterSpeedMultiplier = 1.0f;
	if(bFoundAttribute)
		CharacterSpeedMultiplier = FoundAttributeValue;
	
	const FRotator CalculatedRotation = UKismetMathLibrary::RInterpTo_Constant(GetOwner()->GetActorRotation(),GetLookAtRotation(), DeltaTime, AIRotationSpeed * RotationSpeedMultiplier * CharacterSpeedMultiplier);
	SetCharacterRotation(CalculatedRotation);
}

void URotationComponent::RotateToInPlaceRequestedYaw(const float DeltaTime)
{
	if (const auto GameplayTagInterface = Cast<IGameplayTagAssetInterface>(GetOwner()))
	{
		if(GameplayTagInterface->HasAnyMatchingGameplayTags(BlockingGameplayTags))
			return;
	}

	if(RotateInPlaceTimeElapsed >= RotateInPlaceTime)
	{
		auto Rotation = GetOwner()->GetActorRotation();
		Rotation.Yaw = StartingYaw + RotateInPlaceYaw;
		OnLastMovementInputRotationChangeRequested.Broadcast(Rotation);
		bRotateInPlaceRequested = false;
		SetCharacterRotation(Rotation);
		return;
	}
	
	RotateInPlaceTimeElapsed += DeltaTime;
	if(RotateInPlaceTimeElapsed > RotateInPlaceTime)
		RotateInPlaceTimeElapsed = RotateInPlaceTime;
	
	auto Rotation = GetOwner()->GetActorRotation();
	Rotation.Yaw = StartingYaw + RotateInPlaceYaw * (RotateInPlaceTimeElapsed / RotateInPlaceTime);
	SetCharacterRotation(Rotation);
}

FRotator URotationComponent::GetLookAtRotation() const
{
	if(bHasForcedRotation)
		return ForcedRotation;
	
	FRotator LookAtRotation = UKismetMathLibrary::FindLookAtRotation(Target->GetActorLocation(), GetOwner()->GetActorLocation());
	LookAtRotation.Yaw += 180.0f;
	LookAtRotation.Pitch = 0.0f;
	
	return LookAtRotation;
}

bool URotationComponent::CanTurn() const
{
	if (const auto GameplayTagInterface = Cast<IGameplayTagAssetInterface>(GetOwner()))
	{
		if(GameplayTagInterface->HasAnyMatchingGameplayTags(BlockingGameplayTags))
			return false;
		if(bOnlyRotatesDuringActions)
		{
			if (GameplayTagInterface->HasAnyMatchingGameplayTags(ActionsTags))
				return true;
			return false;
		}
	}
	return true;
}

void URotationComponent::Reset()
{
	bHasForcedRotation = false;
	bWantsToStrafe = false;
	Target = nullptr;
}
