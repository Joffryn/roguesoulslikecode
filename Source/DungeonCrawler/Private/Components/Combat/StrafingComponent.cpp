
#include "Components/Combat/StrafingComponent.h"

#include "MyCharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "BaseCharacter.h"
#include "Enums.h"

UStrafingComponent::UStrafingComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	PossibleStrafeDirections.Add(EDirection::Left);
	PossibleStrafeDirections.Add(EDirection::Right);
	StrafeDirection = EDirection::Left;
}

void UStrafingComponent::SetTarget(AActor* NewTarget)
{
	Target = NewTarget;
}

void UStrafingComponent::StartStrafing()
{
	if (bWantsToStrafe)
		return;
	 
	bWantsToStrafe = true;

	if(PossibleStrafeDirections.Num() > 0)
		StrafeDirection = PossibleStrafeDirections[FMath::RandRange(0,  PossibleStrafeDirections.Num() - 1)];
	
}

FVector UStrafingComponent::GetStrafingDirection() const
{
	const FRotator LookAtRotation = UKismetMathLibrary::FindLookAtRotation(Target->GetActorLocation(), GetOwner()->GetActorLocation());
	switch (StrafeDirection)
	{
		case EDirection::Left:
			return UKismetMathLibrary::GetRightVector(LookAtRotation);
				
		case EDirection::Right:
			return -UKismetMathLibrary::GetRightVector(LookAtRotation);
					
		case EDirection::Back:
			return UKismetMathLibrary::GetForwardVector(LookAtRotation);
				
		case EDirection::Front:
			return -UKismetMathLibrary::GetForwardVector(LookAtRotation);

		case EDirection::FrontLeft:
			return -UKismetMathLibrary::GetForwardVector(LookAtRotation)+ UKismetMathLibrary::GetRightVector(LookAtRotation);
					
		case EDirection::FrontRight:
			return -UKismetMathLibrary::GetForwardVector(LookAtRotation) + -UKismetMathLibrary::GetRightVector(LookAtRotation);
						
		case EDirection::BackLeft:
			return UKismetMathLibrary::GetForwardVector(LookAtRotation) + UKismetMathLibrary::GetRightVector(LookAtRotation);
					
		case EDirection::BackRight:
			return UKismetMathLibrary::GetForwardVector(LookAtRotation) + -UKismetMathLibrary::GetRightVector(LookAtRotation);
		
	default:;
	}
	return FVector::ZeroVector;
}

void UStrafingComponent::StopStrafing()
{
	if (!bWantsToStrafe)
		return;

	bWantsToStrafe = false;
	OnStrafingEnded.Broadcast();
}

void UStrafingComponent::Reset()
{
	StopStrafing();
	bWantsToStrafe = false;
	Target = nullptr;
}

void UStrafingComponent::Strafe() const
{
	if(!Target)
		return;

	if (const auto OwnerBaseCharacter = Cast<ABaseCharacter>(GetOwner()))
		OwnerBaseCharacter->GetMyCharacterMovementComponent()->AddInputVector(GetStrafingDirection());
}

void UStrafingComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(bWantsToStrafe)
		Strafe();
}

