
#include "Components/Combat/PoiseComponent.h"

#include "MyAbilitySystemComponent.h"
#include "BaseCharacter.h"

UPoiseComponent::UPoiseComponent()
{
}

void UPoiseComponent::SetPoise(const EPoiseLevel NewPoiseLevel)
{
	PoiseLevel = NewPoiseLevel;
	HandlePoiseChange();
}

void UPoiseComponent::ChangePoiseLevel(const int32 Amount)
{
	ActualPoiseLevel += Amount;
	int32 CurrentPoiseValue = FMath::Clamp(ActualPoiseLevel, static_cast<int32>(EPoiseLevel::None), static_cast<int32>(EPoiseLevel::Unstaggerable));
	SetPoise(static_cast<EPoiseLevel>(CurrentPoiseValue));
}

void UPoiseComponent::BeginPlay()
{
	Super::BeginPlay();
	HandlePoiseChange();
	ActualPoiseLevel = static_cast<int32>(PoiseLevel);
}

void UPoiseComponent::HandlePoiseChange()
{
	//Only works on MyAbilitySystemComponent, consider refactoring
	if(const auto  AbilitySystemComponent =GetOwner()->FindComponentByClass<UMyAbilitySystemComponent>())
	{
		if(PoiseLevelToTagsMap.Num() == 0)
			return;
			
		auto DefaultPoiseTags = PoiseTags;
		const auto PoiseTag = *PoiseLevelToTagsMap.Find(PoiseLevel);
		DefaultPoiseTags.RemoveTag(PoiseTag);
		AbilitySystemComponent->HandleTagChange(PoiseTag, DefaultPoiseTags,FGameplayTagContainer());
	}
}

