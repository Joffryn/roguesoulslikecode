
#include "Components/Combat/EnchantComponent.h"

#include "AbilitySystemComponent.h"
#include "GameConfigComponent.h"
#include "RandomnessManager.h"
#include "DataTablesManager.h"
#include "GameplayEffect.h"
#include "Structs.h"
#include "Logs.h"

void UEnchantComponent::ApplyEnchantments()
{
	if(!CanApplyEnchantments())
		return;
	
	AddForcedEnchantments();
	CollectPossibleEnchantments();

	for(int i = 0; i < NumberOfRandomPowerUps; i++)
		AddRandomPowerUp();

	for(int i = 0; i < NumberOfRandomElements; i++)
		AddRandomElement();

	for(int i = 0; i < NumberOfRandomImmunities; i++)
		AddRandomImmunity();
}

bool UEnchantComponent::CanApplyEnchantments() const
{
	if(const auto GameConfigComponent = UGameConfigComponent::GetGameConfigComponent(this))
		return GameConfigComponent->bUseEnchants;

	return false;
}

void UEnchantComponent::CollectPossibleEnchantments()
{
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto EnchantsDataTable = DataTablesManager->Enchants)
			for(const auto RowName : EnchantsDataTable->GetRowNames())
				if(const auto Row = EnchantsDataTable->FindRow<FEnchantsParams>(RowName, TEXT("")))
					if(Row->bCanBeRandomlyApplied && !BlockedEnchantments.Contains(Row->Effect) && !AppliedEnchants.Contains(Row->Effect))
						switch (Row->Type)
						{
							case EEnchantmentTypes::PowerUp:
								{
									PossiblePowerUps.Add(Row->Effect);
									break;
								}

							case EEnchantmentTypes::Element:
								{
									PossibleElements.Add(Row->Effect);
									break;
								}
							
							case EEnchantmentTypes::Immunity:
								{
								PossibleImmunities.Add(Row->Effect);
									break;
								}
						}
}

void UEnchantComponent::AddRandomPowerUp()
{
	if(PossiblePowerUps.Num() > 0)
		if(const auto Enchant = PossiblePowerUps[URandomnessManager::GetRandomnessManager(this)->GetStream().RandRange(0, PossiblePowerUps.Num() -1)])
		{
			AddEnchantment(Enchant);
			PossiblePowerUps.Remove(Enchant);
		}
}

void UEnchantComponent::AddRandomElement()
{
	if(PossibleElements.Num() > 0)
		if(const auto Enchant = PossibleElements[URandomnessManager::GetRandomnessManager(this)->GetStream().RandRange(0, PossibleElements.Num() -1)])
		{
			AddEnchantment(Enchant);
			PossibleElements.Remove(Enchant);
		}
}

void UEnchantComponent::AddRandomImmunity()
{
	if(PossibleImmunities.Num() > 0)
		if(const auto Enchant = PossibleImmunities[URandomnessManager::GetRandomnessManager(this)->GetStream().RandRange(0, PossibleImmunities.Num() -1)])
		{
			AddEnchantment(Enchant);
			PossibleImmunities.Remove(Enchant);
		}
}

void UEnchantComponent::AddForcedEnchantments()
{
	for(auto Enchant : ForcedEnchantments)
		if(Enchant)
			AddEnchantment(Enchant);
}

void UEnchantComponent::AddEnchantment(const TSubclassOf<UEnchantment> EnchantmentClass)
{
	if(const auto AbilitySystemComponent = Cast<UAbilitySystemComponent>(GetOwner()->FindComponentByClass<UAbilitySystemComponent>()))
	{
		const FGameplayEffectContextHandle Handle;
		AbilitySystemComponent->ApplyGameplayEffectToSelf(EnchantmentClass.GetDefaultObject(), 0.0f, Handle);
		AppliedEnchants.Add(EnchantmentClass);
		UE_LOG(LogSpawning, Log, TEXT("%s added %s enchant"), *GetOwner()->GetName(), *EnchantmentClass->GetName());
	}
}
