
#include "Components/Combat/EnchantmentHandlingComponent.h"

#include "DamageResponseComponent.h"
#include "AbilitySystemComponent.h"
#include "BaseCharacter.h"

void UEnchantmentHandlingComponent::BeginPlay()
{
	Super::BeginPlay();

	BaseCharacterOwner = Cast<ABaseCharacter>(GetOwner());
	OwnerAbilitySystemComponent = GetOwner()->FindComponentByClass<UAbilitySystemComponent>();

	if(!BaseCharacterOwner)
		return;

	if(!OwnerAbilitySystemComponent)
		return;
	
	BindDelegates();
}

void UEnchantmentHandlingComponent::BindDelegates()
{
	const auto DamageResponseComponent = BaseCharacterOwner->DamageResponseComponent;
	DamageResponseComponent->OnDealedDamage.AddDynamic(this, &UEnchantmentHandlingComponent::OnAttackDamageDealed);
}

void UEnchantmentHandlingComponent::OnAttackDamageDealed(TSubclassOf<UDamageType> DamageType, AActor* DamageCauser, float Amount)
{
	//Life steal enchant
	if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(HealOnHitTag))
		if(const auto MyDamageTypeClass = Cast<UMyDamageType>(DamageType->GetDefaultObject())->GetClass())
			if(Cast<UMyDamageType>(MyDamageTypeClass->GetDefaultObject())->DamageTags.HasTag(DamageMeleeTag))
			{
				const FGameplayEffectContextHandle EffectContext;
				OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(RestoreHealthEffect, Amount, EffectContext);
			}
}

