
#include "CombatComponent.h"

#include "MyAbilitySystemComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "AbilitySystemComponent.h"
#include "GameplayTagContainer.h"
#include "AbilitySystemGlobals.h"
#include "GameConfigComponent.h"
#include "GameplayEffectTypes.h"
#include "Engine/EngineTypes.h"
#include "LookAtComponent.h"
#include "Engine/World.h"
#include "MeleeWeapon.h"
#include "DataAssets.h"
#include "Weapon.h"
#include "Enums.h"
#include "Logs.h"

UCombatComponent::UCombatComponent()
{
	RightHandAttachmentSocket = FName("RightHand");
	LeftHandAttachmentSocket = FName("LeftHand");
	RightLegAttachmentSocket = FName("RightLeg");
	LeftLegAttachmentSocket = FName("LeftLeg");
}

void UCombatComponent::Init()
{
	//Randomize weapon if not set
	if(!WeaponClass)
		if(PossibleWeaponClasses.Num() > 0)
			WeaponClass = PossibleWeaponClasses[FMath::RandRange(0, PossibleWeaponClasses.Num() -1)];
	
	EquipWeaponByClass(WeaponClass);
	
	if(!WeaponClass)
		WeaponClass = UGameConfigComponent::GetGameConfigComponent(this)->HandWeaponClass;

	SpawnAndAttachSecondaryWeapons();
	SpawnAndAttachLegWeapons();

	if(bStartWithNoWeapon)
		InstantlyEquipNoWeapon();
}

void UCombatComponent::BeginPlay()
{
	Super::BeginPlay();
	Init();
}

void UCombatComponent::EquipWeaponByClass(const TSubclassOf<AWeapon> WeaponClassToEquip)
{
	if(RightHandWeapon)
	{ 
		RightHandWeapon->K2_DestroyActor();
		RightHandWeapon = nullptr;
		UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->RemoveActiveGameplayEffect(EquippedRightWeaponEffectHandle);
	}
	
	if (LeftHandWeapon)
	{
		LeftHandWeapon->K2_DestroyActor();
		LeftHandWeapon = nullptr;
		UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->RemoveActiveGameplayEffect(EquippedLeftWeaponEffectHandle);
		OnWeaponEquipped.Broadcast(LeftHandWeapon);
	}
	
	RightHandWeapon = SpawnAndAttachWeapon(WeaponClassToEquip, EWeaponSocket::RightHand);

	if(!RightHandWeapon)
		RightHandWeapon = SpawnAndAttachWeapon(UGameConfigComponent::GetGameConfigComponent(this)->HandWeaponClass, EWeaponSocket::RightHand);

	if(RightHandWeapon)
	{
		if(RightHandWeapon->WeaponConfig && RightHandWeapon->WeaponConfig->WeaponLookAtConfig)
			if(const auto LookAtComponent = GetOwner()->FindComponentByClass<ULookAtComponent>())
				LookAtComponent->SetParamsFromPreset(RightHandWeapon->WeaponConfig->WeaponLookAtConfig);
		
		WeaponClass = WeaponClassToEquip;
		EquippedRightWeaponEffectHandle = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->BP_ApplyGameplayEffectToSelf(RightHandWeapon->EquippedGameplayEffectClass, 0.0f, FGameplayEffectContextHandle());
		OnWeaponEquipped.Broadcast(RightHandWeapon);
	}

	PostEquipActions();
}

void UCombatComponent::SwitchWeapon()
{
	if(RightHandWeapon)
		UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->RemoveActiveGameplayEffect(EquippedRightWeaponEffectHandle);
	
	UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->RemoveActiveGameplayEffect(EquippedRightWeaponEffectHandle);
	const auto SecondaryRightWeaponCache = SecondaryRightHandWeapon;
	SecondaryRightHandWeapon = RightHandWeapon;
	RightHandWeapon = SecondaryRightWeaponCache;

	if(RightHandWeapon)
		EquippedRightWeaponEffectHandle = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->BP_ApplyGameplayEffectToSelf(RightHandWeapon->EquippedGameplayEffectClass, 0.0f, FGameplayEffectContextHandle());

	if(LeftHandWeapon)
		UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->RemoveActiveGameplayEffect(EquippedLeftWeaponEffectHandle);
	
	UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->RemoveActiveGameplayEffect(EquippedLeftWeaponEffectHandle);
	const auto SecondaryLeftWeaponCache = SecondaryLeftHandWeapon;
	SecondaryLeftHandWeapon = LeftHandWeapon;
	LeftHandWeapon = SecondaryLeftWeaponCache;

	if(LeftHandWeapon)
		if(LeftHandWeapon->EquippedLeftHandGameplayEffectClass)
			EquippedLeftWeaponEffectHandle = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->BP_ApplyGameplayEffectToSelf(LeftHandWeapon->EquippedLeftHandGameplayEffectClass, 0.0f, FGameplayEffectContextHandle());

	if(RightHandWeapon)
		WeaponClass = RightHandWeapon->GetClass();
	
	if(SecondaryRightHandWeapon)
		SecondaryWeaponClass = SecondaryRightHandWeapon->GetClass();
		
	if (SecondaryLeftHandWeapon)
		SecondaryLeftWeaponClass = SecondaryLeftHandWeapon->GetClass();
	else
		SecondaryLeftWeaponClass = nullptr;

	UE_LOG(LogCombat, Log, TEXT("%s switched weapon from %s to %s"), *GetOwner()->GetName(), *SecondaryRightHandWeapon->GetName(), *RightHandWeapon->GetName());
	OnWeaponChanged.Broadcast();
}

void UCombatComponent::EquipNoWeapon()
{
	bNoWeaponEquipped = true;
	if(const auto OwnerAbilitySystemComponent =GetOwner()->FindComponentByClass<UMyAbilitySystemComponent>())
		OwnerAbilitySystemComponent->AddGameplayTag(NoWeaponTag.GetSingleTagContainer());

	OnWeaponChanged.Broadcast();
}

void UCombatComponent::UnequipNoWeapon()
{
	bNoWeaponEquipped = false;
	if(const auto OwnerAbilitySystemComponent = GetOwner()->FindComponentByClass<UMyAbilitySystemComponent>())
		OwnerAbilitySystemComponent->RemoveGameplayTag(NoWeaponTag.GetSingleTagContainer());

	OnWeaponChanged.Broadcast();
}

void UCombatComponent::InstantlySwitchWeapon()
{
	SheathWeapon(GetWeaponInSocket(EWeaponSocket::RightHand));
	SheathWeapon(GetWeaponInSocket(EWeaponSocket::LeftHand));
	SwitchWeapon();
	DrawWeapon(GetWeaponInSocket(EWeaponSocket::RightHand));
	DrawWeapon(GetWeaponInSocket(EWeaponSocket::LeftHand));
}

void UCombatComponent::InstantlyEquipNoWeapon()
{
	SheathWeapon(GetWeaponInSocket(EWeaponSocket::RightHand));
	SheathWeapon(GetWeaponInSocket(EWeaponSocket::LeftHand));
	EquipNoWeapon();
}

bool UCombatComponent::HasWeapon() const
{
	return IsValid(RightHandWeapon);
}

AWeapon* UCombatComponent::GetWeaponInSocket(const EWeaponSocket Socket) const
{
	switch (Socket)
	{
		case EWeaponSocket::RightHand:
			return RightHandWeapon;
		case EWeaponSocket::LeftHand:
			return LeftHandWeapon;		
		case EWeaponSocket::RightLeg:
			return RightLeg;			
		case EWeaponSocket::LeftLeg:
			return LeftLeg;
		default:
			return nullptr;
	}
}

AWeapon* UCombatComponent::GetMainWeapon() const
{
	if(bNoWeaponEquipped)
		return UGameConfigComponent::GetGameConfigComponent(this)->NoWeaponClass.GetDefaultObject();

	return GetWeaponInSocket(EWeaponSocket::RightHand);
}

AWeapon* UCombatComponent::GetSecondaryWeaponInSocket(const EWeaponSocket Socket) const
{
	switch (Socket)
	{
		case EWeaponSocket::RightHand:
			return SecondaryRightHandWeapon;
		case EWeaponSocket::LeftHand:
			return SecondaryLeftHandWeapon;		
		case EWeaponSocket::RightLeg:
			return RightLeg;			
		case EWeaponSocket::LeftLeg:
			return LeftLeg;
		default:
			return nullptr;
	}
}

void UCombatComponent::CalculateWeaponAttack(const EWeaponSocket WeaponSocket, float DamageMultiplier, int32 ExtraAttackStaggerStrength) const
{
	AWeapon* Weapon;
	switch (WeaponSocket)
	{
		case EWeaponSocket::RightHand:
			Weapon = RightHandWeapon;
			break;
		case EWeaponSocket::LeftHand:
			Weapon = LeftHandWeapon;
			break;
		case EWeaponSocket::RightLeg:
			Weapon = RightLeg;
			break;
		case EWeaponSocket::LeftLeg:
			Weapon = LeftLeg;
			break;
		default:
			Weapon = nullptr;
		}
	if(CurrentAttackDamageMultiplier != 0.0f)
		DamageMultiplier *= CurrentAttackDamageMultiplier;

	if(CurrentAttackExtraStaggerStrength)
		ExtraAttackStaggerStrength += CurrentAttackExtraStaggerStrength;

	 if (const auto MeleeWeapon = Cast<AMeleeWeapon>(Weapon))
		 MeleeWeapon->CalculateWeaponDamage(DamageMultiplier, ExtraAttackStaggerStrength);
}

void UCombatComponent::StartCharging(const float InChargingAnimRateScale, const float InNoChargingAnimRateScale, const float InTotalDuration, const float InDamageMultiplier, const int32 InExtraStaggerStrength)
{
	if(bIsAttackChargingBlocked == false)
	{
		bIsChargingAttack = true;
		if(const auto CharacterOwner = Cast<ACharacter>(GetOwner()))
		{
			if(const auto SkeletalMesh = CharacterOwner->GetMesh())
			{ 
				SkeletalMesh->GlobalAnimRateScale *= InChargingAnimRateScale;
				CachedChargingAnimRateScale = InChargingAnimRateScale;
				CachedNoChargingAnimRateScale = InNoChargingAnimRateScale;
				OnAttackChargingStarted.Broadcast();
			}
			CachedChargingStartWorldTime = UGameplayStatics::GetTimeSeconds(this);
			CachedChargingDamageMultiplier = InDamageMultiplier;
			CachedChargingExtraStagger = InExtraStaggerStrength;
			CachedChargingTotalDuration = InTotalDuration;
		}
	}
}

void UCombatComponent::StopCharging(const bool bFullyCharged)
{
	if(bIsChargingAttack)
	{
		if(const auto CharacterOwner = Cast<ACharacter>(GetOwner()))
		{
			if(bFullyCharged)
			{
				CurrentAttackDamageMultiplier = 1.0f + CachedChargingDamageMultiplier;
				//Only apply extra stagger if fully charged
				CurrentAttackExtraStaggerStrength = CachedChargingExtraStagger;
			}
			else
			{
				const float CurrentTime = UGameplayStatics::GetTimeSeconds(this);
				const float FinalAnimRateScale = CachedChargingAnimRateScale * CachedNoChargingAnimRateScale;
				const float ChargingProgress = (CurrentTime - CachedChargingStartWorldTime) / CachedChargingTotalDuration;
				CurrentAttackDamageMultiplier = 1.0f + CachedChargingDamageMultiplier * ChargingProgress * FinalAnimRateScale;
				//Only apply extra stagger if fully charged
				CurrentAttackExtraStaggerStrength = 0;
			}
			if(const auto SkeletalMesh = CharacterOwner->GetMesh())
			{
				SkeletalMesh->GlobalAnimRateScale /= CachedChargingAnimRateScale;
				CachedChargingAnimRateScale = 1.0f;
				bIsChargingAttack = false;
				OnAttackChargingFinished.Broadcast();
			}
		}
	}
	else
		if(const auto CharacterOwner = Cast<ACharacter>(GetOwner()))
			if(const auto SkeletalMesh = CharacterOwner->GetMesh())
			{
				SkeletalMesh->GlobalAnimRateScale /= CachedChargingAnimRateScale;
				CachedChargingAnimRateScale = 1.0f;
			}
}

void UCombatComponent::ClearCurrentAttackModifiers()
{
	CurrentAttackDamageMultiplier = 0.0f;
	CurrentAttackExtraStaggerStrength = 0;
}

TArray<AWeapon*> UCombatComponent::GetWeapons() const 
{
	TArray<AWeapon*> Weapons;
	if (RightHandWeapon)
		Weapons.Add(RightHandWeapon);
	if (LeftHandWeapon)
		Weapons.Add(LeftHandWeapon);
	if (RightLeg)
		Weapons.Add(RightLeg);
	if (LeftLeg)
		Weapons.Add(LeftLeg);

	return Weapons;
}

TArray<AWeapon*> UCombatComponent::GetSecondaryWeapons() const
{
	TArray<AWeapon*> Weapons;
	if(SecondaryRightHandWeapon)
		Weapons.Add(SecondaryRightHandWeapon);
	if(SecondaryLeftHandWeapon)
		Weapons.Add(SecondaryLeftHandWeapon);
	
	return Weapons;
}

void UCombatComponent::SheathWeapon(AWeapon* Weapon) const
{
	const auto CharacterOwner = Cast<ACharacter>(GetOwner());
	if(!CharacterOwner)
		return;
	

	if(Weapon)
	{
		UE_LOG(LogCombat, Log, TEXT("%s sheathed weapon %s"), *GetOwner()->GetName(), *Weapon->GetName());
		Weapon->AttachToComponent(CharacterOwner->GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, Weapon->SheathSocket);
		Weapon->SetActorRelativeLocation(Weapon->SheathLocationCorrection);
		Weapon->SetActorRelativeRotation(Weapon->SheathRotationCorrection);
	}
}

void UCombatComponent::DrawWeapon(AWeapon* Weapon) const
{
	const auto CharacterOwner = Cast<ACharacter>(GetOwner());
	if(!CharacterOwner)
		return;
	
	if(Weapon)
	{
		UE_LOG(LogCombat, Log, TEXT("%s drawn weapon %s"), *GetOwner()->GetName(), *Weapon->GetName());
		Weapon->AttachToComponent(CharacterOwner->GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, Weapon->EquipSocket);
		Weapon->SetActorRelativeLocation(Weapon->LocationCorrection);
		Weapon->SetActorRelativeRotation(Weapon->RotationCorrection);
	}
}

AWeapon* UCombatComponent::SpawnAndAttachWeapon(const TSubclassOf< AWeapon> WeaponClassToSpawn, const EWeaponSocket WeaponSocket) const
{
	if (!IsValid(WeaponClassToSpawn))
		return nullptr;

	const auto CharacterOwner = Cast<ACharacter>(GetOwner());
	if(!CharacterOwner)
		return nullptr;

	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Owner = CharacterOwner;
	SpawnInfo.Instigator = CharacterOwner;
	const auto Weapon = GetWorld()->SpawnActor<AWeapon>(WeaponClassToSpawn, FVector::ZeroVector, FRotator::ZeroRotator, SpawnInfo);
	FName AttachmentSocket;
	switch (WeaponSocket)
	{
		case EWeaponSocket::RightHand:
			AttachmentSocket = RightHandAttachmentSocket;
			break;
		case EWeaponSocket::LeftHand:
			AttachmentSocket = LeftHandAttachmentSocket;
			break;
		case EWeaponSocket::RightLeg:
			AttachmentSocket = RightLegAttachmentSocket;
			break;
		case EWeaponSocket::LeftLeg:
			AttachmentSocket = LeftLegAttachmentSocket;
			break;
		case EWeaponSocket::Secondary:
			AttachmentSocket = Weapon->SheathSocket;
			break;
		case EWeaponSocket::SecondaryLeft:
			Weapon->SheathSocket = Weapon->LeftSheathSocket;
			Weapon->EquipSocket = LeftHandAttachmentSocket;
			AttachmentSocket = Weapon->LeftSheathSocket;
			break;
	}

	if(Weapon)
	{
		if(WeaponSocket != EWeaponSocket::Secondary && WeaponSocket != EWeaponSocket::SecondaryLeft)
		{
			Weapon->SetActorRelativeLocation(Weapon->LocationCorrection);
			Weapon->SetActorRelativeRotation(Weapon->RotationCorrection);
		}
		Weapon->AttachToComponent(CharacterOwner->GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, AttachmentSocket);
	}
	
	return Weapon;
}

void UCombatComponent::SpawnAndAttachLegWeapons()
{
	RightLeg = SpawnAndAttachWeapon(UGameConfigComponent::GetGameConfigComponent(this)->LegWeaponClass, EWeaponSocket::RightLeg);
	OnWeaponEquipped.Broadcast(RightLeg);
	LeftLeg = SpawnAndAttachWeapon(UGameConfigComponent::GetGameConfigComponent(this)->LegWeaponClass, EWeaponSocket::LeftLeg);
	OnWeaponEquipped.Broadcast(LeftLeg);
}

void UCombatComponent::SpawnAndAttachSecondaryWeapons()
{
	if(!SecondaryWeaponClass)
		SecondaryWeaponClass = UGameConfigComponent::GetGameConfigComponent(this)->HandWeaponClass;
	
	if(SecondaryWeaponClass)
		SecondaryRightHandWeapon = SpawnAndAttachWeapon(SecondaryWeaponClass, EWeaponSocket::Secondary);
	
	if(SecondaryLeftWeaponClass)
		SecondaryLeftHandWeapon = SpawnAndAttachWeapon(SecondaryLeftWeaponClass, EWeaponSocket::SecondaryLeft);	
}

void UCombatComponent::PostEquipActions() 
{
	if (!RightHandWeapon)
		return;

	RightHandWeapon->SetOwner(GetOwner());
	switch (RightHandWeapon->WeaponConfig->WeaponType)
	{
		case EWeaponType::TwoHanded:
			break;
		case EWeaponType::Katana:
			break;
		case EWeaponType::Rapier:
		case EWeaponType::OneHandedAndShield:
			LeftHandWeapon = SpawnAndAttachWeapon(RightHandWeapon->GetRandomSecondWeaponClass(), EWeaponSocket::LeftHand);
			if(LeftHandWeapon->EquippedLeftHandGameplayEffectClass)
				EquippedLeftWeaponEffectHandle = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->BP_ApplyGameplayEffectToSelf(LeftHandWeapon->EquippedLeftHandGameplayEffectClass, 0.0f, FGameplayEffectContextHandle());
			OnWeaponEquipped.Broadcast(LeftHandWeapon);
			break;
		case EWeaponType::Dual:
			LeftHandWeapon = SpawnAndAttachWeapon(RightHandWeapon->GetRandomSecondWeaponClass(), EWeaponSocket::LeftHand);
			LeftHandWeapon->EquipSocket = LeftHandAttachmentSocket;
			LeftHandWeapon->SheathSocket = LeftHandWeapon->LeftSheathSocket;
			if (LeftHandWeapon->EquippedLeftHandGameplayEffectClass)
				EquippedLeftWeaponEffectHandle = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->BP_ApplyGameplayEffectToSelf(LeftHandWeapon->EquippedLeftHandGameplayEffectClass, 0.0f, FGameplayEffectContextHandle());
			OnWeaponEquipped.Broadcast(LeftHandWeapon);
			break;
		case EWeaponType::Giant:
			if(RightHandWeapon->GetRandomSecondWeaponClass())
			{
				LeftHandWeapon = SpawnAndAttachWeapon(RightHandWeapon->GetRandomSecondWeaponClass(), EWeaponSocket::LeftHand);
				if (LeftHandWeapon->EquippedLeftHandGameplayEffectClass)
					EquippedLeftWeaponEffectHandle = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(GetOwner())->BP_ApplyGameplayEffectToSelf(LeftHandWeapon->EquippedLeftHandGameplayEffectClass, 0.0f, FGameplayEffectContextHandle());
				OnWeaponEquipped.Broadcast(LeftHandWeapon);
			}
			break;
		case EWeaponType::Assassin:
			break;
		case EWeaponType::Shield:
			break;
		case EWeaponType::Greatsword:
			break;
		case EWeaponType::BodyPart:
			break;
		case EWeaponType::OneHanded:
			break;
		case EWeaponType::OrientalSpear:
			break;
		case EWeaponType::Whip:
			break;
		default:
			break;
	}
	if(!LeftHandWeapon)
	{
		LeftHandWeapon = SpawnAndAttachWeapon(UGameConfigComponent::GetGameConfigComponent(this)->HandWeaponClass, EWeaponSocket::LeftHand);
		OnWeaponEquipped.Broadcast(LeftHandWeapon);
	}
}
