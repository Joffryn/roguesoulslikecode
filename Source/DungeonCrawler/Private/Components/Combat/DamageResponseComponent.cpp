
#include "Components/Combat/DamageResponseComponent.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "Perception/AISense_Damage.h"
#include "Kismet/KismetMathLibrary.h"
#include "ReactionSolverComponent.h"
#include "PlayerFeedbackComponent.h"
#include "AbilitySystemComponent.h"
#include "DamageTakingComponent.h"
#include "GameplayAbilityTypes.h"
#include "GameConfigComponent.h"
#include "MyPlayerController.h"
#include "MyGameplayStatics.h"
#include "CombatComponent.h"
#include "MyAttributeSet.h"
#include "BaseCharacter.h"
#include "MyDamageType.h"
#include "CritSolver.h"
#include "DataAssets.h"
#include "Weapon.h"
#include "Logs.h"

bool UDamageResponseComponent::CanTakeDamage(const UDamageType* DamageType) const
{
	if(DamageType)
		if(const auto MyDamageType = Cast<UMyDamageType>(DamageType))
			if(MyDamageType->DamageTags.HasTag(DamageIgnoreInvincibilityTag))
				return true;
	
	if(OwnerAbilitySystemComponent)
		return !OwnerAbilitySystemComponent->HasMatchingGameplayTag(InvincibilityTag);
	
	return true;
}

bool UDamageResponseComponent::ShouldParry(const TSubclassOf<UMyDamageType> DamageType, const FVector& AttackLocation, const AActor* DamageCauser) const
{
	//Isn't in parrying state
	if(!OwnerAbilitySystemComponent->HasMatchingGameplayTag(ParryingTag))
		return false;

	//Unparryable damage type
	if(Cast<UMyDamageType>(DamageType->GetDefaultObject())->DamageTags.HasTag(UnblockableTag))
		return false;
	
	//Can the owner parry unblockable attack
	if(!OwnerAbilitySystemComponent->HasMatchingGameplayTag(UnblockableParryUpgradeTag))
	{
		//Unparryable attack
		if(const auto CauserCombatComponent = DamageCauser->FindComponentByClass<UCombatComponent>())
			if (CauserCombatComponent->CurrentAttackExtraTags.HasTagExact(UnblockableTag))
			{
				UE_LOG(LogCombat, Log, TEXT("%s breaked parry of %s"), *DamageCauser->GetName(), *GetOwner()->GetName());
				return false;
			}
	}
	
	//Angle to great
	if(GetYawAngleBetweenOwnerAndLocation(AttackLocation) > PreventingAngle)
		return false;

	return true;
}

bool UDamageResponseComponent::ShouldBlock(const TSubclassOf<UMyDamageType> DamageType, const FVector& AttackLocation, const AActor* DamageCauser) const
{
	//Isn't in blocking state
	if(!OwnerAbilitySystemComponent->HasMatchingGameplayTag(BlockingTag))
		return false;

	//Unparryable damage type
	if(Cast<UMyDamageType>(DamageType->GetDefaultObject())->DamageTags.HasTag(UnblockableTag))
		return false;

	//Unparryable attack
	if(DamageCauser)
		if (const auto CauserCombatComponent = DamageCauser->FindComponentByClass<UCombatComponent>())
			if (CauserCombatComponent->CurrentAttackExtraTags.HasTagExact(UnblockableTag))
			{
				UE_LOG(LogCombat, Log, TEXT("%s breaked block of %s"), *DamageCauser->GetName(), *GetOwner()->GetName());
				return false;
			}
		
	//Angle to great
	if(GetYawAngleBetweenOwnerAndLocation(AttackLocation) > PreventingAngle)
		return false;

	//Ranged attack can only be blocked if specific tag is applied
	if(Cast<UMyDamageType>(DamageType->GetDefaultObject())->DamageTags.HasTag(RangedDamageTag))
	{
		if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(CanBlockProjectilesTag))
			return true;
		return false;
	}

	return true;
}

bool UDamageResponseComponent::ShouldBackstab(const TSubclassOf<UMyDamageType> DamageType, const FVector& AttackLocation, const AActor* DamageCauser) const
{
	//No damage causer - no backstab
	if(!DamageCauser)
		return false;
		
	//No upgrade available
	if(const auto CauserAbilitySystemComponent = DamageCauser->FindComponentByClass<UAbilitySystemComponent>())
		if(!CauserAbilitySystemComponent->HasMatchingGameplayTag(BackstabUpgradeTag))
			return false;

	//Only for melee
	if(!Cast<UMyDamageType>(DamageType->GetDefaultObject())->DamageTags.HasTag(MeleeDamageTag))
		return false;

	//Angle in range
	if(GetYawAngleBetweenOwnerAndLocation(AttackLocation) > MinBackstabAngle)
		return true;
	
	return false;
}

void UDamageResponseComponent::OnOwnerAnyDamageTaken(AActor* Actor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	if(!CanTakeDamage(DamageType))
		return;

	LastDamageTypeTaken = DamageType->GetClass();
	
	const bool bIsCrit = UCritSolver::GetCritSolver(this)->IsAttackCrit(DamageCauser, Actor, DamageType);
	
	if(bIsCrit)
		if(DamageCauser)
			if (const auto CauserAbilitySystemComponent = DamageCauser->FindComponentByClass<UAbilitySystemComponent>())
				Damage *= CauserAbilitySystemComponent->GetNumericAttribute(UMyAttributeSet::GetCritDamageMultiplierAttribute());

	if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(HealthyTag))
	{
		if(DamageCauser)
			if (const auto CauserAbilitySystemComponent = DamageCauser->FindComponentByClass<UAbilitySystemComponent>())
				if(CauserAbilitySystemComponent->HasMatchingGameplayTag(HealthyMeleeAttackExtraDamageTag))
					//Consider using an attribute
					Damage *= 2.0f;

		if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(HealthyDamageReductionTag))
			Damage /= 2.0f;
	}

	//Global damage multipliers
	if(const auto GameConfigComponent = UGameConfigComponent::GetGameConfigComponent(this))
	{
		if(BaseCharacterOwner)
		{
			if(BaseCharacterOwner->GetController() == AMyPlayerController::GetMyPlayerController(this))
				Damage *= GameConfigComponent->DamageMultiplierPlayer;
			else
				Damage *= GameConfigComponent->DamageMultiplierAI;
		}
		else
			Damage *= GameConfigComponent->DamageMultiplierAI;
	}

	//Resistances
	if(const auto MyDamageType = Cast<UMyDamageType>(DamageType))
	{
		if(MyDamageType->DamageTags.HasTag(PhysicalDamageTag))
		{
			float PhysicalResistance=  OwnerAbilitySystemComponent->GetNumericAttribute(UMyAttributeSet::GetPhysicalDamageResistanceAttribute());
			if(PhysicalResistance > 1.0f)
				PhysicalResistance = 1.0f;

			Damage *= 1.0f - PhysicalResistance;
		}
		if(MyDamageType->DamageTags.HasTag(MagicalDamageTag))
		{
			float MagicalResistance=  OwnerAbilitySystemComponent->GetNumericAttribute(UMyAttributeSet::GetMagicalDamageResistanceAttribute());
			if(MagicalResistance > 1.0f)
				MagicalResistance = 1.0f;

			Damage *= 1.0f - MagicalResistance;
		}
	}
	
	const FGameplayEffectContextHandle EffectContext;
	if(BaseCharacterOwner)
	{
		if(BaseCharacterOwner->GetTeamAttitudeTowardsActor(DamageCauser) == ETeamAttitude::Type::Hostile)
			UAISense_Damage::ReportDamageEvent(this, BaseCharacterOwner, DamageCauser,  Damage, DamageCauser->GetActorLocation(), BaseCharacterOwner->GetActorLocation());
			
		if(BaseCharacterOwner->IsAlive())
		{
			OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(LoseHealthEffect, Damage, EffectContext);
			OnTakenDamage.Broadcast(DamageType->GetClass(), DamageCauser, Damage);
		}

		if(DamageCauser)
		{
			if (const auto CauserAbilitySystemComponent = DamageCauser->FindComponentByClass<UAbilitySystemComponent>())
			{
				if(CauserAbilitySystemComponent->HasMatchingGameplayTag(ManaBurnTag))
				{
					OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(LoseManaEffect, Damage, EffectContext);
					if(GetOwner() == ABaseCharacter::GetMyPlayerCharacter(this))
						if(const auto PlayerFeedbackComponent = AMyPlayerController::GetMyPlayerController(this)->PlayerFeedbackComponent)
							PlayerFeedbackComponent->OnManaBurned();
				}
				
				if(CauserAbilitySystemComponent->HasMatchingGameplayTag(StaminaDrainTag))
				{
					OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(LoseStaminaEffect, Damage, EffectContext);
					if(GetOwner() == ABaseCharacter::GetMyPlayerCharacter(this))
						if(const auto PlayerFeedbackComponent = AMyPlayerController::GetMyPlayerController(this)->PlayerFeedbackComponent)
							PlayerFeedbackComponent->OnStaminaDrained();
				}
			}
		}		
	}
	else
		OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(LoseHealthEffect,  Damage, EffectContext);
		
	const bool bBaseCharacterDead = BaseCharacterOwner && !BaseCharacterOwner->IsAlive();
	
	if(const auto MyDamageType = Cast<UMyDamageType>(DamageType))
	{
		if(DamageCauser)
		{
			if(const auto CauserDamageResponseComponent = DamageCauser->FindComponentByClass<UDamageResponseComponent>())
			{
				if(bIsCrit)
				{
					FVector HitLocation = BaseCharacterOwner->GetActorLocation();
					if(LastPointDamageHitLocation != FVector::ZeroVector)
						HitLocation = (LastPointDamageHitLocation + BaseCharacterOwner->GetActorLocation()) / 2.f;

					FGameplayCueParameters GameplayCueParameters = GetGameplayCueParameters(GetOwner(), HitLocation);
					GameplayCueParameters.RawMagnitude = UKismetMathLibrary::MapRangeClamped(Damage, PointHitDamageRange.X, PointHitDamageRange.Y, PointHitScaleRange.X, PointHitScaleRange.Y);
					GameplayCueParameters. Normal = DamageCauser->GetActorLocation();
		
					UMyGameplayStatics::ExecuteGameplayCue(GetOwner()->FindComponentByClass<UAbilitySystemComponent>(), CritGameplayCueTag, GameplayCueParameters);

					CauserDamageResponseComponent->OnCritDamageDealed.Broadcast(DamageType->GetClass(), DamageCauser, Damage);
					UE_LOG(LogCombat, Log, TEXT("%s critically striked %s"), *DamageCauser->GetName(), *GetOwner()->GetName());
					OnBeingCrit.Broadcast();
				}
				if(MyDamageType->DamageTags.HasTag(MeleeDamageTag))
				{
					if(!bIsCrit)
					{
						CauserDamageResponseComponent->OnHit.Broadcast();
						OnBeingHit.Broadcast();
					}
				}
				else
				{
					if(MyDamageType->HitStaggerStrength != EAttackStaggerStrength::None)
						OnBeingHit.Broadcast();
				}
			
				CauserDamageResponseComponent->OnDealedDamage.Broadcast(DamageType->GetClass(), DamageCauser,Damage);
				if (bBaseCharacterDead && !bHasBroadcastedOwnerDeath)
					CauserDamageResponseComponent->OnKill.Broadcast(BaseCharacterOwner);
			}
			else
			{
				if(MyDamageType->HitStaggerStrength != EAttackStaggerStrength::None)
					OnBeingHit.Broadcast();
			}
			UE_LOG(LogCombat, Log, TEXT("%s dealed %s %s damage to %s"), *DamageCauser->GetName(), *DamageType->GetName(), *FString::SanitizeFloat(Damage), *GetOwner()->GetName());
		}
		else
		{
			if(MyDamageType->HitStaggerStrength != EAttackStaggerStrength::None)
				OnBeingHit.Broadcast();
		}
		
		if(MyDamageType->bDamagesPoise)
		{
			const float Level = Damage * MyDamageType->PoiseDamageMultiplier * OwnerAbilitySystemComponent->GetNumericAttribute(UMyAttributeSet::GetBlockingPoiseCostMultiplierAttribute());
			OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(LosePoiseEffect, Level, EffectContext);
			OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(BlockPoiseRegenEffect, 1.0f, EffectContext);
		}
	}

	if(bBaseCharacterDead && !bHasBroadcastedOwnerDeath)
	{
		if (DamageCauser)
			UE_LOG(LogCombat, Log, TEXT("%s killed %s"), *DamageCauser->GetName(), *GetOwner()->GetName());
			
		const FGameplayEventData Payload;
		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(GetOwner(), DeathReactionTag, Payload);
		OnDeath.Broadcast();
		bHasBroadcastedOwnerDeath = true;
		ShowDamageInfo(Damage, DamageType, bIsCrit);
	}

	//Don't show damage numbers when the owner is dead
	if(BaseCharacterOwner)
		if(!BaseCharacterOwner->IsAlive())
			return;

	ShowDamageInfo(Damage, DamageType, bIsCrit);
}

float UDamageResponseComponent::OnOwnerPointDamageTaken(float Damage, const FVector HitLocation, const FVector HitFromDirection, const TSubclassOf<UDamageType> DamageTypeClass, AActor* DamageCauser)
{
	LastPointDamageHitLocation = HitLocation;
	const auto MyDamageTypeClass = Cast<UMyDamageType>(DamageTypeClass->GetDefaultObject())->GetClass();
	const auto MyDamageType = Cast<UMyDamageType>(MyDamageTypeClass->GetDefaultObject());
	
	if(!MyDamageType)
		return Damage;
	
	//In the case of melee attacks use causer location instead of hit, using hit sometimes pierce blocks when attacked by bigger enemies which weapons can hit entire character and not just front
	FVector AttackLocation = HitLocation;
	if (Cast<UMyDamageType>(MyDamageType)->DamageTags.HasTag(MeleeDamageTag))
		AttackLocation = DamageCauser->GetActorLocation();

	//Make sure that the hit cue is not spawn lower that the centre of the character as it looks off
	if(AttackLocation.Z < GetOwner()->GetActorLocation().Z)
		AttackLocation.Z = GetOwner()->GetActorLocation().Z;
	
	if(ShouldParry(MyDamageTypeClass, AttackLocation, DamageCauser))
	{
		Parry(MyDamageTypeClass, DamageCauser ,AttackLocation, Damage);
		return 0.0f;
	}
	
	if(ShouldBlock(MyDamageTypeClass, AttackLocation, DamageCauser))
	{
		Block(Damage, MyDamageTypeClass, DamageCauser ,AttackLocation);
		return 0.0f;
	}

	if(CanTakeDamage(Cast<UDamageType>(DamageTypeClass->GetDefaultObject())))
	{
		FGameplayCueParameters GameplayCueParameters = GetGameplayCueParameters(GetOwner(), HitLocation);
		GameplayCueParameters.RawMagnitude = UKismetMathLibrary::MapRangeClamped(Damage, PointHitDamageRange.X, PointHitDamageRange.Y, PointHitScaleRange.X, PointHitScaleRange.Y);
		GameplayCueParameters. Normal = HitFromDirection;
    	
		UMyGameplayStatics::ExecuteGameplayCue(GetOwner()->FindComponentByClass<UAbilitySystemComponent>(), PointHitGameplayCueTag, GameplayCueParameters);
		if (ShouldBackstab(MyDamageTypeClass, AttackLocation, DamageCauser))
		{
			//Consider using an attribute
			UE_LOG(LogCombat, Log, TEXT("%s backstabbed striked %s"), *DamageCauser->GetName(), *GetOwner()->GetName());
			Damage *= 2.0f;
		}
		
		const FGameplayEventData Payload;
		if (const auto OwnerCombatComponent = GetOwner()->FindComponentByClass<UCombatComponent>())
		{
			if (const auto Weapon = OwnerCombatComponent->GetRightHandWeapon())
			{
				const EDirection Direction = UMyGameplayStatics::GetHitDirectionForTargetFromOrigin(
					HitLocation, GetOwner(), Weapon->WeaponReactionSet->HitMapType);
				if (MyDamageType->HitStaggerStrength != EAttackStaggerStrength::None)
					UReactionSolverComponent::GetReactionSolver(this)->SendStaggerGameplayEvent(
						GetOwner(), MyDamageType->HitStaggerStrength, DamageCauser, Direction);
			}
		}

		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(GetOwner(), MyDamageType->HitTag, Payload);

		return Damage;
	}
	
	InvincibleHit(MyDamageTypeClass, DamageCauser, HitLocation);
	return 0.0f;
}

float UDamageResponseComponent::OnOwnerRadialDamageTaken(const float Damage, const FVector Origin, const TSubclassOf<UDamageType> DamageTypeClass, AActor* DamageCauser)
{
	LastPointDamageHitLocation = FVector::ZeroVector;
	const auto MyDamageTypeClass = Cast<UMyDamageType>(DamageTypeClass->GetDefaultObject())->GetClass();
	if(ShouldBlock(MyDamageTypeClass, Origin, DamageCauser))
	{
		Block(Damage, MyDamageTypeClass, DamageCauser ,Origin);
		return 0.0f;
	}

	if(CanTakeDamage(Cast<UDamageType>(DamageTypeClass->GetDefaultObject())))
	{
		FGameplayCueParameters GameplayCueParameters = GetGameplayCueParameters(GetOwner(), GetOwner()->GetActorLocation());
		GameplayCueParameters.RawMagnitude = UKismetMathLibrary::MapRangeClamped(Damage, PointHitDamageRange.X, PointHitDamageRange.Y, PointHitScaleRange.X, PointHitScaleRange.Y);
		
		UMyGameplayStatics::ExecuteGameplayCue(GetOwner()->FindComponentByClass<UAbilitySystemComponent>(), PointHitGameplayCueTag, GameplayCueParameters);

		if (const auto MyDamageType = Cast<UMyDamageType>(MyDamageTypeClass->GetDefaultObject()))
		{
			const FGameplayEventData Payload;
			if (const auto OwnerCombatComponent = GetOwner()->FindComponentByClass<UCombatComponent>())
				if (const auto Weapon = OwnerCombatComponent->GetRightHandWeapon())
				{
					const EDirection Direction = UMyGameplayStatics::GetHitDirectionForTargetFromOrigin(Origin, GetOwner(), Weapon->WeaponReactionSet->HitMapType);
					if (MyDamageType->HitStaggerStrength != EAttackStaggerStrength::None)
						UReactionSolverComponent::GetReactionSolver(this)->SendStaggerGameplayEvent(GetOwner(), MyDamageType->HitStaggerStrength, DamageCauser, Direction);
				}

			UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(GetOwner(), MyDamageType->HitTag, Payload);
		}
		return Damage;
	}
	
	InvincibleHit(MyDamageTypeClass, DamageCauser, GetOwner()->GetActorLocation());
	return 0.0f;
}

void UDamageResponseComponent::BeginPlay()
{
	Super::BeginPlay();

	BaseCharacterOwner = Cast<ABaseCharacter>(GetOwner());
	OwnerAbilitySystemComponent = GetOwner()->FindComponentByClass<UAbilitySystemComponent>();
	
	GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UDamageResponseComponent::OnOwnerAnyDamageTaken);
}

void UDamageResponseComponent::ShowDamageInfo(const float Damage, const UDamageType* DamageType, const bool bIsCrit) const
{
	if(const auto  GameConfig = UGameConfigComponent::GetGameConfigComponent(this))
		if(!GameConfig->bShowDamageNumbers)
			return;

	if(const auto OwnerDamageTakingComponent = GetOwner()->FindComponentByClass<UDamageTakingComponent>())
	{
		if(const auto MyDamageType = Cast<UMyDamageType>(DamageType))
			if(MyDamageType->bCanShowNumbers)
				OwnerDamageTakingComponent->AddDamageInfo(MyDamageType->Color, Damage, bIsCrit);	
	}
}

FGameplayCueParameters UDamageResponseComponent::GetGameplayCueParameters(AActor* DamageCauser, const FVector& HitLocation) const
{
	FVector Location = HitLocation;
	if(Location != GetOwner()->GetActorLocation())
		if(const auto CharacterOwner = Cast<ACharacter>( GetOwner()))
			if(const auto Mesh = CharacterOwner->GetMesh())
			{
				Mesh->GetClosestPointOnCollision(HitLocation, Location);
				Location.Z = HitLocation.Z;
			}
	
	FGameplayCueParameters GameplayCueParameters;
	GameplayCueParameters.Location = Location;
	GameplayCueParameters.EffectCauser = DamageCauser;
	GameplayCueParameters.Instigator = DamageCauser;
	GameplayCueParameters.SourceObject = DamageCauser;
	return GameplayCueParameters;
}


void UDamageResponseComponent::InvincibleHit(const TSubclassOf<UMyDamageType> DamageType, AActor* DamageCauser, const FVector& HitLocation) const
{
	UMyGameplayStatics::ExecuteGameplayCue(GetOwner()->FindComponentByClass<UAbilitySystemComponent>(), InvincibleHitGameplayCueTag, GetGameplayCueParameters(DamageCauser, HitLocation));
	
	if(Cast<UMyDamageType>(DamageType->GetDefaultObject())->DamageTags.HasTag(MeleeDamageTag))
		if(DamageCauser)
			if (const auto CauserDamageResponseComponent = DamageCauser->FindComponentByClass<UDamageResponseComponent>())
			{
				CauserDamageResponseComponent->OnInvincibleHit.Broadcast();
				UE_LOG(LogCombat, Log, TEXT("%s dodged attack from %s"), *DamageCauser->GetName(), *GetOwner()->GetName());
			}
}

void UDamageResponseComponent::Parry(const TSubclassOf<UMyDamageType> DamageType, AActor* DamageCauser, const FVector& HitLocation, const float Damage) const
{
	if(Cast<UMyDamageType>(DamageType->GetDefaultObject())->DamageTags.HasTag(MeleeDamageTag))
	{
		const FGameplayEventData Payload;
		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(DamageCauser, ParriedReactionTag, Payload);
		if(DamageCauser)
			if (const auto CauserDamageResponseComponent = DamageCauser->FindComponentByClass<UDamageResponseComponent>())
			{
				CauserDamageResponseComponent->OnBeingParried.Broadcast();
				UE_LOG(LogCombat, Log, TEXT("%s parried %s"), *GetOwner()->GetName(), *DamageCauser->GetName());
			}
	}
	
	UMyGameplayStatics::ExecuteGameplayCue(GetOwner()->FindComponentByClass<UAbilitySystemComponent>(), ParryGameplayCueTag, GetGameplayCueParameters(DamageCauser, HitLocation));
	OnParry.Broadcast(DamageCauser, DamageType, Damage);
}

void UDamageResponseComponent::Block(const float Damage, const TSubclassOf<UMyDamageType> DamageType, AActor* DamageCauser, const FVector HitLocation) const
{
	if(const auto MyDamageTypeClass = Cast<UMyDamageType>(DamageType->GetDefaultObject())->GetClass())
	{
		const auto MyDamageType = Cast<UMyDamageType>(MyDamageTypeClass->GetDefaultObject());

		if(MyDamageType->DamageTags.HasTag(MeleeDamageTag))
		{
			const FGameplayEventData Payload;
			UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(DamageCauser, BlockedReactionTag, Payload);
			if(DamageCauser)
				if (const auto CauserDamageResponseComponent = DamageCauser->FindComponentByClass<UDamageResponseComponent>())
				{
					CauserDamageResponseComponent->OnBeingBlocked.Broadcast();
					UE_LOG(LogCombat, Log, TEXT("%s blocked %s"), *GetOwner()->GetName(), *DamageCauser->GetName());
				}
		}
		if(MyDamageType->bDamagesPoise)
		{
			const FGameplayEffectContextHandle EffectContext;
			const float Level = Damage * MyDamageType->PoiseDamageMultiplier * OwnerAbilitySystemComponent->GetNumericAttribute(UMyAttributeSet::GetBlockingPoiseCostMultiplierAttribute());
			OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(LosePoiseEffect, Level, EffectContext);
			OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(BlockPoiseRegenEffect,  1.0f, EffectContext);
		}
	}

	FGameplayEventData Payload;
	Payload.Instigator = DamageCauser;
	UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(GetOwner(), DamageHitTag, Payload);

	//Free block upgrade
	if(!OwnerAbilitySystemComponent->HasMatchingGameplayTag(StaminaFreeBlockTag))
	{
		const FGameplayEffectContextHandle EffectContext;
		const float Level = OwnerAbilitySystemComponent->GetNumericAttribute(UMyAttributeSet::GetBlockingStaminaCostMultiplierAttribute()) * Damage;
		OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(LoseStaminaEffect, Level, EffectContext);
	}
	
	UMyGameplayStatics::ExecuteGameplayCue(GetOwner()->FindComponentByClass<UAbilitySystemComponent>(), BlockGameplayCueTag, GetGameplayCueParameters(DamageCauser, HitLocation));
	OnBlock.Broadcast();
}

float UDamageResponseComponent::GetYawAngleBetweenOwnerAndLocation(const FVector& Location) const
{
	return FMath::Abs(UMyGameplayStatics::GetYawAngleBetweenOriginAndTarget(Location, GetOwner()));
}
