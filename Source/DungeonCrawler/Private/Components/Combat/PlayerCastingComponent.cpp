
#include "Components/Combat/PlayerCastingComponent.h"

#include "MyAbilitySystemComponent.h"

UPlayerCastingComponent::UPlayerCastingComponent()
{
	for(int32 i = 0; i < NumberOfSkills; i++)
	{
		AbilitiesClasses.Add(nullptr);
		AbilitiesHandles.Add(FGameplayAbilitySpecHandle());
	}
}

void UPlayerCastingComponent::BeginPlay()
{
	Super::BeginPlay();

	CollectInfos();
	SetupAbilities();
}

UGameplayAbility* UPlayerCastingComponent::GetAbilityAtIndex(const int32 Index)
{
	if(!OwnerAbilitySystemComponent)
		return nullptr;

	//Ignore not valid index
	if(AbilitiesHandles.Num() <= Index)
		return nullptr;

	return OwnerAbilitySystemComponent->GetAbilityInstance(AbilitiesHandles[Index]);
}

void UPlayerCastingComponent::AddAbility(const TSubclassOf<UGameplayAbility> AbilityClass)
{
	for(int32 i = 0; i< AbilitiesClasses.Num() ; i++)
	{
		//Only 4 skills are currently supported
		if(AbilitiesClasses[i] == nullptr)
		{
			SetAbilityAtIndex(AbilityClass, i);
			return;
		}
	}
}

void UPlayerCastingComponent::RemoveAbility(const TSubclassOf<UGameplayAbility> AbilityClass)
{
	for(int32 i = 0; i< AbilitiesClasses.Num() ; i++)
		if(AbilitiesClasses[i] == AbilityClass)
		{
			SetAbilityAtIndex(nullptr, i);
			return;
		}
}

void UPlayerCastingComponent::SetAbilityAtIndex(const TSubclassOf<UGameplayAbility> AbilityClass, const int32 Index)
{
	//Ability index is over max, it won't ne usable anyway
	ensure(Index < NumberOfSkills);
	
	if(Index >= NumberOfSkills)
		return;

	AbilitiesClasses[Index] = AbilityClass;
	AddHandleForAbility(AbilityClass, Index);
}

void UPlayerCastingComponent::NotifySkillsReset() const
{
	for(int32 i = 0; i < NumberOfSkills; i++)
		OnSkillAtIndexChanged.Broadcast(i);
}

void UPlayerCastingComponent::AddHandleForAbility(const TSubclassOf<UGameplayAbility> AbilityClass, const int32 Index)
{
	CollectInfos();
	
	OwnerAbilitySystemComponent->ClearAbility(AbilitiesHandles[Index]);
	if(AbilityClass)
	{
		const auto Handle = OwnerAbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(AbilityClass, 1, 0));
		AbilitiesHandles[Index] = Handle;
	}
	OnSkillAtIndexChanged.Broadcast(Index);
}

void UPlayerCastingComponent::CollectInfos()
{
	if(OwnerAbilitySystemComponent)
		return;
	
	OwnerAbilitySystemComponent = GetOwner()->FindComponentByClass<UMyAbilitySystemComponent>();

	//This component require owner with MyAbilitySystemComponent to work correctly
	ensure(OwnerAbilitySystemComponent);
}

void UPlayerCastingComponent::SetupAbilities()
{
	for(int32 i = 0; i< AbilitiesClasses.Num(); i++)
		if(AbilitiesClasses[i])
			SetAbilityAtIndex(AbilitiesClasses[i], i);
}
