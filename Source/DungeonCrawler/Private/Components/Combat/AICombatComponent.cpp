
#include "Components/Combat/AICombatComponent.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AIPerceptionTypes.h"
#include "MyAbilitySystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MyGameplayStatics.h"
#include "CombatComponent.h"
#include "MyAIController.h"
#include "BaseCharacter.h"
#include "DataAssets.h"
#include "Weapon.h"
#include "Logs.h"

UAICombatComponent::UAICombatComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	
	WeaponRelatedActions.Add(EAIActionType::CloseDistanceAttack);
	WeaponRelatedActions.Add(EAIActionType::MeleeAttack);
}

void UAICombatComponent::TryToPlanAction(const int32 Priority)
{
	EAIActionType BestAction = EAIActionType::None;
	for(const auto AIAction : AICombatConfig->AIActionsConfig)
		if(AIAction.Value->bIsGameplayRelevant && !AIAction.Value->bHasToBeForced)
			// Only allow to be overriden by action with higher priority
			if(AIAction.Value->Priority >= Priority)
				if(!IsActionOnCooldown(AIAction.Key))
				{
					if(PlannedAction != EAIActionType::None)
					{
						const auto PlannedActionConfig = *AICombatConfig->AIActionsConfig.Find(PlannedAction);
						if(AIAction.Value->Priority <= PlannedActionConfig->Priority)
							continue;
					}
					
					if(PerformActivationChanceCheck(AIAction.Key))
					{
						// Clear the cooldown of previous best action
						if(BestAction != EAIActionType::None)
							SetActionCooldown(BestAction, 0.0f);

						BestAction = AIAction.Key;
					}
					else
						ApplyActionCooldown(BestAction);
				}
	
	if(BestAction != EAIActionType::None)
	{
		if(PlannedAction != EAIActionType::None)
			SetActionCooldown(PlannedAction, 0.0f);

		PlannedAction = BestAction;
	}
}

void UAICombatComponent::ForceAction(const EAIActionType Action)
{
	ForcedAction = Action;
}

void UAICombatComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdatePerceivedEnemiesInfo();
	UpdateCooldowns(DeltaTime);
	UpdateDistanceAndAngleToTheTarget();
	UpdateLastImpactfulActionTime(DeltaTime);
}

void UAICombatComponent::BeginPlay()
{
	Super::BeginPlay();

	BaseCharacterOwner = Cast<ABaseCharacter>(GetOwner());
	check(BaseCharacterOwner);
	OwnerCombatComponent = BaseCharacterOwner->CombatComponent;
	check(OwnerCombatComponent);
	OwnerAbilitySystemComponent = BaseCharacterOwner->AbilitySystemComponent;
	check(OwnerAbilitySystemComponent);
	
	OwnerCombatComponent->OnWeaponChanged.AddDynamic(this, &UAICombatComponent::OnWeaponChanged);
	OnWeaponChanged();
	
	if (const auto PerceptionComponent = BaseCharacterOwner->GetPerceptionComponent())
		PerceptionComponent->OnTargetPerceptionUpdated.AddDynamic(this, &UAICombatComponent::OnOwnerPerceptionUpdated);
}

void UAICombatComponent::OnActionExecuted(const EAIActionType Action)
{
	if(!AICombatConfig->AIActionsConfig.Contains(Action))
		return;
		
	const auto AIAction = *AICombatConfig->AIActionsConfig.Find(Action);
	ApplyActionCooldown(Action);
	GlobalActionsCooldown = AIAction->GlobalActionCooldown + FMath::FRandRange(0.0f, AIAction->GlobalActionCooldownRandomInterval);

	if(PlannedAction == Action)
		PlannedAction = EAIActionType::None;
	
	if(ForcedAction == Action)
		ForcedAction = EAIActionType::None; 
}

bool UAICombatComponent::PerformActivationChanceCheck(const EAIActionType Action)
{
	if(Action == PlannedAction)
		return true;

	const auto AIAction = *AICombatConfig->AIActionsConfig.Find(Action);
	const float ActivationChance = AIAction->ActivationChance;
	//ActivationChance >= 1.0f -> always activate
	if(ActivationChance  >= 1.0f)
		return true;
	
	//ActivationChance < 1.0f -> Do check if should activate
	if(ActivationChance > FMath::FRandRange(0.0f, 1.0f))
		return true;

	//Apply action cooldown
	ApplyActionCooldown(Action);
	return false;
}

void UAICombatComponent::ApplyActionCooldown(const EAIActionType Action)
{
	if(!AICombatConfig->AIActionsConfig.Contains(Action))
		return;
	
	const auto AIAction = *AICombatConfig->AIActionsConfig.Find(Action);
	SetActionCooldown(Action, AIAction->Cooldown + FMath::FRandRange(0.0f, AIAction->CooldownRandomInterval));
}

void UAICombatComponent::ApplyGlobalActionCooldown(const float Amount)
{
	if(Amount > 0)
		GlobalActionsCooldown += Amount;
}

bool UAICombatComponent::ShouldChaseCauseTooMuchTimePassedSinceLastImpactfulAction() const
{
	return GetTimeSinceLastImpactfulAction() > TimeToForceChaseTargetSinceLastImpactfulAction;
}

void UAICombatComponent::SetActionCooldown(EAIActionType Action, float Cooldown)
{
	if(AIActionToCooldownsSetup.Find(Action))
		AIActionToCooldownsSetup.Emplace(Action, Cooldown);
}

bool UAICombatComponent::IsActionEnabled(const EAIActionType Action) const
{
	if(AICombatConfig->AIActionsConfig.Find(Action))
		return true;
	
	return false;
}

bool UAICombatComponent::IsInRangeOfTheAction(const EAIActionType Action) const
{
	if(const auto AICombatAction = Cast<UAICombatAction>(*AICombatConfig->AIActionsConfig.Find(Action)))
	{
		if(AICombatAction->bRequiresTarget && !bHasTarget)
			return false;

		const auto AbsoluteAngleToTheTarget = FMath::Abs(AngleToTarget);
		bool bIsInDistance;
		bool bIsInAngle;

		if(WeaponRelatedActions.Contains(Action))
		{
			auto Attacks = GetPossibleAttacksForAction(Action);
			for(const auto Attack : Attacks)
			{
				bIsInAngle = AbsoluteAngleToTheTarget >= Attack->MinAngle && AbsoluteAngleToTheTarget <= Attack->MaxAngle;
				bIsInDistance = DistanceToTheTarget >= Attack->MinDistance && DistanceToTheTarget <= Attack->MaxDistance;
			
				if(bIsInAngle && bIsInDistance)
					return true;
			}
			return false;
		}
		
		bIsInAngle = (AbsoluteAngleToTheTarget >= AICombatAction->MinAngle && AbsoluteAngleToTheTarget <= AICombatAction->MaxAngle) || !AICombatAction->bIsAngleDependant;
		bIsInDistance = (DistanceToTheTarget >= AICombatAction->MinRange && DistanceToTheTarget <= AICombatAction->MaxRange) || !AICombatAction->bIsRangeDependant;
		return bIsInDistance && bIsInAngle;
	}
	return false;
}

bool UAICombatComponent::CanExecuteAnyMeaningfulAction(const int32 Priority)
{
	if(IsOnGlobalActionCooldown())
		return false;

	for(const auto AIAction : AICombatConfig->AIActionsConfig)
	{
		if(!AIAction.Value->bIsGameplayRelevant)
			continue;

		if(AIAction.Value->Priority < Priority)
			continue;
		
		if(IsActionOnCooldown(AIAction.Key))
			continue;

		if(IsInRangeOfTheAction(AIAction.Key))
			return true;
	}
	
	return false;
}

bool UAICombatComponent::CanExecutePlannedAction() const
{
	if(PlannedAction == EAIActionType::None)
		return false;
	
	return IsInRangeOfTheAction(PlannedAction);
}

bool UAICombatComponent::DoesRememberAnyAliveEnemy() const
{
	return AIPerceivedEnemiesInfosMap.Num() > 0;
}

float UAICombatComponent::GetDesiredRangeValue()
{
	float HighestRange = -1.0f;
	for(const auto AIAction : AICombatConfig->AIActionsConfig)
	{
		if(!AIAction.Value->bIsGameplayRelevant)
			continue;
			
		if(!AIAction.Value->bShouldAffectMovement)
			continue;

		if(IsActionOnCooldown(AIAction.Key) && !AIAction.Value->bAffectMovementWhenOnCooldown)
			continue;
		
		if(const auto AICombatAction = Cast<UAICombatAction>(AIAction.Value))
		{
			if(WeaponRelatedActions.Contains(AIAction.Key))
			{
				auto Attacks = GetPossibleAttacksForAction(AIAction.Key);
				for(const auto Attack : Attacks)
					if(Attack->MaxDistance > HighestRange)
						HighestRange = Attack->MaxDistance;
			}
			
			if(AICombatAction->bIsRangeDependant)
				if(AICombatAction->MaxRange > HighestRange)
					HighestRange = AICombatAction->MaxRange;
		}
	}
	
	if(HighestRange > 0.0f)
		return HighestRange;
	
	UE_LOG(LogAI, Error, TEXT("%s no desired range found"), *GetOwner()->GetName());
	
	return 2137.0f;
}

bool UAICombatComponent::IsActionOnCooldown(const EAIActionType Action)
{
	if(AIActionToCooldownsSetup.Find(Action))
		return *AIActionToCooldownsSetup.Find(Action) > 0.0f;
		
	return false;
}

bool UAICombatComponent::IsOnGlobalActionCooldown() const
{
	return GlobalActionsCooldown > 0.0f;
}

float UAICombatComponent::GetActionCooldown(const EAIActionType Action)
{
	if(AIActionToCooldownsSetup.Find(Action))
		return *AIActionToCooldownsSetup.Find(Action);
	
	return 0.0f;
}

UAIAttackMontageSetup* UAICombatComponent::GetPossibleAttackParams(const EAIActionType Action)
{
	auto Attacks = GetPossibleAttacksForAction(Action);
	switch (Action)
	{
		case EAIActionType::MeleeAttack:
			return GetBestAttackForAction(EAIActionType::MeleeAttack, LastMeleeAttackIndex);
			
		case EAIActionType::CloseDistanceAttack:
			return GetBestAttackForAction(EAIActionType::CloseDistanceAttack, LastCloseDistanceAttackIndex);

		case EAIActionType::RangedAttack:
			return GetBestAttackForAction(EAIActionType::RangedAttack, LastRangedAttackIndex);
		
		default:
			UE_LOG(LogAI, Error, TEXT("%s is trying to get attack params for a non attack action"), *GetOwner()->GetName());
			ensure(false);
	}
	return nullptr;
}
void UAICombatComponent::OnWeaponChanged()
{
	AICombatConfig = OwnerCombatComponent->GetMainWeapon()->AICombatConfig;
	InitializeCooldowns();
	BuildActionsToTagsMap();
}

void UAICombatComponent::UpdatePerceivedEnemiesInfo()
{
	TArray<AActor*> KeysToRemove;
	for (auto Info : AIPerceivedEnemiesInfosMap)
	{
		if (UGameplayStatics::GetRealTimeSeconds(GetWorld()) - Info.Value > PerceivedActorForgetTime)
		{
			KeysToRemove.Add(Info.Key);
			continue;
		}
		if (const auto BaseCharacterEnemy = Cast<ABaseCharacter>(Info.Key))
			if (!BaseCharacterEnemy->IsAlive())
			{
				KeysToRemove.Add(Info.Key);
				break;
			}
	}

	for (const auto Key : KeysToRemove)
		AIPerceivedEnemiesInfosMap.Remove(Key);
}

void UAICombatComponent::UpdateLastImpactfulActionTime(const float DeltaTime) const
{
	//Only valid if in combat and has a target
	if(!bHasTarget)
		return;
	
	const auto OwnerAIController = GetOwnerAIController();
	if(!OwnerAIController)
		return;
	
	const auto Blackboard = OwnerAIController->GetBlackboardComponent();
	if(BaseCharacterOwner->AbilitySystemComponent->HasMatchingGameplayTag(DuringActionTag))
		return;
	
	Blackboard->SetValueAsFloat(TimeSinceLastImpactfulActionBlackboardName, Blackboard->GetValueAsFloat(TimeSinceLastImpactfulActionBlackboardName) + DeltaTime);
}

void UAICombatComponent::UpdateCooldowns(const float DeltaTime)
{
	if (GlobalActionsCooldown > 0.0f)
	{
		GlobalActionsCooldown -= DeltaTime;
		if (GlobalActionsCooldown < 0.0f)
			GlobalActionsCooldown = 0.0f;
	}
	
	for (TPair<EAIActionType, float>& Pair : AIActionToCooldownsSetup)
		if (Pair.Value > 0.0f)
		{
			Pair.Value -= DeltaTime;
			if (Pair.Value < 0.0f)
				Pair.Value = 0.0f;
		}
}

void UAICombatComponent::UpdateDistanceAndAngleToTheTarget()
{
	const auto OwnerAIController = GetOwnerAIController();
	if(!OwnerAIController)
		return ;
	
	const auto Blackboard = OwnerAIController->GetBlackboardComponent();
	if(Blackboard)
		if(Blackboard->GetValueAsObject(TargetBlackboardName))
		{
			bHasTarget = true;
			DistanceToTheTarget = Blackboard->GetValueAsFloat(DistanceToTargetBlackboardName);
		}
		else
			bHasTarget = false;
	
	const auto FocalPoint = OwnerAIController->GetFocalPoint();
	if(!FAISystem::IsValidLocation(FocalPoint))
		return;
	
	AngleToTarget = UMyGameplayStatics::GetYawAngleBetweenActorAndTarget(GetOwner(), FocalPoint);

	if(Blackboard)
		Blackboard->SetValueAsFloat(AngleToTargetBlackboardName, AngleToTarget);
}

void UAICombatComponent::InitializeCooldowns()
{
	for(auto ActionSetup : AICombatConfig->AIActionsConfig)
		if(!AIActionToCooldownsSetup.Find(ActionSetup.Key))
			AIActionToCooldownsSetup.Add(ActionSetup.Key, 0.0f);
	
	ApplyInitialCooldowns();
}

void UAICombatComponent::ApplyInitialCooldowns()
{
	for(const auto ActionSetup : AICombatConfig->AIActionsConfig)
		if(ActionSetup.Value->bStartOnCooldown)
			ApplyActionCooldown(ActionSetup.Key);
}

void UAICombatComponent::BuildActionsToTagsMap()
{
	if(!AICombatConfig)
		return;
	
	ImportantActionsToTagsMap.Empty();
	for (auto ActionSetup : AICombatConfig->AIActionsConfig)
		if (ActionSetup.Value->bIsGameplayRelevant && ActionSetup.Key != EAIActionType::None)
			ImportantActionsToTagsMap.Add(ActionSetup.Key, *ActionsToTagsMap.Find(ActionSetup.Key));
}

UAIAttackMontageSetup* UAICombatComponent::GetBestAttackForAction(const EAIActionType Action, int32& IndexToChange) const
{
	if(!bHasTarget)
		return nullptr;

	// Only valid for combat actions
	if(Cast<UAICombatAction>(*AICombatConfig->AIActionsConfig.Find(Action)))
	{
		const auto AbsoluteAngleToTheTarget = FMath::Abs(AngleToTarget);
		TArray<FAIAttackWithIndex> AttacksWithIndices;
			
		auto Attacks = GetPossibleAttacksForAction(Action);
		
		for(int32 i = 0; i < Attacks.Num(); i++)
		{
			const bool bIsInAngle = AbsoluteAngleToTheTarget >= Attacks[i]->MinAngle && AbsoluteAngleToTheTarget <= Attacks[i]->MaxAngle;
			const bool bIsInDistance = DistanceToTheTarget >= Attacks[i]->MinDistance && DistanceToTheTarget <= Attacks[i]->MaxDistance;
				
			if(bIsInAngle && bIsInDistance)
				AttacksWithIndices.Add(FAIAttackWithIndex(Attacks[i], i));
		}

		if(AttacksWithIndices.Num() == 0)
		{
			UE_LOG(LogAI, Error, TEXT("%s is trying to play an attack but there is not an available attack to play"), *GetOwner()->GetName());
			ensure(false);
			return nullptr;
		}
			
		// If there less than 3 attacks, just return random one, as the randomization would just lead to repeating the same pattern, same when there is only single available attack
		if(Attacks.Num() < 3 || AttacksWithIndices.Num() == 1)
			return AttacksWithIndices[FMath::RandRange(0, AttacksWithIndices.Num() - 1)].Attack;
		
		int32 IndexToRemove = -1;
		for(int32 j = 0; j < AttacksWithIndices.Num(); j++)
			if(AttacksWithIndices[j].Index == IndexToChange)
				IndexToRemove = j;
				
		if(IndexToRemove != -1)
			AttacksWithIndices.RemoveAt(IndexToRemove);
				
		const int32 RandomIndex = FMath::RandRange(0, AttacksWithIndices.Num() - 1);
		IndexToChange = AttacksWithIndices[RandomIndex].Index;
		return AttacksWithIndices[RandomIndex].Attack;
	}
	return nullptr;
}

TArray<UAIAttackMontageSetup*> UAICombatComponent::GetPossibleAttacksForAction(const EAIActionType Action) const
{
	const auto Weapon = OwnerCombatComponent->GetMainWeapon();
	TArray<FAIAttackWithIndex> AttacksWithIndices;

	TArray<UAIAttackMontageSetup*> Attacks;
	switch (Action)
	{
		case EAIActionType::MeleeAttack:
			Attacks = Weapon->AICombatConfig->MeleeAttacks;
			break;

		case EAIActionType::CloseDistanceAttack:
			Attacks = Weapon->AICombatConfig->CloseDistanceAttacks;
			break;

		case EAIActionType::RangedAttack:
			Attacks = Weapon->AICombatConfig->RangedAttacks;
			break;

		default:
			UE_LOG(LogAI, Error, TEXT("UAICombatComponent::GetPossibleAttacksForAction called with invalid action type"));
			ensure(false);
	}
	
	return Attacks;
}

void UAICombatComponent::OnOwnerPerceptionUpdated(AActor* Actor, const FAIStimulus Stimulus)
{
	if (BaseCharacterOwner && BaseCharacterOwner->GetTeamAttitudeTowards(*Actor) == ETeamAttitude::Hostile)
		if (Stimulus.WasSuccessfullySensed())
		{
			float CurrentTime = UGameplayStatics::GetRealTimeSeconds(GetWorld());
			if(AIPerceivedEnemiesInfosMap.Num() > 0)
			{
				if (AIPerceivedEnemiesInfosMap.Find(Actor))
					AIPerceivedEnemiesInfosMap.Emplace(Actor, CurrentTime);
			}
			else
				AIPerceivedEnemiesInfosMap.Add(Actor, CurrentTime);
		}
}

AMyAIController* UAICombatComponent::GetOwnerAIController() const
{
	return Cast<AMyAIController>(BaseCharacterOwner->GetController());
}

float UAICombatComponent::GetTimeSinceLastImpactfulAction() const
{
	const auto OwnerAIController = GetOwnerAIController();
	if(!OwnerAIController)
		return 0.f;

	const auto Blackboard = OwnerAIController->GetBlackboardComponent();
	return Blackboard->GetValueAsFloat(TimeSinceLastImpactfulActionBlackboardName);
}

