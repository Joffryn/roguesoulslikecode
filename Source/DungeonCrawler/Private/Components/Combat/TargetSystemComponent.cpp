
#include "TargetSystemComponent.h"

#include "Engine/Classes/Kismet/GameplayStatics.h"
#include "TargetSystemTargetableInterface.h"
#include "Components/Managers/AIManager.h"
#include "Engine/Public/TimerManager.h"
#include "TargetSystemLockOnWidget.h"
#include "DamageResponseComponent.h"
#include "MyPlayerCameraManager.h"
#include "AIController.h"
#include "Engine/World.h"
#include "EngineUtils.h"

class ULockOnTargetPositionComponent;

UTargetSystemComponent::UTargetSystemComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	TargetLockedOnWidgetRelativeLocation = FVector(0.0f, 0.0f, 20.0f);
	ShouldControlRotationWhenLockedOn = true;
	ShouldDrawTargetLockedOnWidget = true;
	TargetLockedOnWidgetDrawSize = 32.0f;
	MinimumDistanceToEnable = 1200.0f;
	bIsBreakingLineOfSight = false;
	MaxCameraLookAtPitch = 20.0f;
	ClosestTargetDistance = 0.0f;
	BreakLineOfSightDelay = 2.0f;
	bIsSwitchingTarget = false;
	TargetLocked = false;
}

void UTargetSystemComponent::BeginPlay()
{
	Super::BeginPlay();
	CharacterOwner = GetOwner();
	// Kinda hardcoded, though it should be fine as long as it's only used for the player
	PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
}

void UTargetSystemComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction * ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (TargetLocked && NearestTarget)
	{
		if (!TargetIsTargetable(NearestTarget))
		{
			TargetLockOff();
			return;
		}

		if(ShouldControlRotationWhenLockedOn)
			SetControlRotationOnTarget();
		
		if (GetDistanceFromCharacter(NearestTarget) > MinimumDistanceToEnable)
			TargetLockOff();

		if (ShouldBreakLineOfSight() && !bIsBreakingLineOfSight)
		{
			if (BreakLineOfSightDelay <= 0)
				TargetLockOff();
			else
			{
				bIsBreakingLineOfSight = true;
				GetWorld()->GetTimerManager().SetTimer(
					LineOfSightBreakTimerHandle,
					this,
					&UTargetSystemComponent::BreakLineOfSight,
					BreakLineOfSightDelay
				);
			}
		}
	}
}

void UTargetSystemComponent::TargetActor()
{
	ClosestTargetDistance = MinimumDistanceToEnable;

	if (TargetLocked)
		TargetLockOff();
	else
	{
		const TArray<AActor*> Actors = GetPossibleTargetActors();
		NearestTarget = FindNearestTarget(Actors);

		if(NearestTarget)
			TargetLockOn(NearestTarget);
		else
			if(const auto OwnerPawn = Cast<APawn>(GetOwner()))
				if(const auto Controller = OwnerPawn->GetController())
					Controller->SetControlRotation(FRotator(0.0f, OwnerPawn->GetActorRotation().Yaw, 0.0f));
	}
}

void UTargetSystemComponent::TargetActorWithAxisInput(const float AxisValue)
{
	if (!TargetLocked)
		return;

	if (bIsSwitchingTarget)
		return;

	AActor* CurrentTarget = NearestTarget;
	const float RangeMin = AxisValue < 0.0f ? 0.0f : 180.0f;
	const float RangeMax = AxisValue < 0.0f ? 180.0f : 360.0f;

	ClosestTargetDistance = MinimumDistanceToEnable;
	TArray<AActor*> Actors = GetPossibleTargetActors();
	TArray<AActor*> ActorsToLook;

	TArray<AActor*> ActorsToIgnore ;
	ActorsToIgnore.Add(CurrentTarget);
	for (AActor* Actor : Actors)
		if (LineTraceForActor(Actor, ActorsToIgnore) && IsInViewport(Actor))
			ActorsToLook.Add(Actor);

	TArray<AActor*> TargetsInRange = FindTargetsInRange(ActorsToLook, RangeMin, RangeMax);

	// Find closest target
	AActor* ActorToTarget = nullptr;
	for (AActor* Actor : TargetsInRange)
	{
		if(!IsInViewport(Actor))
			continue;

		// Ignore targets that are too far
		const float Distance = GetDistanceFromCharacter(Actor);
		if (Distance < MinimumDistanceToEnable)
		{
			const float RelativeActorsDistance = CurrentTarget->GetDistanceTo(Actor);
			if (RelativeActorsDistance < ClosestTargetDistance)
			{
				ClosestTargetDistance = RelativeActorsDistance;
				ActorToTarget = Actor;
			}
		}
	}

	if (ActorToTarget)
	{
		bIsSwitchingTarget = true;
		TargetLockOff();
		NearestTarget = ActorToTarget;
		TargetLockOn(ActorToTarget);
		GetWorld()->GetTimerManager().SetTimer(SwitchingTargetTimerHandle, this, &UTargetSystemComponent::ResetIsSwitchingTarget, 0.5f);
	}
}

TArray<AActor*> UTargetSystemComponent::FindTargetsInRange(TArray<AActor*> ActorsToLook, const float RangeMin, const float RangeMax) const
{
	TArray<AActor*> ActorsInRange;

	for (AActor* Actor : ActorsToLook)
	{
		const float Angle = GetAngleUsingCameraRotation(Actor);
		if (Angle >= RangeMin && Angle <= RangeMax)
			ActorsInRange.Add(Actor);
	}

	return ActorsInRange;
}

float UTargetSystemComponent::GetAngleUsingCameraRotation(const AActor* ActorToLook) const
{
	const USkeletalMeshComponent* CameraComponent = nullptr;
	// Only for player, if it will ever be needed make generic logic for every character
	if(const auto CameraManager = AMyPlayerCameraManager::GetMyPlayerCameraManager(this))
		CameraComponent = CameraManager->GetCameraComponent();

	const FRotator CameraWorldRotation = CameraComponent->GetComponentRotation();
	const FRotator LookAtRotation = FindLookAtRotation(CameraComponent->GetComponentLocation(), ActorToLook->GetActorLocation());

	float YawAngle = CameraWorldRotation.Yaw - LookAtRotation.Yaw;
	if (YawAngle < 0)
		YawAngle = YawAngle + 360;

	return YawAngle;
}

FRotator UTargetSystemComponent::FindLookAtRotation(const FVector Start, const FVector Target)
{
	return FRotationMatrix::MakeFromX(Target - Start).Rotator();
}

void UTargetSystemComponent::ResetIsSwitchingTarget()
{
	bIsSwitchingTarget = false;
}

void UTargetSystemComponent::TargetLockOn(AActor* TargetToLockOn)
{
	TargetetActor = TargetToLockOn;
	if (TargetToLockOn)
	{
		TargetLocked = true;
		if (ShouldDrawTargetLockedOnWidget)
			CreateAndAttachTargetLockedOnWidgetComponent(TargetToLockOn);
		
		PlayerController->SetIgnoreLookInput(true);
		if (OnTargetLockedOn.IsBound())
			OnTargetLockedOn.Broadcast(TargetToLockOn);
		
		if(TargetToLockOn->GetClass()->ImplementsInterface(UTargetSystemTargetableInterface::StaticClass()))
			ITargetSystemTargetableInterface::Execute_OnBeingLockedOn(TargetToLockOn);

		// If target died then lock off
		if(const auto TargetDamageResponseComponent = TargetToLockOn->FindComponentByClass<UDamageResponseComponent>())
			TargetDamageResponseComponent->OnDeath.AddDynamic(this, &UTargetSystemComponent::TargetLockOff);
	}
}

void UTargetSystemComponent::TargetLockOff()
{
	if (TargetetActor && TargetetActor->GetClass()->ImplementsInterface(UTargetSystemTargetableInterface::StaticClass()))
		ITargetSystemTargetableInterface::Execute_OnBeingLockedOff(TargetetActor);

	if(TargetetActor)
		if(const auto TargetDamageResponseComponent = TargetetActor->FindComponentByClass<UDamageResponseComponent>())
			TargetDamageResponseComponent->OnDeath.RemoveDynamic(this, &UTargetSystemComponent::TargetLockOff);

	TargetLocked = false;
	TargetetActor = nullptr;
	if (TargetLockedOnWidgetComponent)
		TargetLockedOnWidgetComponent->DestroyComponent();

	if (NearestTarget)
	{
		PlayerController->ResetIgnoreLookInput();
		if (OnTargetLockedOff.IsBound())
			OnTargetLockedOff.Broadcast(NearestTarget);
		
	}
	NearestTarget = nullptr;
}

void UTargetSystemComponent::CreateAndAttachTargetLockedOnWidgetComponent(AActor* TargetActor)
{
	TargetLockedOnWidgetComponent = NewObject<UWidgetComponent>(TargetActor, FName("TargetLockOn"));
	if (TargetLockedOnWidgetClass)
		TargetLockedOnWidgetComponent->SetWidgetClass(TargetLockedOnWidgetClass);
	else
		TargetLockedOnWidgetComponent->SetWidgetClass(UTargetSystemLockOnWidget::StaticClass());

	TargetLockedOnWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
	if (TargetActor->GetClass()->ImplementsInterface(UTargetSystemTargetableInterface::StaticClass()))
		TargetLockedOnWidgetComponent->SetupAttachment(ITargetSystemTargetableInterface::Execute_GetAttachComponent(TargetActor));
	else
		TargetLockedOnWidgetComponent->SetupAttachment(TargetActor->GetRootComponent());

	TargetLockedOnWidgetComponent->SetRelativeLocation(TargetLockedOnWidgetRelativeLocation);
	TargetLockedOnWidgetComponent->SetDrawSize(FVector2D(TargetLockedOnWidgetDrawSize, TargetLockedOnWidgetDrawSize));
	TargetLockedOnWidgetComponent->SetVisibility(true);
	TargetLockedOnWidgetComponent->RegisterComponent();
}

TArray<AActor*> UTargetSystemComponent::GetAllActorsOfClass(const TSubclassOf<AActor> ActorClass) const
{
	TArray<AActor*> Actors;
		
	for (TActorIterator ActorIterator(GetWorld(), ActorClass); ActorIterator; ++ActorIterator)
	{
		AActor* Actor = *ActorIterator;
		if (TargetIsTargetable(Actor))
			Actors.Add(Actor);
	}

	return Actors;
}

bool UTargetSystemComponent::TargetIsTargetable(const AActor* Actor) const
{
	bool bIsTargetable = false;
	if (Actor->GetClass()->ImplementsInterface(UTargetSystemTargetableInterface::StaticClass()))
		bIsTargetable = ITargetSystemTargetableInterface::Execute_IsTargetable(Actor);

	//Do not target friends
	bool bIsEnemy = true;
	if(const IGenericTeamAgentInterface* TeamInterface = Cast<IGenericTeamAgentInterface>(GetOwner()))
		bIsEnemy =  TeamInterface->GetTeamAttitudeTowards(*Actor) != ETeamAttitude::Friendly;
	
	return bIsTargetable && bIsEnemy;
}

void UTargetSystemComponent::UpdateCameraPitchToLookAtTarget() const
{
	const auto CameraComponent = AMyPlayerCameraManager::GetMyPlayerCameraManager(this)->GetCameraComponent();

	FRotator CameraWorldRotation = CameraComponent->GetComponentRotation();
	const FRotator LookAtRotation = FindLookAtRotation(CameraComponent->GetComponentLocation(), TargetLockedOnWidgetComponent->GetComponentLocation());
		
	CameraWorldRotation.Pitch = LookAtRotation.Pitch;
	CameraComponent->SetWorldRotation(CameraWorldRotation);
}

AActor* UTargetSystemComponent::FindNearestTarget(TArray<AActor*> Actors) const
{
	TArray<AActor*> ActorsHit;
	// Find all actors we can line trace to
	for (AActor* Actor : Actors)
		if (LineTraceForActor(Actor) && IsInViewport(Actor))
			ActorsHit.Add(Actor);

	if (ActorsHit.Num() == 0)
		return nullptr;

	// From the hit actors, check distance and return the nearest
	float ClosestDistance = ClosestTargetDistance;
	AActor* Target = nullptr;
	for (AActor* Actor : ActorsHit)
	{
		const float Distance = GetDistanceFromCharacter(Actor);
		if (GetDistanceFromCharacter(Actor) < ClosestDistance)
		{
			ClosestDistance = Distance;
			Target = Actor;
		}
	}

	return Target;
}


bool UTargetSystemComponent::LineTraceForActor(const AActor* OtherActor, const TArray<AActor*>& ActorsToIgnore) const
{
	FHitResult HitResult;
	if (LineTrace(HitResult, OtherActor, ActorsToIgnore))
	{
		const AActor* HitActor = HitResult.GetActor();
		if (HitActor == OtherActor)
			return true;
	}
	return false;
}

bool UTargetSystemComponent::LineTrace(FHitResult& HitResult, const AActor* OtherActor, const TArray<AActor*>& ActorsToIgnore) const
{
	FCollisionQueryParams Params = FCollisionQueryParams(FName("LineTraceSingle"));

	TArray<AActor*> IgnoredActors;
	IgnoredActors.Init(CharacterOwner, 1);
	IgnoredActors += ActorsToIgnore;
	Params.AddIgnoredActors(IgnoredActors);

	return GetWorld()->LineTraceSingleByChannel(HitResult, CharacterOwner->GetActorLocation(),OtherActor->GetActorLocation(), ECC_Pawn, Params);
}

FRotator UTargetSystemComponent::GetControlRotationOnTarget() const
{
	const FRotator ControlRotation = PlayerController->GetControlRotation();
	const FVector CharacterLocation = CharacterOwner->GetActorLocation();

	FVector LocationAdjustment;
	if(TargetetActor)
		if (TargetetActor->GetClass()->ImplementsInterface(UTargetSystemTargetableInterface::StaticClass()))
			LocationAdjustment = ITargetSystemTargetableInterface::Execute_GetLookAtPositionAdjustment(TargetetActor);
		
	const FVector OtherActorLocation = TargetLockedOnWidgetComponent->GetComponentLocation() + LocationAdjustment;
	FRotator LookRotation = FRotationMatrix::MakeFromX(OtherActorLocation - CharacterLocation).Rotator();

	if(LookRotation.Pitch > MaxCameraLookAtPitch)
		LookRotation.Pitch = MaxCameraLookAtPitch;
	
	const FRotator TargetRotation = FRotator(LookRotation.Pitch, LookRotation.Yaw, ControlRotation.Roll);

	return FMath::RInterpTo(ControlRotation, TargetRotation, GetWorld()->GetDeltaSeconds(), 5.0f);
}

void UTargetSystemComponent::SetControlRotationOnTarget() const
{
	if (!PlayerController)
		return;

	PlayerController->SetControlRotation(GetControlRotationOnTarget());
}

float UTargetSystemComponent::GetDistanceFromCharacter(const AActor* OtherActor) const
{
	return CharacterOwner->GetDistanceTo(OtherActor);
}

bool UTargetSystemComponent::ShouldBreakLineOfSight() const
{
	if (!NearestTarget)
		return true;

	TArray<AActor*> ActorsToIgnore = GetPossibleTargetActors();
	ActorsToIgnore.Remove(NearestTarget);

	FHitResult HitResult;
	const bool bHit = LineTrace(HitResult, NearestTarget, ActorsToIgnore);
	if (bHit && HitResult.GetActor() != NearestTarget)
		return true;

	return false;
}

void UTargetSystemComponent::BreakLineOfSight()
{
	bIsBreakingLineOfSight = false;
	if (ShouldBreakLineOfSight())
		TargetLockOff();
}

bool UTargetSystemComponent::IsInViewport(const AActor* TargetActor) const
{
	if (!PlayerController)
		return true;

	FVector2D ScreenLocation;
	PlayerController->ProjectWorldLocationToScreen(TargetActor->GetActorLocation(), ScreenLocation);

	FVector2D ViewportSize;
	GetWorld()->GetGameViewport()->GetViewportSize(ViewportSize);

	return ScreenLocation.X > 0 && ScreenLocation.Y > 0 && ScreenLocation.X < ViewportSize.X && ScreenLocation.Y < ViewportSize.Y;
}

TArray<AActor*> UTargetSystemComponent::GetPossibleTargetActors() const
{
	TArray<AActor*> Actors;
	if (const auto AIManager = UAIManager::GetAIManager(this))
		for (const auto Controller : AIManager->RegisteredAIs)
			if(const auto Pawn = Controller->GetPawn())
				if(Pawn->WasRecentlyRendered())
					Actors.Add(Pawn);

	return Actors;
}
