
#include "SoundsComponent.h"

#include "Kismet/GameplayStatics.h"
#include "GameplayTagContainer.h"
#include "GameConfigComponent.h"
#include "DataAssets.h"

void USoundsComponent::SpawnSound(const FGameplayTag SoundTag, float InVolumeMultiplier,  float InPitchMultiplier) const
{
	if(!SoundsConfig)
		return;

	if(!SoundTag.IsValid())
		return;
	
	if (const auto SoundSetup = SoundsConfig->SoundsToTagsMap.Find(SoundTag))
	{
		if(SoundSetup->bAffectedByOwnerMultipliers)
		{
			InVolumeMultiplier *= VolumeMultiplier;
			InPitchMultiplier *= PitchMultiplier;
		}
		UGameplayStatics::SpawnSoundAtLocation(this, SoundSetup->Sound, GetOwner()->GetActorLocation(), FRotator(), InVolumeMultiplier, InPitchMultiplier, SoundSetup->StartTime, GetBaseAttenuation());
	}
}

void USoundsComponent::SpawnSound2D(const FGameplayTag SoundTag, float InVolumeMultiplier, float InPitchMultiplier) const
{
	if (const auto SoundSetup = SoundsConfig->SoundsToTagsMap.Find(SoundTag))
	{
		if(SoundSetup->bAffectedByOwnerMultipliers)
		{
			InVolumeMultiplier *= VolumeMultiplier;
			InPitchMultiplier *= PitchMultiplier;
		}
		UGameplayStatics::SpawnSound2D(this, SoundSetup->Sound, InVolumeMultiplier, InPitchMultiplier, SoundSetup->StartTime);
	}
}

UAudioComponent* USoundsComponent::SpawnSoundAttached(const FGameplayTag SoundTag, const FName AttachPointName,  float InVolumeMultiplier, float InPitchMultiplier) const
{
	if(!SoundsConfig)
		return nullptr;
	
	if (const auto SoundSetup = SoundsConfig->SoundsToTagsMap.Find(SoundTag))
	{
		if(SoundSetup->bAffectedByOwnerMultipliers)
		{
			InVolumeMultiplier *= VolumeMultiplier;
			InPitchMultiplier *= PitchMultiplier;
		}
		return UGameplayStatics::SpawnSoundAttached(SoundSetup->Sound, GetOwner()->GetRootComponent(), AttachPointName, FVector::ZeroVector, EAttachLocation::Type ::KeepRelativeOffset, true, InVolumeMultiplier,  InPitchMultiplier, SoundSetup->StartTime, GetBaseAttenuation());
	}
	
	return nullptr;
}

USoundAttenuation* USoundsComponent::GetBaseAttenuation() const
{
	if(const auto GameConfigComponent = UGameConfigComponent::GetGameConfigComponent(this))
		return GameConfigComponent->BaseAttenuation;
	
	return nullptr;
}
