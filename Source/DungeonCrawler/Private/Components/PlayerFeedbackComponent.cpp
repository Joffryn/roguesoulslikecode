
#include "PlayerFeedbackComponent.h"

#include "DamageResponseComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MyPlayerCameraManager.h"
#include "MyPlayerController.h"
#include "CombatComponent.h"
#include "BaseCharacter.h"
#include "Logs.h"

void UPlayerFeedbackComponent::BeginPlay()
{
	Super::BeginPlay();
	ControllerOwner =  Cast<AMyPlayerController>(GetOwner());
	if (!ControllerOwner)
	{
		UE_LOG(LogGameplay, Error, TEXT("Added player only component %s to  the %s"), *GetOwner()->GetName(), *GetName(), *GetOwner()->GetName());
		return;
	}
	CharacterOwner = Cast<ABaseCharacter>(ControllerOwner->GetPawn());
	BindDelegates();
}

void UPlayerFeedbackComponent::BindDelegates()
{
	if(!CharacterOwner)
		return;

	if(const auto DamageResponseComponent = CharacterOwner->DamageResponseComponent)
	{
		DamageResponseComponent->OnInvincibleHit.AddDynamic(this, &UPlayerFeedbackComponent::OnAttackHitInvincible);
		DamageResponseComponent->OnBeingBlocked.AddDynamic(this, &UPlayerFeedbackComponent::OnBeingBlocked);
		DamageResponseComponent->OnBeingParried.AddDynamic(this, &UPlayerFeedbackComponent::OnBeingParried);
		DamageResponseComponent->OnBeingCrit.AddDynamic(this, &UPlayerFeedbackComponent::OnBeingCrit);
		DamageResponseComponent->OnBeingHit.AddDynamic(this, &UPlayerFeedbackComponent::OnBeingHit);
		DamageResponseComponent->OnDeath.AddDynamic(this, &UPlayerFeedbackComponent::OnDeath);

		DamageResponseComponent->OnCritDamageDealed.AddDynamic(this, &UPlayerFeedbackComponent::OnAttackCrit);
		DamageResponseComponent->OnBlock.AddDynamic(this, &UPlayerFeedbackComponent::OnAttackBlocked);
		DamageResponseComponent->OnParry.AddDynamic(this, &UPlayerFeedbackComponent::OnAttackParried);
		DamageResponseComponent->OnHit.AddDynamic(this, &UPlayerFeedbackComponent::OnAttackHit);
		DamageResponseComponent->OnKill.AddDynamic(this, &UPlayerFeedbackComponent::OnKill);
	}

	if(const auto CombatComponent = CharacterOwner->CombatComponent)
	{
		CombatComponent->OnAttackChargingStarted.AddDynamic(this, &UPlayerFeedbackComponent::OnAttackChargingStarted);
		CombatComponent->OnAttackChargingFinished.AddDynamic(this, &UPlayerFeedbackComponent::OnAttackChargingFinished);
	}
	
}

void UPlayerFeedbackComponent::OnDeath()
{
	PlayActionFeedback(OnDeathFeedback);
}

void UPlayerFeedbackComponent::OnKill(AActor* Target)
{
	//Ignore self kills :D
	if(Target != GetOwner())
		PlayActionFeedback(OnKillFeedback);
}

void UPlayerFeedbackComponent::OnBeingHit()
{
	PlayActionFeedback(OnBeingHitFeedback);
}

void UPlayerFeedbackComponent::OnBeingCrit()
{
	PlayActionFeedback(OnBeingCritFeedback);
}

void UPlayerFeedbackComponent::OnBeingBlocked()
{
	PlayActionFeedback(OnBeingBlockedFeedback);
}

void UPlayerFeedbackComponent::OnBeingParried()
{
	PlayActionFeedback(OnBeingParriedFeedback);
}

void UPlayerFeedbackComponent::OnAttackHit()
{
	PlayActionFeedback(OnHitFeedback);
}

void UPlayerFeedbackComponent::OnAttackBlocked()
{
	PlayActionFeedback(OnBlockedFeedback);
}

void UPlayerFeedbackComponent::OnAttackParried(AActor* Actor, TSubclassOf<UDamageType> DamageType, float Amount)
{
	PlayActionFeedback(OnParriedFeedback);
}

void UPlayerFeedbackComponent::OnAttackHitInvincible()
{
	PlayActionFeedback(OnHitInvincibleFeedback);
}

void UPlayerFeedbackComponent::OnAttackCrit(TSubclassOf<UDamageType> DamageType, AActor* DamageCauser, float Amount)
{
	PlayActionFeedback(OnCritFeedback);
}

void UPlayerFeedbackComponent::OnAttackChargingStarted()
{
	if(AttackChargingForceFeedback)
		if(auto* PlayerController = AMyPlayerController::GetMyPlayerController(this))
		{
			FForceFeedbackParameters Params = FForceFeedbackParameters();
			Params.bLooping = true;
			PlayerController->PlayForceFeedback_Internal(AttackChargingForceFeedback, Params);
		}
}

void UPlayerFeedbackComponent::OnAttackChargingFinished()
{
	if(AttackChargingForceFeedback)
		if(auto* PlayerController = AMyPlayerController::GetMyPlayerController(this))
		{
			PlayerController->ClientStopForceFeedback(AttackChargingForceFeedback, FName());
			PlayerController->PlayForceFeedback_Internal (AttackChargingEndForceFeedback);
		}
}

void UPlayerFeedbackComponent::OnStaminaDrained() const
{
	UGameplayStatics::PlaySound2D(this, StaminaDrainSound);
}

void UPlayerFeedbackComponent::OnManaBurned() const
{
	UGameplayStatics::PlaySound2D(this, ManaBurnedSound);
}

void UPlayerFeedbackComponent::PlayActionFeedback(const FActionFeedbackSetup& FeedbackSetup)
{
	if(FeedbackSetup.Shake)
		if(const auto MyPlayerCameraManager = AMyPlayerCameraManager::GetMyPlayerCameraManager(this))
			MyPlayerCameraManager->StartCameraShake(FeedbackSetup.Shake);

	if(FeedbackSetup.Sound)
		UGameplayStatics::PlaySound2D(this, FeedbackSetup.Sound);
	
	if(FeedbackSetup.ForceFeedback)
		if(auto* PlayerController = AMyPlayerController::GetMyPlayerController(this))
			PlayerController->PlayForceFeedback_Internal (FeedbackSetup.ForceFeedback);

	//Maybe curve would be better?
	if(FeedbackSetup.SlomoTime > 0.0f)
		TryToApplySlomo(FeedbackSetup.SlomoTime, FeedbackSetup.CustomTimeDilation);
}

void UPlayerFeedbackComponent::TryToApplySlomo(const float Time, const float Value)
{
	//Override bigger slomo with smaller one, never the other way around
	if(Value < CurrentCustomTimeDilation)
	{
		CurrentCustomTimeDilation = Value;
		UGameplayStatics::SetGlobalTimeDilation(this, Value);
		SlomoTimer.Invalidate();
		GetOwner()->GetWorldTimerManager().SetTimer(SlomoTimer, this, &UPlayerFeedbackComponent::OnSlomoEnded, 1.0f, false, Time);
	}
}

void UPlayerFeedbackComponent::OnSlomoEnded()
{
	CurrentCustomTimeDilation = 1.0f;
	UGameplayStatics::SetGlobalTimeDilation(this, 1.0f);
}
