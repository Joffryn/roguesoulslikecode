
#include "MyProjectileMovementComponent.h"

void UMyProjectileMovementComponent::StartMoving()
{
	Velocity = Velocity.GetSafeNormal() * InitialSpeed;
}
