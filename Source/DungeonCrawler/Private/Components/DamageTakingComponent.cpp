
#include "Components/DamageTakingComponent.h"

#include "ColorManager.h"
#include "MyHUD.h"
#include "Logs.h"

void UDamageTakingComponent::AddDamageInfo(const EColor Color, const float Amount, const bool bCrit)
{
	RememberedDamageInfos.Add(FDamageTakenInfo(Color, Amount, bCrit));
	//Already considered
	if(ShowDamageInfosHandle.IsValid())
		return;

	GetWorld()->GetTimerManager().SetTimer(ShowDamageInfosHandle, this, &UDamageTakingComponent::ShowDamageInfos, TimeToShow, false);
}

void UDamageTakingComponent::ShowDamageInfos()
{
	//Shouldn't happen, but just in case
	if(RememberedDamageInfos.Num() == 0)
	{
		UE_LOG(LogCombat, Warning, TEXT("RememberedDamageInfos empty!"));
		return;
	}
	
	TArray<FVector> DamageLocations;
	
	for(const auto Info : RememberedDamageInfos)
	{
		const auto Color = Info.Color;
		float AmountToShow = 0.0f;
		bool bCrit = false;
		
		if(Info.Color == Color)
		{
			AmountToShow += Info.Amount;
			if(Info.bCrit)
				bCrit = true;
		}

		if(const auto MyHud = AMyHUD::GetMyHud(this))
		{
			FLinearColor ActualColor = FLinearColor::White;
			FVector LastDamageLocation;
			if(const auto ColorManager = UColorManager::GetColorManager(this))
				ActualColor = ColorManager->GetColor(Color);
			
			if(DamageLocations.Num() > 0)
			{
				FVector AccumulatedDamageLocation = FVector::ZeroVector;
				for(const auto DamageLocation : DamageLocations)
					AccumulatedDamageLocation += DamageLocation;

				const float XDisplacement = FMath::FRandRange(0.5f, 1.0f) * FMath::RandBool() ? 1 : -1;
				const float YDisplacement = FMath::FRandRange(0.5f, 1.0f) * FMath::RandBool() ? 1 : -1;
				const float ZDisplacement = FMath::FRandRange(0.5f, 1.0f) * FMath::RandBool() ? 1 : -1;

				const FVector Displacement =  FVector(XDisplacement, YDisplacement, ZDisplacement) * FVector(NextHitLocationDisplacement);
				LastDamageLocation = AccumulatedDamageLocation / DamageLocations.Num() +  Displacement;
			}
			else
				LastDamageLocation = GetOwner()->GetActorLocation();
			;
			DamageLocations.Add(LastDamageLocation);
			const FText DamageText = FText::FromString(FString::FromInt( FMath::CeilToInt(AmountToShow)));
			MyHud->ShowDamageInfo(LastDamageLocation, DamageText, ActualColor, bCrit);
		}
	}
	ShowDamageInfosHandle.Invalidate();
	RememberedDamageInfos.Empty();
}

