
#include "Components/AnimationComponent.h"

#include "GameFramework/Character.h"

void UAnimationComponent::HideBones(TArray<FName> BonesToHide)
{
	if(const auto CharacterOwner = Cast<ACharacter>(GetOwner()))
		if(const auto Mesh = CharacterOwner->GetMesh())
			for (auto Bone : BonesToHide)
			{
				HiddenBones.AddUnique(Bone);
				Mesh->HideBoneByName(Bone, PBO_None);
			}
}

void UAnimationComponent::BeginPlay()
{
	Super::BeginPlay();

	HideBones(DefaultHiddenBones);
}
