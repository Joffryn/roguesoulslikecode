
#include "Components/AsleepComponent.h"

#include "Perception/AIPerceptionComponent.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "Perception/AIPerceptionTypes.h"
#include "Components/StatsComponent.h"
#include "MyAbilitySystemComponent.h"
#include "BaseCharacter.h"
#include "Enums.h"

void UAsleepComponent::Wake() const
{
	const FGameplayEventData Payload;
	UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(GetOwner(), ExitSleepModeTag, Payload);
}

void UAsleepComponent::BeginPlay()
{
	Super::BeginPlay();

	BaseCharacterOwner = Cast<ABaseCharacter>(GetOwner());
	check(BaseCharacterOwner);

	if (const auto AbilitySystemComponent = BaseCharacterOwner->AbilitySystemComponent)
	{
		const FGameplayEffectContextHandle EffectContext;
		AbilitySystemComponent->BP_ApplyGameplayEffectToSelf(AddAsleepAbility, 0, EffectContext);
	}

	if (bUseAsleepBehavior)
	{
		const FGameplayEventData Payload;
		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(GetOwner(), EnterSleepModeTag, Payload);
	}

	if (const auto PerceptionComponent = BaseCharacterOwner->GetPerceptionComponent())
		PerceptionComponent->OnTargetPerceptionUpdated.AddDynamic(this, &UAsleepComponent::OnOwnerPerceptionUpdated);

	if (const auto StatsComponent = BaseCharacterOwner->StatsComponent)
		StatsComponent->OnStatChanged.AddDynamic(this, &UAsleepComponent::OnStatChanged);
}

void UAsleepComponent::OnOwnerPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus)
{
	if (WakeMethod == EAIWakeMethod::Default)
		if (BaseCharacterOwner && BaseCharacterOwner->GetTeamAttitudeTowards(*Actor))
			if (Stimulus.WasSuccessfullySensed())
				Wake();
}

void UAsleepComponent::OnStatChanged(EStat Stat, float Amount)
{
	if (WakeMethod == EAIWakeMethod::WhenDamaged && Stat == EStat::Health && Amount < 0.f)
		Wake();
}
