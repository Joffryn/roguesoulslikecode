
#include "Components/LookAtComponent.h"

#include "Kismet/KismetMathLibrary.h"
#include "DataAssets.h"

ULookAtComponent::ULookAtComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void ULookAtComponent::SetParamsFromPreset(const UWeaponLookAtConfig* Preset)
{
	if(!Preset)
		return;
	
	MaxPitchAngles = Preset->MaxPitchAngles;
    YawBreakAngles = Preset->YawBreakAngles;
	MaxYawAngles = Preset->MaxYawAngles;
	InterpSpeed = Preset->InterpSpeed;
}

void ULookAtComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	CalculateLookAtRotation();
	UpdateLookAtRotation(DeltaTime);
}

void ULookAtComponent::CalculateLookAtRotation()
{
	
}

void ULookAtComponent::UpdateLookAtRotation(const float DeltaTime)
{
	if(LookAtRotation == DesiredLookAtRotation)
		return;
	
	LookAtRotation = UKismetMathLibrary::RInterpTo(LookAtRotation, DesiredLookAtRotation, DeltaTime, InterpSpeed);
}

void ULookAtComponent::SetDesiredLookAtRotation(FRotator& BaseRotation)
{
	if(BaseRotation.Yaw < YawBreakAngles.X || BaseRotation.Yaw > YawBreakAngles.Y)
		return;

	BaseRotation.Pitch = FMath::Clamp(BaseRotation.Pitch, MaxPitchAngles.X, MaxPitchAngles.Y);
	BaseRotation.Yaw = FMath::Clamp(BaseRotation.Yaw, MaxYawAngles.X, MaxYawAngles.Y);
	
	DesiredLookAtRotation = FRotator(BaseRotation.Yaw, 0.f, -BaseRotation.Pitch);
}
