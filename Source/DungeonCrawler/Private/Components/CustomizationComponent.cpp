
#include "Components/CustomizationComponent.h"

#include "Components/Managers/RandomnessManager.h"
#include "AnimationComponent.h"
#include "SoundsComponent.h"
#include "CombatComponent.h"
#include "BaseCharacter.h"
#include "Structs.h"

void UCustomizationComponent::FormHeroFromPresets(FRandomHeroSetup Setup)
{
	if (const auto BaseCharacterOwner = Cast<ABaseCharacter>(GetOwner()))
	{
		BaseCharacterOwner->LoadAttributes(Setup.Attributes);
		if (const auto RandomnessManager = URandomnessManager::GetRandomnessManager(this))
		{
			const auto Stream = RandomnessManager->GetStream();
			const auto Mesh = BaseCharacterOwner->GetMesh();
			const auto Material = Setup.PossibleMaterials[Stream.RandRange(0, Setup.PossibleMaterials.Num() - 1)];
			const auto CombatComponent = BaseCharacterOwner->CombatComponent;
			const auto SoundsComponent = BaseCharacterOwner->SoundsComponent;

			switch (Setup.PossibleGenders[Stream.RandRange(0, Setup.PossibleGenders.Num() - 1)])
			{

				case EGender::Male:
					Mesh->SetSkeletalMesh(Setup.MaleMesh);
					SoundsComponent->SoundsConfig = Setup.MaleSoundsConfig;
					AddAttachments(Setup.MaleAttachments);
					break;

				case EGender::Female:
					Mesh->SetSkeletalMesh(Setup.FemaleMesh);
					SoundsComponent->SoundsConfig = Setup.FemaleSoundsConfig;
					AddAttachments(Setup.MaleAttachments);
					break;

				default:
					Mesh->SetSkeletalMesh(Setup.MaleMesh);
					SoundsComponent->SoundsConfig = Setup.FemaleSoundsConfig;

			}
			SoundsComponent->VolumeMultiplier = Setup.VolumeMultiplier;
			SoundsComponent->PitchMultiplier = Setup.PitchMultiplier;
			CombatComponent->SecondaryWeaponClass = Setup.SecondaryWeaponClass;
			CombatComponent->EquipWeaponByClass(Setup.PrimaryWeaponClass);
			Mesh->SetMaterial(0, Material);
			BaseCharacterOwner->AnimationComponent->HideBones(Setup.BoneNamesToHide);
		}
	}
}

void UCustomizationComponent::AddAttachments(TArray<FSocketAttachmentSetup> AttachmentsSetup)
{
	
}
