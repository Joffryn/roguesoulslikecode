
#include "Components/SaveComponent.h"

#include "ObjectAndNameAsStringProxyArchive.h"
#include "Components/AnimationComponent.h"
#include "MyAbilitySystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerCastingComponent.h"
#include "SaveableInterface.h"
#include "RandomnessManager.h"
#include "CombatComponent.h"
#include "SoundsComponent.h"
#include "BaseCharacter.h"
#include "SavingWidget.h"
#include "AttributeSet.h"
#include "EngineUtils.h"
#include "LevelsManager.h"
#include "MyGameState.h"
#include "MyGameMode.h"
#include "MySaveGame.h"
#include "Logs.h"


USaveComponent* USaveComponent::GetSaveComponent(const UObject* WorldContextObject)
{
	if(AMyGameState::GetMyGameState(WorldContextObject))
		return AMyGameState::GetMyGameState(WorldContextObject)->SaveComponent;
	
	return nullptr;
}

void USaveComponent::SaveGame(const FName NextLevelName, const bool bSaveTransform /*= true*/)
{
	Save = Cast<UMySaveGame>(UGameplayStatics::CreateSaveGameObject(UMySaveGame::StaticClass()));
	if(Save)
	{
		PlayerCharacter = ABaseCharacter::GetMyPlayerCharacter(this);
		Save->LevelName = NextLevelName;
		Save->bSaveTransform = bSaveTransform;
		SaveSeed();
		SavePlayerTransform();
		SaveLook();
		SaveWeapons();
		SaveAttributes();
		SaveUpgradeEffects();
		SaveCasting();
		SaveSounds();
		SaveGlobalActors();
		SaveWorldActors();
		
		UGameplayStatics::AsyncSaveGameToSlot(Save, TEXT("Save"), 0,  FAsyncSaveGameToSlotDelegate::CreateUObject(this, &USaveComponent::OnSaveCompleted));
		USavingWidget::GetSavingWidget(this)->Show();
	}
	
	OnSavingStarted.Broadcast();
	UE_LOG(LogGameplay, Log, TEXT("Game save started"));
}

void USaveComponent::DeleteSave()
{
	UGameplayStatics::DeleteGameInSlot(TEXT("Save"), 0);
}

void USaveComponent::LoadWorld()
{
	if(!UGameplayStatics::DoesSaveGameExist(TEXT("Save"), 0))
		return;
	
	bIsWorldLoading = true;
	Save = Cast<UMySaveGame>(UGameplayStatics::LoadGameFromSlot(TEXT("Save"), 0));
	if(Save)
	{
		LoadGlobalActors();
		LoadWorldActors();
	}
	
	UE_LOG(LogGameplay, Log, TEXT("World loaded"));
	bIsWorldLoading = false;
	OnWorldLoaded.Broadcast();
}

void USaveComponent::LoadPlayer()
{
	if(!UGameplayStatics::DoesSaveGameExist(TEXT("Save"), 0))
		return;
	
	bIsPlayerLoading = true;
	Save = Cast<UMySaveGame>(UGameplayStatics::LoadGameFromSlot(TEXT("Save"), 0));
	if(Save)
	{
		PlayerCharacter = ABaseCharacter::GetMyPlayerCharacter(this);
		if(Save->bSaveTransform)
			LoadPlayerTransform();
		
		LoadSeed();
		LoadLook();
		LoadWeapons();
		LoadAttributes();
		LoadUpgradeEffects();
		LoadCasting();
		LoadSounds();
	}
	UE_LOG(LogGameplay, Log, TEXT("Player loaded"));
	bIsPlayerLoading = false;
	OnPlayerLoaded.Broadcast();
}

bool USaveComponent::DoesSaveExist()
{
	return UGameplayStatics::DoesSaveGameExist(TEXT("Save"), 0);
}

FName USaveComponent::GetSavedLevelName()
{
	if(!UGameplayStatics::DoesSaveGameExist(TEXT("Save"), 0))
		return FName();
	
	Save = Cast<UMySaveGame>(UGameplayStatics::LoadGameFromSlot(TEXT("Save"), 0));
	if(Save)
		return Save->LevelName;

	return FName();
}

void USaveComponent::SaveSeed() const
{
	if(const auto RandomnessManager = URandomnessManager::GetRandomnessManager(this))
		Save->Seed = RandomnessManager->Seed;
}

void USaveComponent::SavePlayerTransform() const
{
	Save->PlayerTransform = PlayerCharacter->GetActorTransform();
}

void USaveComponent::SaveLook() const
{
	const auto MeshComponent = PlayerCharacter->GetMesh();
	Save->Mesh = MeshComponent->GetSkeletalMeshAsset();
	Save->Material = MeshComponent->GetMaterial(0)->GetBaseMaterial();
	Save->HiddenBones = PlayerCharacter->AnimationComponent->HiddenBones;
}

void USaveComponent::SaveWeapons() const
{
	const auto CombatComponent = PlayerCharacter->CombatComponent;
	Save->PrimaryWeaponClass = CombatComponent->WeaponClass;
	Save->SecondaryWeaponClass = CombatComponent->SecondaryWeaponClass;
	Save->SecondaryLeftWeaponClass = CombatComponent->SecondaryLeftWeaponClass;
}

void USaveComponent::SaveAttributes()
{
	const auto AbilitySystem = PlayerCharacter->AbilitySystemComponent;
	for(auto Attribute : AttributesToSave)
	{
		if(Save->Attributes.Contains(Attribute))
		{
			const auto Key = Save->Attributes.Find(Attribute);
				*Key = AbilitySystem->GetNumericAttributeBase(Attribute);
		}
		else
			Save->Attributes.Add(Attribute, AbilitySystem->GetNumericAttributeBase(Attribute));
	}
}

void USaveComponent::SaveCasting() const
{
	if (const auto PlayerCastingComponent = PlayerCharacter->FindComponentByClass<UPlayerCastingComponent>())
		for(auto AbilityClass : PlayerCastingComponent->AbilitiesClasses)
			if(IsValid(AbilityClass))
				Save->CastingAbilitiesClasses.Add(AbilityClass);
}

void USaveComponent::SaveUpgradeEffects() const
{
	const auto AbilitySystem = PlayerCharacter->AbilitySystemComponent;
	Save->UpgradeEffects.Empty();
	// Save active gameplay effects that match query
	TArray<FActiveGameplayEffectHandle> ActiveEffectsHandles = AbilitySystem->GetActiveEffects(SaveGameplayEffectsQuery);
	for (const FActiveGameplayEffectHandle& ActiveEffectHandle : ActiveEffectsHandles)
	{
		if (const FActiveGameplayEffect* ActiveGameplayEffect = AbilitySystem->GetActiveGameplayEffect(ActiveEffectHandle))
		{
			FGameplayEffectSaveData EffectSaveData;
			EffectSaveData.EffectSpec = ActiveGameplayEffect->Spec;
			EffectSaveData.EffectElapsedTime = ActiveGameplayEffect->GetDuration() - ActiveGameplayEffect->GetTimeRemaining(AbilitySystem->GetWorld()->GetTimeSeconds());

			Save->UpgradeEffects.Add(EffectSaveData);
		}
	}
}

void USaveComponent::SaveSounds() const
{
	if(const auto SoundsComponent = PlayerCharacter->FindComponentByClass<USoundsComponent>())
	{
		Save->VolumeMultiplier = SoundsComponent->VolumeMultiplier;
		Save->PitchMultiplier = SoundsComponent->PitchMultiplier;
		Save->SoundsConfig = SoundsComponent->SoundsConfig;
	}
}

void USaveComponent::SaveGlobalActors() const
{
	if(const auto MyGameMode = AMyGameMode::GetMyGameMode(this))
	{
		Save->Biom = MyGameMode->LevelsManager->GetCurrentBiom();
		Save->RunSetup = MyGameMode->LevelsManager->RunSetup;
		Save->LevelsToRoomSetupsMap = MyGameMode->LevelsManager->LevelsToRoomSetupsMap;
		Save->LevelDepthToNextRooms = MyGameMode->LevelsManager->LevelDepthToNextRooms; 
	}
}

void USaveComponent::SaveWorldActors() const
{
	for (FActorIterator It(GetWorld()); It; ++It)
	{
		AActor* Actor = *It;
		if (!IsValid(Actor) || !Actor->Implements<USaveableInterface>())
		{
			continue;
		}
		if(Actor->bNetStartup)
		{
			FLevelActorSaveData ActorData;
			ActorData.LevelName = UGameplayStatics::GetCurrentLevelName(this);
			ActorData.ActorName = Actor->GetFName();
			ActorData.Transform = Actor->GetActorTransform();
		
			FMemoryWriter MemWriter(ActorData.ByteData);

			FObjectAndNameAsStringProxyArchive Ar(MemWriter, true);
			Ar.ArIsSaveGame = true;
			Actor->Serialize(Ar);

			Save->SavedLevelActors.Add(ActorData);
		}
		else
		{
			FSpawnedActorSaveData ActorData;
			ActorData.LevelName = UGameplayStatics::GetCurrentLevelName(this);
			ActorData.ActorClass = Actor->GetClass();
			ActorData.Transform = Actor->GetActorTransform();

			FMemoryWriter MemWriter(ActorData.ByteData);

			FObjectAndNameAsStringProxyArchive Ar(MemWriter, true);
			Ar.ArIsSaveGame = true;
			Actor->Serialize(Ar);
			Save->SavedSpawnedActors.Add(ActorData);
		}
	}
}

void USaveComponent::OnSaveCompleted(const FString& SlotName, const int32 UserIndex, bool bSuccess) const
{
	UE_LOG(LogGameplay, Log, TEXT("Game save finished"));
	USavingWidget::GetSavingWidget(this)->Hide();
	OnGameSaved.Broadcast();
}

void USaveComponent::LoadSeed() const
{
	if(const auto RandomnessManager = URandomnessManager::GetRandomnessManager(this))
		RandomnessManager->GenerateStream(Save->Seed);
}
void USaveComponent::LoadPlayerTransform() const
{
	// For the testing purposes ignore the transform load in editor
#if !WITH_EDITOR
	PlayerCharacter->SetCharacterTransform(Save->PlayerTransform);
#endif
}

void USaveComponent::LoadLook() const
{
	const auto MeshComponent = PlayerCharacter->GetMesh();
	MeshComponent->SetSkeletalMesh(Save->Mesh);
	MeshComponent->SetMaterial(0, Save->Material);
	PlayerCharacter->AnimationComponent->HideBones(Save->HiddenBones);
}

void USaveComponent::LoadWeapons() const
{
	const auto CombatComponent = PlayerCharacter->CombatComponent;
	CombatComponent->WeaponClass = Save->PrimaryWeaponClass;
	CombatComponent->SecondaryWeaponClass = Save->SecondaryWeaponClass;
	CombatComponent->SecondaryLeftWeaponClass = Save->SecondaryLeftWeaponClass;
}

void USaveComponent::LoadAttributes()
{
	const auto AbilitySystem = PlayerCharacter->AbilitySystemComponent;
	//for(auto Attribute : AttributesToSave)
	for (auto& Elem : Save->Attributes)
		AbilitySystem->SetNumericAttributeBase(Elem.Key, Elem.Value);
}

void USaveComponent::LoadCasting() const
{
	if (const auto PlayerCastingComponent = PlayerCharacter->FindComponentByClass<UPlayerCastingComponent>())
	{
		PlayerCastingComponent->AbilitiesClasses.Empty();
		
		for(int32 i = 0; i < PlayerCastingComponent->NumberOfSkills; i++)
			if(Save->CastingAbilitiesClasses.Num() > i)
				PlayerCastingComponent->AbilitiesClasses.Add(Save->CastingAbilitiesClasses[i]);
			else
				PlayerCastingComponent->AbilitiesClasses.Add(nullptr);
		
		PlayerCastingComponent->SetupAbilities();
	}
}

void USaveComponent::LoadUpgradeEffects() const
{
	const auto AbilitySystem = PlayerCharacter->AbilitySystemComponent;
	for (const FGameplayEffectSaveData& EffectSaveData : Save->UpgradeEffects)
	{
		const FActiveGameplayEffectHandle RestoredEffectHandle = AbilitySystem->ApplyGameplayEffectSpecToSelf(EffectSaveData.EffectSpec);
		//Granted abilities with handles are not saved, make sure that they are given
		for (FGameplayAbilitySpecDef AbilitySpecDef : EffectSaveData.EffectSpec.GrantedAbilitySpecs)
			AbilitySystem->GiveAbility(FGameplayAbilitySpec(AbilitySpecDef, EffectSaveData.EffectSpec.GetLevel(), INDEX_NONE));

		if (EffectSaveData.EffectSpec.Duration > 0.0f)
			AbilitySystem->ModifyActiveEffectStartTime(RestoredEffectHandle, -EffectSaveData.EffectElapsedTime);
	}
}

void USaveComponent::LoadSounds() const
{
	if (const auto SoundsComponent = PlayerCharacter->FindComponentByClass<USoundsComponent>())
	{
		SoundsComponent->VolumeMultiplier = Save->VolumeMultiplier;
		SoundsComponent->PitchMultiplier = Save->PitchMultiplier;
		SoundsComponent->SoundsConfig = Save->SoundsConfig;
	}
}

void USaveComponent::LoadWorldActors() const
{
	for (FActorIterator It(GetWorld()); It; ++It)
	{
		AActor* Actor = *It;
		if (!Actor->Implements<USaveableInterface>())
		{
			continue;
		}
		
		for (const auto ActorData : Save->SavedLevelActors)
		{
			if (ActorData.LevelName == UGameplayStatics::GetCurrentLevelName(this))
			{
				if (ActorData.ActorName == Actor->GetFName())
				{
					Actor->SetActorTransform(ActorData.Transform);

					FMemoryReader MemReader(ActorData.ByteData);

					FObjectAndNameAsStringProxyArchive Ar(MemReader, true);
					Ar.ArIsSaveGame = true;
					Actor->Serialize(Ar);

					ISaveableInterface::Execute_OnActorLoaded(Actor);
					break;
				}
			}
		}
	}

	for(const auto ActorData : Save->SavedSpawnedActors)
	{
		if (ActorData.ActorClass && ActorData.LevelName == UGameplayStatics::GetCurrentLevelName(this))
		{
			FActorSpawnParameters SpawnInfo;
			SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			
			auto Actor = GetWorld()->SpawnActor<AActor>(ActorData.ActorClass, ActorData.Transform, SpawnInfo);

			FMemoryReader MemReader(ActorData.ByteData);

			FObjectAndNameAsStringProxyArchive Ar(MemReader, true);
			Ar.ArIsSaveGame = true;
			Actor->Serialize(Ar);

			if (!Actor->Implements<USaveableInterface>())
			{
				continue;
			}
			
			ISaveableInterface::Execute_OnActorLoaded(Actor);
		}
	}
}

void USaveComponent::LoadGlobalActors() const
{
	if(const auto MyGameMode = AMyGameMode::GetMyGameMode(this))
	{
		MyGameMode->LevelsManager->SetCurrentBiom(Save->Biom);
		MyGameMode->LevelsManager->RunSetup = Save->RunSetup;
		MyGameMode->LevelsManager->LevelsToRoomSetupsMap = Save->LevelsToRoomSetupsMap;
		MyGameMode->LevelsManager->LevelDepthToNextRooms = Save->LevelDepthToNextRooms; 
	}
}

