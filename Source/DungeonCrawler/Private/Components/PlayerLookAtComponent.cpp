
#include "Components/PlayerLookAtComponent.h"

#include "Kismet/KismetMathLibrary.h"
#include "GameFramework/Character.h"
#include "AbilitySystemComponent.h"
#include "TargetSystemComponent.h"
#include "MyPlayerCameraManager.h"

void UPlayerLookAtComponent::CalculateLookAtRotation()
{
	DesiredLookAtRotation = FRotator::ZeroRotator;
	
	const auto OwnerCharacter = Cast<ACharacter>(GetOwner());
	if(!OwnerCharacter)
		return;

	if(const auto AbilitySystemComponent = OwnerCharacter->FindComponentByClass<UAbilitySystemComponent>())
		if(AbilitySystemComponent->HasMatchingGameplayTag(CannotLookAtTag))
			return;
	
	const auto TargetSystemComponent = OwnerCharacter->FindComponentByClass<UTargetSystemComponent>();
	if(!TargetSystemComponent)
		return;

	const auto OwnerMesh = OwnerCharacter->GetMesh();
	if(!OwnerMesh)
		return;
	
	const auto Target = Cast<ACharacter>(TargetSystemComponent->TargetetActor);
	const FRotator OwnerRotation = OwnerCharacter->GetActorRotation();
	
	FRotator RawLookAtRotation;
	if(Target)
		RawLookAtRotation = UKismetMathLibrary::FindLookAtRotation(OwnerMesh->GetSocketLocation(LookAtBoneName), Target->GetMesh()->GetSocketLocation(LookAtBoneName));
	else
	{
		// if no target found rotate to the camera direction
		const auto CameraManager = AMyPlayerCameraManager::GetMyPlayerCameraManager(this);
		if(!CameraManager)
			return;
		
		RawLookAtRotation = CameraManager->GetCameraRotation();
	}
	
	auto DeltaRotation = UKismetMathLibrary::NormalizedDeltaRotator(RawLookAtRotation, OwnerRotation);
	SetDesiredLookAtRotation(DeltaRotation);
	
}
