
#include "Components/UpgradeHandlingComponent.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "DamageResponseComponent.h"
#include "Kismet/GameplayStatics.h"
#include "AbilitySystemComponent.h"
#include "Engine/DamageEvents.h"
#include "MyAttributeSet.h"
#include "StatsComponent.h"
#include "BaseCharacter.h"

void UUpgradeHandlingComponent::BeginPlay()
{
	Super::BeginPlay();

	BaseCharacterOwner = Cast<ABaseCharacter>(GetOwner());
	OwnerAbilitySystemComponent = GetOwner()->FindComponentByClass<UAbilitySystemComponent>();

	if(!BaseCharacterOwner)
		return;

	if(!OwnerAbilitySystemComponent)
		return;
	
	BindDelegates();
}

void UUpgradeHandlingComponent::BindDelegates()
{
	const auto StatsComponent = BaseCharacterOwner->StatsComponent;

	const auto DamageResponseComponent = BaseCharacterOwner->DamageResponseComponent;
	StatsComponent->OnStatChanged.AddDynamic(this, &UUpgradeHandlingComponent::OnStatChanged);

	DamageResponseComponent->OnDealedDamage.AddDynamic(this, &UUpgradeHandlingComponent::OnAttackDamageDealed);
	DamageResponseComponent->OnTakenDamage.AddDynamic(this, &UUpgradeHandlingComponent::OnAttackDamageTaken);
	DamageResponseComponent->OnCritDamageDealed.AddDynamic(this, &UUpgradeHandlingComponent::OnAttackCrit);
	DamageResponseComponent->OnParry.AddDynamic(this, &UUpgradeHandlingComponent::OnAttackParried);
	DamageResponseComponent->OnKill.AddDynamic(this, &UUpgradeHandlingComponent::OnKill);
}

void UUpgradeHandlingComponent::OnStatChanged(EStat Stat, float Amount)
{
	switch (Stat)
	{
	//Mana restore on damage taken upgrade
	case EStat::Health:
		if(Amount < 0.0f)
			if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(DamageTakeManaGainTag))
			{
				const FGameplayEffectContextHandle EffectContext;

				OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(RestoreManaEffect, -1.0f * DamageTakenManaGainMultiplier * Amount, EffectContext);
			}
	
	default: ;
	}
}

void UUpgradeHandlingComponent::OnAttackCrit(TSubclassOf<UDamageType> DamageType, AActor* DamageCauser, float Amount)
{
	if(DamageCauser == GetOwner())
		return;
	
	//Stamina restore on crit
	if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(CritStaminaGainTag))
	{
		const FGameplayEffectContextHandle EffectContext;
		OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(RestoreStaminaWithCueEffect, StaminaCritAmount, EffectContext);
	}
	//Heal on crit
	if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(CritHealTag))
	{
		const FGameplayEffectContextHandle EffectContext;
		OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(RestoreHealthWithCueEffect, HealthGainPerCrit, EffectContext);
	}
	//Double mana on crit
	OnAttackDamageDealed(DamageType, DamageCauser, Amount);
}

void UUpgradeHandlingComponent::OnAttackParried(AActor* Actor, TSubclassOf<UDamageType> DamageType, float Amount)
{
	if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(ParryStaminaRestorationTag))
	{
		bool bSuccess;
		const float Value = UAbilitySystemBlueprintLibrary::GetFloatAttribute(GetOwner(), UMyAttributeSet::GetCurrentActionStaminaCostAttribute(), bSuccess);
		const FGameplayEffectContextHandle EffectContext;
		OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(RestoreStaminaWithCueEffect, Value, EffectContext);
	}
	if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(ParryDamageDealTag))
		UGameplayStatics::ApplyDamage(Actor, Amount, BaseCharacterOwner->GetController(), BaseCharacterOwner, DamageType);
}

void UUpgradeHandlingComponent::OnAttackDamageDealed(TSubclassOf<UDamageType> DamageType, AActor* DamageCauser, float Amount)
{
	if(DamageCauser == GetOwner())
		return;
	
	//Mana restore on melee attack, enabled by default
	if(!OwnerAbilitySystemComponent->HasMatchingGameplayTag(ManaGenerationBlockedTag))
		if(const auto MyDamageTypeClass = Cast<UMyDamageType>(DamageType->GetDefaultObject())->GetClass())
			if(Cast<UMyDamageType>(MyDamageTypeClass->GetDefaultObject())->DamageTags.HasTag(DamageMeleeTag))
			{
				const FGameplayEffectContextHandle EffectContext;
				OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(RestoreManaEffect, ManaPerMeleeDamageGainMultiplier * Amount, EffectContext);
			}
}

void UUpgradeHandlingComponent::OnAttackDamageTaken(TSubclassOf<UDamageType> DamageType, AActor* DamageCauser, float Amount)
{
	if(DamageCauser && DamageCauser != GetOwner())
		if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(DamageReflectionTag))
		{
			FDamageEvent DamageEvent;
			DamageEvent.DamageTypeClass = DeflectedDamageTypeClass;
			DamageCauser->TakeDamage(Amount, DamageEvent, GetOwner()->GetInstigatorController(), GetOwner());
		}
}

void UUpgradeHandlingComponent::OnKill(AActor* Target)
{
	// Do not trigger when suiciding
	if(Target == GetOwner())
		return;
	
	//Heal on kill upgrade
	if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(HealOnKillTag))
	{
		const FGameplayEffectContextHandle EffectContext;
		OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(RestoreHealthWithCueEffect, HealthGainPerKill, EffectContext);
	}
	//Mana restore on kill upgrade
	if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(ManaRestoreOnKillTag))
	{
		const FGameplayEffectContextHandle EffectContext;
		OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(RestoreManaWithCueEffect, ManaRestorePerKill, EffectContext);
	}
	//Stamina restore on kill upgrade
	if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(StaminaRestoreOnKillTag))
	{
		const FGameplayEffectContextHandle EffectContext;
		OwnerAbilitySystemComponent->BP_ApplyGameplayEffectToSelf(RestoreStaminaWithCueEffect, StaminaRestorePerKill, EffectContext);
	}
}
