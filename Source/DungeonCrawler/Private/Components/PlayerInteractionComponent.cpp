
#include "PlayerInteractionComponent.h"

#include "Kismet/KismetSystemLibrary.h"
#include "InteractableInterface.h"
#include "InteractionComponent.h"

void UPlayerInteractionComponent::BeginPlay()
{
	Super::BeginPlay();

	OnComponentBeginOverlap.AddDynamic(this, &UPlayerInteractionComponent::OnBeginOverlap);
	OnComponentEndOverlap.AddDynamic(this, &UPlayerInteractionComponent::OnEndOverlap);
}

void UPlayerInteractionComponent::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{		
	if(const auto InteractionComponent = Cast<UInteractionComponent>(OtherComp))
		PossibleInteractionCandidates.AddUnique(InteractionComponent);
}

void UPlayerInteractionComponent::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (const auto OtherCompInteraction = Cast<UInteractionComponent>(OtherComp))
	{
		if(OtherCompInteraction == InteractionCandidate)
			SetInteractionCandidate(nullptr);
		
		PossibleInteractionCandidates.Remove( Cast<UInteractionComponent>(OtherCompInteraction));
	}
}

void UPlayerInteractionComponent::InteractAction()
{
	if (InteractionCandidate)
		InteractionCandidate->Interact(this);
}

AActor* UPlayerInteractionComponent::GetOwningActor() const
{
	if(const auto Controller = Cast<AController>(GetOwner()))
		return Controller->GetPawn();
	
	return GetOwner();
}

void UPlayerInteractionComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	
	if(UInteractionComponent* BestCandidate = FindBestCandidate())
		SetInteractionCandidate(BestCandidate);
	
	if(InteractionCandidate)
		InteractionCandidate->bIsFocused = true;
}

void UPlayerInteractionComponent::SetInteractionCandidate(UInteractionComponent* NewInteractionCandidate)
{
	if(InteractionCandidate != NewInteractionCandidate)
	{
		InteractionCandidate = NewInteractionCandidate;
		OnInteractionCandidateSet.Broadcast(InteractionCandidate);
	}
}

UInteractionComponent* UPlayerInteractionComponent::FindBestCandidate()
{
	UInteractionComponent* BestCandidate = nullptr;
	
	TArray<AActor*> ActorsToIgnore;
	FHitResult HitResult;

	float DistanceToTheClosestCandidate = 9999999.0f;
	for(const auto Candidate : PossibleInteractionCandidates)
	{
		if(!Candidate)
			continue;
		
		//Only consider candidates that can be interacted with
		const auto CandidateOwnerActor = Candidate->GetOwner();
		if(const auto InteractableInterface = Cast<IInteractableInterface>(CandidateOwnerActor))
			if(!InteractableInterface->Execute_CanInteract(CandidateOwnerActor) || Candidate->bInteractionBlocked)
				continue;
			
		//Ignore obscured candidates.
		ActorsToIgnore.Add(Candidate->GetOwner());
		const bool bVisibilityBlocked = UKismetSystemLibrary::LineTraceSingle(this, GetComponentLocation(), Candidate->GetComponentLocation(), ETraceTypeQuery::TraceTypeQuery1, false, ActorsToIgnore, EDrawDebugTrace::None, HitResult, true);
		ActorsToIgnore.Empty();
		if(bVisibilityBlocked)
			continue;

		Candidate->bIsFocused = false;
		const float DistanceToTheCurrentCandidate = (GetComponentLocation() - Candidate->GetComponentLocation()).Size();
		if(DistanceToTheCurrentCandidate < DistanceToTheClosestCandidate)
		{
			DistanceToTheClosestCandidate = DistanceToTheCurrentCandidate;
			BestCandidate = Candidate;
		}
	}
	return BestCandidate;
}