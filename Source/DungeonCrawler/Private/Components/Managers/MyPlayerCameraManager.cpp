
#include "MyPlayerCameraManager.h"

#include "Kismet/GameplayStatics.h"

AMyPlayerCameraManager* AMyPlayerCameraManager::GetMyPlayerCameraManager(const UObject* WorldContextObject)
{
	return Cast<AMyPlayerCameraManager>(UGameplayStatics::GetPlayerCameraManager(WorldContextObject, 0));
}

void AMyPlayerCameraManager::ApplyCameraModifiers(const float DeltaTime, FMinimalViewInfo& InOutPOV)
{
	Super::ApplyCameraModifiers(DeltaTime, InOutPOV);

	if (UGameplayStatics::IsGamePaused(this))
	{
		PostProcessSettingsCache = InOutPOV.PostProcessSettings;
		InOutPOV.PostProcessSettings = PostProcessSettingsOverrideWhenPaused;
	}
	else
		InOutPOV.PostProcessSettings = PostProcessSettingsCache;
}