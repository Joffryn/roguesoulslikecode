
#include "Components/Managers/DataTablesManager.h"

#include "MyGameMode.h"

UDataTablesManager* UDataTablesManager::GetDataTablesManager(const UObject* WorldContextObject)
{
	if(const auto MyGameMode = AMyGameMode::GetMyGameMode(WorldContextObject))
		return MyGameMode->DataTablesManager;

	return nullptr;
}
