
#include "Components/Managers/LevelsManager.h"

#include "MyGameMode.h"

ULevelsManager* ULevelsManager::GetLevelsManager(const UObject* WorldContextObject)
{
	if(const auto MyGameMode = AMyGameMode::GetMyGameMode(WorldContextObject))
		return MyGameMode->LevelsManager;

	return nullptr;
}

void ULevelsManager::StartNewRun(const EBiom InBiom)
{
	SetCurrentBiom(InBiom);
	SetupRun();
	GenerateRunBiomSetups();
}

EBiom ULevelsManager::GetCurrentBiom() const
{
	return CurrentBiom;
}

void ULevelsManager::SetCurrentBiom(const EBiom NewBiom)
{
	CurrentBiom = NewBiom;
}

void ULevelsManager::SetupRun()
{
	if(const auto BiomsDataTable = GetCurrentBiomDataTable())
		for(const auto RowName : BiomRunSetups->GetRowNames())
			if(const auto Row = BiomsDataTable->FindRow<FRunSetup>(RowName, TEXT("")))
				if(Row->Biom == CurrentBiom)
					RunSetup = *Row;
}

UDataTable* ULevelsManager::GetCurrentBiomDataTable()
{
	if(BiomsToChambersConfig.Contains(CurrentBiom))
		return *BiomsToChambersConfig.Find(CurrentBiom);

	return nullptr;
}

void ULevelsManager::GenerateRunBiomSetups()
{
	RoomsNamesStorage = FRoomsNamesStorage();
	
	if(const auto BiomsDataTable = GetCurrentBiomDataTable())
		for(const auto RowName : BiomsDataTable->GetRowNames())
			if(const auto Row = BiomsDataTable->FindRow<FLevelSetup>(RowName, TEXT("")))
			{
				switch (Row->Position)
				{
				case EChamberPositionType::Entrance:
					RoomsNamesStorage.EntranceLevels.Add(Row->LevelName);
					break;
				case EChamberPositionType::Common:
					RoomsNamesStorage.CommonLevels.Add(Row->LevelName);
					break;
				case EChamberPositionType::Special:
					RoomsNamesStorage.SpecialLevels.Add(Row->LevelName);
					break;
				case EChamberPositionType::PreBoss:
					RoomsNamesStorage.PreBossLevels.Add(Row->LevelName);
					break;
				case EChamberPositionType::Boss:
					RoomsNamesStorage.BossLevels.Add(Row->LevelName);
					break;
				case EChamberPositionType::Shop:
					RoomsNamesStorage.ShopLevels.Add(Row->LevelName);
					break;
				case EChamberPositionType::Respite:
					RoomsNamesStorage.RespiteLevels.Add(Row->LevelName);
					break;
				case EChamberPositionType::None:
					break;
				default:
					break;
				}
			}

	GenerateEntranceRoom();
	// Skip 3 levels, entrance, preboss and boss
	for(int32 i = 1; i < RunSetup.RunLength - 2; i++)
		GenerateRoomSetupForSingleDepth(i);
	
	GeneratePreBossRooms();
	GenerateBossRoom();

	OnRunSetupGenerated.Broadcast();
}

void ULevelsManager::PrintRunSetup()
{
	for(auto Pair : LevelDepthToNextRooms)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, "Depth : " + FString::FromInt(Pair.Key));
		for(auto Room : Pair.Value.Rooms)
				GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Green, "Level name : " + Room.LevelName.ToString());
		
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Yellow, "");
	}
}

void ULevelsManager::GenerateEntranceRoom()
{
	if(RoomsNamesStorage.EntranceLevels.Num() > 0)
	{
		const FName RoomName = RoomsNamesStorage.EntranceLevels[FMath::RandRange(0, RoomsNamesStorage.EntranceLevels.Num() - 1)];
		
		const FName FirstRoomName = RoomsNamesStorage.CommonLevels[FMath::RandRange(0, RoomsNamesStorage.CommonLevels.Num() - 1)];
		RoomsNamesStorage.CommonLevels.Remove(FirstRoomName);
		const FName SecondLevelName = RoomsNamesStorage.CommonLevels[FMath::RandRange(0, RoomsNamesStorage.CommonLevels.Num() - 1)];
		RoomsNamesStorage.CommonLevels.Remove(SecondLevelName);
		
		FRunRoomRandomizedSetup RoomSetup;
		RoomSetup.ChamberType = EChamberType::Entrance;
		RoomSetup.NextLevelsNames.Add(FirstRoomName);
		RoomSetup.NextLevelsNames.Add(SecondLevelName);
		RoomSetup.LevelName = RoomName;
		
		LevelsToRoomSetupsMap.Add(RoomName, RoomSetup);
		
		FRunRoomArray RunRoomArray;
		RunRoomArray.Rooms.Add(RoomSetup);
		LevelDepthToNextRooms.Add(0, RunRoomArray);
	}
}

void ULevelsManager::GenerateRoomSetupForSingleDepth(const int32 Depth)
{
	if(RoomsNamesStorage.CommonLevels.Num() > 0)
	{
		const FName FirstExistingRoomName = LevelDepthToNextRooms.Find(Depth - 1)->Rooms[0].NextLevelsNames[0];
		const FName SecondExistingRoomName = LevelDepthToNextRooms.Find(Depth - 1)->Rooms[0].NextLevelsNames[1];

		FName FirstRoomName;
		FName SecondLevelName;
		if(Depth == RunSetup.RunLength - 3)
		{
			FirstRoomName = RoomsNamesStorage.PreBossLevels[FMath::RandRange(0, RoomsNamesStorage.PreBossLevels.Num() - 1)];
			RoomsNamesStorage.PreBossLevels.Remove(FirstRoomName);
			SecondLevelName = RoomsNamesStorage.PreBossLevels[FMath::RandRange(0, RoomsNamesStorage.PreBossLevels.Num() - 1)];
			RoomsNamesStorage.PreBossLevels.Remove(SecondLevelName);
		}
		else
		{
			FirstRoomName = RoomsNamesStorage.CommonLevels[FMath::RandRange(0, RoomsNamesStorage.CommonLevels.Num() - 1)];
			RoomsNamesStorage.CommonLevels.Remove(FirstRoomName);
			SecondLevelName = RoomsNamesStorage.CommonLevels[FMath::RandRange(0, RoomsNamesStorage.CommonLevels.Num() - 1)];
			RoomsNamesStorage.CommonLevels.Remove(SecondLevelName);
		}
		
		FRunRoomRandomizedSetup FirstRoomSetup;
		FirstRoomSetup.ChamberType = EChamberType::Encounter;
		FirstRoomSetup.NextLevelsNames.Add(FirstRoomName);
		FirstRoomSetup.NextLevelsNames.Add(SecondLevelName);
		FirstRoomSetup.LevelName = FirstExistingRoomName;
		
		FRunRoomRandomizedSetup SecondRoomSetup;
		SecondRoomSetup.ChamberType = EChamberType::Encounter;
		SecondRoomSetup.NextLevelsNames.Add(FirstRoomName);
		SecondRoomSetup.NextLevelsNames.Add(SecondLevelName);
		SecondRoomSetup.LevelName = SecondExistingRoomName;
		
		LevelsToRoomSetupsMap.Add(FirstExistingRoomName, FirstRoomSetup);
		LevelsToRoomSetupsMap.Add(SecondExistingRoomName, SecondRoomSetup);
		
		FRunRoomArray RunRoomArray;
		RunRoomArray.Rooms.Add(FirstRoomSetup);
		RunRoomArray.Rooms.Add(SecondRoomSetup);
		LevelDepthToNextRooms.Add(Depth, RunRoomArray);
	}
}

void ULevelsManager::GeneratePreBossRooms()
{
	if(RoomsNamesStorage.PreBossLevels.Num() > 0)
	{
		const int32 Depth = RunSetup.RunLength - 2;
		const FName FirstExistingRoomName = LevelDepthToNextRooms.Find(Depth - 1)->Rooms[0].NextLevelsNames[0];
		const FName SecondExistingRoomName = LevelDepthToNextRooms.Find(Depth - 1)->Rooms[0].NextLevelsNames[1];

		const FName BossRoomName = RoomsNamesStorage.BossLevels[FMath::RandRange(0, RoomsNamesStorage.BossLevels.Num() - 1)];
		RoomsNamesStorage.BossLevels.Remove(BossRoomName);
		
		FRunRoomRandomizedSetup FirstRoomSetup;
		FirstRoomSetup.ChamberType = EChamberType::Encounter;
		FirstRoomSetup.NextLevelsNames.Add(BossRoomName);
		FirstRoomSetup.LevelName = FirstExistingRoomName;
		
		FRunRoomRandomizedSetup SecondRoomSetup;
		SecondRoomSetup.ChamberType = EChamberType::Encounter;
		SecondRoomSetup.NextLevelsNames.Add(BossRoomName);
		SecondRoomSetup.LevelName = SecondExistingRoomName;
		
		LevelsToRoomSetupsMap.Add(FirstExistingRoomName, FirstRoomSetup);
		LevelsToRoomSetupsMap.Add(SecondExistingRoomName, SecondRoomSetup);
		
		FRunRoomArray RunRoomArray;
		RunRoomArray.Rooms.Add(FirstRoomSetup);
		RunRoomArray.Rooms.Add(SecondRoomSetup);
		LevelDepthToNextRooms.Add(Depth, RunRoomArray);
	}
}

void ULevelsManager::GenerateBossRoom()
{
	if(RoomsNamesStorage.PreBossLevels.Num() > 0)
	{
		const int32 Depth = RunSetup.RunLength - 1;
		const FName ExistingRoomName = LevelDepthToNextRooms.Find(Depth - 1)->Rooms[0].NextLevelsNames[0];
		
		FRunRoomRandomizedSetup RoomSetup;
		RoomSetup.ChamberType = EChamberType::Boss;
		RoomSetup.LevelName = ExistingRoomName;
		
		LevelsToRoomSetupsMap.Add(ExistingRoomName, RoomSetup);
		
		FRunRoomArray RunRoomArray;
		RunRoomArray.Rooms.Add(RoomSetup);
		LevelDepthToNextRooms.Add(Depth, RunRoomArray);
	}
}

void ULevelsManager::GenerateRoom(EChamberPositionType Type)
{
	
}

void ULevelsManager::BeginPlay()
{
	Super::BeginPlay();

}


