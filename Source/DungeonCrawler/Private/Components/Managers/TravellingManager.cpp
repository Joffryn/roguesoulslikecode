
#include "Components/Managers/TravellingManager.h"

#include "MyPlayerCameraManager.h"
#include "MyPlayerController.h"
#include "MyGameplayStatics.h"
#include "MyGameInstance.h"
#include "BaseCharacter.h"
#include "SaveComponent.h"
#include "MyGameMode.h"
#include "MyHUD.h"
#include "Logs.h"

UTravellingManager* UTravellingManager::GetTravellingManager(const UObject* WorldContextObject)
{
	if(const auto MyGameMode = AMyGameMode::GetMyGameMode(WorldContextObject))
		return MyGameMode->TravellingManager;

	return nullptr;
}

void UTravellingManager::TravelToLevel(const FName LevelName)
{
	bHasPendingSave = true;
	bHaseFadedOut = false;
	if(const auto SaveComponent =  USaveComponent::GetSaveComponent(this))
		SaveComponent->SaveGame(LevelName, false);
	
	if(const auto MyHud =  AMyHUD::GetMyHud(this))
		MyHud->HideHudWidget();
	
	if(const auto PlayerCharacter = ABaseCharacter::GetMyPlayerCharacter(this))
		PlayerCharacter->DisableInput(AMyPlayerController::GetMyPlayerController(this));
			
	if(const auto MyPlayerCameraManager = AMyPlayerCameraManager::GetMyPlayerCameraManager(this))
		MyPlayerCameraManager->StartCameraFade(StartTravelFadeSetup.FromAlpha, StartTravelFadeSetup.ToAlpha, StartTravelFadeSetup.Duration,
			FLinearColor::Black, StartTravelFadeSetup.bShouldFadeAudio, StartTravelFadeSetup.bHoldWhenFinished);

	UE_LOG(LogGameplay, Log, TEXT("Travel to level %s requested"), *LevelName.ToString());

	RequestedLevelName = LevelName;
	FTimerDelegate TimerCallback;
	TimerCallback.BindUObject(this, &UTravellingManager::Travel);
	GetWorld()->GetTimerManager().SetTimer(Handle, TimerCallback, StartTravelFadeSetup.Duration, false);
}

void UTravellingManager::HandleLevelLoaded()
{
	const auto MyGameInstance =  UMyGameInstance::GetMyGameInstance(this);

	if(!MyGameInstance)
		return;

	if(!MyGameInstance->bIsTravellingBetweenLevels)
		return;

	//Give a frame for HUD to create widget
	GetWorld()->GetTimerManager().SetTimerForNextTick(FTimerDelegate::CreateLambda([=]()
	{
		if(const auto MyHud =  AMyHUD::GetMyHud(this))
        		MyHud->ShowHudWidget();
	}));
	
	if(const auto PlayerCharacter = ABaseCharacter::GetMyPlayerCharacter(this))
		PlayerCharacter->DisableInput(AMyPlayerController::GetMyPlayerController(this));
			
	if(const auto MyPlayerCameraManager = AMyPlayerCameraManager::GetMyPlayerCameraManager(this))
		MyPlayerCameraManager->StartCameraFade(EndTravelFadeSetup.FromAlpha, EndTravelFadeSetup.ToAlpha, EndTravelFadeSetup.Duration,
			FLinearColor::Black, EndTravelFadeSetup.bShouldFadeAudio, EndTravelFadeSetup.bHoldWhenFinished);
	
	FTimerDelegate TimerCallback;
	TimerCallback.BindUObject(this, &UTravellingManager::BeginPlayFadeFinished);
	//Let player move before the end of the fade
	GetWorld()->GetTimerManager().SetTimer(Handle, TimerCallback, EndTravelFadeSetup.Duration / 2.0f, false);
}

void UTravellingManager::BeginPlay()
{
	Super::BeginPlay();
	
	HandleLevelLoaded();

	if(const auto SaveComponent =  USaveComponent::GetSaveComponent(this))
		SaveComponent->OnGameSaved.AddDynamic(this, &UTravellingManager::OnSaveFinished);
	
}

void UTravellingManager::Travel()
{
	bHaseFadedOut = true;
	if(bHasPendingSave)
		return;
	
	GetWorld()->GetTimerManager().ClearTimer(Handle);
	
	UMyGameplayStatics::TravelToLevel(this, RequestedLevelName);
}

void UTravellingManager::BeginPlayFadeFinished()
{
	GetWorld()->GetTimerManager().ClearTimer(Handle);
	
	if(const auto PlayerCharacter = ABaseCharacter::GetMyPlayerCharacter(this))
		PlayerCharacter->EnableInput(AMyPlayerController::GetMyPlayerController(this));
}

void UTravellingManager::OnSaveFinished()
{
	bHasPendingSave = false;
	if(bHaseFadedOut)
		Travel();
}

