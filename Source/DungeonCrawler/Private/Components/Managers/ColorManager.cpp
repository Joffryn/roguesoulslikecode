
#include "Components/Managers/ColorManager.h"

#include "MyGameMode.h"

UColorManager* UColorManager::GetColorManager(const UObject* WorldContextObject)
{
	if(const auto MyGameMode = AMyGameMode::GetMyGameMode(WorldContextObject))
		return MyGameMode->ColorManager;

	return nullptr;
}

FLinearColor UColorManager::GetColor(const EColor Color)
{
	if(const auto FoundColor = ColorsMap.Find(Color))
		return *FoundColor;

	return FLinearColor::White;
}
