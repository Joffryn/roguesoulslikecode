
#include "Components/Managers/RandomnessManager.h"

#include "Kismet/GameplayStatics.h"
#include "MyGameMode.h"
#include "Logs.h"

URandomnessManager* URandomnessManager::GetRandomnessManager(const UObject* WorldContextObject)
{
	if(const auto MyGameMode = AMyGameMode::GetMyGameMode(WorldContextObject))
		return MyGameMode->RandomnessManager;

	return nullptr;
}

FRandomStream URandomnessManager::GetStream() const
{
	if(bAlwaysUseRandomSeed)
	{
		FMath::SRandInit(UGameplayStatics::GetTimeSeconds(this));
		return FRandomStream(FMath::Rand());
	}
	
	return Stream;
}

void URandomnessManager::GenerateRandomStream()
{
	FMath::SRandInit(UGameplayStatics::GetTimeSeconds(this));
	GenerateStream(FMath::Rand());
	UE_LOG(LogSpawning, Log, TEXT("Generated seed %d"), Seed);
}

void URandomnessManager::GenerateStream(const int32 InSeed)
{
	Seed = InSeed;
	Stream = FRandomStream(Seed);
}

void URandomnessManager::BeginPlay()
{
	Super::BeginPlay();

	if(!Seed)
		GenerateRandomStream();
}

