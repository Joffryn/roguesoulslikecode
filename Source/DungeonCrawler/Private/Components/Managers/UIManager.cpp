
#include "Components/Managers/UIManager.h"

#include "MyGameMode.h"

UUIManager* UUIManager::GetAIManager(const UObject* WorldContextObject)
{
	if(const auto MyGameMode = AMyGameMode::GetMyGameMode(WorldContextObject))
		return MyGameMode->UIManager;

	return nullptr;
}

