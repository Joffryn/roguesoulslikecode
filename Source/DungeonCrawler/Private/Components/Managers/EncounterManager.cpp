
#include "Components/Managers/EncounterManager.h"

#include "MyAbilitySystemComponent.h"
#include "MyGameplayStatics.h"
#include "LootComponent.h"
#include "BaseCharacter.h"
#include "MyGameMode.h"

UEncounterManager* UEncounterManager::GetEncounterManager(const UObject* WorldContextObject)
{
	if(const auto MyGameMode = AMyGameMode::GetMyGameMode(WorldContextObject))
		return MyGameMode->EncounterManager;

	return nullptr;
}

void UEncounterManager::RegisterEncounter(AEncounter* Encounter)
{
	if(Encounter)
	{
		RegisteredEncounters.AddUnique(Encounter);
		Encounter->OnEncounterStarted.AddDynamic(this, &UEncounterManager::OnEncounterStarted);
		Encounter->OnEncounterFinished.AddDynamic(this, &UEncounterManager::OnEncounterFinished);
		Encounter->OnEncounterCharacterDeath.AddDynamic(this, &UEncounterManager::OnEncounterCharacterDeath);
	}
}

void UEncounterManager::BeginPlay()
{
	Super::BeginPlay();
	PlayerAbilitySystemComponent = UMyAbilitySystemComponent::GetPlayerAbilitySystemComponent(this);
	PlayerLootComponent = UPlayerLootComponent::GetPlayerLootComponent(this);
	ApplyOutsideEncounterEffects();
}

bool UEncounterManager::IsDuringEncounter() const
{
	if(PlayerAbilitySystemComponent)
		return PlayerAbilitySystemComponent->HasMatchingGameplayTag(DuringEncounterTag);

	return false;
}

void UEncounterManager::ApplyOutsideEncounterEffects()
{
	const FGameplayEffectContextHandle EffectContext;
	for(auto Effect : EffectsToApplyOutsideEncounter)
		if(PlayerAbilitySystemComponent)
			PlayerAbilitySystemComponent->ApplyGameplayEffectToSelf(Effect.GetDefaultObject(), 0.0f, EffectContext);
}

void UEncounterManager::RemoveOutsideEncounterEffects()
{
	for(const auto Effect : EffectsToApplyOutsideEncounter)
		if(PlayerAbilitySystemComponent)
			PlayerAbilitySystemComponent->RemoveActiveGameplayEffectBySourceEffect(Effect, nullptr);
}

void UEncounterManager::ApplyDuringEncounterEffects()
{
	const FGameplayEffectContextHandle EffectContext;
	for(auto Effect : EffectsToApplyDuringEncounter)
		if(PlayerAbilitySystemComponent)
			PlayerAbilitySystemComponent->ApplyGameplayEffectToSelf(Effect.GetDefaultObject(), 0.0f, EffectContext);
}

void UEncounterManager::RemoveDuringEncounterEffects()
{
	for(const auto Effect : EffectsToApplyDuringEncounter)
		if(PlayerAbilitySystemComponent)
			PlayerAbilitySystemComponent->RemoveActiveGameplayEffectBySourceEffect(Effect, nullptr);
}

void UEncounterManager::ApplyAfterEncounterEffects()
{
	for(const auto EffectSetup : EffectSetupsToApplyAfterEncounter)
	{
		if(PlayerAbilitySystemComponent->HasAnyMatchingGameplayTags(EffectSetup.BlockingTags))
			continue;

		const FGameplayEffectContextHandle EffectContext;
		if(PlayerAbilitySystemComponent->HasAnyMatchingGameplayTags(EffectSetup.RequiredTags) || EffectSetup.RequiredTags.IsEmpty())
			if(PlayerAbilitySystemComponent)
				PlayerAbilitySystemComponent->ApplyGameplayEffectToSelf(EffectSetup.Effect.GetDefaultObject(), EffectSetup.Level, EffectContext);
	}
}

void UEncounterManager::FinishCurrentEncounters()
{
	for(const auto Encounter : RegisteredEncounters)
		if(Encounter->GetState() == EEncounterState::Active)
			Encounter->ForceFinish();
}

void UEncounterManager::OnEncounterCharacterDeath(ABaseCharacter* Character)
{
	
}

void UEncounterManager::OnEncounterStarted()
{
	RemoveOutsideEncounterEffects();
	ApplyDuringEncounterEffects();
}

void UEncounterManager::OnEncounterFinished()
{
	RemoveDuringEncounterEffects();
	ApplyOutsideEncounterEffects();
	ApplyAfterEncounterEffects();
}
