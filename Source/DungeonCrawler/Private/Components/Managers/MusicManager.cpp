
#include "Components/Managers/MusicManager.h"

#include "Components/AudioComponent.h"
#include "MyGameMode.h"
#include "Logs.h"

UMusicManager* UMusicManager::GetMusicManager(const UObject* WorldContextObject)
{
	if(const auto MyGameMode = AMyGameMode::GetMyGameMode(WorldContextObject))
		return MyGameMode->MusicManager;

	return nullptr;
}

void UMusicManager::PlayMusic(USoundBase* Music, const float FadeInTime  /*= 1.5f*/)
{
	if (!Music)
		return;

	if (CurrentlyPlayedMusic)
	{
		QueuedMusics.Add(FMusic(Music));
		return;
	}

	if(!AudioComponent)
		CreateMusicAudioComponent();
	
	CurrentlyPlayedMusic = Music;
	AudioComponent->SetSound(Music);
	FadeInMusic(FadeInTime);
}

void UMusicManager::StopMusic(USoundBase* Music /*= nullptr*/, const float FadeOutTime /*= 1.5f*/)
{
	if (CurrentlyPlayedMusic == Music || Music == nullptr)
		FadeOutMusic(FadeOutTime);
	else
		RemoveFromQueue(Music);
}

void UMusicManager::PauseMusic() const
{
	AudioComponent->SetPaused(true);
}

void UMusicManager::UnpauseMusic() const 
{
	AudioComponent->SetPaused(false);
}

void UMusicManager::BeginPlay()
{
	Super::BeginPlay();
	CreateMusicAudioComponent();
}

void UMusicManager::FadeInMusic(const float FadeInTime /*= 1.5f*/) const
{
	if(AudioComponent->Sound)
		UE_LOG(LogGameplay, Log, TEXT("Music stared: %s"), *AudioComponent->Sound->GetName());

	AudioComponent->FadeIn(FadeInTime, 1.0f, 0.f, EAudioFaderCurve::Logarithmic);
}

void UMusicManager::FadeOutMusic(const float FadeOutTime /*= 1.5f*/)
{
	if (AudioComponent->Sound)
		UE_LOG(LogGameplay, Log, TEXT("Music stopped: %s"), *AudioComponent->Sound->GetName());

	CurrentlyPlayedMusic = nullptr;
	AudioComponent->FadeOut(FadeOutTime, 0.0f);
	HandleMusicQueue();
}

void UMusicManager::HandleMusicQueue()
{
	if(!QueuedMusics.IsEmpty())
	{
		PlayMusic(QueuedMusics[0].Sound, DefaultMusicFadeTime);
		QueuedMusics.RemoveAt(0);
	}
}

void UMusicManager::RemoveFromQueue(const USoundBase* Music)
{
	int32 Index = -1;
	for (int32 i = 0; i < QueuedMusics.Num(); i++)
	{
		if (QueuedMusics[i].Sound == Music)
		{
			Index = i;
			break;
		}
	}
	if (Index >= 0)
		QueuedMusics.RemoveAt(Index);
}

void UMusicManager::CreateMusicAudioComponent()
{
	if(AudioComponent)
		return;
	
	AudioComponent = NewObject<UAudioComponent>(this, UAudioComponent::StaticClass(), "Music");
	AudioComponent->AttachToComponent(GetOwner()->GetRootComponent(), FAttachmentTransformRules::KeepRelativeTransform);
	AudioComponent->bIsUISound = true;
}
