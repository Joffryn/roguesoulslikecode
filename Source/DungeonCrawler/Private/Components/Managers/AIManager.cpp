
#include "Components/Managers/AIManager.h"

#include "BossComponent.h"
#include "AIController.h"
#include "MyGameMode.h"
#include "Logs.h"

UAIManager* UAIManager::GetAIManager(const UObject* WorldContextObject)
{
	if(const auto MyGameMode = AMyGameMode::GetMyGameMode(WorldContextObject))
		return MyGameMode->AIManager;

	return nullptr;
}

bool UAIManager::AreThereAnyEnemies() const
{
	return RegisteredAIs.Num() > 0;
}

bool UAIManager::IsThereAIAwareOfThePlayer() const
{
	return AIsAwareOfThePlayer.Num() > 0;
}

void UAIManager::AIStartBeingAwareOfThePlayer(AAIController* AIController)
{
	if (AIsAwareOfThePlayer.Num() == 0)
	{
		OnFirstEnemyStartBeingAwareOfThePlayer.Broadcast();
		UE_LOG(LogAI, Log, TEXT("Player has been detected."));
	}
	
	AIsAwareOfThePlayer.AddUnique(AIController);
	
	if (const auto Pawn = AIController->GetPawn())
	{
		UE_LOG(LogAI, Log, TEXT("AI started being aware of the player: %s"), *Pawn->GetName());
		if (Pawn->FindComponentByClass<UBossComponent>())
			BossesAwareOfThePlayer.Add(AIController);
	}
}

void UAIManager::AIStopBeingAwareOfThePlayer(AAIController* AIController)
{
	if(BossesAwareOfThePlayer.Contains(AIController))
	{
		BossesAwareOfThePlayer.Remove(AIController);
		if(BossesAwareOfThePlayer.Num() == 0)
			OnLastBossStopBeingAwareOfThePlayer.Broadcast();
	}

	AIsAwareOfThePlayer.Remove(AIController);
	if (RegisteredAIs.Contains(AIController))
		if (const auto Pawn = AIController->GetPawn())
			UE_LOG(LogAI, Log, TEXT("AI stoppdd being aware of the player: %s"), *Pawn->GetName());
}

void UAIManager::RegisterAI(AAIController* AIController)
{
	RegisteredAIs.AddUnique(AIController);
	if (const auto Pawn = AIController->GetPawn())
	{
		UE_LOG(LogAI, Log, TEXT("Registered AI: %s"), *Pawn->GetName());
		if (Pawn->FindComponentByClass<UBossComponent>())
			RegisteredBosses.AddUnique(AIController);
	}
}

void UAIManager::UnregisterAI(AAIController* AIController)
{
	if(RegisteredBosses.Contains(AIController))
	{
		RegisteredBosses.Remove(AIController);
		if(RegisteredBosses.Num() == 0)
			OnLastBossDeath.Broadcast();
	}

	RegisteredAIs.Remove(AIController);
	AIStopBeingAwareOfThePlayer(AIController);
	if (RegisteredAIs.Num() == 0)
		OnLastAIDeath.Broadcast();
	
	if (const auto Pawn = AIController->GetPawn())
		UE_LOG(LogAI, Log, TEXT("Unregistered AI: %s"), *Pawn->GetName());
}
