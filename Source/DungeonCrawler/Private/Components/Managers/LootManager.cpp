
#include "Components/Managers/LootManager.h"
#include "MyGameMode.h"

ULootManager* ULootManager::GetLootManager(const UObject* WorldContextObject)
{
	if(const auto MyGameMode = AMyGameMode::GetMyGameMode(WorldContextObject))
		return MyGameMode->LootManager;

	return nullptr;
}
