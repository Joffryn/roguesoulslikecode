
#include "Components/ColorChangingComponent.h"

#include "MyGameplayStatics.h"

UColorChangingComponent::UColorChangingComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UColorChangingComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(MeshMaterialDynamicInstances.Num() > 0)
	{
		CalculateDesiredColor();
		for(const auto MaterialInstance : MeshMaterialDynamicInstances)
			if(MaterialInstance)
				if(DesiredColor != MaterialInstance->K2_GetVectorParameterValue(FName("Color")))
				{
					FLinearColor ColorDiff = DesiredColor - CurrentColor;
					const float ChangeSpeed = ColorChangingSpeed * DeltaTime;
					ColorDiff *= ChangeSpeed;
					CurrentColor +=  ColorDiff;
					MaterialInstance->SetVectorParameterValue(FName("Color"), CurrentColor);
				}
	}
}

void UColorChangingComponent::AddColorChange(const FLinearColor Color, const FGameplayTag Reason)
{
	if(!ReasonsToColorMap.Find(Reason))
	{
		//Unfortunately has to be here instead of the begin play as there are cases where the mesh is changed during playtime.
		Setup();
		ReasonsToColorMap.Add(Reason, Color);
	}
}

void UColorChangingComponent::RemoveColorChange(const FGameplayTag Reason)
{
	if(ReasonsToColorMap.Find(Reason))
		ReasonsToColorMap.Remove(Reason);
}

void UColorChangingComponent::CalculateDesiredColor()
{
	if(ReasonsToColorMap.Num() > 0)
	{
		DesiredColor = GetAverageColor();
	}
	else
		DesiredColor = DefaultColor;
}

bool UColorChangingComponent::IsModifyingColor() const
{
	return ReasonsToColorMap.Num() > 0;
}

FLinearColor UColorChangingComponent::GetAverageColor()
{
	TArray<FLinearColor> Colors;
	for(auto Pair : ReasonsToColorMap)
		Colors.Add(Pair.Value);
	
	return  UMyGameplayStatics::GetAverageColor(Colors);
}

void UColorChangingComponent::Setup()
{
	MeshMaterialDynamicInstances.Empty();
	TArray<UActorComponent*> ResultComponents;
	GetOwner()->GetComponents(UMeshComponent::StaticClass(), ResultComponents);
	
	 for(const auto Component : ResultComponents)
	 	if(const auto MeshComponent = Cast<UMeshComponent>(Component))
	 		if(MeshComponent->GetMaterial(0))
	 			MeshMaterialDynamicInstances.Add(MeshComponent->CreateDynamicMaterialInstance(0));
}

