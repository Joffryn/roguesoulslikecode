
#include "Components/MyPhysicalAnimationComponent.h"

#include "GameFramework/Character.h"
#include "BaseCharacter.h"

void UMyPhysicalAnimationComponent::BeginPlay()
{
	Super::BeginPlay();

	if (const auto CharacterOwner = Cast<ACharacter>(GetOwner()))
		SetSkeletalMeshComponent(CharacterOwner->GetMesh());
}

void UMyPhysicalAnimationComponent::RagdollBlendStart()
{
	//Required to clear and reinit physical settings
	SetSkeletalMeshComponent(GetSkeletalMeshComponent());
	
	//ApplyPhysicalAnimationProfileBelow(PelvisBone, "Ragdoll");
	ApplyPhysicalAnimationSettings(PelvisBone, RagdollPhysicalAnimationData);
	
	bBlendRagdoll = true;
	GetSkeletalMeshComponent()->SetAllBodiesBelowSimulatePhysics(PelvisBone, true, false);
	GetSkeletalMeshComponent()->SetAllBodiesBelowPhysicsBlendWeight(PelvisBone, 0.f, false, false);
}

void UMyPhysicalAnimationComponent::RagdollBlendEnd()
{
	GetSkeletalMeshComponent()->SetSimulatePhysics(true);
	GetSkeletalMeshComponent()->SetAllBodiesBelowPhysicsBlendWeight(PelvisBone, 1.0f, false, false);
	bBlendRagdoll = false;
}

void UMyPhysicalAnimationComponent::UpdateRagdollBlend() const
{
	if (bBlendRagdoll)
	{
		float FinalValue = RagdollBlend;
		if (RagdollBlendInCurve)
			FinalValue = RagdollBlendInCurve->GetFloatValue(RagdollBlend);

		GetSkeletalMeshComponent()->SetAllBodiesBelowPhysicsBlendWeight(PelvisBone, FinalValue, false, false);
	}
}
