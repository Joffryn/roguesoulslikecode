
#include "Components/DissolveComponent.h"

#include "Components/CapsuleComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "MyGameplayStatics.h"
#include "ColorManager.h"

UDissolveComponent::UDissolveComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UDissolveComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if(bIsDissolving)
	{
		DissolveProgress += DeltaTime / DissolveTime;

		if(DissolveProgress > 1.0f)
		{
			DissolveProgress = 1.0f;
			bIsDissolving = false;
			OnFullyDissolved.Broadcast(GetOwner());
			if(bDestroyActorWhenDone)
				GetOwner()->Destroy();
			else
				UMyGameplayStatics::DeactivateActor(GetOwner());
		}
		
		float FinalValue = DissolveProgress;
		if(DissolveCurve)
			FinalValue = DissolveCurve->GetFloatValue(DissolveProgress);
		
		ApplyDissolveOnMaterials(FinalValue);
	}
	else if(bIsMaterializing)
	{
		DissolveProgress -= DeltaTime / MaterializeTime;
		if(DissolveProgress < 0.0f)
		{
			DissolveProgress = 0.0f;
			bIsMaterializing = false;
			OnFullyMaterialized.Broadcast(GetOwner());
		}
		
		float FinalValue = DissolveProgress;
		if(MaterializeCurve)
			FinalValue = MaterializeCurve->GetFloatValue(DissolveProgress);
		
		ApplyDissolveOnMaterials(FinalValue);
	}
}

void UDissolveComponent::Dissolve()
{
	FLinearColor Color = FLinearColor::Red;
	if(const auto ColorManager = UColorManager::GetColorManager(this))
		Color = ColorManager->GetColor(DissolveColor);
	
	CollectInfos();
	ApplyColorOnMaterials(Color);
	ApplyDissolveOnMaterials(0.0f);
	DissolveProgress = 0.0f;
	if(const auto CharacterOwner = Cast<ACharacter>(GetOwner()))
	{
		CharacterOwner->GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
		CharacterOwner->GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
		CharacterOwner->GetMesh()->SetCollisionResponseToChannel(ECC_GameTraceChannel3, ECR_Ignore);
		CharacterOwner->GetMesh()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECR_Ignore);
	}
	bIsDissolving = true;
	UGameplayStatics::PlaySoundAtLocation(this, DissolveSound, GetOwner()->GetActorLocation(), FRotator::ZeroRotator, 1.0f,1.0f, 0.0f, Attenuation);
}

void UDissolveComponent::Materialize()
{
	FLinearColor Color = FLinearColor::Red;
	if(const auto ColorManager = UColorManager::GetColorManager(this))
		Color = ColorManager->GetColor(MaterializeColor);
	
	CollectInfos();
	ApplyColorOnMaterials(Color);
	ApplyDissolveOnMaterials(1.0f);
	DissolveProgress = 1.0f;
	bIsMaterializing = true;
	UGameplayStatics::PlaySoundAtLocation(this, MaterializeSound, GetOwner()->GetActorLocation(), FRotator::ZeroRotator, 1.0f,1.0f, 0.0f, Attenuation);
}

void UDissolveComponent::CollectInfos()
{
	MaterialsToDissolve.Empty();
	for(const auto Mesh : UMyGameplayStatics::GetActorMeshes(GetOwner()))
		if(Mesh->GetMaterial(0))
			MaterialsToDissolve.Add(Mesh->CreateDynamicMaterialInstance(0, nullptr));
}

void UDissolveComponent::ApplyDissolveOnMaterials(const float Progress)
{
	for(const auto Material : MaterialsToDissolve)
		if(Material)
			Material->SetScalarParameterValue(ProgressParameter, Progress);
}

void UDissolveComponent::ApplyColorOnMaterials(const FLinearColor Color)
{
	for(const auto Material : MaterialsToDissolve)
		if(Material)
			Material->SetVectorParameterValue(ColorParameter, Color);
}

