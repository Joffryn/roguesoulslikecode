
#include "Components/PlayerLootComponent.h"
#include "MyBlueprintFunctionLibrary.h"
#include "BaseCharacter.h"
#include "MyHUD.h"

UPlayerLootComponent* UPlayerLootComponent::GetPlayerLootComponent(const UObject* WorldContextObject)
{
	if(const auto PlayerCharacter = ABaseCharacter::GetMyPlayerCharacter(WorldContextObject))
		return PlayerCharacter->FindComponentByClass<UPlayerLootComponent>();
	
	return nullptr;
}

void UPlayerLootComponent::AddGold(const float Amount)
{
	if(Amount > 0.0f)
	{ 
		if (const auto AbilitySystem = GetOwner()->FindComponentByClass<UAbilitySystemComponent>())
			UMyBlueprintFunctionLibrary::ApplyEffectWithMagnitudeByCaller(AbilitySystem, AddGoldEffect, SetByCallerGoldTag, Amount);
		
		AMyHUD::GetMyHud(this)->ShowGoldCollectedInfo(GetOwner()->GetActorLocation(), Amount);
	}
}
