
#include "Components/EditorTickComponent.h"

UEditorTickComponent::UEditorTickComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	bTickInEditor = true;
}

void UEditorTickComponent::TickComponent(const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

#if WITH_EDITOR
	EWorldType::Type WorldType = EWorldType::None;

	if (GetWorld())
		WorldType = GetWorld()->WorldType;

	if (WorldType == EWorldType::PIE || WorldType == EWorldType::Game)
	{
		PrimaryComponentTick.bCanEverTick = false;
		SetActiveFlag(false);
		return;
	}

	if (EditorTick.IsBound())
	{
		TGuardValue AutoRestore(GAllowActorScriptExecutionInEditor, true);
		EditorTick.Broadcast(DeltaTime);
	}
#else
	PrimaryComponentTick.bCanEverTick = false;
	SetActiveFlag(false);
#endif
}

void UEditorTickComponent::PostLoad()
{
	if (bEditorOnlyTick && GetWorld() && GetWorld()->IsGameWorld())
	{
		PrimaryComponentTick.bCanEverTick = false;
		SetActiveFlag(false);
	}

	Super::PostLoad();
}

void UEditorTickComponent::OnComponentCreated()
{
	if (bEditorOnlyTick && GetWorld() && GetWorld()->IsGameWorld())
	{
		PrimaryComponentTick.bCanEverTick = false;
		SetActiveFlag(false);
	}

	Super::OnComponentCreated();
}
