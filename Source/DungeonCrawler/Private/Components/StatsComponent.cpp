
#include "Components/StatsComponent.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "GameplayEffectTypes.h"
#include "MyAttributeSet.h"
#include "Enums.h"

float UStatsComponent::GetStatValue(const EStat Stat) const 
{
	bool bSuccess;
	const float Value = UAbilitySystemBlueprintLibrary::GetFloatAttribute(GetOwner(), *AttributesToStatsMap.FindKey(Stat), bSuccess);
	return Value;
}

void UStatsComponent::OnAttributeChange(const FOnAttributeChangeData& ChangeData)
{
	const EStat Stat = *AttributesToStatsMap.Find(ChangeData.Attribute);
	const float Diff = ChangeData.NewValue - ChangeData.OldValue;
	OnStatChanged.Broadcast(Stat, Diff);

	if(GetStatValue(Stat) == 0.0f)
		OnStatDepleted.Broadcast(Stat);

}

void UStatsComponent::BeginPlay()
{
	Super::BeginPlay();

	BindDelegates();
	
}

void UStatsComponent::BindDelegates()
{
	if (!BindDelegatesTimerHandle.IsValid())
	{
		BindDelegatesTimerHandle = GetWorld()->GetTimerManager().SetTimerForNextTick(FTimerDelegate::CreateLambda([this]
		{
			if(const auto AbilitySystem =UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(GetOwner()))
			{
				AbilitySystem->GetGameplayAttributeValueChangeDelegate(UMyAttributeSet::GetMaxHealthAttribute()).AddUObject(this, &UStatsComponent::OnAttributeChange);
				AbilitySystem->GetGameplayAttributeValueChangeDelegate(UMyAttributeSet::GetHealthAttribute()).AddUObject(this, &UStatsComponent::OnAttributeChange);
				
				AbilitySystem->GetGameplayAttributeValueChangeDelegate(UMyAttributeSet::GetMaxManaAttribute()).AddUObject(this, &UStatsComponent::OnAttributeChange);
				AbilitySystem->GetGameplayAttributeValueChangeDelegate(UMyAttributeSet::GetManaAttribute()).AddUObject(this, &UStatsComponent::OnAttributeChange);
				
				AbilitySystem->GetGameplayAttributeValueChangeDelegate(UMyAttributeSet::GetMaxStaminaAttribute()).AddUObject(this, &UStatsComponent::OnAttributeChange);
				AbilitySystem->GetGameplayAttributeValueChangeDelegate(UMyAttributeSet::GetStaminaAttribute()).AddUObject(this, &UStatsComponent::OnAttributeChange);

				AbilitySystem->GetGameplayAttributeValueChangeDelegate(UMyAttributeSet::GetMaxPoiseAttribute()).AddUObject(this, &UStatsComponent::OnAttributeChange);
				AbilitySystem->GetGameplayAttributeValueChangeDelegate(UMyAttributeSet::GetPoiseAttribute()).AddUObject(this, &UStatsComponent::OnAttributeChange);
			}
			OnComponentSetup.Broadcast();
		}));
	}
}
