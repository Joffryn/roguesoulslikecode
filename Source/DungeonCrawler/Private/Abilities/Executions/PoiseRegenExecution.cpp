
#include "Abilities/Executions/PoiseRegenExecution.h"

#include "MyAttributeSet.h"

//Helper struct for fetching the stats necessary for the calculation.
struct FPoiseStatistics
{
	//Capturedef declarations for attributes.
	DECLARE_ATTRIBUTE_CAPTUREDEF(MaxPoise);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Poise);
	DECLARE_ATTRIBUTE_CAPTUREDEF(PoiseRegen);
	DECLARE_ATTRIBUTE_CAPTUREDEF(PoiseGainMultiplier);
	DECLARE_ATTRIBUTE_CAPTUREDEF(PoiseLossMultiplier);

	//Default constructor.
	FPoiseStatistics()
	{
		//Capturedef definitions for attributes. 
		DEFINE_ATTRIBUTE_CAPTUREDEF(UMyAttributeSet, MaxPoise, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UMyAttributeSet, Poise, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UMyAttributeSet, PoiseRegen, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UMyAttributeSet, PoiseGainMultiplier, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UMyAttributeSet, PoiseLossMultiplier, Target, false);
	}
};

static const FPoiseStatistics& PoiseStatistics()
{
	static FPoiseStatistics PoiseStatistics;
	return PoiseStatistics;
}

UPoiseRegenExecution::UPoiseRegenExecution()
{
	RelevantAttributesToCapture.Add(PoiseStatistics().MaxPoiseDef);
	RelevantAttributesToCapture.Add(PoiseStatistics().PoiseDef);
	RelevantAttributesToCapture.Add(PoiseStatistics().PoiseRegenDef);
	RelevantAttributesToCapture.Add(PoiseStatistics().PoiseLossMultiplierDef);
	RelevantAttributesToCapture.Add(PoiseStatistics().PoiseGainMultiplierDef);
}

void UPoiseRegenExecution::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	const UAbilitySystemComponent* TargetAbsc = ExecutionParams.GetTargetAbilitySystemComponent();
	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();
	const float Period = Spec.GetPeriod();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.TargetTags = TargetTags;

	float CurrentPoise = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(PoiseStatistics().PoiseDef, EvaluationParameters, CurrentPoise);
	
		
	//Block stamina regen when any of this tags is applied.
	if(TargetAbsc->HasAnyMatchingGameplayTags(PoiseRegenBlockingTags))
		return;

	float MaxPoise = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(PoiseStatistics().MaxPoiseDef, EvaluationParameters, MaxPoise);

	float PoiseRegen = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(PoiseStatistics().PoiseRegenDef, EvaluationParameters, PoiseRegen);

	float GainMultiplier = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(PoiseStatistics().PoiseGainMultiplierDef, EvaluationParameters, GainMultiplier);
	PoiseRegen *= GainMultiplier;

	PoiseRegen *= Period;

	const float MissingPoise = MaxPoise - CurrentPoise;

	if (PoiseRegen >= MissingPoise)
		PoiseRegen = MissingPoise;

	OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(PoiseStatistics().PoiseProperty, EGameplayModOp::Additive, PoiseRegen));
}
