
#include "Abilities/Executions/StaminaRegenExecution.h"

#include "AbilitySystemComponent.h"
#include "MyAttributeSet.h"

//Helper struct for fetching the stats necessary for the calculation.
struct FStaminaStatistics
{
	//Capturedef declarations for attributes.
	DECLARE_ATTRIBUTE_CAPTUREDEF(MaxStamina);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Stamina);
	DECLARE_ATTRIBUTE_CAPTUREDEF(StaminaRegen);
	DECLARE_ATTRIBUTE_CAPTUREDEF(StaminaSprintCost);
	DECLARE_ATTRIBUTE_CAPTUREDEF(StaminaLossMultiplier);
	DECLARE_ATTRIBUTE_CAPTUREDEF(StaminaGainMultiplier);

	//Default constructor.
	FStaminaStatistics()
	{
		//Capturedef definitions for attributes. 
		DEFINE_ATTRIBUTE_CAPTUREDEF(UMyAttributeSet, MaxStamina, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UMyAttributeSet, Stamina, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UMyAttributeSet, StaminaRegen, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UMyAttributeSet, StaminaSprintCost, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UMyAttributeSet, StaminaLossMultiplier, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UMyAttributeSet, StaminaGainMultiplier, Target, false);
	}
};

static const FStaminaStatistics& StaminaStatistics()
{
	static FStaminaStatistics StaStatistics;
	return StaStatistics;
}

UStaminaRegenExecution::UStaminaRegenExecution()
{
	RelevantAttributesToCapture.Add(StaminaStatistics().MaxStaminaDef);
	RelevantAttributesToCapture.Add(StaminaStatistics().StaminaDef);
	RelevantAttributesToCapture.Add(StaminaStatistics().StaminaRegenDef);
	RelevantAttributesToCapture.Add(StaminaStatistics().StaminaSprintCostDef);
	RelevantAttributesToCapture.Add(StaminaStatistics().StaminaLossMultiplierDef);
	RelevantAttributesToCapture.Add(StaminaStatistics().StaminaGainMultiplierDef);
}

void UStaminaRegenExecution::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, OUT FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	const UAbilitySystemComponent* TargetAbsc = ExecutionParams.GetTargetAbilitySystemComponent();
	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();
	const float Period = Spec.GetPeriod();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.TargetTags = TargetTags;

	float CurrentStamina = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(StaminaStatistics().StaminaDef, EvaluationParameters, CurrentStamina);

	//Handle passive stamina cost during sprint
	if (TargetAbsc->HasMatchingGameplayTag(SprintTag))
	{
		float SprintCost = 0.f;
		ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(StaminaStatistics().StaminaSprintCostDef, EvaluationParameters, SprintCost);

		float LossMultiplier;
		ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(StaminaStatistics().StaminaLossMultiplierDef, EvaluationParameters, LossMultiplier);
		SprintCost *= LossMultiplier;

		SprintCost *= Period;
		const float SprintCostValue = FMath::Min(CurrentStamina, SprintCost);

		OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(StaminaStatistics().StaminaProperty, EGameplayModOp::Additive, -SprintCostValue));
		return;
	}

	//Block stamina regen when guarding and don't have a upgrade.
	if(TargetAbsc->HasMatchingGameplayTag(GuardTag) && !TargetAbsc->HasMatchingGameplayTag(GuardRegenTag))
		return;
		
	//Block stamina regen when any of this tags is applied.
	if(TargetAbsc->HasAnyMatchingGameplayTags(StaminaRegenBlockingTags))
		return;

	float MaxStamina = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(StaminaStatistics().MaxStaminaDef, EvaluationParameters, MaxStamina);

	float StaminaRegen = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(StaminaStatistics().StaminaRegenDef, EvaluationParameters, StaminaRegen);

	float GainMultiplier = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(StaminaStatistics().StaminaGainMultiplierDef, EvaluationParameters, GainMultiplier);
	StaminaRegen *= GainMultiplier;

	StaminaRegen *= Period;
	const float MissingStamina = MaxStamina - CurrentStamina;
	
	if (StaminaRegen >= MissingStamina)
		StaminaRegen = MissingStamina;

	OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(StaminaStatistics().StaminaProperty, EGameplayModOp::Additive, StaminaRegen));
}
