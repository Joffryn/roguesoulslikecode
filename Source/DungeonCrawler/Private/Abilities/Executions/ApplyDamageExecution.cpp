
#include "Abilities/Executions/ApplyDamageExecution.h"

#include "AbilitySystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MyDamageType.h"

void UApplyDamageExecution::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();
	
	const float Level = Spec.GetLevel();
	const float Duration = Spec.GetDuration();
	const float Period = Spec.GetPeriod();
	const float Damage = Level * Period / Duration;
	
	const FGameplayEffectContextHandle EffectContext = Spec.GetContext();
	AController* Instigator = Cast<AController>(EffectContext.GetInstigator());
	const UAbilitySystemComponent* TargetAbilitySystem = ExecutionParams.GetTargetAbilitySystemComponent();
	UGameplayStatics::ApplyDamage(TargetAbilitySystem->GetOwner(), Damage, Instigator, EffectContext.GetEffectCauser(), DamageType);
}
