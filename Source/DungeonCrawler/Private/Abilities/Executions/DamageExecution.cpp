
#include "Abilities/Executions/DamageExecution.h"

#include "AbilitySystemComponent.h"
#include "MyAttributeSet.h"

//Helper struct for fetching the stats necessary for the melee calculation.
struct FDamageStatistics
{
	//Capturedef declarations for attributes.
	DECLARE_ATTRIBUTE_CAPTUREDEF(MaxHealth);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Health);

	//Default constructor.
	FDamageStatistics()
	{
		//Capturedef definitions for attributes. 
		DEFINE_ATTRIBUTE_CAPTUREDEF(UMyAttributeSet, MaxHealth, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UMyAttributeSet, Health, Target, false);
	}

};

//Static helper function to quickly fetch the melee damage capture attributes.
static const FDamageStatistics& DamageStatistics()
{
	static FDamageStatistics DmgStatics;
	return DmgStatics;
}

UDamageExecution::UDamageExecution()
{
	RelevantAttributesToCapture.Add(DamageStatistics().MaxHealthDef);
	RelevantAttributesToCapture.Add(DamageStatistics().HealthDef);
}

void UDamageExecution::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, OUT FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();

	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();
	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	FGameplayTagContainer PassedInTags = ExecutionParams.GetPassedInTags();
	FGameplayEffectContextHandle EffectContext = ExecutionParams.GetOwningSpec().GetContext();
	
	float Health = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatistics().HealthDef, EvaluationParameters, Health);

	float DamageDone = Spec.GetLevel();
	if(DamageDone >= Health)
		DamageDone = Health;

	OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(DamageStatistics().HealthProperty, EGameplayModOp::Additive, -DamageDone));
}

