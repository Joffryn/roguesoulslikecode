
#include "Abilities/MyGameplayAbility.h"

#include "Abilities/GameplayAbilityTypes.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystemGlobals.h"
#include "BaseCharacter.h"
#include "MyAbilitySystemComponent.h"

UMyAbilitySystemComponent* UMyGameplayAbility::GetMyAbilitySystemComponentFromOwner() const
{
	return Cast<UMyAbilitySystemComponent>(GetAbilitySystemComponentFromActorInfo());
}

void UMyGameplayAbility::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);

	if (const auto Asc = ActorInfo->AbilitySystemComponent; Asc.IsValid() && !CancelByAbilitiesWithTag.IsEmpty())
		CancelTagEventHandle = Asc->RegisterGenericGameplayTagEvent().AddUObject(this, &UMyGameplayAbility::GameplayTagCallback);
}
 

void UMyGameplayAbility::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const bool bReplicateEndAbility, const bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);

	if (!ActorInfo)
		return;

	const auto Asc = ActorInfo->AbilitySystemComponent;
	if (Asc.IsValid() && CancelTagEventHandle.IsValid())
		Asc->RegisterGenericGameplayTagEvent().Remove(CancelTagEventHandle);
}

bool UMyGameplayAbility::CanQueueAbility(const FGameplayAbilitySpecHandle AbilityToQueue, const FGameplayAbilityActorInfo* ActorInfo, const UAbilitySystemComponent* AbilitySystem) const 
{
	if (!AbilitySystem)
		return false;

	FGameplayTagContainer AbilitySystemTags;
	AbilitySystem->GetOwnedGameplayTags(AbilitySystemTags);

	const bool bHasRequiredQueueTags = AbilitySystemTags.HasAll(QueueRequiredTags);
	const bool bHasBlockedQueueTags = AbilitySystemTags.HasAny(QueueBlockedTags);
	const bool bAllowedByTags = bHasRequiredQueueTags && !bHasBlockedQueueTags;

	const bool bAllowedByCosts = IsAllowedByCosts(AbilityToQueue, ActorInfo);
	const bool bAllowedByAbility = CanBeQueued(AbilitySystem);

	return bAllowedByTags && bAllowedByCosts && bAllowedByAbility;
}

FGameplayTagContainer UMyGameplayAbility::GetAbilityTags() const
{
	return AbilityTags;
}

ABaseCharacter* UMyGameplayAbility::GetOwningBaseCharacterFromActorInfo() const
{
	return Cast<ABaseCharacter>(GetOwningActorFromActorInfo());
}

float UMyGameplayAbility::GetCostPrimaryAttributeAmount() const
{
	const auto CostEffect = CostGameplayEffectClass.GetDefaultObject();
	if(CostEffect->Modifiers.Num() > 0)
	{
		float Magnitude;
		FGameplayEffectSpec Spec;
		Spec.Def = CostEffect;
		TArray<FGameplayEffectAttributeCaptureDefinition> CaptureDefs;
		CostEffect->Modifiers[0].ModifierMagnitude.GetAttributeCaptureDefinitions(CaptureDefs);
		for(auto CaptureDef : CaptureDefs)
		{
			Spec.CapturedRelevantAttributes.AddCaptureDefinition(CaptureDef);
		}

		CostEffect->Modifiers[0].ModifierMagnitude.AttemptCalculateMagnitude(Spec, Magnitude);
		return -Magnitude;
	}
	return 0.0f;
}

bool UMyGameplayAbility::IsAllowedByCosts(const FGameplayAbilitySpecHandle AbilityToQueue, const FGameplayAbilityActorInfo* ActorInfo) const
{
	const bool bHasEnoughToPay = UAbilitySystemGlobals::Get().ShouldIgnoreCosts() || CheckCost(AbilityToQueue, ActorInfo);
	return bHasEnoughToPay;
}

bool UMyGameplayAbility::CanBeQueued_Implementation(const UAbilitySystemComponent* AbilitySystem) const
{
	return true;
}

void UMyGameplayAbility::GameplayTagCallback(const FGameplayTag Tag, const int32 NewCount)
{
	if (NewCount > 0 && CancelByAbilitiesWithTag.HasTagExact(Tag))
		CancelAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true);
}
