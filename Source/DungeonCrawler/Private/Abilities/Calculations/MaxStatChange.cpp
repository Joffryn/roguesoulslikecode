
#include "Abilities/Calculations/MaxStatChange.h"

float UMaxStatChange::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{
	const float StatChange = Spec.GetLevel();

	return StatChange;
}
