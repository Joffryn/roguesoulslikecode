
#include "Abilities/Calculations/ActionCostCalculation.h"

float UActionCostCalculation::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{
	if (RelevantAttributesToCapture.Num() < 3)
		return 0.0f;

	float CurrentStat = 0.0f;
	float StatLossMultiplier = 0.0f;
	float CurrentActionCost = 0.0f;

	const FAggregatorEvaluateParameters EvaluateParameters;
	GetCapturedAttributeMagnitude(RelevantAttributesToCapture[0], Spec, EvaluateParameters, CurrentStat);
	GetCapturedAttributeMagnitude(RelevantAttributesToCapture[1], Spec, EvaluateParameters, StatLossMultiplier);
	GetCapturedAttributeMagnitude(RelevantAttributesToCapture[2], Spec, EvaluateParameters, CurrentActionCost);
	CurrentActionCost *= StatLossMultiplier;

	const float StatValue = FMath::Min(CurrentStat, CurrentActionCost);
	return -StatValue;
}
