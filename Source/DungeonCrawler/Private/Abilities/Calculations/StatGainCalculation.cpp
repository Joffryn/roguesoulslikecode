
#include "Abilities/Calculations/StatGainCalculation.h"

float UStatGainCalculation::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{
	float StatToRestore = Spec.GetLevel();

	if (RelevantAttributesToCapture.Num() < 3)
		return StatToRestore;

	const FAggregatorEvaluateParameters EvaluateParameters;
	float CurrentStat = 0.0f;
	float MaxStat = 0.0f;
	float StatGainMultiplier = 0.0f;

	GetCapturedAttributeMagnitude(RelevantAttributesToCapture[0], Spec, EvaluateParameters, CurrentStat);
	GetCapturedAttributeMagnitude(RelevantAttributesToCapture[1], Spec, EvaluateParameters, MaxStat);
	GetCapturedAttributeMagnitude(RelevantAttributesToCapture[2], Spec, EvaluateParameters, StatGainMultiplier);
	StatToRestore *= StatGainMultiplier;

	const float StatValue = FMath::Min(MaxStat - CurrentStat, StatToRestore);
	return StatValue;
}
