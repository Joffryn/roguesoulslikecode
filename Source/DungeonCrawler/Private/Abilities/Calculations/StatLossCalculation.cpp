
#include "Abilities/Calculations/StatLossCalculation.h"

float UStatLossCalculation::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{
	float StatLoss = Spec.GetLevel();

	if (RelevantAttributesToCapture.Num() < 2)
		return -StatLoss;

	float CurrentStat = 0.0f;
	float StatLossMultiplier = 0.0f;
	const FAggregatorEvaluateParameters EvaluateParameters;
	GetCapturedAttributeMagnitude(RelevantAttributesToCapture[0], Spec, EvaluateParameters, CurrentStat);
	GetCapturedAttributeMagnitude(RelevantAttributesToCapture[1], Spec, EvaluateParameters, StatLossMultiplier);
	StatLoss *= StatLossMultiplier;

	const float StatValue = FMath::Min(CurrentStat, StatLoss);
	return -StatValue;
}
