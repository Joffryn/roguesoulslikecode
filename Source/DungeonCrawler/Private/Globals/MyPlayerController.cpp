
#include "MyPlayerController.h"

#include "PlayerInteractionComponent.h"
#include "GameFramework/PlayerInput.h"
#include "Engine/GameViewportClient.h"
#include "MyAbilitySystemComponent.h"
#include "PlayerFeedbackComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TargetSystemComponent.h"
#include "PostProcessManager.h"
#include "MyGameplayStatics.h"
#include "MyGameInstance.h"
#include "ImGuiDebugMenu.h"
#include "BaseCharacter.h"
#include "ImGuiCommon.h"

AMyPlayerController::AMyPlayerController()
{
	PlayerInteractionComponent = CreateDefaultSubobject<UPlayerInteractionComponent>(TEXT("PlayerInteractionComponent"));
	PlayerInteractionComponent->AttachToComponent(GetRootComponent(),  FAttachmentTransformRules::KeepRelativeTransform);
	
	PlayerFeedbackComponent = CreateDefaultSubobject<UPlayerFeedbackComponent>(TEXT("PlayerFeedbackComponent"));
}

AMyPlayerController* AMyPlayerController::GetMyPlayerController(const UObject* WorldContextObject)
{
	return Cast<AMyPlayerController>(UGameplayStatics::GetPlayerController(WorldContextObject, 0));
}

void AMyPlayerController::ReturnToPlayerCharacter()
{
	const auto CurrentCharacterCache= GetPawn();
		
	if(CachedPlayerCharacter)
		Possess(CachedPlayerCharacter);

	CurrentCharacterCache->SpawnDefaultController();
}

void AMyPlayerController::TryToPossesTargetetCharacter()
{
	if(const auto PossessedBaseCharacter = Cast<ABaseCharacter>(GetPawn()))
		if(const auto TargetSystemComponent = PossessedBaseCharacter->TargetSystemComponent)
			if(const auto TargetBaseCharacter =  Cast<ABaseCharacter>(TargetSystemComponent->TargetetActor))
			{
				TargetSystemComponent->TargetLockOff();
				if (PossessedBaseCharacter->AbilitySystemComponent->HasMatchingGameplayTag(PlayerTag))
					CachedPlayerCharacter = PossessedBaseCharacter;

				Possess(TargetBaseCharacter);
			}
}

void AMyPlayerController::PlayForceFeedback(UForceFeedbackEffect* ForceFeedbackEffect, const FName Tag, const bool bLooping, const bool bIgnoreTimeDilation, const bool bPlayWhilePaused)
{
	if(UMyGameplayStatics::IsUsingGamepad(this))
	{
		FForceFeedbackParameters Params;
		Params.Tag = Tag;
		Params.bLooping = bLooping;
		Params.bIgnoreTimeDilation = bIgnoreTimeDilation;
		Params.bPlayWhilePaused = bPlayWhilePaused;
	
		PlayForceFeedback_Internal(ForceFeedbackEffect, Params);
	}
}

void AMyPlayerController::PlayForceFeedback_Internal(UForceFeedbackEffect* ForceFeedbackEffect, const FForceFeedbackParameters Params)
{
	if(UMyGameplayStatics::IsUsingGamepad(this))
	{
		ClientPlayForceFeedback(ForceFeedbackEffect, Params);
	}
}

void AMyPlayerController::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);

#if WITH_IMGUI
	if (DebugMenu == nullptr)
		DebugMenu = new ImGuiDebugMenu();
	
	DebugMenu->Render(this);
#endif
	
} 

bool AMyPlayerController::InputKey(const FInputKeyParams& Params)
{
	if(const auto MyGameInstance = UMyGameInstance::GetMyGameInstance(this))
		MyGameInstance->SetIsUsingGamepad(Params.Key.IsGamepadKey());
	
	return Super::InputKey(Params);
}

void AMyPlayerController::SetInputMode(const FInputModeDataBase& InData)
{
	Super::SetInputMode(InData);

	const auto GameViewportClient = GetWorld()->GetGameViewport();
	GameViewportClient->SetIgnoreInput(false);
}

void AMyPlayerController::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	SpawnPostProcessManager();
}

void AMyPlayerController::SpawnPostProcessManager()
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.Owner = this;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	const FRotator InSpawnRotation = FRotator::ZeroRotator;
	const FVector InSpawnLocation = FVector::ZeroVector;
			
	PostProcessManager = GetWorld()->SpawnActor<APostProcessManager>(PostProcessManagerClass, InSpawnLocation, InSpawnRotation, SpawnInfo);
}
