
#include "MyGameInstance.h"

#include "Kismet/GameplayStatics.h"

UMyGameInstance* UMyGameInstance::GetMyGameInstance(const UObject* WorldContextObject)
{
	return Cast<UMyGameInstance>(UGameplayStatics::GetGameInstance(WorldContextObject));
}

void UMyGameInstance::SetIsUsingGamepad(const bool bNewIsUsingGamepad)
{
	if(bNewIsUsingGamepad != bIsUsingGamepad)
	{
		bIsUsingGamepad = bNewIsUsingGamepad;
		OnIsUsingGamepadSet.Broadcast(bIsUsingGamepad);
		UE_LOG(LogViewport, Display, TEXT("Game instance bIsUsingGamepad changed to %s"), bIsUsingGamepad ? TEXT("True") : TEXT("False"));
	}
}
