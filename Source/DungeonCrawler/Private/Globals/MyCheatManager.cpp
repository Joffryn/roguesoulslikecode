
#include "MyCheatManager.h"

#include "Components/CustomizationComponent.h"
#include "Components/Combat/BossComponent.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "MyBlueprintFunctionLibrary.h"
#include "MyAbilitySystemComponent.h"
#include "PlayerCastingComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TargetSystemComponent.h"
#include "GameConfigComponent.h"
#include "MyPlayerController.h"
#include "RandomnessManager.h"
#include "DataTablesManager.h"
#include "EncounterManager.h"
#include "CombatComponent.h"
#include "MyAttributeSet.h"
#include "PoiseComponent.h"
#include "BaseCharacter.h"
#include "SaveComponent.h"
#include "AIController.h"
#include "AIManager.h"
#include "Upgrade.h"
#include "Structs.h"
#include "MyHUD.h"
#include "Logs.h"

#if WITH_IMGUI
#include "ImGuiDebugMenu.h"
#include "ImGuiModule.h"
#include "ImGuiCommon.h"
#endif

UMyCheatManager* UMyCheatManager::GetMyCheatManager(const UObject* WorldContextObject)
{
	if(const auto MyPlayerController = AMyPlayerController::GetMyPlayerController(WorldContextObject))
	{
		return Cast<UMyCheatManager>(MyPlayerController->CheatManager);
	}
	return nullptr;
}

void UMyCheatManager::FriendlyFire() const
{
	UGameConfigComponent::GetGameConfigComponent(this)->bFriendlyFireEnabled = !UGameConfigComponent::GetGameConfigComponent(this)->bFriendlyFireEnabled;
}

void UMyCheatManager::ChangePoiseLevel(const int32 Level) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
		Player->PoiseComponent->ChangePoiseLevel(Level);
}

void UMyCheatManager::Unstaggerable() const
{
	if(const auto PoiseComponent = ABaseCharacter::GetMyPlayerCharacter(this)->PoiseComponent)
		if(PoiseComponent->ActualPoiseLevel < 1000)
			PoiseComponent->ChangePoiseLevel(1000);
		else
			PoiseComponent->ChangePoiseLevel(-1000);
}

void UMyCheatManager::Suicide() const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
		UGameplayStatics::ApplyDamage(Player, 99999999999.0f, nullptr, nullptr, InevitableDamageType);
}

void UMyCheatManager::DebugMenu()
{
#if WITH_IMGUI
	const auto Properties = FImGuiModule::Get().GetProperties();
	FImGuiModule::Get().GetProperties().SetInputEnabled(!Properties.IsInputEnabled());
#endif
}

void UMyCheatManager::EncounterDebug() const
{
#if WITH_IMGUI
	if (const auto MyPlayerController = AMyPlayerController::GetMyPlayerController(this))
		MyPlayerController->DebugMenu->EncounterManagerDebugger.bEnabled = !MyPlayerController->DebugMenu->EncounterManagerDebugger.bEnabled;
#endif
}

void UMyCheatManager::AICombatDebug() const
{
#if WITH_IMGUI
	if (const auto MyPlayerController = AMyPlayerController::GetMyPlayerController(this))
		MyPlayerController->DebugMenu->AICombatDebugger.bEnabled = !MyPlayerController->DebugMenu->AICombatDebugger.bEnabled;
#endif
}

void UMyCheatManager::MusicManagerDebug() const
{
#if WITH_IMGUI
	if (const auto MyPlayerController = AMyPlayerController::GetMyPlayerController(this))
		MyPlayerController->DebugMenu->MusicManagerDebugger.bEnabled = !MyPlayerController->DebugMenu->MusicManagerDebugger.bEnabled;
#endif
}

void UMyCheatManager::Kill() const
{
	if(const auto Target = ABaseCharacter::GetMyPlayerCharacter(this)->TargetSystemComponent->TargetetActor)
		UGameplayStatics::ApplyDamage(Target, 99999.0f, nullptr, nullptr, InevitableDamageType);
}

void UMyCheatManager::EquipWeapon(const FName WeaponName) const
{
	if(const auto WeaponClass = Weapons.Find(WeaponName))
		if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
			Player->CombatComponent->EquipWeaponByClass(*WeaponClass);
}

void UMyCheatManager::AddPerk(const FName PerkName) const
{
	const FGameplayEffectContextHandle EffectContext;
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto PerksDataTable = DataTablesManager->Perks)
			if(const auto Row = PerksDataTable->FindRow<FUpgradeChoice>(PerkName, TEXT("")))
				if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
					Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(Row->EffectToApply.GetDefaultObject(), 1.0f, EffectContext);
}

void UMyCheatManager::AddAllPerks() const
{
	const FGameplayEffectContextHandle EffectContext;
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto PerksDataTable = DataTablesManager->Perks)
			for(const auto RowName : PerksDataTable->GetRowNames())
				if(const auto Row = PerksDataTable->FindRow<FUpgradeChoice>(RowName, TEXT("")))
					if(Row->bCanBeLearnedFromScroll)
						if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
							Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(Row->EffectToApply.GetDefaultObject(), 1.0f, EffectContext);
}

void UMyCheatManager::RemovePerk(const FName PerkName) const
{
	const FGameplayEffectContextHandle EffectContext;
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto PerksDataTable = DataTablesManager->Perks)
			if(const auto Row = PerksDataTable->FindRow<FUpgradeChoice>(PerkName, TEXT("")))
				if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
					Player->AbilitySystemComponent->RemoveActiveGameplayEffectBySourceEffect(Row->EffectToApply, nullptr);
}

void UMyCheatManager::RemoveAllPerks() const
{
	const FGameplayEffectContextHandle EffectContext;
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto PerksDataTable = DataTablesManager->Perks)
			for(const auto RowName : PerksDataTable->GetRowNames())
				if(const auto Row = PerksDataTable->FindRow<FUpgradeChoice>(RowName, TEXT("")))
					if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
						Player->AbilitySystemComponent->RemoveActiveGameplayEffectBySourceEffect(Row->EffectToApply, nullptr);
}

void UMyCheatManager::AddSkill(const FName SkillName) const
{
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto SkillsDataTable = DataTablesManager->Skills)
			if(const auto Row = SkillsDataTable->FindRow<FUpgradeChoice>(SkillName, TEXT("")))
				if(Row->bCanBeLearnedFromScroll)
					AddSkillByChoice(Row);
}

void UMyCheatManager::AddAllSkills() const
{
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto SkillsDataTable = DataTablesManager->Skills)
			for(const auto RowName : SkillsDataTable->GetRowNames())
				if(const auto Row = SkillsDataTable->FindRow<FUpgradeChoice>(RowName, TEXT("")))
					AddSkillByChoice(Row);
}

void UMyCheatManager::RemoveSkill(const FName SkillName) const
{
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto SkillsDataTable = DataTablesManager->Skills)
			if(const auto Row = SkillsDataTable->FindRow<FUpgradeChoice>(SkillName, TEXT("")))
				RemoveSkillByClass(Row->EffectToApply);
}

void UMyCheatManager::RemoveAllSkills() const
{
	//Clear the bound skills
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto SkillsDataTable = DataTablesManager->Skills)
			for(const auto RowName : SkillsDataTable->GetRowNames())
				if(const auto Row = SkillsDataTable->FindRow<FUpgradeChoice>(RowName, TEXT("")))
					RemoveSkillByClass(Row->EffectToApply);

	//And remove the effects
	const FGameplayEffectContextHandle EffectContext;
	if(const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if(const auto SkillsDataTable = DataTablesManager->Skills)
			for(const auto RowName : SkillsDataTable->GetRowNames())
				if(const auto Row = SkillsDataTable->FindRow<FUpgradeChoice>(RowName, TEXT("")))
					if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
						Player->AbilitySystemComponent->RemoveActiveGameplayEffectBySourceEffect(Row->EffectToApply, nullptr);
}

void UMyCheatManager::Damage(const float Amount) const
{
	if(const auto Target = ABaseCharacter::GetMyPlayerCharacter(this)->TargetSystemComponent->TargetetActor)
		UGameplayStatics::ApplyDamage(Target, Amount, nullptr, nullptr, InevitableDamageType);
}

void UMyCheatManager::SetPhase(const int32 Phase) const
{
	if (const auto Target = ABaseCharacter::GetMyPlayerCharacter(this)->TargetSystemComponent->TargetetActor)
		if (const auto BossComponent = Target->FindComponentByClass<UBossComponent>())
			BossComponent->SetPhase(Phase);
}

void UMyCheatManager::KillAll() const
{
	if(const auto AIManager = UAIManager::GetAIManager(this))
		for(int32 i = AIManager->RegisteredAIs.Num() - 1; i >= 0; i-- )
			UGameplayStatics::ApplyDamage(AIManager->RegisteredAIs[i]->GetPawn(), 99999.0f, nullptr, nullptr, InevitableDamageType);
}

void UMyCheatManager::FinishEncounter() const
{
	if(const auto EncounterManager = UEncounterManager::GetEncounterManager(this))
		EncounterManager->FinishCurrentEncounters();
}

void UMyCheatManager::AddDrinkCharge(const float Amount) const
{
	if(const auto AbilitySystem = UMyBlueprintFunctionLibrary::GetPlayerAbilitySystemComponent(this))
		UMyBlueprintFunctionLibrary::ApplyEffectWithMagnitudeByCaller(AbilitySystem, AddDrinkChargeEffect, SetByCallerDrinkChargeTag, Amount);
}

void UMyCheatManager::SpendDrinkCharge(const float Amount) const
{
	if(const auto AbilitySystem = UMyBlueprintFunctionLibrary::GetPlayerAbilitySystemComponent(this))
		UMyBlueprintFunctionLibrary::ApplyEffectWithMagnitudeByCaller(AbilitySystem, AddDrinkChargeEffect, SetByCallerDrinkChargeTag, -Amount);
}

void UMyCheatManager::AddGold(const float Amount) const
{
	if(const auto AbilitySystem = UMyBlueprintFunctionLibrary::GetPlayerAbilitySystemComponent(this))
		UMyBlueprintFunctionLibrary::ApplyEffectWithMagnitudeByCaller(AbilitySystem, AddGoldEffect, SetByCallerGoldTag, Amount);
}

void UMyCheatManager::SpendGold(const float Amount) const
{
	if(const auto AbilitySystem = UMyBlueprintFunctionLibrary::GetPlayerAbilitySystemComponent(this))
		UMyBlueprintFunctionLibrary::ApplyEffectWithMagnitudeByCaller(AbilitySystem, AddGoldEffect, SetByCallerGoldTag, -Amount);
}

void UMyCheatManager::TakeDamage(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
		UGameplayStatics::ApplyDamage(Player, Amount, nullptr, nullptr, InevitableDamageType);
}

void UMyCheatManager::OneHitKill() const
{
	DamageMultiplierAI(1000.f);
}

void UMyCheatManager::DamageMultiplierAI(const float Multiplier) const
{
	if(const auto GameConfigComponent = UGameConfigComponent::GetGameConfigComponent(this))
		GameConfigComponent->DamageMultiplierAI = Multiplier;
}

void UMyCheatManager::DamageMultiplierPlayer(const float Multiplier) const
{
	if(const auto GameConfigComponent = UGameConfigComponent::GetGameConfigComponent(this))
		GameConfigComponent->DamageMultiplierPlayer = Multiplier;
}

void UMyCheatManager::SetHeroClass(const FName HeroClassName) const
{
	if (const auto DataTablesManager = UDataTablesManager::GetDataTablesManager(this))
		if (const auto PlayerHeroSetups = DataTablesManager->PlayerHeroSetups)
			if (const auto HeroSetup = PlayerHeroSetups->FindRow<FRandomHeroSetup>(HeroClassName, TEXT("")))
				if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
					if (const auto CustomizationComponent = Player->FindComponentByClass<UCustomizationComponent>())
						CustomizationComponent->FormHeroFromPresets(*HeroSetup);
}

void UMyCheatManager::RestoreAll() const
{
	RestoreAllHealth();
	RestoreAllMana();
	RestoreAllStamina();
}

void UMyCheatManager::SaveGame() const
{
	if(const auto SaveComponent = USaveComponent::GetSaveComponent(this))
		SaveComponent->SaveGame(FName(*UGameplayStatics::GetCurrentLevelName(this)));
}

void UMyCheatManager::DeleteSave() const
{
	if(const auto SaveComponent = USaveComponent::GetSaveComponent(this))
		SaveComponent->DeleteSave();
}

void UMyCheatManager::LoadGame() const
{
	if(const auto SaveComponent = USaveComponent::GetSaveComponent(this))
		SaveComponent->LoadWorld();
}

void UMyCheatManager::TargetRestoreHealth(const float Amount) const
{
	if (const auto Target = ABaseCharacter::GetMyPlayerCharacter(this)->TargetSystemComponent->TargetetActor)
		if(const auto AbilitySystemComponent = Target->FindComponentByClass<UAbilitySystemComponent>())
		{
			const FGameplayEffectContextHandle EffectContext;
			AbilitySystemComponent->ApplyGameplayEffectToSelf(RestoreHealthEffect.GetDefaultObject(), Amount, EffectContext);
		}
}

void UMyCheatManager::TargetRestoreAllHealth() const
{
	if (const auto Target = ABaseCharacter::GetMyPlayerCharacter(this)->TargetSystemComponent->TargetetActor)
		if(const auto AbilitySystemComponent = Target->FindComponentByClass<UAbilitySystemComponent>())
		{
			const FGameplayEffectContextHandle EffectContext;
			AbilitySystemComponent->ApplyGameplayEffectToSelf(RestoreHealthEffect.GetDefaultObject(), 999999.0f, EffectContext);
		}
}

void UMyCheatManager::RestoreHealth(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(RestoreHealthEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::RestoreAllHealth() const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(RestoreHealthEffect.GetDefaultObject(), 999999.0f, EffectContext);
	}
}

void UMyCheatManager::IncreaseMaxHealth(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(ChangeMaxHealthEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::DecreaseMaxHealth(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(ChangeMaxHealthEffect.GetDefaultObject(), -Amount, EffectContext);
	}
}

void UMyCheatManager::InfiniteMana() const
{
	const FGameplayEffectContextHandle EffectContext;
	const auto Player = ABaseCharacter::GetMyPlayerCharacter(this);
	if(!Player)
		return;
	
	bool bFound;
	if (UAbilitySystemBlueprintLibrary::GetFloatAttribute(Player, UMyAttributeSet::GetManaLossMultiplierAttribute(), bFound) == 0.0f)        
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(DefaultManaConsumptionEffect.GetDefaultObject(), 1.0f, EffectContext);
	else
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(InfiniteManaEffect.GetDefaultObject(), 1.0f, EffectContext);
}

void UMyCheatManager::RestoreMana(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(RestoreManaEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::ConsumeMana(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(LoseManaEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::ConsumeAllMana() const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(LoseManaEffect.GetDefaultObject(), 999999.0f, EffectContext);
	}
}

void UMyCheatManager::RestoreAllMana() const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(RestoreManaEffect.GetDefaultObject(), 999999.0f, EffectContext);
	}
}

void UMyCheatManager::IncreaseMaxMana(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(ChangeMaxManaEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::DecreaseMaxMana(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(ChangeMaxManaEffect.GetDefaultObject(), -Amount, EffectContext);
	}
}
void UMyCheatManager::ConsumePoise(const float Amount) const
{
	if(const auto Target = Cast<ABaseCharacter>(ABaseCharacter::GetMyPlayerCharacter(this)->TargetSystemComponent->TargetetActor))
	{
		const FGameplayEffectContextHandle EffectContext;
		Target->AbilitySystemComponent->ApplyGameplayEffectToSelf(LosePoiseEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::BreakStance() const
{
	if(const auto Target = Cast<ABaseCharacter>(ABaseCharacter::GetMyPlayerCharacter(this)->TargetSystemComponent->TargetetActor))
	{
		const FGameplayEffectContextHandle EffectContext;
		Target->AbilitySystemComponent->ApplyGameplayEffectToSelf(LosePoiseEffect.GetDefaultObject(), 999999.0f, EffectContext);
	}
}

void UMyCheatManager::InfiniteStamina() const
{
	const FGameplayEffectContextHandle EffectContext;
	const auto Player = ABaseCharacter::GetMyPlayerCharacter(this);
	if(!Player)
		return;
	
	bool bFound;
	if (UAbilitySystemBlueprintLibrary::GetFloatAttribute(Player, UMyAttributeSet::GetStaminaLossMultiplierAttribute(), bFound) == 0.0f)
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(DefaultStaminaConsumptionEffect.GetDefaultObject(), 1.0f, EffectContext);
	else
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(InfiniteStaminaEffect.GetDefaultObject(), 1.0f, EffectContext);
}

void UMyCheatManager::RestoreStamina(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(RestoreStaminaEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::ConsumeStamina(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(LoseStaminaEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::ConsumeAllStamina() const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(LoseStaminaEffect.GetDefaultObject(), 999999.0f, EffectContext);
	}
}

void UMyCheatManager::RestoreAllStamina() const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(RestoreStaminaEffect.GetDefaultObject(), 999999.0f, EffectContext);
	}
}

void UMyCheatManager::IncreaseMaxStamina(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(ChangeMaxStaminaEffect.GetDefaultObject(), Amount, EffectContext);
	}
}

void UMyCheatManager::DecreaseMaxStamina(const float Amount) const
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
	{
		const FGameplayEffectContextHandle EffectContext;
		Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(ChangeMaxStaminaEffect.GetDefaultObject(), -Amount, EffectContext);
	}
}

void UMyCheatManager::ResetSeed() const
{
	if (const auto RandomnessManager = URandomnessManager::GetRandomnessManager(this))
		RandomnessManager->GenerateRandomStream();
}

void UMyCheatManager::Possess() const
{
	if (const auto MyPlayerController = AMyPlayerController::GetMyPlayerController(this))
		MyPlayerController->TryToPossesTargetetCharacter();
}

void UMyCheatManager::Unpossess() const
{
	if (const auto MyPlayerController = AMyPlayerController::GetMyPlayerController(this))
		MyPlayerController->ReturnToPlayerCharacter();
}

void UMyCheatManager::Demigod() const
{
	constexpr float DemigodDamageMultiplier = 0.1f;
	if(const auto GameConfigComponent = UGameConfigComponent::GetGameConfigComponent(this))
		if(GameConfigComponent->DamageMultiplierPlayer == DemigodDamageMultiplier)
			GameConfigComponent->DamageMultiplierPlayer = 1.f;
		else
			GameConfigComponent->DamageMultiplierPlayer = DemigodDamageMultiplier;

	InfiniteStamina();
	Unstaggerable();
	InfiniteMana();
}

void UMyCheatManager::OutsideTheControlToggle() const
{
	if(const auto EncounterManager = UEncounterManager::GetEncounterManager(this))
	{
		if(EncounterManager->IsDuringEncounter())
		{
			EncounterManager->RemoveDuringEncounterEffects();
			EncounterManager->ApplyOutsideEncounterEffects();
		}
		else
		{
			EncounterManager->RemoveOutsideEncounterEffects();
			EncounterManager->ApplyDuringEncounterEffects();
		}
	}
}

void UMyCheatManager::ShowVictoryMessage() const
{
	if(const auto HUD = AMyHUD::GetMyHud(this))
		HUD->ShowVictoryMessage();
}

void UMyCheatManager::God()
{
	Super::God();

	UE_LOG(LogGameplay, Log, TEXT("God mode on"));
	Unstaggerable();
	InfiniteStamina();
	InfiniteMana();
}

void UMyCheatManager::AddSkillByChoice(const FUpgradeChoice* Choice) const
{
	if(Choice->bCanBeLearnedFromScroll)
		if(Choice->Type == EUpgradeType::Active)
			AddSkillByClass(Choice->EffectToApply);
		else
		{
			const FGameplayEffectContextHandle EffectContext;
			if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
				Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(Choice->EffectToApply.GetDefaultObject(), 1.0f, EffectContext);
		}
}

void UMyCheatManager::AddSkillByClass(const TSubclassOf<UGameplayEffect> SkillToAdd) const
{
	auto AbilitiesGrantedByEffect = UMyBlueprintFunctionLibrary::GetAbilitiesClassesGrantedByEffect(SkillToAdd);
	if(AbilitiesGrantedByEffect.Num() == 0)
		return;

	if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
		if(const auto PlayerCastingComponent = Player->FindComponentByClass<UPlayerCastingComponent>())
		{
			const FGameplayEffectContextHandle EffectContext;
			Player->AbilitySystemComponent->ApplyGameplayEffectToSelf(SkillToAdd.GetDefaultObject(), 1.0f, EffectContext);
			PlayerCastingComponent->AddAbility(AbilitiesGrantedByEffect[0]);
		}
}

void UMyCheatManager::RemoveSkillByClass(const TSubclassOf<UGameplayEffect> SkillToRemove) const
{
	auto AbilitiesGrantedByEffect = UMyBlueprintFunctionLibrary::GetAbilitiesClassesGrantedByEffect(SkillToRemove);
	if(AbilitiesGrantedByEffect.Num() == 0)
		return;

	if (const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
		if (const auto PlayerCastingComponent = Player->FindComponentByClass<UPlayerCastingComponent>())
		{
			const FGameplayEffectContextHandle EffectContext;
			Player->AbilitySystemComponent->RemoveActiveGameplayEffectBySourceEffect(SkillToRemove, nullptr);
			PlayerCastingComponent->RemoveAbility(AbilitiesGrantedByEffect[0]);
		}
}
