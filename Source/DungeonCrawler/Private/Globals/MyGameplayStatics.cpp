
#include "Globals/MyGameplayStatics.h"

#include "AbilitiesGameplayEffectComponent.h"
#include "Components/Combat/ReactionSolverComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "MyAbilitySystemComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "AbilitySystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameplayTagContainer.h"
#include "CombatComponent.h"
#include "MyGameInstance.h"
#include "BaseCharacter.h"
#include "AIController.h"
#include "Enums.h"
#include "Logs.h"

void UMyGameplayStatics::ExecuteGameplayCue(UAbilitySystemComponent* AbilitySystem, const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters)
{
	if (AbilitySystem)
		AbilitySystem->ExecuteGameplayCue(GameplayCueTag, GameplayCueParameters);
}

void UMyGameplayStatics::RemoveGameplayCue(UAbilitySystemComponent* AbilitySystem, const FGameplayTag GameplayCueTag)
{
	if (AbilitySystem)
		AbilitySystem->RemoveGameplayCue(GameplayCueTag);
}

void UMyGameplayStatics::AddGameplayCue(UAbilitySystemComponent* AbilitySystem, const FGameplayTag GameplayCueTag, const FGameplayCueParameters& GameplayCueParameters)
{
	if (AbilitySystem)
		AbilitySystem->AddGameplayCue(GameplayCueTag, GameplayCueParameters);
}

bool UMyGameplayStatics::IsUsingGamepad(const UObject* WorldContextObject)
{
	if(const auto MyGameInstance = UMyGameInstance::GetMyGameInstance(WorldContextObject))
		return MyGameInstance->bIsUsingGamepad;
	
	return false;
}

bool UMyGameplayStatics::IsEditor()
{
#if WITH_EDITOR
	return true;
#endif
	return false;
}

bool UMyGameplayStatics::IsDebug()
{
#if UE_BUILD_DEBUG
	return true;
#endif
	return false;
}

bool UMyGameplayStatics::IsDevelopment()
{
#if UE_BUILD_DEVELOPMENT
	return true;
#endif
	return false;
}

bool UMyGameplayStatics::IsShipping()
{
#if UE_BUILD_SHIPPING
	return true;
#endif
	return false;
}

bool UMyGameplayStatics::GetActorNetStartup(const AActor* Actor)
{
	return Actor->bNetStartup;
}

UMyAbilitySystemComponent* UMyGameplayStatics::GetMyAbilitySystemComponent(const AActor* Actor)
{
	if(Actor)
		return Actor->FindComponentByClass<UMyAbilitySystemComponent>();
	
	return nullptr;
}

UCombatComponent* UMyGameplayStatics::GetCombatComponent(const AActor* Actor)
{
	if(Actor)
		return Actor->FindComponentByClass<UCombatComponent>();
	
	return nullptr;
}

bool UMyGameplayStatics::IsPlayerAlive(const UObject* WorldContextObject)
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(WorldContextObject))
		return Player->IsAlive();
	
	return false;
}

void UMyGameplayStatics::TravelToLevel(const UObject* WorldContextObject, const FName LevelName, const bool bAbsolute, const FString Options)
{
	if(const auto MyGameInstance = UMyGameInstance::GetMyGameInstance(WorldContextObject))
		MyGameInstance->bIsTravellingBetweenLevels = true;

	UGameplayStatics::OpenLevel(WorldContextObject, LevelName, bAbsolute, Options);
}

void UMyGameplayStatics::DeactivateActor(AActor* Actor)
{
	Actor->SetActorEnableCollision(false);
	Actor->SetActorHiddenInGame(true);
	Actor->SetActorTickEnabled(false);
}

void UMyGameplayStatics::SetAudioAssetSoundClass(USoundBase* Sound, USoundClass* InSoundClass)
{
	if(Sound)
		if(InSoundClass)
		  Sound->SoundClassObject = InSoundClass;
}

USoundClass* UMyGameplayStatics::GetAudioAssetSoundClass(USoundBase* Sound)
{
	if(Sound)
		return Sound->SoundClassObject;

	return nullptr;
}

FLinearColor UMyGameplayStatics::GetAverageColor(TArray<FLinearColor> Colors)
{
	FLinearColor FinalColor = FLinearColor::Black;
	for(auto Color : Colors)
		FinalColor += Color / Colors.Num();
	
	return FinalColor;
}

TArray<UMeshComponent*> UMyGameplayStatics::GetActorMeshes(const AActor* Actor)
{
	TArray<UMeshComponent*> ActorMeshComponents;
	if(!Actor)
		return ActorMeshComponents;
	
	TArray<UActorComponent*> Components;
	Actor->GetComponents(UMeshComponent::StaticClass(), Components);
	for(const auto Component : Components)
		if(auto MeshComponent = Cast<UMeshComponent>(Component))
			ActorMeshComponents.Add(MeshComponent);

	return ActorMeshComponents;
}

float UMyGameplayStatics::GetYawAngleBetweenActorAndTarget(const AActor* Actor, const FVector& Target)
{
	const FRotator LookAtRotation = UKismetMathLibrary::FindRelativeLookAtRotation(Actor->GetActorTransform(), Target);
	const float Angle = LookAtRotation.Yaw;
	
	return Angle;
}

float UMyGameplayStatics::GetYawAngleBetweenOriginAndTarget(const FVector& Origin, const AActor* Target)
{
	const FRotator LookAtRotation = UKismetMathLibrary::FindLookAtRotation(Target->GetActorLocation(), Origin);
	const FRotator TargetRotation = Target->GetActorRotation();
	const FRotator DeltaRotator = LookAtRotation - TargetRotation;
	
	float Angle = DeltaRotator.Yaw;
	if(FMath::Abs(Angle) > 180.f)
		Angle = (360.0f - FMath::Abs(DeltaRotator.Yaw)) * FMath::Sign<double>(DeltaRotator.Yaw * -1);
	
	return Angle;
}

EDirection UMyGameplayStatics::GetHitDirectionForTargetFromOrigin(const FVector& Origin, const AActor* Target, const EHitMapType HitMapType /*= EHitMapType::ThreeDirections*/)
{
	const float Angle = GetYawAngleBetweenOriginAndTarget(Origin, Target);
	if (const auto ReactionSolver = UReactionSolverComponent::GetReactionSolver(Target))
		return ReactionSolver->GetDirectionFromAngle(Angle, HitMapType);

	return EDirection::None;
}

FGameplayEffectContextHandle UMyGameplayStatics::MakeGameplayEffectContextWithParams(UAbilitySystemComponent* AbilitySystemComponent, AActor* Instigator, AActor* Causer)
{
	FGameplayEffectContextHandle GameplayEffectContextHandle = AbilitySystemComponent->MakeEffectContext();
	if (IsValid(Instigator) || IsValid(Causer))
		GameplayEffectContextHandle.AddInstigator(Instigator, Causer);

	return GameplayEffectContextHandle;
}

void UMyGameplayStatics::GameplayLog(const FString Message)
{
	UE_LOG(LogGameplay, Log, TEXT("%s"), *Message);
}

void UMyGameplayStatics::SpawningLog(const FString Message)
{
	UE_LOG(LogSpawning, Log, TEXT("%s"), *Message);
}

void UMyGameplayStatics::CombatLog(const FString Message)
{
	UE_LOG(LogCombat, Log, TEXT("%s"), *Message);
}

void UMyGameplayStatics::AILog(const FString Message)
{
	UE_LOG(LogAI, Log, TEXT("%s"), *Message);
}

void UMyGameplayStatics::SetAIControllerPerceptionComponent(AAIController* Controller, UAIPerceptionComponent* PerceptionComponent)
{
	Controller->SetPerceptionComponent(*PerceptionComponent);
}
