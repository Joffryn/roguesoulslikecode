
#include "MyGameState.h"

#include "Kismet/GameplayStatics.h"
#include "SaveComponent.h"

AMyGameState::AMyGameState()
{
	SaveComponent = CreateDefaultSubobject<USaveComponent>(TEXT("SaveComponent"));
}

AMyGameState* AMyGameState::GetMyGameState(const UObject* WorldContextObject)
{
	return Cast<AMyGameState>(UGameplayStatics::GetGameState(WorldContextObject));
}
