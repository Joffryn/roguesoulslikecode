
#include "MyGameMode.h"

#include "Components/Combat/ReactionSolverComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameConfigComponent.h"
#include "DataTablesManager.h"
#include "RandomnessManager.h"
#include "TravellingManager.h"
#include "EncounterManager.h"
#include "LevelsManager.h"
#include "SaveComponent.h"
#include "MusicManager.h"
#include "ColorManager.h"
#include "LootManager.h"
#include "CritSolver.h"
#include "AIManager.h"
#include "UIManager.h"

AMyGameMode::AMyGameMode()
{
	GameConfigComponent = CreateDefaultSubobject<UGameConfigComponent>(TEXT("GameConfigComponent"));
	TravellingManager = CreateDefaultSubobject<UTravellingManager>(TEXT("TravellingManager"));
	RandomnessManager = CreateDefaultSubobject<URandomnessManager>(TEXT("RandomnessManager"));
	ReactionSolver = CreateDefaultSubobject<UReactionSolverComponent>(TEXT("ReactionSolver"));
	DataTablesManager = CreateDefaultSubobject<UDataTablesManager>(TEXT("DataTablesManager"));
	EncounterManager = CreateDefaultSubobject<UEncounterManager>(TEXT("EncounterManager"));
	LevelsManager = CreateDefaultSubobject<ULevelsManager>(TEXT("LevelsManager"));
	ColorManager = CreateDefaultSubobject<UColorManager>(TEXT("ColorManager"));
	MusicManager = CreateDefaultSubobject<UMusicManager>(TEXT("MusicManager"));
	LootManager = CreateDefaultSubobject<ULootManager>(TEXT("LootManager"));
	CritSolver = CreateDefaultSubobject<UCritSolver>(TEXT("CritSolver"));
	AIManager = CreateDefaultSubobject<UAIManager>(TEXT("AIManager"));
	UIManager = CreateDefaultSubobject<UUIManager>(TEXT("UIManager"));
}

AMyGameMode* AMyGameMode::GetMyGameMode(const UObject* WorldContextObject)
{
	return Cast<AMyGameMode>(UGameplayStatics::GetGameMode(WorldContextObject));
}

void AMyGameMode::StartPlay()
{
	if(bLoadRunDataOnStartPlay)
		if(const auto SaveComponent =USaveComponent::GetSaveComponent(this))
			if(SaveComponent->DoesSaveExist())
				SaveComponent->LoadWorld();
	
	Super::StartPlay();
}
