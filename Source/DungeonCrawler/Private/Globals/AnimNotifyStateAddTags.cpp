
#include "Globals/AnimNotifyStateAddTags.h"

#include "Components/SkeletalMeshComponent.h"
#include "AbilitySystemInterface.h"
#include "AbilitySystemComponent.h"

void UAnimNotifyStateAddTags::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration)
{
	if (TagsToApply.IsEmpty())
		return;

	if (!MeshComp)
		return;

	AActor* MeshOwner = MeshComp->GetOwner();
	if (!MeshOwner)
		return;

	const IAbilitySystemInterface* AbilitySystemInterface = Cast< IAbilitySystemInterface >(MeshOwner);
	if (!AbilitySystemInterface)
		return;

	UAbilitySystemComponent* AbilitySystemComponent = AbilitySystemInterface->GetAbilitySystemComponent();
	if (!AbilitySystemComponent)
		return;

	AbilitySystemComponent->AddLooseGameplayTags(TagsToApply);
}

void UAnimNotifyStateAddTags::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	if (TagsToApply.IsEmpty())
		return;

	if (!MeshComp)
		return;

	AActor* MeshOwner = MeshComp->GetOwner();
	if (!MeshOwner)
		return;

	const IAbilitySystemInterface* AbilitySystemInterface = Cast< IAbilitySystemInterface >(MeshOwner);
	if (!AbilitySystemInterface)
		return;

	UAbilitySystemComponent* AbilitySystemComponent = AbilitySystemInterface->GetAbilitySystemComponent();
	if (!AbilitySystemComponent)
		return;

	AbilitySystemComponent->RemoveLooseGameplayTags(TagsToApply);
}

FString UAnimNotifyStateAddTags::GetNotifyName_Implementation() const
{
	return TagsToApply.ToStringSimple();
}
