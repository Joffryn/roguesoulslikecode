
#include "Globals/MyBlueprintFunctionLibrary.h"

#include "AbilitiesGameplayEffectComponent.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "AbilitySystemComponent.h"
#include "GameplayTagContainer.h"
#include "GameplayEffectTypes.h"
#include "MyAttributeSet.h"
#include "GameplayEffect.h"
#include "BaseCharacter.h"
#include "HudWidget.h"

UActorComponent* UMyBlueprintFunctionLibrary::AddActorComponent(AActor* Owner, const TSubclassOf<UActorComponent> ActorComponentClass)
{
	if (const UClass* BaseClass = FindObject<UClass>(Owner, TEXT("ActorComponent")))
		if(ActorComponentClass->IsChildOf(BaseClass))
		{
			UActorComponent* NewComp = NewObject<UActorComponent>(Owner, ActorComponentClass);
			if (!NewComp)
				return nullptr;

			NewComp->RegisterComponent();

			return NewComp;
		}
	
	return nullptr;
}

AActor* UMyBlueprintFunctionLibrary::GetClosestActorOfClass(const UObject* WorldContextObject, const FVector& Location, const TSubclassOf<AActor> ActorClass)
{
	AActor* ClosestActor = nullptr;
	float DistanceToTheClosestActor = FLT_MAX;

	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsOfClass(WorldContextObject, ActorClass, Actors);

	for (const auto Actor : Actors)
	{
		const float DistanceToTheActor = (Actor->GetActorLocation() - Location).Size();
		if (DistanceToTheActor < DistanceToTheClosestActor)
		{
			DistanceToTheClosestActor = DistanceToTheActor;
			ClosestActor = Actor;
		}
	}

	return ClosestActor;
}

TArray<TSubclassOf<UGameplayAbility>> UMyBlueprintFunctionLibrary::GetAbilitiesClassesGrantedByEffect(const TSubclassOf<UGameplayEffect> EffectClass)
{
	TArray<TSubclassOf<UGameplayAbility>> AbilitiesClasses;
	const auto DefaultObject = EffectClass.GetDefaultObject();
	UAbilitiesGameplayEffectComponent& Component = DefaultObject->FindOrAddComponent<UAbilitiesGameplayEffectComponent>();

	for(auto GrantedAbility : Component.GrantAbilityConfigs)
		AbilitiesClasses.Add(GrantedAbility.Ability);
	
	return AbilitiesClasses;
}

UMyAbilitySystemComponent* UMyBlueprintFunctionLibrary::GetPlayerAbilitySystemComponent(const UObject* WorldContextObject)
{
	if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(WorldContextObject))
		return Player->AbilitySystemComponent;

	return nullptr;
}

float UMyBlueprintFunctionLibrary::GetScaledAvailableMana(const AActor* Owner)
{
	bool bSuccessfullyFound;
	const float Mana = UAbilitySystemBlueprintLibrary::GetFloatAttribute(Owner, UMyAttributeSet::GetManaAttribute(), bSuccessfullyFound);
	const float ManaLossMultiplier = UAbilitySystemBlueprintLibrary::GetFloatAttribute(Owner, UMyAttributeSet::GetManaLossMultiplierAttribute(), bSuccessfullyFound);
	if(ManaLossMultiplier <= 0.0f)
		return FLT_MAX;
	
	return Mana / ManaLossMultiplier;
}

bool UMyBlueprintFunctionLibrary::TryToCastAbilityWithMana(const AActor* Owner, const float ManaCost)
{
	const float AvailableMana = GetScaledAvailableMana(Owner);
	if(AvailableMana < ManaCost || FMath::IsNearlyZero(AvailableMana, 0.05f))
		UHudWidget::GetHudWidget(Owner)->ShowErrorMessage(FText::FromString(TEXT("Not enough mana!")));

	//Never cast at 0 mana
	if(FMath::IsNearlyZero(AvailableMana, 0.05f))
		return false;
	
	return AvailableMana >= ManaCost;
}

void UMyBlueprintFunctionLibrary::ApplyEffectWithMagnitudeByCaller(UAbilitySystemComponent* AbilitySystemComponent, const TSubclassOf<UGameplayEffect> EffectToApply, const FGameplayTag SetByCallerTag, const float Magnitude)
{
	if(!AbilitySystemComponent)
		return;

	auto Spec = AbilitySystemComponent->MakeOutgoingSpec(EffectToApply, 1.0f, FGameplayEffectContextHandle());
	Spec = UAbilitySystemBlueprintLibrary::AssignTagSetByCallerMagnitude(Spec, SetByCallerTag, Magnitude);
	AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*Spec.Data.Get());
}
