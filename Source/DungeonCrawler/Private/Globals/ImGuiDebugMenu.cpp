
#include "Globals/ImGuiDebugMenu.h"

#if WITH_IMGUI

#include "TargetSystemComponent.h"
#include "MyPlayerController.h"
#include "AICombatComponent.h"
#include "InteractionActor.h"
#include "EncounterManager.h"
#include "BaseCharacter.h"
#include "MusicManager.h"
#include "ImGuiModule.h"
#include "Encounter.h"

ImGuiDebugMenu::ImGuiDebugMenu()
{
	FImGuiModule::Get().GetProperties().SetGamepadNavigationEnabled(true);
}

void ImGuiDebugMenu::Render(const AMyPlayerController* PlayerController)
{
	if (!PlayerController)
		return;

	if(ImGui::IsKeyReleased(ImGuiKey_KeypadSubtract))
		FImGuiModule::Get().SetInputMode(false);
	
	if (FImGuiModule::Get().IsInputMode())
		if (ImGui::BeginMainMenuBar())
		{
			if (ImGui::MenuItem("AI Combat")) 
				AICombatDebugger.bEnabled = !AICombatDebugger.bEnabled;

			if (ImGui::MenuItem("Music Manager")) 
				MusicManagerDebugger.bEnabled = !MusicManagerDebugger.bEnabled;

			if (ImGui::MenuItem("Encounter Manager"))
				EncounterManagerDebugger.bEnabled = !EncounterManagerDebugger.bEnabled;
			
			ImGui::EndMainMenuBar();
		}
	
	if (AICombatDebugger.bEnabled)
		AICombatDebugger.Render(PlayerController);

	if (MusicManagerDebugger.bEnabled)
		MusicManagerDebugger.Render(PlayerController);
	
	if (EncounterManagerDebugger.bEnabled)
		EncounterManagerDebugger.Render(PlayerController);
}

void ImGuiDebugMenu::FAICombatDebugger::Render(const AMyPlayerController* PlayerController)
{
	ImGui::SetNextWindowSize(ImVec2(400, 300), ImGuiCond_FirstUseEver);
	if (ImGui::Begin("AI Combat", &bEnabled))
	{
		const auto World = PlayerController->GetWorld();
		if(!World)
			return;
		
		const auto Player = ABaseCharacter::GetMyPlayerCharacter(World);
		if(!Player)
			return;
		
		if (const auto Target = Cast<ABaseCharacter>(Player->TargetSystemComponent->TargetetActor))
		{
			const auto AICombatComponent = Target->FindComponentByClass<UAICombatComponent>();
			if(!AICombatComponent)
				return;

			ImGui::Text("Distance to the target: %f", AICombatComponent->DistanceToTheTarget);
			ImGui::Text("Desired range: %f", AICombatComponent->GetDesiredRangeValue());
			
			ImGui::Text("Time since last impactful action %f / %f ", AICombatComponent->GetTimeSinceLastImpactfulAction(), AICombatComponent->TimeToForceChaseTargetSinceLastImpactfulAction);
			ImGui::Text("Planned action: %s", TCHAR_TO_ANSI(LexToString(AICombatComponent->PlannedAction)));
			
			if (ImGui::CollapsingHeader("Cooldowns", ImGuiTreeNodeFlags_DefaultOpen))
			{
				ImGui::Text("Global Actions Cooldown: %f", AICombatComponent->GlobalActionsCooldown);
				
				for(const auto ActionCooldown : AICombatComponent->AIActionToCooldownsSetup)
					ImGui::Text("Action: %s, Cooldown: %f", TCHAR_TO_ANSI(LexToString(ActionCooldown.Key)), ActionCooldown.Value);
			}
			
			ImGui::EndChild();
		}
	}
	ImGui::End();
}

void ImGuiDebugMenu::FMusicManagerDebugger::Render(const AMyPlayerController* PlayerController)
{
	ImGui::SetNextWindowSize(ImVec2(250, 200), ImGuiCond_FirstUseEver);
	if (ImGui::Begin("Music Manager", &bEnabled))
	{
		const auto World = PlayerController->GetWorld();
		if(!World)
			return;
		
		if(const auto MusicManager = UMusicManager::GetMusicManager(World))
		{
			if(MusicManager->CurrentlyPlayedMusic)

				ImGui::Text("Currently played music: %s", TCHAR_TO_ANSI(*MusicManager->CurrentlyPlayedMusic->GetName()));
			else
				ImGui::Text("No music is played.");
			
			for (const auto Music : MusicManager->QueuedMusics)
				if(Music.Sound)
					ImGui::Text("Queued music: %s" , TCHAR_TO_ANSI(*Music.Sound->GetName()));	
				else
					ImGui::Text("Null music queued!");

			ImGui::EndChild();
		}
	}
	ImGui::End();
}

void ImGuiDebugMenu::FEncounterManagerDebugger::Render(const AMyPlayerController* PlayerController)
{
	ImGui::SetNextWindowSize(ImVec2(640, 480), ImGuiCond_FirstUseEver);
	ImGui::SetNextWindowPos(ImVec2(50, 25), ImGuiCond_FirstUseEver);
	if (ImGui::Begin("Encounter Manager", &bEnabled))
	{
		if(const auto EncounterManager = UEncounterManager::GetEncounterManager(PlayerController))
			for(const auto Encounter : EncounterManager->RegisteredEncounters)
			{
				//Ignore disabled ones
				if(!Encounter->GetIsEnabled())
					continue;
				
				RenderEncounter(Encounter);
			}
		ImGui::EndChild();
	}
	ImGui::End();
}

void ImGuiDebugMenu::FEncounterManagerDebugger::RenderEncounter(AEncounter* Encounter) const
{
	if(ImGui::TreeNode(TCHAR_TO_ANSI(*Encounter->EncounterName)))
	{
		ImGui::PushID(Encounter);
		ImGui::Text("State: %s", TCHAR_TO_ANSI(LexToString(Encounter->GetState())));
		
		if(Encounter->GetState() == EEncounterState::Inactive || Encounter->GetState() == EEncounterState::NotSpawned)
		{
			if(ImGui::SmallButton("Start encounter"))
				Encounter->Start();
		}
		else if(Encounter->GetState() == EEncounterState::Finished)
		{
			if(ImGui::SmallButton("Reset encounter"))
				Encounter->ResetEncounter();
		}
		else
		{
			if(ImGui::SmallButton("Kill current characters"))
				Encounter->Kill();
			if(ImGui::SmallButton("Finish encounter"))
				Encounter->ForceFinish();

			switch (Encounter->EncounterType)
			{
				case EEncounterType::Randomized:
					ImGui::Text("Encounter Value %d / %d", Encounter->GetSpawnedEncounterEnemyValue(), Encounter->EncounterEnemyValue);
					ImGui::Text("Max simultaneous value  %d", Encounter->MaxSimultaneousEncounterEnemyValue);
					for(const auto Character : Encounter->GetSpawnedActors())
						if(ImGui::SmallButton("Kill"))
					break;
				
				case EEncounterType::Sure:
					ImGui::Text("Alive Characters %d / %d", Encounter->GetSpawnedActors().Num(), Encounter->SureEnemiesClassesToSpawn.Num());
					break;
				
				case EEncounterType::Interaction:
					{
						int32 CurrentlyInteractedActors = 0;
						for(const auto InteractionActor : Encounter->InteractionActors)
							if(InteractionActor->bHasBeenInteractedWith)
								CurrentlyInteractedActors++;
						ImGui::Text("InteractionActors %d / %d ", CurrentlyInteractedActors, Encounter->InteractionActors.Num());
						break;
					}
				default:;
			}
		}
		
		ImGui::TreePop();
		ImGui::PopID();
	}
	ImGui::NewLine();
}

#endif
