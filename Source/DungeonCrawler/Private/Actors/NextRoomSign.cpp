
#include "Actors/NextRoomSign.h"

#include "Components/TextRenderComponent.h"
#include "Components/StaticMeshComponent.h"
#include "EditorTickComponent.h"
#include "MapExit.h"

ANextRoomSign::ANextRoomSign()
{
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;
	
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SignMesn"));
	StaticMeshComponent->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
	
	TextRenderComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TextRenderComponent"));
	TextRenderComponent->AttachToComponent(Root, FAttachmentTransformRules::KeepRelativeTransform);
	
#if UE_BUILD_SHIPPING
	TextRenderComponent->bHiddenInGame = true;
#endif
	
	EditorTickComponent = CreateDefaultSubobject<UEditorTickComponent>(TEXT("EditorTickComponent"));
}

void ANextRoomSign::OnMapExitLevelNameChanged(FName NewLevelName)
{
	LevelName = NewLevelName;
	LoadSetupFromLevelsManager();
}
