
#include "MeleeWeapon.h"

#include "Components/Combat/ReactionSolverComponent.h"
#include "Components/StaticMeshComponent.h"
#include "AbilitySystemBlueprintLibrary.h"
#include "Components/SceneComponent.h"
#include "GenericTeamAgentInterface.h"
#include "Globals/MyGameplayStatics.h"
#include "MyAbilitySystemComponent.h"
#include "ColorChangingComponent.h"
#include "Kismet/GameplayStatics.h"
#include "GameConfigComponent.h"
#include "Engine/EngineTypes.h"
#include "SoundsComponent.h"
#include "WeaponCollision.h"
#include "TrailComponent.h"
#include "MyAttributeSet.h"
#include "ColorManager.h"
#include "DataAssets.h"
#include "Enums.h"
#include "Logs.h"

AMeleeWeapon::AMeleeWeapon()
{
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponMesh"));
	RootComponent = StaticMeshComponent;
	WeaponCollision = CreateDefaultSubobject<UWeaponCollision>(TEXT("WeaponCollision"));
	WeaponCollision->AttachToComponent(StaticMeshComponent, FAttachmentTransformRules::KeepRelativeTransform);

	TrailComponent = CreateDefaultSubobject<UTrailComponent>(TEXT("TrailComponent"));
}

void AMeleeWeapon::OnActorOverlapped(AActor* Actor, const FHitResult& HitResult)
{
	//Simple null check 
	if(!Actor)
		return;

	//Weapon is overlapping with owner all the time, let's just ignore it
	if (Actor == GetOwner())
		return;

	//Ignore everything that is owned by owner of this
	if (Actor->GetOwner() == GetOwner())
		return;

	//Don't do anything if attack animation is not played
	if (!bIsDealingDamage)
		return;

	//Do not damage same actor twice in 1 attack
	if (ActorsHitInCurrentAttack.Contains(Actor))
		return;

	//Do not damage friends
	if (FGenericTeamId::GetAttitude(Actor, GetOwner()) == ETeamAttitude::Friendly && !UGameConfigComponent::GetGameConfigComponent(this)->bFriendlyFireEnabled)
		return;
	
	bool bFoundAttribute = false; 

	const float OwnerDamageMultiplier = UAbilitySystemBlueprintLibrary::GetFloatAttribute(GetOwner(), UMyAttributeSet::GetDamageMultiplierAttribute(), bFoundAttribute);
	ActorsHitInCurrentAttack.Add(Actor);
	float DamageDealt = 0.0f;
	
	for(auto DamageParams : CurrentDamageParams)
	{
		if(bFoundAttribute)
			DamageParams.Amount *= OwnerDamageMultiplier;

		DamageDealt += UGameplayStatics::ApplyPointDamage(Actor, DamageParams.Amount, GetActorLocation(), HitResult, GetInstigator()->GetController(), GetOwner(), DamageParams.DamageClass);
	}
	
	if(DamageDealt > 0.0f)
	{
		PlayHitSound();
		const EDirection Direction = UMyGameplayStatics::GetHitDirectionForTargetFromOrigin(HitResult.ImpactPoint, Actor, WeaponReactionSet->HitMapType);
		UReactionSolverComponent::GetReactionSolver(this)->SendStaggerGameplayEvent(Actor, CurrentAttackStaggerStrength, GetOwner(), Direction);
		ApplyOnHitEffects(Actor);
	}
}

void AMeleeWeapon::ClearHitActors()
{
	ActorsHitInCurrentAttack.Empty();
}

void AMeleeWeapon::PlayHitSound() const
{
	SoundsComponent->SpawnSound(WeaponHitTag);
}

void AMeleeWeapon::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsDealingDamage)
		TraceWeaponCollisions();

	CacheWeaponCollisionTransforms();
}

void AMeleeWeapon::BeginPlay()
{
	Super::BeginPlay();
	CollectCollisionComponents();
	LoadConfigFromDataAsset();
	DecideStartingLook();
	BindDelegates();
}

UStaticMeshComponent* AMeleeWeapon::GetStaticMesh_Implementation()
{
	return StaticMeshComponent;
}

void AMeleeWeapon::DetachWeapon()
{
	Super::DetachWeapon();
	
	TrailComponent->EndTrail();
	bIsDealingDamage = false;
	
	AttackFinished();
	if(StaticMeshComponent)
	{
		StaticMeshComponent->SetCollisionProfileName("DroppedWeapon");
		StaticMeshComponent->SetSimulatePhysics(true);
	}
}

void AMeleeWeapon::CalculateWeaponDamage(const float DamageMultiplier, const int32 ExtraAttackStaggerStrength)
{
	CurrentDamageParams.Empty();
	for(auto DamageParams : BaseDamageParams )
	{
		DamageParams.Amount *= DamageMultiplier;
		CurrentDamageParams.Add(DamageParams);
	}
	
	int32 CurrentAttackStaggerValue = static_cast<int32>(WeaponConfig->BaseAttackStaggerStrength) + ExtraAttackStaggerStrength;
	CurrentAttackStaggerValue = FMath::Clamp(CurrentAttackStaggerValue, static_cast<int32>(EAttackStaggerStrength::None), static_cast<int32>(EAttackStaggerStrength::UltraHeavy));
	CurrentAttackStaggerStrength = static_cast<EAttackStaggerStrength>(CurrentAttackStaggerValue);
}

void AMeleeWeapon::HideWeapon()
{
	Super::HideWeapon();
	CollisionEnabledCache = StaticMeshComponent->GetCollisionEnabled();

	TArray<UActorComponent*> Components;
	GetComponents(UMeshComponent::StaticClass(), Components);
	for(const auto Component : Components)
		if(const auto MeshComponent = Cast<UMeshComponent>(Component))
			{
				MeshComponent->SetCollisionEnabled(ECollisionEnabled::Type::NoCollision);
				MeshComponent->SetVisibility(false);
			}	
}

void AMeleeWeapon::ShowWeapon()
{
	Super::ShowWeapon();
	TArray<UActorComponent*> Components;
	GetComponents(UMeshComponent::StaticClass(), Components);
	for(const auto Component : Components)
		if(const auto MeshComponent = Cast<UMeshComponent>(Component))
		{
			MeshComponent->SetCollisionEnabled(CollisionEnabledCache);
			MeshComponent->SetVisibility(true);
		}
}

void AMeleeWeapon::TraceWeaponCollisions()
{
	for (int32 i = 0; i < CachedWeaponCollisionComponents.Num(); i++)
	{
		auto CollisionProfileName = CachedWeaponCollisionComponents[i]->GetCollisionProfileName();
		FRotator PreviousRotation = FRotator(PreviousWeaponCollisionsTransformCache[i].GetRotation());
		FRotator CurrentRotation = CachedWeaponCollisionComponents[i]->GetComponentRotation();
		FRotator MergedRotation = FRotator((PreviousRotation.Pitch + CurrentRotation.Pitch) / 2.f, (PreviousRotation.Yaw + CurrentRotation.Yaw) / 2.f, (PreviousRotation.Roll + CurrentRotation.Roll) / 2.f);
		TArray<AActor*> ActorsToIgnore = ActorsHitInCurrentAttack;
		ActorsToIgnore.Add(GetOwner());
		TArray<FHitResult> OutHits;

		UKismetSystemLibrary::BoxTraceMultiByProfile(this, PreviousWeaponCollisionsTransformCache[i].GetLocation(), CachedWeaponCollisionComponents[i]->GetComponentLocation(), CachedWeaponCollisionComponents[i]->GetScaledBoxExtent(), MergedRotation,
			CollisionProfileName, true, ActorsToIgnore, bShowDebug ? EDrawDebugTrace::Type::ForDuration : EDrawDebugTrace::Type::None, OutHits, true);

		for (auto Hit : OutHits)
			OnActorOverlapped(Hit.GetActor(), Hit);
	}
}

void AMeleeWeapon::LoadConfigFromDataAsset()
{
	if(!WeaponConfig)
	{
		UE_LOG(LogCombat, Error, TEXT("%s weapon config not found"), *GetName());
		return;
	}
	
	BaseDamageParams = WeaponConfig->DamageParams;
	BaseOnHitEffects = WeaponConfig->OnHitEffects;
}

void AMeleeWeapon::ApplyOnHitEffects(const AActor* Target)
{
	if(const auto AbilitySystemComponent = Target->FindComponentByClass<UAbilitySystemComponent>())
	{
		const FGameplayEffectContextHandle EffectContext;
		for (const auto OnHitEffect : BaseOnHitEffects)
			ApplyOnHitEffect(Target, AbilitySystemComponent, EffectContext, OnHitEffect);
		
		for(const auto OnHitEffect : ExtraOnHitEffects)
			ApplyOnHitEffect(Target, AbilitySystemComponent, EffectContext, OnHitEffect);		
	}
}

void AMeleeWeapon::ApplyOnHitEffect(const AActor* Target, UAbilitySystemComponent* AbilitySystemComponent, const FGameplayEffectContextHandle& EffectContext, const FOnHitEffect OnHitEffect) const
{
	AbilitySystemComponent->ApplyGameplayEffectToSelf(OnHitEffect.Effect.GetDefaultObject(), OnHitEffect.Level, EffectContext);
	UE_LOG(LogCombat, Log, TEXT("%s applied %s on hit effect on %s"), *GetOwner()->GetName(), *OnHitEffect.Effect->GetName(), *Target->GetName());
}

void AMeleeWeapon::DecideStartingLook()
{
	bool bFound = false;
	if(const auto OwnerAbilitySystemComponent = GetOwner()->FindComponentByClass<UAbilitySystemComponent>())
		for(auto Pair : ElementalEnchantMap)
			if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(Pair.Key))
			{
				ExtraOnHitEffects.Add(Pair.Value.OnHitEffect);
				bFound = true;
			}
	
	if(bFound)
		DecideLook();
}

void AMeleeWeapon::DecideLook()
{
	if(const auto OwnerAbilitySystemComponent = Cast<UMyAbilitySystemComponent>(GetOwner()->FindComponentByClass<UAbilitySystemComponent>()))
	{
		bool bFound = false;
		for(const auto Pair : ElementalEnchantMap)
		{
			if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(Pair.Key))
			{
				TrailComponent->BaseTrail = Pair.Value.TrailOverride;
				bFound = true;
				ColorChangingComponent->AddColorChange(UColorManager::GetColorManager(this)->GetColor(Pair.Value.WeaponColor), Pair.Key);
			}
			else
				ColorChangingComponent->RemoveColorChange(Pair.Key);
			
		}
		if(!bFound)
			TrailComponent->BaseTrail = UGameConfigComponent::GetGameConfigComponent(this)->BaseTrail;
	}
}

void AMeleeWeapon::CollectCollisionComponents()
{
	TArray<UActorComponent*> Components;
	GetComponents(UWeaponCollision::StaticClass(), Components);
	for(const auto Component : Components)
		CachedWeaponCollisionComponents.Add(Cast<UWeaponCollision>(Component));
}

void AMeleeWeapon::BindDelegates()
{
	if(const auto OwnerAbilitySystemComponent = Cast<UMyAbilitySystemComponent>(GetOwner()->FindComponentByClass<UAbilitySystemComponent>()))
	{
		OwnerAbilitySystemComponent->OnGameplayTagAdded.AddDynamic(this, &AMeleeWeapon::OnGameplayTagAddedToTheOwner);
		OwnerAbilitySystemComponent->OnGameplayTagRemoved.AddDynamic(this, &AMeleeWeapon::OnGameplayTagRemovedFromTheOwner);
	}
}

void AMeleeWeapon::OnGameplayTagAddedToTheOwner(FGameplayTag Tag)
{
	if(const auto OwnerAbilitySystemComponent = Cast<UMyAbilitySystemComponent>(GetOwner()->FindComponentByClass<UAbilitySystemComponent>()))
		if(OwnerAbilitySystemComponent->HasMatchingGameplayTag(Tag))
			if(ElementalEnchantMap.Find(Tag))
			{
				ExtraOnHitEffects.Add(ElementalEnchantMap.Find(Tag)->OnHitEffect);
				DecideLook();
			}
}

void AMeleeWeapon::OnGameplayTagRemovedFromTheOwner(FGameplayTag Tag)
{
	if(ElementalEnchantMap.Find(Tag))
	{
		for(int32 i = ExtraOnHitEffects.Num() -1; i >= 0; i--)
			if(ExtraOnHitEffects[i].Effect == ElementalEnchantMap.Find(Tag)->OnHitEffect.Effect)
				ExtraOnHitEffects.RemoveAt(i);
		
		DecideLook();
	}
}

void AMeleeWeapon::CacheWeaponCollisionTransforms()
{
	PreviousWeaponCollisionsTransformCache.Empty();
	for (const auto CollisionComponent : CachedWeaponCollisionComponents)
		if(CollisionComponent)
			PreviousWeaponCollisionsTransformCache.Add(CollisionComponent->GetComponentTransform());
}
