
#include "Weapon.h"

#include "ColorChangingComponent.h"
#include "DissolveComponent.h"
#include "MyGameplayStatics.h"
#include "RandomnessManager.h"
#include "SoundsComponent.h"

AWeapon::AWeapon()
{
	ColorChangingComponent = CreateDefaultSubobject<UColorChangingComponent>(TEXT("ColorChangingComponent"));
	DissolveComponent = CreateDefaultSubobject<UDissolveComponent>(TEXT("DissolveComponent"));
	SoundsComponent = CreateDefaultSubobject<USoundsComponent>(TEXT("SoundsComponent"));
	
	PrimaryActorTick.bCanEverTick = true;
	SheathSocket = TEXT("WeaponBack");
	EquipSocket = TEXT("RightHand");
}

TSubclassOf<AWeapon> AWeapon::GetRandomSecondWeaponClass()
{
	if(SecondWeaponClasses.Num() > 0)
		return SecondWeaponClasses[URandomnessManager::GetRandomnessManager(this)->GetStream().RandRange(0, SecondWeaponClasses.Num() - 1)];
		
		return nullptr;
}

void AWeapon::SetCurrentAttack(UAttackMontageSetup* NewAttack)
{
	CurrentAttack = NewAttack;
}

void AWeapon::AttackStarted()
{
	OnAttackStarted.Broadcast();
	OnAttackStarted.Clear();
}

void AWeapon::HideWeapon()
{
}

void AWeapon::ShowWeapon()
{
}

void AWeapon::AttackFinished()
{
	OnAttackFinished.Broadcast();
	OnAttackFinished.Clear();
}

void AWeapon::DetachWeapon()
{
	bIsDropped = true;
	
	for(const auto MeshComponent : UMyGameplayStatics::GetActorMeshes(this))
	{
		MeshComponent->SetRenderCustomDepth(true);
		MeshComponent->SetCustomDepthStencilValue(0);
	}
	DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
}

