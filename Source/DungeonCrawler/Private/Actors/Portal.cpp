
#include "Actors/Portal.h"

void APortal::SetLabelFromEncounter(const FString& EncounterName)
{
#if WITH_EDITORONLY_DATA
	SetActorLabel("BP_" + EncounterName + "_Portal");
#endif
}

void APortal::BeginPlay()
{
	Super::BeginPlay();

	if(bStartOpened)
		Open(true);
}
