
#include "BaseCharacter.h"

#include "Components/Combat/RotationComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AIPerceptionSystem.h"
#include "Components/AnimationComponent.h"
#include "MyPhysicalAnimationComponent.h"
#include "MyCharacterMovementComponent.h"
#include "MyAbilitySystemComponent.h"
#include "Perception/AISense_Touch.h"
#include "DamageResponseComponent.h"
#include "Perception/AISense_Team.h"
#include "Kismet/GameplayStatics.h"
#include "ColorChangingComponent.h"
#include "TargetSystemComponent.h"
#include "DamageTakingComponent.h"
#include "Engine/DamageEvents.h"
#include "MyPlayerController.h"
#include "SoundsComponent.h"
#include "CombatComponent.h"
#include "StatsComponent.h"
#include "PoiseComponent.h"
#include "MyAIController.h"
#include "DataAssets.h"
#include "Enums.h"

ABaseCharacter::ABaseCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UMyCharacterMovementComponent>(CharacterMovementComponentName))
{
	PhysicalAnimationComponent = CreateDefaultSubobject<UMyPhysicalAnimationComponent>(TEXT("PhysicalAnimationComponent"));
	DamageResponseComponent = CreateDefaultSubobject<UDamageResponseComponent>(TEXT("DamageResponseComponent"));
	AbilitySystemComponent = CreateDefaultSubobject<UMyAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	ColorChangingComponent = CreateDefaultSubobject<UColorChangingComponent>(TEXT("ColorChangingComponent"));
	TargetSystemComponent = CreateDefaultSubobject<UTargetSystemComponent>(TEXT("TargetSystemComponent"));
	DamageTakingComponent = CreateDefaultSubobject<UDamageTakingComponent>(TEXT("DamageTakingComponent"));
	AnimationComponent = CreateDefaultSubobject<UAnimationComponent>(TEXT("AnimationComponent"));
	RotationComponent = CreateDefaultSubobject<URotationComponent>(TEXT("RotationComponent"));
	CombatComponent = CreateDefaultSubobject<UCombatComponent>(TEXT("CombatComponent"));
	SoundsComponent = CreateDefaultSubobject<USoundsComponent>(TEXT("SoundsComponent"));
	StatsComponent = CreateDefaultSubobject<UStatsComponent>(TEXT("StatsComponent"));
	PoiseComponent = CreateDefaultSubobject<UPoiseComponent>(TEXT("PoiseComponent"));
}

ABaseCharacter* ABaseCharacter::GetMyPlayerCharacter(const UObject* WorldContextObject)
{
	return Cast<ABaseCharacter>(UGameplayStatics::GetPlayerPawn(WorldContextObject, 0));
}

void ABaseCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	if (AbilitySystemComponent)
		for (TSubclassOf<UGameplayEffect> EffectClass : DefaultInfiniteEffects)
			AbilitySystemComponent->ApplyGameplayEffectToTarget(EffectClass.GetDefaultObject(), AbilitySystemComponent, 1);
}

 UMyCharacterMovementComponent* ABaseCharacter::GetMyCharacterMovementComponent() const 
{
	return Cast<UMyCharacterMovementComponent>(GetCharacterMovement());
}

 bool ABaseCharacter::IsPossessedByPlayerController() const
 {
	 return GetController() == UGameplayStatics::GetPlayerController(this, 0);
 }

void ABaseCharacter::SetTeamID(const FGenericTeamId ID)
{
	SetGenericTeamId(ID);
}

float ABaseCharacter::GetAnimRootMotionScale() const
{
	return AnimRootMotionTranslationScale;
}

void ABaseCharacter::SetAnimRootMotionScale(const float Value /* = 1.0f */)
{
	AnimRootMotionTranslationScale = Value;
}

bool ABaseCharacter::IsAlive() const
{
	return StatsComponent->GetStatValue(EStat::Health) > 0.0f;
}

ETeamAttitude::Type ABaseCharacter::GetTeamAttitudeTowardsActor(const AActor* Target) const
{
	return GetTeamAttitudeTowards(*Target);
}

void ABaseCharacter::SendTeamEvent(AActor* Target, const float EventRange /*= 1000.0f*/)
{
	//Only send event if target is hostile  
	if (GetTeamAttitudeTowards(*Target) == ETeamAttitude::Hostile)
		UAIPerceptionSystem::OnEvent<FAITeamStimulusEvent, FAITeamStimulusEvent::FSenseClass>(GetWorld(),
			FAITeamStimulusEvent(this, Target, Target->GetActorLocation(), EventRange));
}

void ABaseCharacter::SendTouchEvent(AActor* TouchReceiver)
{
	//Only send event to the hostile
	if(GetTeamAttitudeTowards(*TouchReceiver) == ETeamAttitude::Hostile)
		UAIPerceptionSystem::OnEvent<FAITouchEvent, FAITouchEvent::FSenseClass>(GetWorld(), FAITouchEvent(TouchReceiver, this, GetActorLocation()));
}

void ABaseCharacter::GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const
{
	TagContainer.Reset();
	if (IsValid(AbilitySystemComponent))
		AbilitySystemComponent->GetOwnedGameplayTags(TagContainer);
}

void ABaseCharacter::SetGenericTeamId(const FGenericTeamId& NewTeamID)
{
	TeamID = NewTeamID;
	//For AIs we need to set the team ID for the perception component
	if(const auto MyAIController = Cast<AMyAIController>(GetController()))
		MyAIController->SetGenericTeamId(NewTeamID);
}

FGenericTeamId ABaseCharacter::GetGenericTeamId() const
{
	return TeamID;
}

// Helper function to get the perception component, for player it will return nullptr which is fine
UAIPerceptionComponent* ABaseCharacter::GetPerceptionComponent()
{
	if(const auto MyAIController = Cast<AMyAIController>(GetController()))
		return MyAIController->GetPerceptionComponent();

	return nullptr;
}

UAbilitySystemComponent* ABaseCharacter::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

void ABaseCharacter::LoadAttributes(UCharacterAttributesConfig* InAttributes) const
{
	//if no attributes send, try to use the default one
	if(!InAttributes)
		InAttributes = Attributes;
		
	if(InAttributes)
		for(auto& Elem : InAttributes->AttributesToValuesSetup)
			AbilitySystemComponent->SetNumericAttributeBase(Elem.Key, Elem.Value);
}

void ABaseCharacter::BeginPlay() 
{
	Super::BeginPlay();
	
	//Ignore for the player, as it will be loaded by the save system
	if(!Cast<AMyPlayerController>(GetController()))
		LoadAttributes();
}

float ABaseCharacter::InternalTakePointDamage(const float Damage, FPointDamageEvent const& PointDamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	return DamageResponseComponent->OnOwnerPointDamageTaken(Damage, PointDamageEvent.HitInfo.Location, PointDamageEvent.ShotDirection, PointDamageEvent.DamageTypeClass, DamageCauser);
}

float ABaseCharacter::InternalTakeRadialDamage(const float Damage, FRadialDamageEvent const& RadialDamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Super::InternalTakeRadialDamage(Damage, RadialDamageEvent, EventInstigator, DamageCauser);
	return DamageResponseComponent->OnOwnerRadialDamageTaken(Damage, RadialDamageEvent.Origin, RadialDamageEvent.DamageTypeClass, DamageCauser);
}
