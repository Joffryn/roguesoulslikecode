
#include "Actors/OverlapPickup.h"

#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/BoxComponent.h"

AOverlapPickup::AOverlapPickup()
{
	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	RootComponent = Box;
	
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Box);
	CollectionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollectSphere"));
	CollectionSphere->SetupAttachment(Box);
	MagnetismSphere = CreateDefaultSubobject<USphereComponent>(TEXT("MagnetismSphere"));
	MagnetismSphere->SetupAttachment(Box);
	
	PrimaryActorTick.bCanEverTick = true;
}
