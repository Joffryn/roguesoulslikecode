
#include "Actors/Projectile.h"

#include "MyProjectileMovementComponent.h"
#include "ColorChangingComponent.h"
#include "DissolveComponent.h"
#include "ColorManager.h"

AProjectile::AProjectile()
{
	ProjectileMovementComponent = CreateDefaultSubobject<UMyProjectileMovementComponent>(TEXT("ProjectileMovementComponent"));
	ColorChangingComponent = CreateDefaultSubobject<UColorChangingComponent>(TEXT("ColorChangingComponent"));
	DissolveComponent = CreateDefaultSubobject<UDissolveComponent>(TEXT("DissolveComponent"));
}

void AProjectile::BeginPlay()
{
	Super::BeginPlay();
	
	IgnoreOwnerWhileMoving();

	if(bIsChangingColor)
		if(const auto ColorManager = UColorManager::GetColorManager(this))
			ColorChangingComponent->AddColorChange(ColorManager->GetColor(Color));
	
	GetWorld()->GetTimerManager().SetTimer(DissolveHandle, FTimerDelegate::CreateLambda([=]
	{
		if(this)
			DissolveComponent->Dissolve();
	}), LifeSpan, false);
}

void AProjectile::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	ClearDissolveHandle();
}

void AProjectile::SimulatePhysics_Implementation()
{
	if(ProjectileMovementComponent)
		ProjectileMovementComponent->DestroyComponent();
	
	if (UPrimitiveComponent* RootPrimitiveComponent = Cast<UPrimitiveComponent>(GetRootComponent()))
		RootPrimitiveComponent->SetSimulatePhysics(true);

	ClearTrails();
}

void AProjectile::IgnoreOwnerWhileMoving() const
{
	if (UPrimitiveComponent* RootPrimitiveComponent = Cast<UPrimitiveComponent>(GetRootComponent()))
	{
		if (GetOwner())
		{
			RootPrimitiveComponent->IgnoreActorWhenMoving(GetOwner(), true);
			TArray<UChildActorComponent*> ChildComponents;
			GetOwner()->GetComponents<UChildActorComponent>(ChildComponents);
			for (const auto ChildComponent : ChildComponents)
				RootPrimitiveComponent->IgnoreActorWhenMoving(ChildComponent->GetChildActor(), true);
		}
	}
}

void AProjectile::ClearTrails()
{
	if (const auto Mesh = GetProjectileMesh())
	{
		TArray<USceneComponent*> MeshChildren;
		Mesh->GetChildrenComponents(true, MeshChildren);
		for (const auto MeshChild : MeshChildren)
			MeshChild->DestroyComponent();
	}
}

void AProjectile::ClearDissolveHandle()
{
	if(GetWorld())
		GetWorld()->GetTimerManager().ClearTimer(DissolveHandle);
}
