
#include "Actors/Props/InteractionActor.h"

#include "Logs.h"

void AInteractionActor::Interact_Implementation()
{
	IInteractableInterface::Interact_Implementation();
	bHasBeenInteractedWith = true;
#if WITH_EDITOR
	UE_LOG(LogGameplay, Log, TEXT("%s has been interacted with."), *GetActorLabel());
#endif
	OnInteracted.Broadcast();
}
