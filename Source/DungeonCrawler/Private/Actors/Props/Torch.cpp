
#include "Actors/Props/Torch.h"

#include "Particles/ParticleSystemComponent.h"
#include "Components/PointLightComponent.h"
#include "Components/AudioComponent.h"

ATorch::ATorch()
{
	ParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystemComponent"));
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	PointLightComponent = CreateDefaultSubobject<UPointLightComponent>(TEXT("PointLightComponent"));
	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

	SetRootComponent(Root);
	StaticMeshComponent->SetupAttachment(Root);
	ParticleSystemComponent->SetupAttachment(StaticMeshComponent);
	PointLightComponent->SetupAttachment(ParticleSystemComponent);
	AudioComponent->SetupAttachment(ParticleSystemComponent);

	StaticMeshComponent->SetCastShadow(false);
}
