
#include "Actors/Props/Trap.h"

void ATrap::SetLabelFromEncounter(const FString& EncounterName)
{
#if WITH_EDITORONLY_DATA
	SetActorLabel("BP_" + EncounterName + "_Trap_" + TrapName);
#endif
}
