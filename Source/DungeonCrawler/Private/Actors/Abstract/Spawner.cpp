
#include "Actors/Abstract/Spawner.h"

#include "Components/BillboardComponent.h"
#include "MyAbilitySystemComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MyPlayerController.h"
#include "DissolveComponent.h"
#include "AICombatComponent.h"
#include "CombatComponent.h"
#include "BaseCharacter.h"
#include "Globals/Logs.h"
#include "Encounter.h"
#include "Weapon.h"

ASpawner::ASpawner()
{
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	SpriteComponent = CreateDefaultSubobject<UBillboardComponent>(TEXT("Sprite"));
	if (SpriteComponent)
	{
		SpriteComponent->bHiddenInGame = true;
		SpriteComponent->SetupAttachment(Root);
	}
	
	TeamID = FGenericTeamId(1);

	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
}

void ASpawner::SetEncounterOwner(AEncounter* InEncounterOwner)
{
	//Remove the reference from the current parent to not allow to have be references by multiple Encounters
	if(EncounterOwner && EncounterOwner != InEncounterOwner)
		EncounterOwner->Spawners.Remove(this);
	
	EncounterOwner = InEncounterOwner;
}

void ASpawner::KillCharacters()
{
	auto SpawnedCharacterCache = SpawnedCharacters;
	SpawnedCharacters.Empty();
	for(const auto SpawnedCharacter : SpawnedCharacterCache)
		if(SpawnedCharacter)
			SpawnedCharacter->ForceDeath();
}

TArray<ABaseCharacter*> ASpawner::GetSpawnedActors() const
{
	return SpawnedCharacters;
}

ABaseCharacter* ASpawner::Spawn(const TSubclassOf<ABaseCharacter> CharacterClass, const bool bWithMaterialization, const bool bRotatedTowardsPlayer)
{
	if(!CharacterClass)
		return nullptr;

	if(CharacterClass)
	{
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.Owner = this;
		SpawnInfo.Instigator = GetInstigator();
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

		// Rotation correction
		FRotator SpawnRotation = GetActorRotation();
		if(bRotatedTowardsPlayer)
			if(const auto Player = ABaseCharacter::GetMyPlayerCharacter(this))
				SpawnRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), Player->GetActorLocation());

		// Z location correction
		FVector SpawnLocation = GetActorLocation();
		if(const auto ZLocationDiff = AIClassesZCorrectionMap.Find(CharacterClass))
			SpawnLocation.Z += *ZLocationDiff;
			
		if(const auto SpawnedCharacter = GetWorld()->SpawnActor<ABaseCharacter>(CharacterClass, SpawnLocation, SpawnRotation, SpawnInfo))
		{
			OnCharacterSpawned.Broadcast(SpawnedCharacter);
			SpawnedCharacter->OnDeath.AddDynamic(this, &ASpawner::OnSpawnedCharacterDeath);
			SpawnedCharacter->SpawnDefaultController();
			SpawnedCharacter->SetTeamID(TeamID);
			SpawnedCharacter->AbilitySystemComponent->AddGameplayTag(FGameplayTagContainer(InactiveTag));
			SpawnedCharacter->AbilitySystemComponent->AddGameplayTag(CustomTagsToApply);
			
			if(bWithMaterialization)
			{
				if(const auto DissolveComponent = SpawnedCharacter->FindComponentByClass<UDissolveComponent>())
				{
					DissolveComponent->Materialize();
					DissolveComponent->OnFullyMaterialized.AddDynamic(this, &ASpawner::AfterSpawn);
					for(const auto Weapon : SpawnedCharacter->CombatComponent->GetWeapons())
						if(const auto WeaponDissolveComponent = Weapon->FindComponentByClass<UDissolveComponent>())
							WeaponDissolveComponent->Materialize();
				}
				else
					AfterSpawn(SpawnedCharacter);
			}
			else
				AfterSpawn(SpawnedCharacter);

			SpawnedCharacters.Add(SpawnedCharacter);
			SpawnedCharacter->OnTargetSet.AddDynamic(this, &ASpawner::OnSpawnedActorTargetSet);

			TimeToResetRecentlySpawned = RecentlySpawnedResetTime;
			bRecentlySpawnedCharacter = true;
			
			return SpawnedCharacter;
		}
		return nullptr;
	}
	
	return nullptr;
}

bool ASpawner::IsOccupied()
{
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
	ObjectTypes.Add(EObjectTypeQuery::ObjectTypeQuery10);
	const TArray<AActor*> ActorsToIgnore;
	FHitResult Hit;
	
	return !UKismetSystemLibrary::SphereTraceSingleForObjects(this, GetActorLocation(), GetActorLocation(), TooCloseRadius, ObjectTypes, false, ActorsToIgnore, EDrawDebugTrace::None, Hit, true);
}

void ASpawner::SetLabelFromEncounter(const FString& EncounterName)
{
#if WITH_EDITORONLY_DATA
	SetActorLabel("BP_" + EncounterName + "_Spawner_" + LexToString(SpawnerTier));
#endif
}

bool ASpawner::IsOnScreen() const
{
	FVector2D ScreenPosition;
	return UGameplayStatics::ProjectWorldToScreen(AMyPlayerController::GetMyPlayerController(this), GetActorLocation(), ScreenPosition);
}

void ASpawner::Tick(const float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if(bRecentlySpawnedCharacter)
	{
		TimeToResetRecentlySpawned -= DeltaSeconds;
		if(TimeToResetRecentlySpawned <= 0.f)
		{
			bRecentlySpawnedCharacter = false;
			TimeToResetRecentlySpawned = 0.f;
		}
	}
}

void ASpawner::OnSpawnedActorTargetSet(AActor* Actor)
{
	if(Actor == UGameplayStatics::GetPlayerCharacter(this, 0))
		OnTargetAsPlayerSet.Broadcast();
}

void ASpawner::AfterSpawn(AActor* Actor)
{
	if(const auto SpawnedCharacter = Cast<ABaseCharacter>(Actor))
	{
		SpawnedCharacter->AbilitySystemComponent->SetLooseGameplayTagCount(InactiveTag, 0);
		if(const auto AICombatComponent = SpawnedCharacter->FindComponentByClass<UAICombatComponent>())
			AICombatComponent->NoCombatBehavior = AINoCombatBehavior;
		
		UE_LOG(LogSpawning, Log, TEXT(" %s has spawned %s."), *GetName(), *SpawnedCharacter->GetName());
	}
}

void ASpawner::OnSpawnedCharacterDeath(ABaseCharacter* SpawnedCharacter)
{
	if(SpawnedCharacter)
		SpawnedCharacters.Remove(SpawnedCharacter);
}
 
void ASpawner::InactivateCharacters() const
{
	for(const auto SpawnedCharacter : SpawnedCharacters)
		if(SpawnedCharacter)
			SpawnedCharacter->AbilitySystemComponent->AddGameplayTag(FGameplayTagContainer(InactiveTag));
}

void ASpawner::ActivateCharacters() const
{
	for(const auto SpawnedCharacter : SpawnedCharacters)
		if(SpawnedCharacter)
			SpawnedCharacter->AbilitySystemComponent->SetLooseGameplayTagCount(InactiveTag, 0);
}
