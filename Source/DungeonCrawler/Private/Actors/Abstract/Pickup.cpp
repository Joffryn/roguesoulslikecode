
#include "Actors/Abstract/Pickup.h"

#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "InteractionComponent.h"

APickup::APickup()
{
	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	RootComponent = Box;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Box);

	Interaction = CreateDefaultSubobject<UInteractionComponent>(TEXT("Interaction"));
	Interaction->SetupAttachment(Box);

}

