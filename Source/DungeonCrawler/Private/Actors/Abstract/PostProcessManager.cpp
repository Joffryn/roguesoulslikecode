
#include "Actors/Abstract/PostProcessManager.h"

#include "MyPlayerController.h"
#include "Components/PostProcessComponent.h"
#include "Components/BoxComponent.h"

APostProcessManager::APostProcessManager()
{
	PostProcessesBox = CreateDefaultSubobject<UBoxComponent>(TEXT("PostProcessesBox"));
	SetRootComponent(PostProcessesBox);
	//PostProcessesBox->AttachToComponent(GetRootComponent(),  FAttachmentTransformRules::KeepRelativeTransform);
	
	Interaction = CreateDefaultSubobject<UPostProcessComponent>(TEXT("Interaction"));
	Interaction->AttachToComponent(PostProcessesBox,  FAttachmentTransformRules::KeepRelativeTransform);
	
	Vignette = CreateDefaultSubobject<UPostProcessComponent>(TEXT("Vignette"));
	Vignette->AttachToComponent(PostProcessesBox,  FAttachmentTransformRules::KeepRelativeTransform);
}

APostProcessManager* APostProcessManager::GetPostProcessManager(const UObject* WorldContextObject)
{
	if(const auto MyPlayerController = AMyPlayerController::GetMyPlayerController(WorldContextObject))
		return MyPlayerController->PostProcessManager;

	return nullptr;
}

void APostProcessManager::BeginPlay()
{
	Super::BeginPlay();
	
}

void APostProcessManager::Tick(const float DeltaTime)
{
	Super::Tick(DeltaTime);

}

