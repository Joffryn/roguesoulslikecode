
#include "Actors/Abstract/EncounterRewardSpawnPoint.h"

#include "RandomnessManager.h"
#include "EncounterReward.h"

AEncounterRewardSpawnPoint::AEncounterRewardSpawnPoint()
{
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->bHiddenInGame = true;
	StaticMesh->SetupAttachment(Root);
}

void AEncounterRewardSpawnPoint::Spawn()
{
	if(const auto RewardClass = PossibleRewards[URandomnessManager::GetRandomnessManager(this)->GetStream().RandRange(0, PossibleRewards.Num() -1)])
	{
		FActorSpawnParameters SpawnInfo;
		SpawnInfo.Owner = this;
		SpawnInfo.Instigator = GetInstigator();
		SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		if(const auto SpawnedReward = GetWorld()->SpawnActor<AEncounterReward>(RewardClass, GetActorLocation(), GetActorRotation(), SpawnInfo))
			SpawnedReward->Materialize();
	}
}

void AEncounterRewardSpawnPoint::SetLabelFromEncounter(const FString& EncounterName)
{
#if WITH_EDITORONLY_DATA
	SetActorLabel("BP_" + EncounterName + "_Reward_" + RewardName);
#endif
}
