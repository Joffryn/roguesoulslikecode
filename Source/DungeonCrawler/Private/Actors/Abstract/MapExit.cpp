
#include "Actors/Abstract/MapExit.h"

#include "Kismet/GameplayStatics.h"
#include "EditorTickComponent.h"
#include "LevelsManager.h"
#include "NextRoomSign.h"

AMapExit::AMapExit()
{
	EditorTickComponent = CreateDefaultSubobject<UEditorTickComponent>(TEXT("EditorTickComponent"));
}

#if WITH_EDITORONLY_DATA

void AMapExit::PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent)
{
	Super::PostEditChangeChainProperty(PropertyChangedEvent);
	if(PropertyChangedEvent.GetPropertyName() == "LevelName")
		if(BoundNextRoomSign)
			BoundNextRoomSign->OnMapExitLevelNameChanged(LevelName);
}

#endif

void AMapExit::BeginPlay()
{
	Super::BeginPlay();
	if(const auto LevelsManager = ULevelsManager::GetLevelsManager(this))
		LevelsManager->OnRunSetupGenerated.AddDynamic(this, &AMapExit::LoadSetupFromLevelsManager);
	
	LoadSetupFromLevelsManager();
}

void AMapExit::LoadSetupFromLevelsManager()
{
	if(const auto LevelsManager = ULevelsManager::GetLevelsManager(this))
	{
		const FName CurrentLevelName = FName(UGameplayStatics::GetCurrentLevelName(this, true));
		if(LevelsManager->LevelsToRoomSetupsMap.Contains(CurrentLevelName))
		{
			const auto RoomSetup = LevelsManager->LevelsToRoomSetupsMap.Find(CurrentLevelName);
			if(RoomSetup->NextLevelsNames.Num() > Index)
			{
				LevelName = RoomSetup->NextLevelsNames[Index];
				if(BoundNextRoomSign)
					BoundNextRoomSign->OnMapExitLevelNameChanged(LevelName);
			}
		}
	}
}
