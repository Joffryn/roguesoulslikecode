
#include "Actors/Abstract/Encounter.h"

#include "Components/EditorTickComponent.h"
#include "Perception/AISense_Prediction.h"
#include "Components/BillboardComponent.h"
#include "EncounterCharacterComponent.h"
#include "EncounterRewardSpawnPoint.h"
#include "Kismet/GameplayStatics.h"
#include "EncounterBlockingWall.h"
#include "MyGameplayStatics.h"
#include "RandomnessManager.h"
#include "EncounterManager.h"
#include "BaseCharacter.h"
#include "MusicManager.h"
#include "Structs.h"
#include "Portal.h"
#include "Trap.h"
#include "Logs.h"

AEncounter::AEncounter()
{
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;
	SpriteComponent = CreateDefaultSubobject<UBillboardComponent>(TEXT("Sprite"));
	if (SpriteComponent)
	{
		SpriteComponent->bHiddenInGame = true;
		SpriteComponent->SetupAttachment(Root);
	}
	EditorTickComponent = CreateDefaultSubobject<UEditorTickComponent>(TEXT("EditorTickComponent"));

	PrimaryActorTick.bStartWithTickEnabled = true;
	PrimaryActorTick.bCanEverTick = true;
}

void AEncounter::Start()
{
	if(!bIsEnabled)
		return;
	
	if(bHasBeenFinished)
		return;

	if(State == EEncounterState::Active || State == EEncounterState::Finished)
		return;

	if(State == EEncounterState::NotSpawned)
		Spawn(true, false);

	State = EEncounterState::Active;
	Activate();
	StartMusic();

	UE_LOG(LogGameplay, Log, TEXT("%s encounter started"), *EncounterName);
	OnEncounterStarted.Broadcast();
}

void AEncounter::CloseWalls()
{
	UE_LOG(LogGameplay, Log, TEXT("%s walls has been closed"), *EncounterName);
	for(const auto Wall : Walls)
		Wall->Close();
}

void AEncounter::OpenWalls()
{
	UE_LOG(LogGameplay, Log, TEXT("%s walls has been opened"), *EncounterName);
	for(const auto Wall : Walls)
		Wall->Open();
}

void AEncounter::PreSpawn()
{
	if(bHasBeenFinished)
		return;

	UE_LOG(LogGameplay, Log, TEXT("%s encounter has been pre spawned"), *EncounterName);
	State = EEncounterState::Inactive;
	Spawn(false, bBeginInactive);
}

void AEncounter::Spawn(const bool bWithMaterialization, const bool bInactive)
{
	if(EncounterType == EEncounterType::Sure)
		SpawnSureSetup();
	else if(!EncounterTierSetups.IsEmpty())
		while(ShouldSpawnMoreEnemies())
			SpawnCharacter(GetRandomEnemyClassFromTiers(), bWithMaterialization, bInactive);
}

void AEncounter::Kill()
{
	for(const auto Spawner : Spawners)
		Spawner->KillCharacters();
}

void AEncounter::ForceFinish()
{
	SpawnedEncounterEnemyValue = EncounterEnemyValue;
	ClearPendingSpawnRequests();
	Kill();
	Finish();
}

void AEncounter::ResetEncounter()
{
	UE_LOG(LogGameplay, Log, TEXT("%s walls has been reseted"), *EncounterName);
	ToDefaultState();
}

void AEncounter::SpawnRewards()
{
	for(const auto Reward : Rewards)
		if(Reward->bEnabled)
			Reward->Spawn();
}

void AEncounter::Activate()
{
	if(State != EEncounterState::Active)
		return;
		
	for(const auto Spawner : Spawners)
		Spawner->ActivateCharacters();
	
	UGameplayStatics::PlaySound2D(this, Begin);
	CloseWalls();
}

bool AEncounter::HasInteractedWithAllActors() const
{
	for(const auto InteractionActor : InteractionActors)
		if(!InteractionActor->bHasBeenInteractedWith)
			return false;
	
	return true;
}

TArray<ABaseCharacter*> AEncounter::GetSpawnedActors()
{
	return SpawnedCharacters;
}

#if WITH_EDITORONLY_DATA

void AEncounter::PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent)
{
	Super::PostEditChangeChainProperty(PropertyChangedEvent);
	if(PropertyChangedEvent.GetPropertyName() == "EncounterName")
		UpdateReferences();

	if(PropertyChangedEvent.GetPropertyName() == "TrapsSetups" ||
		PropertyChangedEvent.GetPropertyName() == "Spawners" ||
		PropertyChangedEvent.GetPropertyName() == "Rewards" ||
		PropertyChangedEvent.GetPropertyName() == "Portals" ||
		PropertyChangedEvent.GetPropertyName() == "Walls")
	{
		UpdateReferences();
	}
}

void AEncounter::PostDuplicate(const bool bDuplicateForPie)
{
	Super::PostDuplicate(bDuplicateForPie);

	//More than single encounter manager referencing a spawner can lead to very messy behaviors, so disable it
	if(!bDuplicateForPie)
		ClearReferences();
}

#endif

void AEncounter::BeginPlay()
{
	Super::BeginPlay();
	BuildRandomizationSetup();
	
	if(bIsEnabled)
	{
		BindDelegates();
		Register();

		for(const auto Wall : Walls)
			Wall->OnPlayerTriggerEnter.AddDynamic(this, &AEncounter::OnWallTriggerEntered);
	}
	
	if(!bHasBeenFinished && bPreSpawnAtBeginPlay)
		PreSpawn();

	// Only valid when started without save, as the traps are saved themselves
	if(!bHasBeenLoadedFromSave)
		SetupTraps();
}

void AEncounter::Tick(const float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	TArray<int32> ElementsToRemove;
	for(int32 i = 0; i < PendingSpawnRequests.Num() ; i++)
	{
		const auto SpawnRequest = &PendingSpawnRequests[i];
		SpawnRequest->Time -= DeltaSeconds;
		if(SpawnRequest->Time <= 0.f)
		{
			SpawnSingleCharacter(SpawnRequest->Character);
			ElementsToRemove.Add(i);
		}
	}
	
	for(int32 i = ElementsToRemove.Num() - 1; i >= 0  ; i--)
		PendingSpawnRequests.RemoveAt(ElementsToRemove[i]);
}

void AEncounter::OnCharacterSpawned(ABaseCharacter* Character)
{
	SpawnedCharacters.Add(Character);
	Character->OnDeath.AddDynamic(this, &AEncounter::OnSpawnedEnemyDeath);
	if(bAwareAboutPlayerFromTheSpawn)
		UAISense_Prediction::RequestPawnPredictionEvent(Character, UGameplayStatics::GetPlayerCharacter(this, 0), 0.f);
}

void AEncounter::OnInteractionWithActor()
{
	if(EncounterType != EEncounterType::Interaction)
		return;
	
	if(HasInteractedWithAllActors())
		Finish();
}

void AEncounter::OnSpawnedEnemyDeath(ABaseCharacter* Character)
{
	if(Character)
	{
		SpawnedCharacters.Remove(Character);

		if(EncounterType == EEncounterType::Sure)
		{
			if(SpawnedCharacters.IsEmpty())
				Finish();
		}
		else
		{
			int32 EnemyValue = 1;
			if(const auto EncounterCharacterComponent = Character->FindComponentByClass<UEncounterCharacterComponent>())
				EnemyValue = *TiersToCosts.Find(EncounterCharacterComponent->Tier);
		
			CurrentEncounterEnemyValue -= EnemyValue;

			if(ShouldSpawnMoreEnemies())
			{
				const auto RandomEnemy = GetRandomEnemyClassFromTiers();
				IncreaseSpawnedUnitCosts(*TiersToCosts.Find(RandomEnemy.Tier));
				PendingSpawnRequests.Add(FSpawnRequest(RandomEnemy, URandomnessManager::GetRandomnessManager(this)->GetStream().RandRange(EnemySpawnTimeRange.X, EnemySpawnTimeRange.Y)));
			}
			else if(SpawnedCharacters.IsEmpty() && PendingSpawnRequests.Num() == 0)
				Finish();
		}
		OnEncounterCharacterDeath.Broadcast(Character);
	}
}

void AEncounter::BindDelegates()
{
	for(const auto Spawner : Spawners)
		Spawner->OnCharacterSpawned.AddDynamic(this, &AEncounter::OnCharacterSpawned);

	for(const auto InteractionActor : InteractionActors)
		InteractionActor->OnInteracted.AddDynamic(this, &AEncounter::OnInteractionWithActor);
}

void AEncounter::Finish()
{
	if(State == EEncounterState::Finished)
		return;
	
	State = EEncounterState::Finished;
	bHasBeenFinished = true;
	if(bOpenWallsWhenFinished)
		OpenWalls();

	SpawnRewards();
	
	for(const auto NextEncounter : NextEncounters)
		NextEncounter->PreSpawn();

	for(const auto Portal : Portals)
		Portal->Open();
	
	UGameplayStatics::PlaySound2D(this, End);
	StopMusic();
	UE_LOG(LogGameplay, Log, TEXT("%s encounter finished"), *EncounterName);
	OnEncounterFinished.Broadcast();
}

void AEncounter::Register()
{
	if(const auto EncounterManager = UEncounterManager::GetEncounterManager(this))
		EncounterManager->RegisterEncounter(this);
}

void AEncounter::ToDefaultState()
{
	State = EEncounterState::NotSpawned;
	SpawnedEncounterEnemyValue = 0;
	CurrentEncounterEnemyValue = 0;
	PendingSpawnRequests.Empty();
	bHasBeenFinished = false;
}

void AEncounter::SetupTraps()
{
	for(const auto TrapSetup : TrapsSetups)
	{
		if(TrapsSetups.Num() == 0 || TrapSetup.AmountToLeft == 0)
			return;
		
		auto Traps = TrapSetup.PossibleTraps;
		TArray<ATrap*> TrapsToDeactivate;
		while(Traps.Num() > TrapSetup.AmountToLeft)
		{
			const int32 ID = URandomnessManager::GetRandomnessManager(this)->GetStream().RandRange(0, Traps.Num() -1);
			TrapsToDeactivate.Add(Traps[ID]);
			Traps.RemoveAt(ID);
		}
		
		for(const auto Trap : TrapsToDeactivate)
		{
			UMyGameplayStatics::DeactivateActor(Trap);
			Trap->bHasBeenDeactivated = true;
		}
	}
}

void AEncounter::BuildRandomizationSetup()
{
	BuildSpawnersToTiersMaps();
	BuildProbabilityMap();
}

void AEncounter::BuildProbabilityMap()
{
	for(const auto Pair :EncounterTierSetups)
	{
		TiersToCosts.Add(Pair.Key, Pair.Value.UnitCost);
		for(int32 i = 0 ; i < Pair.Value.SpawnChance ; i++)
		{
			ProbabilitySetup.Add(Pair.Key);
		}
	}
}

void AEncounter::BuildSpawnersToTiersMaps()
{
	for(auto Spawner : Spawners)
		if(TiersToSpawners.Contains(Spawner->SpawnerTier))
			TiersToSpawners.Find(Spawner->SpawnerTier)->Spawners.Add(Spawner);
		else
		{
			TiersToSpawners.Add(Spawner->SpawnerTier, FSpawners());
			TiersToSpawners.Find(Spawner->SpawnerTier)->Spawners.Add(Spawner);
		}
}

void AEncounter::UpdateSpawnersReferences()
{
	for(const auto Spawner : Spawners)
		if(Spawner)
			Spawner->SetEncounterOwner(this);
}

void AEncounter::ClearReferences()
{
	TrapsSetups.Empty();
	Spawners.Empty();
	Rewards.Empty();
	Portals.Empty();
	Walls.Empty();
}

void AEncounter::ClearDuplicatedReferences()
{
	TArray<ASpawner*> NewSpawners;
	for(auto Spawner : Spawners)
		NewSpawners.AddUnique(Spawner);
	
	Spawners = NewSpawners;
}

void AEncounter::UpdateEncounter()
{
	ClearEmptyReferences();
	UpdateReferences();
}

void AEncounter::ClearDuplicateSpawners()
{
	TArray<ASpawner*> NewSpawners;
	for(auto Spawner : Spawners)
		if(Spawner)
			NewSpawners.AddUnique(Spawner);

	Spawners = NewSpawners;
}

void AEncounter::CollectUnassignedSpawners()
{
	TArray<AActor*> Actors;
	UGameplayStatics::GetAllActorsOfClass(this, ASpawner::StaticClass(), Actors);
	for(const auto Actor : Actors)
		if(auto Spawner = Cast<ASpawner>(Actor))
			if(Spawner->GetEncounterOwner() == nullptr || Spawner->GetEncounterOwner() == this)
				Spawners.Add(Spawner);

	UpdateEncounter();
}

void AEncounter::ClearEmptyReferences()
{
	TArray<ASpawner*> SpawnersArray;
	for(const auto Spawner : Spawners)
		if(Spawner)
			SpawnersArray.Add(Spawner);
	
	Spawners = SpawnersArray;
}

void AEncounter::UpdateReferences()
{
	UpdateSpawnersReferences();
	UpdateReferencedActorsNames();
}

void AEncounter::UpdateReferencedActorsNames()
{
	if(EncounterName.IsEmpty())
		EncounterName = "UnnamedEncounter";

#if WITH_EDITORONLY_DATA
	SetActorLabel("BP_" + EncounterName + "_Encounter");
#endif
	for(const auto Spawner : Spawners)
		if(Spawner)
			Spawner->SetLabelFromEncounter(EncounterName);

	for(const auto Reward : Rewards)
		if(Reward)
			Reward->SetLabelFromEncounter(EncounterName);
	
	for(const auto Wall : Walls)
		if(Wall)
			Wall->SetLabelFromEncounter(EncounterName);

	for(const auto TrapSetup : TrapsSetups)
		for(const auto Trap : TrapSetup.PossibleTraps)
			Trap->SetLabelFromEncounter(EncounterName);

	for(const auto Portal : Portals)
		if(Portal)
			Portal->SetLabelFromEncounter(EncounterName);
}

void AEncounter::OnWallTriggerEntered()
{
	if(EncounterType == EEncounterType::Sure && SureEnemiesClassesToSpawn.IsEmpty())
		return;
	
	if(State != EEncounterState::Active && (EncounterActivationType == EEncounterActivationType::Any || EncounterActivationType == EEncounterActivationType::Trigger))
		Start();
}

void AEncounter::OnSpawnedCharacterPlayerTargetSet(AActor* Target)
{
	if(State != EEncounterState::Active && (EncounterActivationType == EEncounterActivationType::Any || EncounterActivationType == EEncounterActivationType::Perception))
		Start();
}

void AEncounter::SpawnCharacter(const FTierAndCharacter Character, const bool bWitMaterialization, const bool bInactive, const bool bModifyEncounterValues, const bool bRotatedTowardsPlayer, const bool bShouldSpawnerByOnScreen)
{
	if(bModifyEncounterValues)
		IncreaseSpawnedUnitCosts(*TiersToCosts.Find(Character.Tier));
	
	if(TiersToSpawners.IsEmpty())
		return;

	TArray<ASpawner*> TierSpawners;
	if(TiersToSpawners.Find(Character.Tier))
		TierSpawners = TiersToSpawners.Find(Character.Tier)->Spawners;
	
	if( TiersToSpawners.Contains(EEnemyTier::Any))
	{
		const auto AnySpawners = TiersToSpawners.Find(EEnemyTier::Any)->Spawners;
		if(Character.Tier != EEnemyTier::Any)
			TierSpawners.Append(AnySpawners);
	}

	TArray<ASpawner*> FinalSpawners;
	if(bShouldSpawnerByOnScreen)
		for(auto Spawner : TierSpawners)
			if(Spawner->IsOnScreen() && !Spawner->bRecentlySpawnedCharacter && !Spawner->IsOccupied())
				FinalSpawners.Add(Spawner);

	if(FinalSpawners.IsEmpty())
		FinalSpawners = TierSpawners;
	
	const int32 SpawnerToUseID = URandomnessManager::GetRandomnessManager(this)->GetStream().RandRange(0, FinalSpawners.Num() -1);
	const auto Spawner = TierSpawners[SpawnerToUseID];
	
	const auto SpawnedActor = Spawner->Spawn(Character.CharacterClass, bWitMaterialization, bRotatedTowardsPlayer);
	if(const auto EncounterCharacterComponent = SpawnedActor->FindComponentByClass<UEncounterCharacterComponent>())
		EncounterCharacterComponent->Tier = Character.Tier;
	
	if(bInactive)
		Spawner->InactivateCharacters();
	else 
		SpawnedActor->OnTargetSet.AddDynamic(this, &AEncounter::OnSpawnedCharacterPlayerTargetSet);
}

void AEncounter::SpawnSingleCharacter(const FTierAndCharacter Character)
{
	if(!EncounterTierSetups.IsEmpty())
		SpawnCharacter(Character, true, false, false, true, true);
}

void AEncounter::SpawnSureSetup()
{
	TArray<ASpawner*> SpawnersToUse = Spawners;
	for(const auto EnemyClass : SureEnemiesClassesToSpawn)
	{
		const int32 ID = URandomnessManager::GetRandomnessManager(this)->GetStream().RandRange(0, SpawnersToUse.Num() -1);
		SpawnersToUse[ID]->Spawn(EnemyClass, true);
		SpawnersToUse.RemoveAt(ID);
		if(SpawnersToUse.IsEmpty())
			SpawnersToUse = Spawners;
	}
}

void AEncounter::IncreaseSpawnedUnitCosts(const int32 Cost)
{
	CurrentEncounterEnemyValue += Cost;
	SpawnedEncounterEnemyValue += Cost;
}

bool AEncounter::ShouldSpawnMoreEnemies() const
{
	if(EncounterType != EEncounterType::Randomized)
		return false;
	
	return CurrentEncounterEnemyValue < MaxSimultaneousEncounterEnemyValue &&  EncounterEnemyValue > SpawnedEncounterEnemyValue;
}

void AEncounter::ClearPendingSpawnRequests()
{
	PendingSpawnRequests.Empty();
}

void AEncounter::StartMusic()
{
	if(bMusicIsEnabled && PossibleMusics.Num() > 0 && UMyGameplayStatics::IsPlayerAlive(this))
	{
		const auto MusicManager = UMusicManager::GetMusicManager(this);
		CurrentMusic = PossibleMusics[URandomnessManager::GetRandomnessManager(this)->GetStream().RandRange(0, PossibleMusics.Num() -1)];
		MusicManager->PlayMusic(CurrentMusic, MusicFadeInTime);
	}
}

void AEncounter::StopMusic()
{
	if(CurrentMusic)
	{
		const auto MusicManager = UMusicManager::GetMusicManager(this);
		MusicManager->StopMusic(CurrentMusic, MusicFadeOutTime);
		CurrentMusic = nullptr;
	}
}

FTierAndCharacter AEncounter::GetRandomEnemyClassFromTiers() const
{
	if(EncounterTierSetups.IsEmpty())
		return FTierAndCharacter();

	const EEnemyTier Tier = ProbabilitySetup[URandomnessManager::GetRandomnessManager(this)->GetStream().RandRange(0, ProbabilitySetup.Num() -1)];
	const auto Value = EncounterTierSetups.Find(Tier);
	const int32 CharacterID = URandomnessManager::GetRandomnessManager(this)->GetStream().RandRange(0, Value->PossibleCharacterClasses.Num() -1);
	
	FTierAndCharacter Pair;
	Pair.Tier = Tier;
	Pair.CharacterClass = Value->PossibleCharacterClasses[CharacterID];
	return Pair;
}
