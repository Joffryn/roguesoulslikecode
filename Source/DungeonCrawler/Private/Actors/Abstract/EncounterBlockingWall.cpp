
#include "Actors/EncounterBlockingWall.h"

#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"

AEncounterBlockingWall::AEncounterBlockingWall()
{
	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	SetRootComponent(Box);
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->AttachToComponent(Box, FAttachmentTransformRules::KeepRelativeTransform);
	Trigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	Trigger->AttachToComponent(Box, FAttachmentTransformRules::KeepRelativeTransform);
}

void AEncounterBlockingWall::SetLabelFromEncounter(const FString& EncounterName)
{
#if WITH_EDITORONLY_DATA
	SetActorLabel("BP_" + EncounterName + "_Blockade" );
#endif
}
